<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('cms_menus')->insert(
            [
                [
                    'name'     => 'Pais',
                    'type'    => 'Route',
                    'path'    => 'AdminTbPais1ControllerGetIndex',
                    'color'    => '',
                    'icon'    => 'fa fa-glass',
                    'parent_id'    => '6',
                    'is_active'    => '1',
                    'is_dashboard'    => '0',
                    'id_cms_privileges'    => '1',
                    'sorting'    => '1',
                ],
                [
                    'name'     => 'Departamentos',
                    'type'    => 'Route',
                    'path'    => 'AdminTbDepartamentos1ControllerGetIndex',
                    'color'    => '',
                    'icon'    => 'fa fa-glass',
                    'parent_id'    => '6',
                    'is_active'    => '1',
                    'is_dashboard'    => '0',
                    'id_cms_privileges'    => '1',
                    'sorting'    => '2',
                ]
            ]
        );
    }
}
