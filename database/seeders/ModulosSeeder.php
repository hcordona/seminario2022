<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ModulosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('cms_moduls')->insert([
            [
            'name' => 'Notificaciones',
            'icon' => 'fa fa-cog',
            'path' => 'notifications',
            'table_name' => 'cms_notifications',
            'controller' => 'NotificationsController',
            'is_protected' => '1',
            'is_active' => '1',
            'created_at' => Carbon::now(),




        ],

        [
            'name' => 'Privilegios',
            'icon' => 'fa fa-cog',
            'path' => 'privileges',
            'table_name' => 'cms_privileges',
            'controller' => 'PrivilegesController',
            'is_protected' => '1',
            'is_active' => '1',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Privilegios & Roles',
            'icon' => 'fa fa-cog',
            'path' => 'privileges_roles',
            'table_name' => 'cms_privileges_roles',
            'controller' => 'PrivilegesRolesController',
            'is_protected' => '1',
            'is_active' => '1',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Gestión de usuarios',
            'icon' => 'fa fa-cog',
            'path' => 'users',
            'table_name' => 'users',
            'controller' => 'AdminCmsUsersController',
            'is_protected' => '0',
            'is_active' => '1',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Ajustes',
            'icon' => 'fa fa-cog',
            'path' => 'settings',
            'table_name' => 'cms_settings',
            'controller' => 'SettingsController',
            'is_protected' => '1',
            'is_active' => '1',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Generador de Módulos',
            'icon' => 'fa fa-database',
            'path' => 'module_generator',
            'table_name' => 'cms_moduls',
            'controller' => 'ModulsController',
            'is_protected' => '1',
            'is_active' => '1',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Gestión de Menús',
            'icon' => 'fa fa-bars',
            'path' => 'menu_management',
            'table_name' => 'cms_menus',
            'controller' => 'MenusController',
            'is_protected' => '1',
            'is_active' => '1',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Plantillas de Correo',
            'icon' => 'fa fa-envelope-o',
            'path' => 'email_templates',
            'table_name' => 'cms_email_templates',
            'controller' => 'EmailTemplatesController',
            'is_protected' => '1',
            'is_active' => '1',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Generador de Estadísticas',
            'icon' => 'fa fa-dashboard',
            'path' => 'statistic_builder',
            'table_name' => 'cms_statistics',
            'controller' => 'StatisticBuilderController',
            'is_protected' => '1',
            'is_active' => '1',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Generador de API',
            'icon' => 'fa fa-cloud-download',
            'path' => 'api_generator',
            'table_name' => '',
            'controller' => 'ApiCustomController',
            'is_protected' => '1',
            'is_active' => '1',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Log de Accesos (Usuarios)',
            'icon' => 'fa fa-flag-o',
            'path' => 'logs',
            'table_name' => 'cms_logs',
            'controller' => 'LogsController',
            'is_protected' => '1',
            'is_active' => '1',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Pais',
            'icon' => 'fa fa-glass',
            'path' => 'tb_pais',
            'table_name' => 'tb_pais',
            'controller' => 'AdminTbPais1Controller',
            'is_protected' => '0',
            'is_active' => '0',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Departamentos',
            'icon' => 'fa fa-glass',
            'path' => 'tb_departamentos',
            'table_name' => 'tb_departamentos',
            'controller' => 'AdminTbDepartamentos1Controller',
            'is_protected' => '0',
            'is_active' => '0',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Municipios',
            'icon' => 'fa fa-glass',
            'path' => 'tb_municipios',
            'table_name' => 'tb_municipios',
            'controller' => 'AdminTbMunicipios1Controller',
            'is_protected' => '0',
            'is_active' => '0',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Regiones',
            'icon' => 'fa fa-glass',
            'path' => 'tb_regiones',
            'table_name' => 'tb_regiones',
            'controller' => 'AdminTbRegiones1Controller',
            'is_protected' => '0',
            'is_active' => '0',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Genero',
            'icon' => 'fa fa-glass',
            'path' => 'tb_generos',
            'table_name' => 'tb_generos',
            'controller' => 'AdminTbGeneros1Controller',
            'is_protected' => '0',
            'is_active' => '0',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Etnias',
            'icon' => 'fa fa-glass',
            'path' => 'tb_etnias',
            'table_name' => 'tb_etnias',
            'controller' => 'AdminTbEtnias1Controller',
            'is_protected' => '0',
            'is_active' => '0',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Documentos de Identificación',
            'icon' => 'fa fa-glass',
            'path' => 'tb_identification_documents',
            'table_name' => 'tb_identification_documents',
            'controller' => 'AdminTbIdentificationDocuments1Controller',
            'is_protected' => '0',
            'is_active' => '0',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Estado Civil',
            'icon' => 'fa fa-glass',
            'path' => 'tb_marital_status',
            'table_name' => 'tb_marital_status',
            'controller' => 'AdminTbMaritalStatus1Controller',
            'is_protected' => '0',
            'is_active' => '0',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Profesiones',
            'icon' => 'fa fa-glass',
            'path' => 'tb_profeciones',
            'table_name' => 'tb_profeciones',
            'controller' => 'AdminTbProfeciones1Controller',
            'is_protected' => '0',
            'is_active' => '0',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Salarios',
            'icon' => 'fa fa-glass',
            'path' => 'tb_salarios',
            'table_name' => 'tb_salarios',
            'controller' => 'AdminTbSalarios1Controller',
            'is_protected' => '0',
            'is_active' => '0',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Empleados',
            'icon' => 'fa fa-glass',
            'path' => 'tb_empleados',
            'table_name' => 'tb_empleados',
            'controller' => 'AdminTbEmpleados1Controller',
            'is_protected' => '0',
            'is_active' => '0',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Datos Médicos',
            'icon' => 'fa fa-glass',
            'path' => 'tb_datos_medicos',
            'table_name' => 'tb_datos_medicos',
            'controller' => 'AdminTbDatosMedicosController',
            'is_protected' => '0',
            'is_active' => '0',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Otros Seguros',
            'icon' => 'fa fa-glass',
            'path' => 'tb_seguros_medicos',
            'table_name' => 'tb_seguros_medicos',
            'controller' => 'AdminTbSegurosMedicos1Controller',
            'is_protected' => '0',
            'is_active' => '0',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Tipo de Contratos',
            'icon' => 'fa fa-glass',
            'path' => 'tb_tipo_contratos',
            'table_name' => 'tb_tipo_contratos',
            'controller' => 'AdminTbTipoContratos1Controller',
            'is_protected' => '0',
            'is_active' => '0',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Dias Laborables',
            'icon' => 'fa fa-glass',
            'path' => 'tb_dias_laborados',
            'table_name' => 'tb_dias_laborados',
            'controller' => 'AdminTbDiasLaboradosController',
            'is_protected' => '0',
            'is_active' => '0',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Horas Laborables',
            'icon' => 'fa fa-glass',
            'path' => 'tb_hrs_laboradas',
            'table_name' => 'tb_hrs_laboradas',
            'controller' => 'AdminTbHrsLaboradasController',
            'is_protected' => '0',
            'is_active' => '0',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Tipos de Salario',
            'icon' => 'fa fa-glass',
            'path' => 'tb_tipo_salarios',
            'table_name' => 'tb_tipo_salarios',
            'controller' => 'AdminTbTipoSalariosController',
            'is_protected' => '0',
            'is_active' => '0',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Tipo de Contratos',
            'icon' => 'fa fa-glass',
            'path' => 'tb_tipo_contratos30',
            'table_name' => 'tb_tipo_contratos',
            'controller' => 'AdminTbTipoContratos30Controller',
            'is_protected' => '0',
            'is_active' => '0',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Contratos',
            'icon' => 'fa fa-glass',
            'path' => 'tb_contratos',
            'table_name' => 'tb_contratos',
            'controller' => 'AdminTbContratosController',
            'is_protected' => '0',
            'is_active' => '0',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Bajas',
            'icon' => 'fa fa-glass',
            'path' => 'tb_empleados35',
            'table_name' => 'tb_empleados',
            'controller' => 'AdminTbEmpleados35Controller',
            'is_protected' => '0',
            'is_active' => '0',
            'created_at' => Carbon::now(),

        ],

        [
            'name' => 'Información Académica',
            'icon' => 'fa fa-glass',
            'path' => 'tb_información_academicas',
            'table_name' => 'tb_información_academicas',
            'controller' => 'AdminTbInformaciónAcademicas1Controller',
            'is_protected' => '0',
            'is_active' => '0',
            'created_at' => Carbon::now(),

        ]

        ]


                                    );
    }
}
