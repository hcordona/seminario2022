<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            $table->id();
            $table->string('fotografia')->nullable();
            $table->string('primer_nombre');
            $table->string('segundo_nombre')->nullable();
            $table->string('primer_apellido');
            $table->string('segundo_apellido')->nullable();
            $table->date('fecha_nacimiento');
            $table->bigInteger('documento_id')->unsigned();
            $table->foreign('documento_id')->references('id')->on('tipodocsidentificadores')->cascadeOnDelete();
            $table->string('no_documento');
            $table->date('fecha_expiracion');
            $table->string('nit')->nullable();
            $table->bigInteger('genero_id')->unsigned();
            $table->foreign('genero_id')->references('id')->on('generos')->cascadeOnDelete();
            $table->bigInteger('etnia_id')->unsigned();
            $table->foreign('etnia_id')->references('id')->on('etnias')->cascadeOnDelete();
            $table->bigInteger('estado_civil_id')->unsigned();
            $table->foreign('estado_civil_id')->references('id')->on('civilestados')->cascadeOnDelete();
            $table->string('apellido_casadas')->nullable();
            $table->string('telefono_casa')->nullable();
            $table->string('telefono_personal');
            $table->string('email_personal');
            $table->string('email_corporativo')->nullable();

            $table->bigInteger('ocupacional_id')->unsigned();
            $table->foreign('ocupacional_id')->references('id')->on('codocupacionales')->cascadeOnDelete();

            $table->bigInteger('profecion_id')->unsigned();
            $table->foreign('profecion_id')->references('id')->on('profesiones')->cascadeOnDelete();
            $table->string('colegiado_activo')->nullable();
            $table->string('seguro_social')->nullable();
            $table->bigInteger('pais_id')->unsigned();
            $table->foreign('pais_id')->references('id')->on('pais')->cascadeOnDelete();
            $table->bigInteger('region_id')->unsigned();
            $table->foreign('region_id')->references('id')->on('regiones')->cascadeOnDelete();
            $table->bigInteger('departamento_id')->unsigned();
            $table->foreign('departamento_id')->references('id')->on('departamentos')->cascadeOnDelete();
            $table->bigInteger('municipio_id')->unsigned();
            $table->foreign('municipio_id')->references('id')->on('municipios')->cascadeOnDelete();
            $table->bigInteger('bloqueo_id')->unsigned();
            $table->foreign('bloqueo_id')->references('id')->on('bloqueos')->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleados');
    }
}
