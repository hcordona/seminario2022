<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContratosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratos', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('contrato_id')->unsigned();
            $table->bigInteger('empleado_id')->unsigned();
            $table->date('fecha_inicio')->nullable();
            $table->date('fecha_fin')->nullable();
            $table->integer('anio_antiguedad')->nullable();
            $table->bigInteger('salario_id')->unsigned();
            $table->bigInteger('pago_id')->unsigned();
            $table->bigInteger('sede_id')->unsigned();

            $table->string('observaciones')->nullable();
            $table->string('documentos')->nullable();
            $table->bigInteger('estado_id')->unsigned();
            $table->foreign('estado_id')->references('id')->on('bloqueos')->cascadeOnDelete();
            $table->foreign('salario_id')->references('id')->on('tiposalarios')->cascadeOnDelete();
            $table->foreign('contrato_id')->references('id')->on('tipocontratos')->cascadeOnDelete();
            $table->foreign('empleado_id')->references('id')->on('empleados')->cascadeOnDelete();
            $table->foreign('sede_id')->references('id')->on('sedes')->cascadeOnDelete();
            $table->foreign('pago_id')->references('id')->on('tipopagos')->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contratos');
    }
}
