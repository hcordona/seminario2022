<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatosmedicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datosmedicos', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('gruposanguineo_id')->unsigned();
           $table->foreign('gruposanguineo_id')->references('id')->on('gruposanguineos')->cascadeOnDelete();
           $table->string('enfermedades')->nullable();
           $table->string('medicamentos')->nullable();
           $table->bigInteger('empleado_id')->unsigned();
           $table->foreign('empleado_id')->references('id')->on('empleados')->cascadeOnDelete();

           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datosmedicos');
    }
}
