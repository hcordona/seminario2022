<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTiposalariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tiposalarios', function (Blueprint $table) {
            $table->id();
            $table->string('salario');
            $table->double('monto');
            $table->double('bonificacion_incentiva')->nullable();
            $table->string('fundamento_legal')->nullable();
            $table->string('estado');
            $table->integer('anio');
            $table->string('observaciones')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tiposalarios');
    }
}
