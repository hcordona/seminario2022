<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTipocontratosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipocontratos', function (Blueprint $table) {
            $table->id();
            $table->string('contrato');
            $table->bigInteger('id_laboradas')->unsigned();
            $table->bigInteger('id_laborados')->unsigned();
            $table->string('justificacion')->nullable();
            $table->string('formato');
            $table->string('observacion')->nullable();
            $table->string('status');
            $table->foreign('id_laboradas')->references('id')->on('hrslaboradas')->cascadeOnDelete();
            $table->foreign('id_laborados')->references('id')->on('diaslaborados')->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipocontratos');
    }
}
