<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PdfController;
use App\Http\Controllers\ReportController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    
    return redirect('/admin');
});

Route::get('/password', function () {
    
    return redirect('/password/reset');
});

Route::get('/reporte1', function () {
    
   return view('fpdf.reporte1');
});

Route::get('pdf', [PdfController::class, 'index']);

Route::get('pdf2', [ReportController::class, 'generar']);




Route::get('/admin/password/reset', function () {
    return redirect('/password/reset');
})->middleware('guest')->name('password.request');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
