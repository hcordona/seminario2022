-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: mysqlc
-- Tiempo de generación: 17-11-2022 a las 01:34:43
-- Versión del servidor: 5.7.28
-- Versión de PHP: 8.0.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dashboard`
--
CREATE DATABASE IF NOT EXISTS `dashboard` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci;
USE `dashboard`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `email_template`
--

CREATE TABLE `email_template` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `email_template`
--

INSERT INTO `email_template` (`id`, `created_at`, `updated_at`, `content`, `name`, `subject`) VALUES
(1, NULL, NULL, '<!DOCTYPE html>\r\n                <html lang=\"en\">\r\n                <head>\r\n                    <meta charset=\"utf-8\">\r\n                    <meta name=\"viewport\" content=\"width=device-width\">\r\n                    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"> \r\n                    <meta name=\"x-apple-disable-message-reformatting\">\r\n                    <title>Example</title>\r\n                    <style>\r\n                        body {\r\n                            background-color:#fff;\r\n                            color:#222222;\r\n                            margin: 0px auto;\r\n                            padding: 0px;\r\n                            height: 100%;\r\n                            width: 100%;\r\n                            font-weight: 400;\r\n                            font-size: 15px;\r\n                            line-height: 1.8;\r\n                        }\r\n                        .continer{\r\n                            width:400px;\r\n                            margin-left:auto;\r\n                            margin-right:auto;\r\n                            background-color:#efefef;\r\n                            padding:30px;\r\n                        }\r\n                        .btn{\r\n                            padding: 5px 15px;\r\n                            display: inline-block;\r\n                        }\r\n                        .btn-primary{\r\n                            border-radius: 3px;\r\n                            background: #0b3c7c;\r\n                            color: #fff;\r\n                            text-decoration: none;\r\n                        }\r\n                        .btn-primary:hover{\r\n                            border-radius: 3px;\r\n                            background: #4673ad;\r\n                            color: #fff;\r\n                            text-decoration: none;\r\n                        }\r\n                    </style>\r\n                </head>\r\n                <body>\r\n                    <div class=\"continer\">\r\n                        <h1>Lorem ipsum dolor</h1>\r\n                        <h4>Ipsum dolor cet emit amet</h4>\r\n                        <p>\r\n                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea <strong>commodo consequat</strong>. \r\n                            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \r\n                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n                        </p>\r\n                        <h4>Ipsum dolor cet emit amet</h4>\r\n                        <p>\r\n                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod <a href=\"#\">tempor incididunt ut labore</a> et dolore magna aliqua.\r\n                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \r\n                        </p>\r\n                        <h4>Ipsum dolor cet emit amet</h4>\r\n                        <p>\r\n                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\r\n                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \r\n                        </p>\r\n                        <a href=\"#\" class=\"btn btn-primary\">Lorem ipsum dolor</a>\r\n                        <h4>Ipsum dolor cet emit amet</h4>\r\n                        <p>\r\n                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\r\n                            Ut enim ad minim veniam, quis nostrud exercitation <a href=\"#\">ullamco</a> laboris nisi ut aliquip ex ea commodo consequat. \r\n                            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \r\n                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n                        </p>\r\n                    </div>\r\n                </body>\r\n                </html>', 'Example E-mail', 'Example E-mail');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `example`
--

CREATE TABLE `example` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `example`
--

INSERT INTO `example` (`id`, `created_at`, `updated_at`, `name`, `description`, `status_id`) VALUES
(1, NULL, NULL, 'Ut magnam ab.', 'Fugiat est voluptas dolore distinctio. Iusto assumenda quisquam sequi vitae fugiat distinctio.', 3),
(2, NULL, NULL, 'Aut illum rerum enim.', 'Similique aut quae repudiandae rem.', 2),
(3, NULL, NULL, 'Possimus provident cum voluptates vel.', 'Qui ipsam ut amet et. Minima rerum sit quia reiciendis.', 2),
(4, NULL, NULL, 'Et est voluptatem perferendis ut.', 'Et cupiditate odit illum. Enim hic dolore error magnam ex.', 1),
(5, NULL, NULL, 'Et soluta praesentium adipisci autem est.', 'Totam officia quasi odio fugiat quis eveniet.', 4),
(6, NULL, NULL, 'Aliquid architecto id nisi sapiente dolore.', 'Aut ut deleniti unde rerum recusandae voluptatum et. Ipsa quis enim delectus nulla.', 1),
(7, NULL, NULL, 'Dolores veritatis sed.', 'Et odit eos soluta et voluptates cupiditate reiciendis.', 3),
(8, NULL, NULL, 'Est eum corrupti repellendus.', 'Non ea in in aut.', 1),
(9, NULL, NULL, 'Reprehenderit eum vel sit repellat qui.', 'Quas optio non pariatur ipsam deleniti omnis repellat iste. Neque hic minima iste ratione et eveniet.', 1),
(10, NULL, NULL, 'Et ea esse odio culpa.', 'Sit nulla mollitia voluptas quo. Quia rerum maiores sed veritatis voluptas.', 1),
(11, NULL, NULL, 'Commodi sint et.', 'Reprehenderit qui esse iure nihil. Libero in laboriosam molestiae deserunt explicabo.', 1),
(12, NULL, NULL, 'Eos excepturi ea illo nisi hic.', 'Consequuntur qui ut eius sit iste.', 4),
(13, NULL, NULL, 'Aut enim aut sit quis optio.', 'Et sed fugit sed explicabo non minus consequatur.', 3),
(14, NULL, NULL, 'Laboriosam ut eos aut.', 'Ex delectus aliquam dolorum at quia. Doloribus eos ea aut ut ut.', 3),
(15, NULL, NULL, 'Enim soluta id ab nobis nostrum.', 'Omnis est libero voluptatibus earum nam.', 4),
(16, NULL, NULL, 'Unde minus maiores.', 'Labore placeat quia tenetur est quis laboriosam. Eaque voluptas praesentium voluptatem pariatur voluptatem qui.', 2),
(17, NULL, NULL, 'Suscipit qui molestiae unde quia aut.', 'Suscipit ea est sed maxime.', 4),
(18, NULL, NULL, 'Excepturi rerum cum ut.', 'Non eos aut soluta perspiciatis commodi voluptatem sit.', 1),
(19, NULL, NULL, 'Rerum sint rerum quod minus.', 'Et sunt beatae architecto numquam quaerat.', 2),
(20, NULL, NULL, 'Dolorum veritatis animi neque.', 'Quam totam blanditiis nesciunt vitae et veritatis.', 1),
(21, NULL, NULL, 'Rerum rerum sit quam.', 'Fugiat esse dolorum qui magnam.', 1),
(22, NULL, NULL, 'Et beatae rerum beatae quas in.', 'Aspernatur debitis est velit ex rem quidem nulla possimus.', 3),
(23, NULL, NULL, 'Saepe est rerum quibusdam nostrum cum.', 'Molestias ut dolorem nesciunt quae est laborum.', 4),
(24, NULL, NULL, 'Tenetur necessitatibus cumque.', 'Libero esse voluptatem magnam excepturi. Quas sed dolorum voluptas recusandae dolorem neque consequatur.', 4),
(25, NULL, NULL, 'Corrupti magni recusandae.', 'Aperiam reprehenderit dolorem amet. Velit minus autem debitis sed magnam.', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `folder`
--

CREATE TABLE `folder` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `folder_id` int(10) UNSIGNED DEFAULT NULL,
  `resource` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `folder`
--

INSERT INTO `folder` (`id`, `created_at`, `updated_at`, `name`, `folder_id`, `resource`) VALUES
(1, NULL, NULL, 'root', NULL, NULL),
(2, NULL, NULL, 'resource', 1, 1),
(3, NULL, NULL, 'documents', 1, NULL),
(4, NULL, NULL, 'graphics', 1, NULL),
(5, NULL, NULL, 'other', 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `form`
--

CREATE TABLE `form` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `read` tinyint(1) NOT NULL,
  `edit` tinyint(1) NOT NULL,
  `add` tinyint(1) NOT NULL,
  `delete` tinyint(1) NOT NULL,
  `pagination` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `form`
--

INSERT INTO `form` (`id`, `created_at`, `updated_at`, `name`, `table_name`, `read`, `edit`, `add`, `delete`, `pagination`) VALUES
(1, NULL, NULL, 'Example', 'example', 1, 1, 1, 1, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `form_field`
--

CREATE TABLE `form_field` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `browse` tinyint(1) NOT NULL,
  `read` tinyint(1) NOT NULL,
  `edit` tinyint(1) NOT NULL,
  `add` tinyint(1) NOT NULL,
  `relation_table` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relation_column` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_id` int(10) UNSIGNED NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `form_field`
--

INSERT INTO `form_field` (`id`, `created_at`, `updated_at`, `name`, `type`, `browse`, `read`, `edit`, `add`, `relation_table`, `relation_column`, `form_id`, `column_name`) VALUES
(1, NULL, NULL, 'Title', 'text', 1, 1, 1, 1, NULL, NULL, 1, 'name'),
(2, NULL, NULL, 'Description', 'text_area', 1, 1, 1, 1, NULL, NULL, 1, 'description'),
(3, NULL, NULL, 'Status', 'relation_select', 1, 1, 1, 1, 'status', 'name', 1, 'status_id');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `media`
--

CREATE TABLE `media` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `conversions_disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` bigint(20) UNSIGNED NOT NULL,
  `uuid` bigint(20) UNSIGNED NOT NULL,
  `manipulations` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `custom_properties` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `responsive_images` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `order_column` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menulist`
--

CREATE TABLE `menulist` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `menulist`
--

INSERT INTO `menulist` (`id`, `name`) VALUES
(1, 'sidebar menu'),
(2, 'top menu');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menus`
--

CREATE TABLE `menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `href` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `menu_id` int(10) UNSIGNED NOT NULL,
  `sequence` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `menus`
--

INSERT INTO `menus` (`id`, `name`, `href`, `icon`, `slug`, `parent_id`, `menu_id`, `sequence`) VALUES
(1, 'Dashboard', '/', 'cil-speedometer', 'link', NULL, 1, 1),
(2, 'Settings', NULL, 'cil-calculator', 'dropdown', NULL, 1, 2),
(3, 'Notes', '/notes', NULL, 'link', 2, 1, 3),
(4, 'Users', '/users', NULL, 'link', 2, 1, 4),
(5, 'Edit menu', '/menu/menu', NULL, 'link', 2, 1, 5),
(6, 'Edit menu elements', '/menu/element', NULL, 'link', 2, 1, 6),
(7, 'Edit roles', '/roles', NULL, 'link', 2, 1, 7),
(8, 'Media', '/media', NULL, 'link', 2, 1, 8),
(9, 'BREAD', '/bread', NULL, 'link', 2, 1, 9),
(10, 'Email', '/mail', NULL, 'link', 2, 1, 10),
(11, 'Login', '/login', 'cil-account-logout', 'link', NULL, 1, 11),
(12, 'Register', '/register', 'cil-account-logout', 'link', NULL, 1, 12),
(13, 'Theme', NULL, NULL, 'title', NULL, 1, 13),
(14, 'Colors', '/colors', 'cil-drop1', 'link', NULL, 1, 14),
(15, 'Typography', '/typography', 'cil-pencil', 'link', NULL, 1, 15),
(16, 'Base', NULL, 'cil-puzzle', 'dropdown', NULL, 1, 16),
(17, 'Breadcrumb', '/base/breadcrumb', NULL, 'link', 16, 1, 17),
(18, 'Cards', '/base/cards', NULL, 'link', 16, 1, 18),
(19, 'Carousel', '/base/carousel', NULL, 'link', 16, 1, 19),
(20, 'Collapse', '/base/collapse', NULL, 'link', 16, 1, 20),
(21, 'Forms', '/base/forms', NULL, 'link', 16, 1, 21),
(22, 'Jumbotron', '/base/jumbotron', NULL, 'link', 16, 1, 22),
(23, 'List group', '/base/list-group', NULL, 'link', 16, 1, 23),
(24, 'Navs', '/base/navs', NULL, 'link', 16, 1, 24),
(25, 'Pagination', '/base/pagination', NULL, 'link', 16, 1, 25),
(26, 'Popovers', '/base/popovers', NULL, 'link', 16, 1, 26),
(27, 'Progress', '/base/progress', NULL, 'link', 16, 1, 27),
(28, 'Scrollspy', '/base/scrollspy', NULL, 'link', 16, 1, 28),
(29, 'Switches', '/base/switches', NULL, 'link', 16, 1, 29),
(30, 'Tables', '/base/tables', NULL, 'link', 16, 1, 30),
(31, 'Tabs', '/base/tabs', NULL, 'link', 16, 1, 31),
(32, 'Tooltips', '/base/tooltips', NULL, 'link', 16, 1, 32),
(33, 'Buttons', NULL, 'cil-cursor', 'dropdown', NULL, 1, 33),
(34, 'Buttons', '/buttons/buttons', NULL, 'link', 33, 1, 34),
(35, 'Buttons Group', '/buttons/button-group', NULL, 'link', 33, 1, 35),
(36, 'Dropdowns', '/buttons/dropdowns', NULL, 'link', 33, 1, 36),
(37, 'Brand Buttons', '/buttons/brand-buttons', NULL, 'link', 33, 1, 37),
(38, 'Charts', '/charts', 'cil-chart-pie', 'link', NULL, 1, 38),
(39, 'Icons', NULL, 'cil-star', 'dropdown', NULL, 1, 39),
(40, 'CoreUI Icons', '/icon/coreui-icons', NULL, 'link', 39, 1, 40),
(41, 'Flags', '/icon/flags', NULL, 'link', 39, 1, 41),
(42, 'Brands', '/icon/brands', NULL, 'link', 39, 1, 42),
(43, 'Notifications', NULL, 'cil-bell', 'dropdown', NULL, 1, 43),
(44, 'Alerts', '/notifications/alerts', NULL, 'link', 43, 1, 44),
(45, 'Badge', '/notifications/badge', NULL, 'link', 43, 1, 45),
(46, 'Modals', '/notifications/modals', NULL, 'link', 43, 1, 46),
(47, 'Widgets', '/widgets', 'cil-calculator', 'link', NULL, 1, 47),
(48, 'Extras', NULL, NULL, 'title', NULL, 1, 48),
(49, 'Pages', NULL, 'cil-star', 'dropdown', NULL, 1, 49),
(50, 'Login', '/login', NULL, 'link', 49, 1, 50),
(51, 'Register', '/register', NULL, 'link', 49, 1, 51),
(52, 'Error 404', '/404', NULL, 'link', 49, 1, 52),
(53, 'Error 500', '/500', NULL, 'link', 49, 1, 53),
(54, 'Download CoreUI', 'https://coreui.io', 'cil-cloud-download', 'link', NULL, 1, 54),
(55, 'Try CoreUI PRO', 'https://coreui.io/pro/', 'cil-layers', 'link', NULL, 1, 55),
(56, 'Pages', NULL, '', 'dropdown', NULL, 2, 56),
(57, 'Dashboard', '/', NULL, 'link', 56, 2, 57),
(58, 'Notes', '/notes', NULL, 'link', 56, 2, 58),
(59, 'Users', '/users', NULL, 'link', 56, 2, 59),
(60, 'Settings', NULL, '', 'dropdown', NULL, 2, 60),
(61, 'Edit menu', '/menu/menu', NULL, 'link', 60, 2, 61),
(62, 'Edit menu elements', '/menu/element', NULL, 'link', 60, 2, 62),
(63, 'Edit roles', '/roles', NULL, 'link', 60, 2, 63),
(64, 'Media', '/media', NULL, 'link', 60, 2, 64),
(65, 'BREAD', '/bread', NULL, 'link', 60, 2, 65);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_role`
--

CREATE TABLE `menu_role` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menus_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `menu_role`
--

INSERT INTO `menu_role` (`id`, `role_name`, `menus_id`) VALUES
(1, 'guest', 1),
(2, 'user', 1),
(3, 'admin', 1),
(4, 'admin', 2),
(5, 'admin', 3),
(6, 'admin', 4),
(7, 'admin', 5),
(8, 'admin', 6),
(9, 'admin', 7),
(10, 'admin', 8),
(11, 'admin', 9),
(12, 'admin', 10),
(13, 'guest', 11),
(14, 'guest', 12),
(15, 'user', 13),
(16, 'admin', 13),
(17, 'user', 14),
(18, 'admin', 14),
(19, 'user', 15),
(20, 'admin', 15),
(21, 'user', 16),
(22, 'admin', 16),
(23, 'user', 17),
(24, 'admin', 17),
(25, 'user', 18),
(26, 'admin', 18),
(27, 'user', 19),
(28, 'admin', 19),
(29, 'user', 20),
(30, 'admin', 20),
(31, 'user', 21),
(32, 'admin', 21),
(33, 'user', 22),
(34, 'admin', 22),
(35, 'user', 23),
(36, 'admin', 23),
(37, 'user', 24),
(38, 'admin', 24),
(39, 'user', 25),
(40, 'admin', 25),
(41, 'user', 26),
(42, 'admin', 26),
(43, 'user', 27),
(44, 'admin', 27),
(45, 'user', 28),
(46, 'admin', 28),
(47, 'user', 29),
(48, 'admin', 29),
(49, 'user', 30),
(50, 'admin', 30),
(51, 'user', 31),
(52, 'admin', 31),
(53, 'user', 32),
(54, 'admin', 32),
(55, 'user', 33),
(56, 'admin', 33),
(57, 'user', 34),
(58, 'admin', 34),
(59, 'user', 35),
(60, 'admin', 35),
(61, 'user', 36),
(62, 'admin', 36),
(63, 'user', 37),
(64, 'admin', 37),
(65, 'user', 38),
(66, 'admin', 38),
(67, 'user', 39),
(68, 'admin', 39),
(69, 'user', 40),
(70, 'admin', 40),
(71, 'user', 41),
(72, 'admin', 41),
(73, 'user', 42),
(74, 'admin', 42),
(75, 'user', 43),
(76, 'admin', 43),
(77, 'user', 44),
(78, 'admin', 44),
(79, 'user', 45),
(80, 'admin', 45),
(81, 'user', 46),
(82, 'admin', 46),
(83, 'user', 47),
(84, 'admin', 47),
(85, 'user', 48),
(86, 'admin', 48),
(87, 'user', 49),
(88, 'admin', 49),
(89, 'user', 50),
(90, 'admin', 50),
(91, 'user', 51),
(92, 'admin', 51),
(93, 'user', 52),
(94, 'admin', 52),
(95, 'user', 53),
(96, 'admin', 53),
(97, 'guest', 54),
(98, 'user', 54),
(99, 'admin', 54),
(100, 'guest', 55),
(101, 'user', 55),
(102, 'admin', 55),
(103, 'guest', 56),
(104, 'user', 56),
(105, 'admin', 56),
(106, 'guest', 57),
(107, 'user', 57),
(108, 'admin', 57),
(109, 'user', 58),
(110, 'admin', 58),
(111, 'admin', 59),
(112, 'admin', 60),
(113, 'admin', 61),
(114, 'admin', 62),
(115, 'admin', 63),
(116, 'admin', 64),
(117, 'admin', 65);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_10_11_085455_create_notes_table', 1),
(5, '2019_10_12_115248_create_status_table', 1),
(6, '2019_11_08_102827_create_menus_table', 1),
(7, '2019_11_13_092213_create_menurole_table', 1),
(8, '2019_12_10_092113_create_permission_tables', 1),
(9, '2019_12_11_091036_create_menulist_table', 1),
(10, '2019_12_18_092518_create_role_hierarchy_table', 1),
(11, '2020_01_07_093259_create_folder_table', 1),
(12, '2020_01_08_184500_create_media_table', 1),
(13, '2020_01_21_161241_create_form_field_table', 1),
(14, '2020_01_21_161242_create_form_table', 1),
(15, '2020_01_21_161243_create_example_table', 1),
(16, '2020_03_12_111400_create_email_template_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(2, 'App\\Models\\User', 1),
(2, 'App\\Models\\User', 2),
(2, 'App\\Models\\User', 3),
(2, 'App\\Models\\User', 4),
(2, 'App\\Models\\User', 5),
(2, 'App\\Models\\User', 6),
(2, 'App\\Models\\User', 7),
(2, 'App\\Models\\User', 8),
(2, 'App\\Models\\User', 9),
(2, 'App\\Models\\User', 10),
(2, 'App\\Models\\User', 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notes`
--

CREATE TABLE `notes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `note_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `applies_to_date` date NOT NULL,
  `users_id` int(10) UNSIGNED NOT NULL,
  `status_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `notes`
--

INSERT INTO `notes` (`id`, `title`, `content`, `note_type`, `applies_to_date`, `users_id`, `status_id`, `created_at`, `updated_at`) VALUES
(1, 'Expedita vero beatae aut dolorum perspiciatis.', 'Veniam mollitia et excepturi omnis aliquid blanditiis vel. Sapiente incidunt recusandae facilis. Qui consequatur voluptatem enim officia aspernatur.', 'hic ad', '2007-09-10', 4, 4, NULL, NULL),
(2, 'Temporibus quis natus.', 'Voluptas ea dolorum quia recusandae deleniti. Veniam aut odit voluptas sint facere autem harum reiciendis. In quasi rerum officia. Qui est autem accusantium ea molestiae sed harum.', 'libero', '2014-07-07', 9, 1, NULL, NULL),
(3, 'Asperiores vitae omnis sequi.', 'Voluptatem omnis voluptate in possimus error vero illum minus. Exercitationem qui illum et dolor molestias optio necessitatibus. Iusto sed esse est voluptate illo.', 'quas vero', '1983-02-04', 2, 2, NULL, NULL),
(4, 'Quis omnis quos blanditiis fuga.', 'Recusandae repellat esse sunt excepturi qui reiciendis. In numquam dolore unde rem voluptatem nulla sed.', 'dolorem', '2007-04-29', 8, 1, NULL, NULL),
(5, 'Consequatur saepe aliquid.', 'Non repellat quos mollitia fuga quae hic. Porro molestiae earum rerum aut.', 'illum', '2000-08-13', 9, 1, NULL, NULL),
(6, 'Autem dolor mollitia in.', 'Qui similique in dolor at voluptates. Autem enim assumenda perspiciatis omnis quia. Numquam velit in esse fuga et. Ut autem laboriosam dignissimos itaque vel id amet similique.', 'dolorum harum', '1999-11-16', 8, 2, NULL, NULL),
(7, 'Molestiae sequi animi ipsa quia.', 'Autem numquam et sint molestias illo similique cum. Harum non soluta culpa quia quisquam nihil qui et. Aliquid rerum tempore rerum quae facere necessitatibus quidem. Aperiam est nulla excepturi illum itaque. Saepe sed ea sint quo.', 'dolores', '1995-07-24', 5, 3, NULL, NULL),
(8, 'Voluptatibus non sunt hic est et.', 'Dolor excepturi explicabo qui alias et. Quibusdam iste quos magnam quidem et est eligendi. Facilis vel deserunt veritatis sequi ullam rerum.', 'ea', '1988-01-28', 4, 2, NULL, NULL),
(9, 'Consectetur ipsam itaque non.', 'Ut ut nostrum quae doloribus neque. Nam sunt illum sint. Vero sit aliquam ex adipisci. Sequi rem quo enim perferendis ut sequi quia.', 'saepe', '2013-01-12', 10, 1, NULL, NULL),
(10, 'Eveniet quia saepe libero deserunt.', 'Quo aut eaque esse fuga. Voluptas excepturi ex magni soluta temporibus officiis. Ratione reiciendis et quibusdam voluptas dolor vitae quis expedita. Commodi sit et assumenda alias harum aut repellendus.', 'ut id', '1973-10-31', 8, 4, NULL, NULL),
(11, 'Unde sit ea.', 'Consequatur corrupti sed quo aut ad ut. Qui placeat consequatur voluptatem et tempore. Quibusdam rem dolorem cum mollitia corporis autem minima. Sequi vero eum ratione.', 'tempore', '1998-10-29', 4, 3, NULL, NULL),
(12, 'Minus deserunt qui sequi.', 'Rem beatae dolorem dolor hic excepturi eum mollitia. Dolorem vel sit eum recusandae porro. Eos quod debitis et blanditiis omnis doloremque. Nihil illo eveniet aspernatur et minus unde.', 'iusto optio', '1971-02-12', 6, 3, NULL, NULL),
(13, 'Qui quia voluptatem voluptatum quia.', 'Quo beatae ipsam deleniti nihil. Cumque consectetur vitae quia qui error quo soluta.', 'sed dolor', '1987-12-10', 7, 3, NULL, NULL),
(14, 'Aliquid sequi suscipit dolorum.', 'Ut quia rerum blanditiis ab sed laudantium. Vero ipsa dolorum error sit. Ducimus dolorem est numquam nulla. A officia nesciunt culpa dolores.', 'cupiditate iste', '1986-10-12', 7, 4, NULL, NULL),
(15, 'Corrupti voluptatum ex nam consequatur temporibus.', 'Et officia natus quo reprehenderit accusamus. Dolores in soluta suscipit aut et fugiat iure. Velit sit qui aspernatur consequuntur alias.', 'error voluptate', '1994-07-05', 11, 3, NULL, NULL),
(16, 'Nemo eligendi dolorem excepturi.', 'Doloribus beatae et quae. Deleniti voluptatem deleniti eligendi voluptatem numquam voluptas sit. Quae atque debitis et asperiores dolor porro est.', 'minima', '2006-02-27', 7, 2, NULL, NULL),
(17, 'Similique labore enim.', 'Nisi et eos dolore qui facere qui est. Sed dolores ab voluptatibus totam blanditiis corporis tenetur et. Et ab optio ut magni veritatis dolore autem.', 'repellendus ut', '2020-10-02', 8, 4, NULL, NULL),
(18, 'Dolor ut in quo tenetur.', 'Sunt et veritatis vel mollitia. Repellendus ut facilis voluptatem vel. Qui non doloribus aut velit. Blanditiis aliquam autem mollitia provident quia assumenda tempora sunt.', 'cupiditate', '2018-10-21', 8, 1, NULL, NULL),
(19, 'Iusto nobis odit repellendus.', 'Alias sit qui alias sed provident iste. Beatae rerum dignissimos sapiente earum. Molestiae qui sapiente sunt suscipit expedita laboriosam odit.', 'et', '1976-11-20', 2, 4, NULL, NULL),
(20, 'Aut consequatur facilis nostrum.', 'Voluptatem ad dicta aut quaerat occaecati. Fugiat ipsa qui est perspiciatis eos rerum. Possimus repellat ex aperiam iusto commodi animi.', 'rem', '1989-06-25', 9, 3, NULL, NULL),
(21, 'Neque quaerat suscipit.', 'Vel tempora aut unde. Quam quo perspiciatis dolorem rerum. Tempora est et ex consequatur labore iure eaque. Et eligendi voluptas rem itaque ut nostrum.', 'consequuntur', '1980-09-18', 5, 2, NULL, NULL),
(22, 'Esse magnam ex eveniet.', 'Sint et exercitationem quos explicabo. Rerum quo aliquam qui repellendus suscipit autem id facere. Ut pariatur reiciendis quibusdam omnis ratione est.', 'qui', '2020-11-19', 10, 3, NULL, NULL),
(23, 'Voluptatibus accusamus aut suscipit.', 'Et voluptatem ullam voluptas. Voluptas in inventore eos delectus. Dolor rerum vel placeat occaecati magni vel laborum.', 'cupiditate', '1974-08-08', 9, 2, NULL, NULL),
(24, 'Blanditiis est et totam corporis culpa.', 'Sed a officiis aspernatur quod maiores delectus corporis natus. Et aspernatur officiis ad ipsa fugit. Placeat sit maiores cum. Vero facilis esse porro officiis quis.', 'quibusdam', '2013-01-01', 11, 2, NULL, NULL),
(25, 'Et dolores consequatur quos.', 'Nesciunt possimus rerum natus qui omnis recusandae vel. Aut expedita ut cumque quod facilis in ea. Voluptates maiores praesentium reprehenderit consequatur.', 'cumque', '2017-06-21', 5, 3, NULL, NULL),
(26, 'Maxime exercitationem reiciendis earum vel.', 'Aliquid iste doloremque consectetur qui. Labore laboriosam et dolorum aut nihil eos soluta eum. Quas nesciunt minus nobis quisquam ullam. Earum labore ut perspiciatis sed illo.', 'voluptatum aspernatur', '1972-04-07', 6, 4, NULL, NULL),
(27, 'Rerum vel minus iste.', 'Perferendis ullam non repudiandae. Distinctio veniam ut hic dolores doloribus consequuntur quisquam et. Sequi dignissimos consectetur reprehenderit deserunt illum.', 'eos', '1995-01-09', 3, 4, NULL, NULL),
(28, 'Reiciendis voluptatem optio fugit nobis ut.', 'Tempora autem quisquam nisi dolorum et est. Molestias adipisci deserunt qui aut quae. Quisquam voluptatem doloribus illum et dignissimos. Perferendis magnam aliquam soluta laborum.', 'quis fugiat', '2021-03-04', 2, 2, NULL, NULL),
(29, 'Culpa aut libero voluptatem.', 'Molestiae iusto ipsa omnis molestias sed consequatur autem ea. Consequatur consequatur quidem corrupti recusandae expedita modi dolores autem. Praesentium est doloribus placeat quas minima saepe aliquam. Et at rerum qui deleniti velit fugit enim. Similique et et qui voluptates voluptas.', 'autem nam', '1972-08-17', 6, 2, NULL, NULL),
(30, 'Et dolores rerum sit necessitatibus in.', 'Omnis libero et repellendus nostrum sunt similique eligendi enim. Vero maiores dolore molestias iste omnis.', 'quis non', '1995-03-09', 2, 4, NULL, NULL),
(31, 'Quaerat unde qui sunt.', 'Qui possimus enim praesentium enim. Quaerat aut sit explicabo. Velit ut quibusdam tempora.', 'excepturi', '1974-10-31', 8, 4, NULL, NULL),
(32, 'Velit suscipit corrupti non.', 'Aperiam non enim ad consequatur. Minima nobis ut suscipit autem voluptate accusantium. Aut a et quisquam ut sunt.', 'quisquam qui', '2002-02-14', 2, 3, NULL, NULL),
(33, 'Ut vero magnam explicabo.', 'Ut temporibus et itaque consequuntur est iste aut. Fugiat qui autem illum illo doloremque cumque. Numquam inventore eaque error natus ratione.', 'laboriosam', '1984-12-25', 10, 4, NULL, NULL),
(34, 'Nisi deserunt hic.', 'Quisquam dolores quaerat quasi sed atque tempore. Et dolor dolorem veniam. Tempore ullam et dolor omnis natus.', 'et quidem', '2007-12-23', 3, 3, NULL, NULL),
(35, 'At aut in.', 'Accusantium fugit fugiat nemo molestias amet veniam. Vel neque vel sapiente quod voluptates minima. Dolores consectetur eos velit. Ducimus nihil deserunt maiores excepturi laboriosam.', 'iusto', '1993-01-18', 4, 2, NULL, NULL),
(36, 'Praesentium laudantium quas omnis nulla.', 'Quidem culpa laborum omnis amet. Vel optio numquam placeat atque at quos sed. Quibusdam beatae tempora nemo suscipit. Eligendi corrupti soluta iure.', 'non', '2017-05-04', 9, 1, NULL, NULL),
(37, 'Explicabo consequatur voluptatem veniam ratione.', 'Aut sit et aliquid veritatis rerum ut. Velit molestias in aperiam aut tempora nihil. Sint consequatur vero saepe numquam officiis nemo impedit. Qui voluptatem excepturi ut et vel ut amet quis. Vero deleniti officia tempora ex et.', 'voluptatum enim', '1977-05-16', 8, 2, NULL, NULL),
(38, 'Quae odit debitis qui quod perferendis.', 'Hic a sed omnis veritatis quia voluptate tenetur. Sint placeat natus placeat quia est. Ipsam earum nobis in reiciendis sint. Voluptas magni aut et error.', 'amet quos', '1985-04-12', 2, 4, NULL, NULL),
(39, 'Sapiente cum vel iste voluptatibus.', 'Porro ut labore veritatis. Dicta dignissimos molestias aut dolorum. Aliquam accusantium voluptatem ut est.', 'nam id', '1972-02-29', 2, 4, NULL, NULL),
(40, 'Velit et corrupti corrupti autem.', 'Omnis aspernatur facilis quis et. Laboriosam vero veniam magni illum. Rerum accusantium labore veritatis et molestias quos. Ea dicta ratione totam eligendi.', 'possimus', '2016-05-15', 2, 3, NULL, NULL),
(41, 'Labore recusandae illum similique ut fugit.', 'Voluptatem quia ipsa et consequatur tempora unde laudantium fugiat. Optio velit doloribus aperiam accusantium aliquam neque dolores suscipit. Ut nulla saepe laudantium voluptate. Iste pariatur aut eos laboriosam et quasi.', 'ad', '2022-09-03', 3, 4, NULL, NULL),
(42, 'Fugit veniam numquam.', 'Deserunt iste ut dolorum eum eaque perferendis ut. Pariatur corporis sunt labore quos facilis quis et. Unde mollitia vero neque nostrum nesciunt quo. Tempora harum culpa accusamus doloribus omnis officiis provident. Dolorem nemo deserunt quis quas impedit vel et non.', 'veniam quo', '2018-06-11', 6, 2, NULL, NULL),
(43, 'Voluptas sed molestiae est.', 'Odio quibusdam harum nihil. Et magnam et quos et. Et quo enim voluptatem error fuga id.', 'alias qui', '2000-06-06', 3, 4, NULL, NULL),
(44, 'Distinctio occaecati rerum.', 'Aut quis doloremque nulla iure minus qui vitae sint. Ea dolores animi qui a dolor illum natus. Sunt repellat autem nihil.', 'dignissimos', '1993-09-22', 3, 1, NULL, NULL),
(45, 'Accusantium quibusdam vero adipisci.', 'Dolorem hic nulla quasi dolores perferendis et aliquam. Eos iure hic eos ea reprehenderit. Enim velit ipsa et est iure aspernatur perspiciatis. Qui assumenda est minima ab dolorem est.', 'reiciendis', '1993-10-19', 7, 3, NULL, NULL),
(46, 'Consequatur commodi minima adipisci earum nobis.', 'Et officia ut est saepe fugit labore voluptatem. Deserunt sint facilis vero et a id consequatur. Dicta nisi reiciendis saepe perferendis sunt in. Qui ducimus blanditiis ut occaecati autem rerum.', 'delectus', '2013-12-24', 9, 4, NULL, NULL),
(47, 'Eum ea id.', 'Voluptas est ea similique asperiores eos ab. Dolore nesciunt inventore quia praesentium. Dolores reprehenderit velit numquam nihil nemo. Dolore quis dolores aut ducimus sint.', 'quis', '2005-03-07', 3, 3, NULL, NULL),
(48, 'Magni nobis officia veniam similique.', 'Sint provident et deserunt ab consequuntur tempora. Et sit molestiae enim voluptates. Consequatur voluptates harum est illum dignissimos molestiae odio sunt.', 'enim dolore', '1988-11-03', 7, 3, NULL, NULL),
(49, 'Ullam exercitationem quam ipsum nesciunt.', 'Architecto et eum doloribus vero tenetur similique. Dolor quas labore nesciunt facilis laboriosam. Quis ipsam quisquam et vel autem quo eum. Quis id sint sint eum ea.', 'perspiciatis et', '1983-05-01', 8, 2, NULL, NULL),
(50, 'Autem et accusamus atque.', 'Iusto aut et enim ratione animi. Ut quae eius debitis et. Minus libero magni voluptates eius temporibus corrupti distinctio aut.', 'reprehenderit est', '1991-08-18', 5, 3, NULL, NULL),
(51, 'Inventore impedit aliquid eveniet libero.', 'Et non delectus eveniet eius dicta recusandae. Est iste corrupti quo accusamus quia.', 'qui', '2013-02-11', 10, 4, NULL, NULL),
(52, 'Expedita rerum tenetur accusantium fuga.', 'Laudantium aut sunt deleniti aut mollitia. Impedit ullam magnam nisi. Voluptas aspernatur libero rerum est.', 'optio aut', '1981-03-03', 6, 2, NULL, NULL),
(53, 'Facilis animi iste.', 'Consequatur aspernatur aut qui rem et voluptas deleniti tempora. Consectetur placeat ut quis et numquam velit quam. Aut aut dolor autem qui. Magnam odio odit iusto quos aliquid aspernatur incidunt.', 'blanditiis', '1972-06-16', 7, 4, NULL, NULL),
(54, 'Et suscipit qui enim.', 'Minima dolorem non dolore ad. Quaerat illum voluptates quidem maiores eius saepe. Fugiat autem minus sit placeat vitae.', 'fugit', '1988-01-27', 11, 4, NULL, NULL),
(55, 'Dolores qui ullam.', 'Cumque aut beatae rem ipsam. Sunt et voluptatem doloremque repudiandae ut possimus. Exercitationem ipsa vel aut quia maxime ad aut. Enim illum iusto expedita quos mollitia incidunt nulla.', 'cum recusandae', '1997-12-11', 3, 4, NULL, NULL),
(56, 'Rerum velit qui illum modi ut.', 'Facere cum reprehenderit veritatis alias non reiciendis sit. Facere id placeat omnis exercitationem laudantium accusamus. Et doloremque illum eveniet asperiores quaerat sapiente. Voluptates consequatur nesciunt laboriosam accusantium.', 'qui est', '1997-11-14', 6, 4, NULL, NULL),
(57, 'Voluptatem sed ut consequuntur explicabo.', 'Numquam beatae culpa qui et. Soluta et nesciunt et dicta dignissimos. Magnam exercitationem at dicta.', 'qui', '1998-03-08', 8, 2, NULL, NULL),
(58, 'Et ipsam quia.', 'Quis neque minus facere sunt nesciunt reprehenderit. Earum voluptatum non et. Consequatur exercitationem quo quia quis. Corporis repellat qui quisquam quae ipsa consequuntur et.', 'non', '1987-11-20', 2, 1, NULL, NULL),
(59, 'Aspernatur possimus esse optio nobis.', 'Odit quidem occaecati accusamus maxime mollitia. Explicabo fugit eveniet mollitia corrupti repellat. Cumque totam aut blanditiis mollitia ab magni.', 'saepe', '1995-08-01', 4, 4, NULL, NULL),
(60, 'Natus iure ea in molestias.', 'Et explicabo non numquam nulla aut aliquam. Et sed id exercitationem possimus provident. Id neque eos dolorem optio officia sequi. Enim nulla minima fugit provident fugit nisi et est.', 'aut', '2008-10-22', 8, 4, NULL, NULL),
(61, 'Et autem occaecati et dolores.', 'Deserunt veniam ipsum nam officia laudantium. Voluptatem omnis fuga laboriosam non. Qui in sed aut vero eos.', 'dolorem maxime', '2021-09-06', 2, 3, NULL, NULL),
(62, 'Nam assumenda voluptas voluptates voluptas.', 'Quidem omnis quisquam facilis pariatur perspiciatis et dicta soluta. Aut ipsum nihil omnis voluptas consectetur. Sunt aut pariatur ad qui commodi dolor atque at. Doloremque ducimus commodi consequuntur quisquam voluptate qui.', 'tempora voluptatum', '1970-08-01', 5, 1, NULL, NULL),
(63, 'Expedita error qui omnis veniam.', 'Dolores est ex optio magnam soluta iure et. Odio quibusdam et asperiores in placeat. Eos dolores voluptas qui enim. Quae et minima incidunt optio velit inventore.', 'deleniti enim', '2020-12-06', 5, 2, NULL, NULL),
(64, 'Atque sit numquam et.', 'Beatae optio voluptas et architecto dolores perferendis ratione sint. Consequatur quia et enim quas culpa. Ex optio quaerat delectus quaerat cupiditate nulla quia.', 'earum', '2006-03-09', 11, 3, NULL, NULL),
(65, 'Error exercitationem et qui.', 'Officia aliquid adipisci et rerum dolor ducimus voluptas. Ad velit adipisci est architecto eum eum. Aut molestiae iure eveniet est aut nulla.', 'amet', '2017-07-13', 10, 1, NULL, NULL),
(66, 'Ea consequatur eius.', 'Suscipit facere eligendi recusandae exercitationem rem cum voluptas non. Molestias quae inventore dolor veritatis vero omnis neque. In molestiae quae dignissimos accusamus ipsum rerum consequuntur. Sapiente voluptatem sed sint necessitatibus quidem ut adipisci.', 'qui amet', '1985-04-05', 6, 2, NULL, NULL),
(67, 'Quia eaque eaque alias aut voluptas.', 'Earum deserunt eum quam voluptatibus et et quod. Incidunt iste non et. Nemo repudiandae sed quas repudiandae. Dicta iste mollitia temporibus sint quia maiores.', 'sunt', '2018-08-09', 11, 4, NULL, NULL),
(68, 'Sed esse ullam.', 'Corporis sit fugiat laborum nam voluptate. Et aliquam consequatur corporis voluptatem sunt error. Dolore alias temporibus repellat dolores eligendi.', 'modi recusandae', '1991-09-30', 11, 1, NULL, NULL),
(69, 'Repudiandae sunt recusandae omnis et.', 'Architecto dolorum enim accusantium laboriosam ut. Exercitationem nobis qui quia molestiae non. Beatae aut laudantium odit consectetur.', 'quas', '2003-09-23', 7, 3, NULL, NULL),
(70, 'Et cupiditate provident ut est incidunt.', 'Ipsam aut cum magnam consequatur. Veritatis repellendus et architecto consequatur repudiandae sed id. Vitae hic perspiciatis est veniam. Maiores dolore architecto est expedita ut eveniet ad ipsam.', 'sint', '2000-12-07', 5, 1, NULL, NULL),
(71, 'Excepturi harum exercitationem.', 'Modi illo nobis consequuntur est aut atque. Quibusdam vel eum delectus sunt.', 'molestiae', '1997-08-23', 6, 3, NULL, NULL),
(72, 'Sint aut non architecto voluptatem.', 'Natus quidem tempora perspiciatis animi veniam. Molestias cupiditate quasi debitis voluptas. Corrupti vel excepturi repellat est quis.', 'sit molestiae', '1975-07-04', 5, 2, NULL, NULL),
(73, 'Eligendi sed blanditiis id atque.', 'Consectetur magnam veniam culpa modi quia voluptatem. Laborum eos est incidunt exercitationem. Cupiditate enim soluta aut in reprehenderit sint ipsam. Aut dolor sed autem occaecati vel.', 'doloribus quae', '1986-04-20', 9, 1, NULL, NULL),
(74, 'Repudiandae possimus corporis sunt laboriosam.', 'Temporibus ad illo exercitationem possimus vero fuga. Libero pariatur et illo consequatur suscipit. Deserunt ut perspiciatis et. Aliquid ipsum sint vel accusantium soluta nostrum. Non impedit veritatis in autem.', 'et soluta', '1996-07-21', 6, 2, NULL, NULL),
(75, 'Dolorem debitis qui animi.', 'Sapiente praesentium unde ab consequatur. Dolorum deserunt facere voluptates tempora sit. Neque fuga a placeat dolores nemo quibusdam. Et laboriosam quod et sit et.', 'ipsam omnis', '1972-03-10', 4, 4, NULL, NULL),
(76, 'Quam eum eius magni est.', 'Dolores cupiditate omnis rerum enim. Pariatur ab aut vero voluptas.', 'voluptatem', '1996-03-10', 11, 1, NULL, NULL),
(77, 'Est iusto explicabo numquam nulla.', 'Alias tempora eos ad est pariatur dolore molestiae. Vitae consequuntur et ad provident molestiae fugiat labore quos. Aspernatur alias quae repellat dolores. Sit error adipisci perferendis ipsa.', 'voluptatem explicabo', '1995-04-03', 5, 2, NULL, NULL),
(78, 'Aut omnis eius repellendus ea.', 'Totam illo mollitia vitae totam. Veniam earum illo exercitationem eum molestiae iste. Sunt natus qui ab eos commodi sed tempora qui. Tempora sunt nesciunt recusandae nemo blanditiis omnis.', 'consequatur facere', '2002-09-30', 8, 3, NULL, NULL),
(79, 'Rem sequi ipsum delectus odio illo.', 'Nisi quo omnis itaque odit voluptates omnis necessitatibus. Voluptatem et qui quia fuga ab cupiditate voluptatibus. Aliquid repellat ut nostrum. Consequatur quae occaecati provident.', 'in', '2011-04-14', 9, 3, NULL, NULL),
(80, 'Blanditiis rerum praesentium ipsa quia modi.', 'Aut minus animi temporibus ut. Et accusantium impedit voluptas ut in. Nam et et accusamus officiis. Consequuntur sit voluptatibus reiciendis quos qui. Temporibus eligendi eum at.', 'quam', '2002-07-13', 5, 1, NULL, NULL),
(81, 'Sit nihil unde voluptatem corporis.', 'Doloremque sint fuga et aut ut rerum natus. Soluta quas magnam qui voluptatem amet facere. Quae cupiditate quibusdam accusamus reiciendis.', 'nihil', '1995-05-02', 4, 1, NULL, NULL),
(82, 'Qui ut hic.', 'Mollitia qui fuga velit id labore. Adipisci eius mollitia voluptas. Aspernatur dolores sint corporis quod nostrum. Corporis aut incidunt praesentium reprehenderit inventore maxime rerum.', 'dolorem', '1982-02-01', 9, 4, NULL, NULL),
(83, 'Rerum quisquam corrupti repudiandae enim.', 'In praesentium delectus ab ea omnis dicta. Eum porro consequatur quod laudantium ut autem enim dolorem. Iure assumenda consequatur ea.', 'quis iusto', '1973-07-15', 2, 1, NULL, NULL),
(84, 'Beatae laudantium quia commodi.', 'Sit officiis consequatur blanditiis. Quisquam delectus qui maxime sunt et tenetur. Perferendis voluptatem porro quas aut pariatur deleniti tempore. Provident est quidem accusantium repellat possimus.', 'molestias error', '2021-08-30', 7, 2, NULL, NULL),
(85, 'Qui omnis odit molestiae earum magnam.', 'Numquam nesciunt non sint non architecto sapiente. Et quia aut provident.', 'omnis necessitatibus', '1989-06-14', 4, 1, NULL, NULL),
(86, 'Consequatur voluptas non voluptates.', 'Rerum hic assumenda quas molestiae illo non quia nesciunt. Sit ut qui dicta rerum odit rem. Dolore vitae delectus id.', 'tempora', '2020-12-06', 5, 1, NULL, NULL),
(87, 'Accusantium corporis fugit aspernatur cupiditate id.', 'Neque neque quia natus enim maxime. Tempora asperiores reprehenderit et. Facilis optio et delectus veniam labore. Fuga consectetur illo voluptatem numquam amet explicabo.', 'dolore laborum', '1971-07-13', 8, 1, NULL, NULL),
(88, 'Delectus dolores vero omnis.', 'Ut dolore qui reiciendis et. Ut ut autem delectus quia voluptatum. Odio voluptas vero facilis dolorem. Soluta excepturi iste quidem illo. Et omnis quos quisquam dolores sint.', 'sint', '2006-12-28', 10, 1, NULL, NULL),
(89, 'Animi exercitationem ad rerum.', 'Facere laboriosam labore aspernatur dolores non voluptatem. Ad aut vel earum sit. Porro quo odio eveniet sit voluptatem.', 'cupiditate', '2016-10-29', 4, 1, NULL, NULL),
(90, 'Eligendi reprehenderit quas ratione laborum.', 'Corrupti quos illo in ex. Id est vero nihil. Nulla explicabo enim non ratione.', 'maiores neque', '1988-10-08', 10, 3, NULL, NULL),
(91, 'Adipisci ut quidem non est.', 'Asperiores voluptas nam autem voluptas. Odit provident et assumenda deleniti earum. Nisi saepe aut excepturi sunt ratione fugit. Quia optio repellendus eaque qui.', 'soluta', '1997-03-08', 2, 1, NULL, NULL),
(92, 'Error voluptatem sit officia cupiditate.', 'Alias in ut non adipisci odio reiciendis. Delectus illo ipsam est non. Eligendi est necessitatibus corrupti. Quasi rerum voluptas fuga ratione.', 'velit vitae', '2018-07-07', 6, 4, NULL, NULL),
(93, 'Est dignissimos eos omnis.', 'Ratione voluptate molestiae dolor voluptatem itaque. Optio aut omnis doloribus est cupiditate. Eos est quo totam velit minima. Et aut eos molestiae.', 'aliquam', '2007-10-02', 9, 3, NULL, NULL),
(94, 'Placeat nostrum qui nisi et sequi.', 'Cum sit libero eum aspernatur consectetur. Sunt est recusandae cum sunt cumque optio. Ipsum vero minima officiis velit. Repellat voluptatum consequatur repellat dolor et.', 'impedit', '1989-01-02', 9, 1, NULL, NULL),
(95, 'Placeat aspernatur qui sunt ab et.', 'Sapiente odio praesentium sit doloribus. Sit perspiciatis dolores ut distinctio. Dolor repellat est voluptates voluptatibus. Et alias qui asperiores consequuntur sint corporis.', 'quibusdam nihil', '1987-12-24', 9, 2, NULL, NULL),
(96, 'Nemo omnis omnis voluptate ad.', 'Eum ea sint quia incidunt ea culpa et cupiditate. Officiis laudantium consectetur autem sed voluptatem. Perspiciatis et aut libero.', 'quia exercitationem', '1989-11-13', 10, 3, NULL, NULL),
(97, 'Reprehenderit iste nisi commodi unde.', 'Et tenetur expedita quasi sint et consectetur. Nam blanditiis laboriosam ex aut ea tempora et optio.', 'et tempore', '1998-09-19', 7, 3, NULL, NULL),
(98, 'Ducimus beatae quia.', 'Occaecati dicta voluptas ex provident. Aut incidunt qui quia nobis enim quasi. Aut modi blanditiis necessitatibus sint sunt quasi magnam.', 'voluptas enim', '2003-07-30', 4, 3, NULL, NULL),
(99, 'Alias autem ut ut quaerat quaerat.', 'Porro rerum corporis recusandae veritatis ut optio porro. Saepe quo ut et asperiores ipsa sed. Qui sapiente enim quis veniam ad ut aliquam. Qui dolorem ratione earum et sunt nihil nesciunt vel.', 'autem', '1980-06-11', 2, 1, NULL, NULL),
(100, 'Rerum earum harum nesciunt itaque.', 'Laborum architecto inventore cumque. Ipsam ipsa maxime sit omnis facilis. Commodi quae et repellat iure ut.', 'et', '1976-03-18', 2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'browse bread 1', 'web', '2022-11-08 07:08:27', '2022-11-08 07:08:27'),
(2, 'read bread 1', 'web', '2022-11-08 07:08:27', '2022-11-08 07:08:27'),
(3, 'edit bread 1', 'web', '2022-11-08 07:08:27', '2022-11-08 07:08:27'),
(4, 'add bread 1', 'web', '2022-11-08 07:08:27', '2022-11-08 07:08:27'),
(5, 'delete bread 1', 'web', '2022-11-08 07:08:27', '2022-11-08 07:08:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'web', '2022-11-08 07:08:26', '2022-11-08 07:08:26'),
(2, 'user', 'web', '2022-11-08 07:08:26', '2022-11-08 07:08:26'),
(3, 'guest', 'web', '2022-11-08 07:08:26', '2022-11-08 07:08:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 3),
(2, 3),
(3, 3),
(4, 3),
(5, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_hierarchy`
--

CREATE TABLE `role_hierarchy` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `hierarchy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `role_hierarchy`
--

INSERT INTO `role_hierarchy` (`id`, `role_id`, `hierarchy`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status`
--

CREATE TABLE `status` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `status`
--

INSERT INTO `status` (`id`, `name`, `class`) VALUES
(1, 'ongoing', 'badge badge-pill badge-primary'),
(2, 'stopped', 'badge badge-pill badge-secondary'),
(3, 'completed', 'badge badge-pill badge-success'),
(4, 'expired', 'badge badge-pill badge-warning');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menuroles` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `menuroles`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', 'admin@admin.com', '2022-11-08 07:08:26', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user,admin', 'v91Ua6PQpP', '2022-11-08 07:08:26', '2022-11-08 07:08:26', NULL),
(2, 'Hallie Pouros MD', 'katrine15@example.org', '2022-11-08 07:08:26', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user', 'ihHArojca6', '2022-11-08 07:08:26', '2022-11-08 07:08:26', NULL),
(3, 'Prof. Monty Pacocha', 'isai04@example.com', '2022-11-08 07:08:26', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user', 'xrT7BVQLNo', '2022-11-08 07:08:26', '2022-11-08 07:08:26', NULL),
(4, 'Dr. Wilfredo Lowe MD', 'oebert@example.net', '2022-11-08 07:08:26', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user', 'engePJC646', '2022-11-08 07:08:26', '2022-11-08 07:08:26', NULL),
(5, 'Liliana Fisher', 'freddie.gleichner@example.org', '2022-11-08 07:08:26', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user', 'erUwrmpwH3', '2022-11-08 07:08:26', '2022-11-08 07:08:26', NULL),
(6, 'Barry Reinger', 'kbednar@example.org', '2022-11-08 07:08:26', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user', 'K0fui0jtOq', '2022-11-08 07:08:26', '2022-11-08 07:08:26', NULL),
(7, 'Jan Abernathy', 'pauline46@example.org', '2022-11-08 07:08:26', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user', 'M9BRnGByOF', '2022-11-08 07:08:26', '2022-11-08 07:08:26', NULL),
(8, 'Garret Friesen', 'pstark@example.org', '2022-11-08 07:08:26', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user', '8DsQTSyLDR', '2022-11-08 07:08:26', '2022-11-08 07:08:26', NULL),
(9, 'Everett Ernser', 'jevon.williamson@example.org', '2022-11-08 07:08:26', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user', 'hl4vJ1lCzl', '2022-11-08 07:08:26', '2022-11-08 07:08:26', NULL),
(10, 'Vincenzo Stroman', 'btowne@example.com', '2022-11-08 07:08:26', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user', 'iLubJDPXPL', '2022-11-08 07:08:26', '2022-11-08 07:08:26', NULL),
(11, 'Hardy Deckow', 'prince47@example.org', '2022-11-08 07:08:26', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'user', 's9HEsvIFuf', '2022-11-08 07:08:26', '2022-11-08 07:08:26', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `email_template`
--
ALTER TABLE `email_template`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `example`
--
ALTER TABLE `example`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `folder`
--
ALTER TABLE `folder`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `form`
--
ALTER TABLE `form`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `form_field`
--
ALTER TABLE `form_field`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indices de la tabla `menulist`
--
ALTER TABLE `menulist`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menu_role`
--
ALTER TABLE `menu_role`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indices de la tabla `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indices de la tabla `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `role_hierarchy`
--
ALTER TABLE `role_hierarchy`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `email_template`
--
ALTER TABLE `email_template`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `example`
--
ALTER TABLE `example`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `folder`
--
ALTER TABLE `folder`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `form`
--
ALTER TABLE `form`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `form_field`
--
ALTER TABLE `form_field`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `media`
--
ALTER TABLE `media`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `menulist`
--
ALTER TABLE `menulist`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT de la tabla `menu_role`
--
ALTER TABLE `menu_role`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `notes`
--
ALTER TABLE `notes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `role_hierarchy`
--
ALTER TABLE `role_hierarchy`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `status`
--
ALTER TABLE `status`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
--
-- Base de datos: `dashboard2`
--
CREATE DATABASE IF NOT EXISTS `dashboard2` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci;
USE `dashboard2`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@galpha.co', '2022-11-08 07:18:57', '$2y$10$TX.OIRv1cNu6j5jO0mR8Iu.ZktNQ2BbI1IbXZ4QVcLiWlGGn2M2ZC', NULL, '2022-11-08 07:18:57', '2022-11-08 07:18:57');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Base de datos: `db_rrhh`
--
CREATE DATABASE IF NOT EXISTS `db_rrhh` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci;
USE `db_rrhh`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `campuses`
--

CREATE TABLE `campuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `whatsapp` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_apicustom`
--

CREATE TABLE `cms_apicustom` (
  `id` int(10) UNSIGNED NOT NULL,
  `permalink` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tabel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aksi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kolom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderby` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_query_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sql_where` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `method_type` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` longtext COLLATE utf8mb4_unicode_ci,
  `responses` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_apicustom`
--

INSERT INTO `cms_apicustom` (`id`, `permalink`, `tabel`, `aksi`, `kolom`, `orderby`, `sub_query_1`, `sql_where`, `nama`, `keterangan`, `parameter`, `created_at`, `updated_at`, `method_type`, `parameters`, `responses`) VALUES
(1, 'departamentos', 'tb_departamentos', 'list', NULL, NULL, NULL, NULL, 'Listing', NULL, NULL, NULL, NULL, 'get', 'a:4:{i:0;a:5:{s:4:\"name\";s:6:\"codigo\";s:4:\"type\";s:6:\"string\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:1;a:5:{s:4:\"name\";s:12:\"departamento\";s:4:\"type\";s:6:\"string\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:2;a:5:{s:4:\"name\";s:2:\"id\";s:4:\"type\";s:6:\"string\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:3;a:5:{s:4:\"name\";s:7:\"pais_id\";s:4:\"type\";s:7:\"integer\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}}', 'a:4:{i:0;a:4:{s:4:\"name\";s:6:\"codigo\";s:4:\"type\";s:3:\"int\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:1;a:4:{s:4:\"name\";s:12:\"departamento\";s:4:\"type\";s:6:\"string\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:2;a:4:{s:4:\"name\";s:2:\"id\";s:4:\"type\";s:6:\"bigint\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:3;a:4:{s:4:\"name\";s:7:\"pais_id\";s:4:\"type\";s:7:\"integer\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_apikey`
--

CREATE TABLE `cms_apikey` (
  `id` int(10) UNSIGNED NOT NULL,
  `screetkey` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hit` int(11) DEFAULT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_apikey`
--

INSERT INTO `cms_apikey` (`id`, `screetkey`, `hit`, `status`, `created_at`, `updated_at`) VALUES
(1, '0fc0740694e9efa1d4e4a796a8c54cd4', 0, 'active', '2022-10-26 07:42:15', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_dashboard`
--

CREATE TABLE `cms_dashboard` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_email_queues`
--

CREATE TABLE `cms_email_queues` (
  `id` int(10) UNSIGNED NOT NULL,
  `send_at` datetime DEFAULT NULL,
  `email_recipient` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_content` text COLLATE utf8mb4_unicode_ci,
  `email_attachments` text COLLATE utf8mb4_unicode_ci,
  `is_sent` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_email_templates`
--

CREATE TABLE `cms_email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_email_templates`
--

INSERT INTO `cms_email_templates` (`id`, `name`, `slug`, `subject`, `content`, `description`, `from_name`, `from_email`, `cc_email`, `created_at`, `updated_at`) VALUES
(1, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2022-10-25 22:18:42', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_logs`
--

CREATE TABLE `cms_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `ipaddress` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `useragent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `id_cms_users` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_logs`
--

INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `details`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(1, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de admin@crudbooster.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-25 22:22:18', NULL),
(2, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com se desconectó', '', 1, '2022-10-25 22:22:56', NULL),
(3, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de admin@crudbooster.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-25 22:22:59', NULL),
(4, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com se desconectó', '', 1, '2022-10-25 22:26:00', NULL),
(5, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de admin@crudbooster.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-25 22:32:18', NULL),
(6, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/module_generator/delete/13', 'Eliminar información Pais en Generador de Módulos', '', 1, '2022-10-25 22:34:42', NULL),
(7, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_pais/add-save', 'Añadir nueva información  en Pais', '', 1, '2022-10-25 22:34:54', NULL),
(8, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com se desconectó', '', 1, '2022-10-25 22:36:11', NULL),
(9, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de admin@crudbooster.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-25 22:36:12', NULL),
(10, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/users/edit-save/1', 'Actualizar información Hannsel Cordon en Gestión de usuarios', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Super Admin</td><td>Hannsel Cordon</td></tr><tr><td>photo</td><td></td><td>uploads/1/2022-10/logo_search_grid_1x_1.png</td></tr><tr><td>email</td><td>admin@crudbooster.com</td><td>hannselcordon@gmail.com</td></tr><tr><td>email_verified_at</td><td></td><td></td></tr><tr><td>password</td><td>$2y$10$wYD7H85eiAjGtrgNE0TFluWH7Fm7Td1hgmVAckatJDLdfKXb//aFS</td><td>$2y$10$9RiKnOUKHlq72KVYa7rKjOF24Q6pXwQhsgLoHrJzM9Yygd5U9LDtC</td></tr><tr><td>remember_token</td><td></td><td></td></tr><tr><td>status</td><td>Active</td><td></td></tr></tbody></table>', 1, '2022-10-25 22:37:00', NULL),
(11, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-25 22:37:22', NULL),
(12, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-25 22:37:24', NULL),
(13, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-25 22:37:31', NULL),
(14, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-25 22:37:43', NULL),
(15, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-25 22:38:22', NULL),
(16, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', ' se desconectó', '', NULL, '2022-10-25 22:44:54', NULL),
(17, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-25 23:09:32', NULL),
(18, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-25 23:09:37', NULL),
(19, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-25 23:10:58', NULL),
(20, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-25 23:11:02', NULL),
(21, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8003/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-26 00:16:44', NULL),
(22, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8003/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-26 00:17:19', NULL),
(23, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8003/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-26 00:19:14', NULL),
(24, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8003/admin/tb_departamentos/add-save', 'Añadir nueva información  en Departamentos', '', 1, '2022-10-26 00:49:24', NULL),
(25, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8003/admin/tb_departamentos/add-save', 'Añadir nueva información  en Departamentos', '', 1, '2022-10-26 00:49:24', NULL),
(26, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8003/admin/tb_departamentos/delete/1', 'Eliminar información 1 en Departamentos', '', 1, '2022-10-26 00:49:38', NULL),
(27, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8003/admin/tb_municipios/add-save', 'Añadir nueva información  en Municipios', '', 1, '2022-10-26 00:50:06', NULL),
(28, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8003/admin/tb_regiones/add-save', 'Añadir nueva información  en Regiones', '', 1, '2022-10-26 00:50:25', NULL),
(29, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8003/admin/menu_management/add-save', 'Añadir nueva información División Política en Gestión de Menús', '', 1, '2022-10-26 00:55:20', NULL),
(30, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8003/admin/tb_generos/add-save', 'Añadir nueva información  en Genero', '', 1, '2022-10-26 01:04:31', NULL),
(31, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8003/admin/tb_generos/add-save', 'Añadir nueva información  en Genero', '', 1, '2022-10-26 01:04:38', NULL),
(32, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8003/admin/tb_etnias/add-save', 'Añadir nueva información  en Etnias', '', 1, '2022-10-26 01:05:55', NULL),
(33, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8003/admin/tb_identification_documents/add-save', 'Añadir nueva información  en Documentos de Identificación', '', 1, '2022-10-26 01:11:45', NULL),
(34, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8003/admin/tb_marital_status/add-save', 'Añadir nueva información  en Estado Civil', '', 1, '2022-10-26 01:14:00', NULL),
(35, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8003/admin/tb_marital_status/add-save', 'Añadir nueva información  en Estado Civil', '', 1, '2022-10-26 01:14:40', NULL),
(36, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8003/admin/menu_management/add-save', 'Añadir nueva información Ajustes de Información en Gestión de Menús', '', 1, '2022-10-26 01:16:46', NULL),
(37, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8003/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-26 01:28:27', NULL),
(38, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8003/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-26 01:30:27', NULL),
(39, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8003/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-26 01:30:32', NULL),
(40, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-26 03:07:02', NULL),
(41, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-26 03:07:07', NULL),
(42, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-26 03:18:01', NULL),
(43, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-26 03:18:41', NULL),
(44, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-26 03:30:45', NULL),
(45, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-26 03:31:21', NULL),
(46, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-26 03:36:25', NULL),
(47, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/users/delete-image', 'Eliminar la imagen de Hannsel Cordon en Gestión de usuarios', '', 1, '2022-10-26 03:37:10', NULL),
(48, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/users/edit-save/1', 'Actualizar información Hannsel Cordon en Gestión de usuarios', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td></td><td>uploads/1/2022-10/talita.png</td></tr><tr><td>email_verified_at</td><td></td><td></td></tr><tr><td>password</td><td>$2y$10$lBC3fwQbLURtQCTUjcvuou0yNcIA3UuU58EmnmFxa1LOD6cImyKVS</td><td>$2y$10$C97D2J4WoK2uEe8gldIIAOtOKj9Ti4f9vsOLrSFdO/yT2vKXdYOT2</td></tr><tr><td>id_cms_privileges</td><td>1</td><td></td></tr><tr><td>remember_token</td><td>Q1UYEE5Ze3nwMaGxnBhWLLQJ5ncKHIHNJwD103KCvBUpk2txAQej8sdP9Nvi</td><td></td></tr><tr><td>status</td><td>Active</td><td></td></tr></tbody></table>', 1, '2022-10-26 03:37:22', NULL),
(49, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/users/edit-save/1', 'Actualizar información Hannsel Cordon en Gestión de usuarios', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email_verified_at</td><td></td><td></td></tr><tr><td>password</td><td>$2y$10$C97D2J4WoK2uEe8gldIIAOtOKj9Ti4f9vsOLrSFdO/yT2vKXdYOT2</td><td>$2y$10$sljp7hKDwhKxpya3YRWa/u8LTiu2RqS6.kMBO3lwU1/aTLRXqRkMq</td></tr><tr><td>id_cms_privileges</td><td>1</td><td></td></tr><tr><td>remember_token</td><td>Q1UYEE5Ze3nwMaGxnBhWLLQJ5ncKHIHNJwD103KCvBUpk2txAQej8sdP9Nvi</td><td></td></tr><tr><td>status</td><td>Active</td><td></td></tr></tbody></table>', 1, '2022-10-26 03:37:37', NULL),
(50, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-26 03:37:45', NULL),
(51, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-26 03:37:47', NULL),
(52, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-26 03:38:21', NULL),
(53, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-26 03:39:22', NULL),
(54, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-26 07:41:08', NULL),
(55, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-26 07:59:57', NULL),
(56, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-26 08:00:02', NULL),
(57, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-26 08:02:35', NULL),
(58, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-26 08:02:53', NULL),
(59, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-26 08:03:01', NULL),
(60, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-26 08:13:40', NULL),
(61, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-27 00:09:33', NULL),
(62, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_profeciones/add-save', 'Añadir nueva información  en Profesiones', '', 1, '2022-10-27 00:14:47', NULL),
(63, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_profeciones/add-save', 'Añadir nueva información  en Profesiones', '', 1, '2022-10-27 00:14:55', NULL),
(64, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_profeciones/add-save', 'Añadir nueva información  en Profesiones', '', 1, '2022-10-27 00:15:46', NULL),
(65, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_empleados/add-save', 'Añadir nueva información 30387632 en Empleados', '', 1, '2022-10-27 00:52:26', NULL),
(66, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_datos_medicos/add-save', 'Añadir nueva información  en Datos Médicos', '', 1, '2022-10-27 00:59:30', NULL),
(67, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_seguros_medicos/add-save', 'Añadir nueva información  en Otros Seguros', '', 1, '2022-10-27 01:08:29', NULL),
(68, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-27 01:51:55', NULL),
(69, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-27 04:00:30', NULL),
(70, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-27 04:00:37', NULL),
(71, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-27 04:02:21', NULL),
(72, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-27 04:02:36', NULL),
(73, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-27 05:54:56', NULL),
(74, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/module_generator/delete/26', 'Eliminar información Tipo de Contratos en Generador de Módulos', '', 1, '2022-10-27 00:15:40', NULL),
(75, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/module_generator/delete/22', 'Eliminar información Salarios en Generador de Módulos', '', 1, '2022-10-27 00:15:46', NULL),
(76, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_dias_laborados/add-save', 'Añadir nueva información  en Dias Laborables', '', 1, '2022-10-27 00:41:32', NULL),
(77, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_hrs_laboradas/add-save', 'Añadir nueva información  en Horas Laborables', '', 1, '2022-10-27 00:45:20', NULL),
(78, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_hrs_laboradas/add-save', 'Añadir nueva información  en Horas Laborables', '', 1, '2022-10-27 00:45:32', NULL),
(79, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Añadir nueva información Empleados en Gestión de Menús', '', 1, '2022-10-27 00:47:14', NULL),
(80, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_tipo_salarios/add-save', 'Añadir nueva información  en Tipos de Salario', '', 1, '2022-10-27 01:01:07', NULL),
(81, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_tipo_salarios/add-save', 'Añadir nueva información  en Tipos de Salario', '', 1, '2022-10-27 01:15:37', NULL),
(82, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_tipo_salarios/add-save', 'Añadir nueva información  en Tipos de Salario', '', 1, '2022-10-27 02:42:23', NULL),
(83, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_tipo_salarios/edit-save/2', 'Actualizar información  en Tipos de Salario', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>monto</td><td>2959</td><td>2959.00</td></tr></tbody></table>', 1, '2022-10-27 02:42:41', NULL),
(84, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_tipo_salarios/edit-save/3', 'Actualizar información  en Tipos de Salario', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>monto</td><td>2705</td><td>2705.00</td></tr><tr><td>bonificacion_incentiva</td><td>250</td><td>250.00</td></tr></tbody></table>', 1, '2022-10-27 02:57:27', NULL),
(85, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_tipo_salarios/edit-save/3', 'Actualizar información  en Tipos de Salario', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>monto</td><td>2705</td><td>2705.23</td></tr></tbody></table>', 1, '2022-10-27 02:57:41', NULL),
(86, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_tipo_salarios/edit-save/3', 'Actualizar información  en Tipos de Salario', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>monto</td><td>2705.23</td><td>2705.22</td></tr></tbody></table>', 1, '2022-10-27 03:01:50', NULL),
(87, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_tipo_salarios/edit-save/3', 'Actualizar información  en Tipos de Salario', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2022-10-27 03:02:24', NULL),
(88, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_tipo_salarios/edit-save/3', 'Actualizar información  en Tipos de Salario', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>bonificacion_incentiva</td><td>250</td><td>250.12</td></tr></tbody></table>', 1, '2022-10-27 03:04:11', NULL),
(89, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_tipo_salarios/edit-save/3', 'Actualizar información  en Tipos de Salario', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>monto</td><td>2705.22</td><td>2954.35</td></tr></tbody></table>', 1, '2022-10-27 03:05:05', NULL),
(90, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/module_generator/delete/30', 'Eliminar información Tipo de Contratos en Generador de Módulos', '', 1, '2022-10-27 03:34:34', NULL),
(91, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/module_generator/delete/31', 'Eliminar información Tipo de Contratos en Generador de Módulos', '', 1, '2022-10-27 03:40:08', NULL),
(92, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_tipo_contratos32/add-save', 'Añadir nueva información  en Tipo de Contratos', '', 1, '2022-10-27 03:56:45', NULL),
(93, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/module_generator/delete/33', 'Eliminar información Contratos en Generador de Módulos', '', 1, '2022-10-27 04:39:27', NULL),
(94, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_contratos34/add-save', 'Añadir nueva información  en Contrato', '', 1, '2022-10-27 04:52:00', NULL),
(95, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_contratos34/add-save', 'Añadir nueva información  en Contrato', '', 1, '2022-10-27 04:54:49', NULL),
(96, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_contratos34/delete-image', 'Eliminar la imagen de 2 en Contrato', '', 1, '2022-10-27 04:59:46', NULL),
(97, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_contratos34/edit-save/2', 'Actualizar información  en Contrato', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>documentos</td><td></td><td>uploads/1/2022-10/modelo_de_contrato.doc</td></tr></tbody></table>', 1, '2022-10-27 05:00:02', NULL),
(98, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-27 05:22:19', NULL),
(99, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-27 05:22:23', NULL),
(100, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_tipo_contratos32/edit-save/1', 'Actualizar información  en Tipo de Contratos', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>formato</td><td></td><td>uploads/1/2022-10/modelo_de_contrato.doc</td></tr></tbody></table>', 1, '2022-10-27 05:28:04', NULL),
(101, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_tipo_salarios/edit-save/3', 'Actualizar información  en Tipos de Salario', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>bonificacion_incentiva</td><td>250.12</td><td>250</td></tr></tbody></table>', 1, '2022-10-27 05:42:45', NULL),
(102, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_tipo_salarios/edit-save/2', 'Actualizar información  en Tipos de Salario', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>monto</td><td>2959</td><td>3209.24</td></tr></tbody></table>', 1, '2022-10-27 05:43:48', NULL),
(103, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_tipo_salarios/edit-save/1', 'Actualizar información  en Tipos de Salario', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>monto</td><td>3122</td><td>2954.35</td></tr></tbody></table>', 1, '2022-10-27 05:44:18', NULL),
(104, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_empleados/add-save', 'Añadir nueva información 1232 en Empleados', '', 1, '2022-10-27 06:00:06', NULL),
(105, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_tipo_salarios/edit-save/3', 'Actualizar información  en Tipos de Salario', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>estado</td><td>Activo</td><td>Inactivo</td></tr></tbody></table>', 1, '2022-10-27 06:08:02', NULL),
(106, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_tipo_salarios/edit-save/3', 'Actualizar información  en Tipos de Salario', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>estado</td><td>Inactivo</td><td>Activo</td></tr></tbody></table>', 1, '2022-10-27 06:13:34', NULL),
(107, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_empleados/add-save', 'Añadir nueva información 255 en Empleados', '', 1, '2022-10-27 06:24:26', NULL),
(108, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_empleados35/edit-save/3', 'Actualizar información  en Dar de Baja', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>fotografia</td><td>uploads/1/2022-10/logo_search_grid_1x_4.png</td><td></td></tr><tr><td>primer_nombre</td><td>Sonia</td><td></td></tr><tr><td>segundo_nombre</td><td>Maribel</td><td></td></tr><tr><td>primer_apellido</td><td>Yat</td><td></td></tr><tr><td>segundo_apellido</td><td>Ochoa</td><td></td></tr><tr><td>fecha_expiracion</td><td>2027-07-21</td><td></td></tr><tr><td>nit</td><td>12345678</td><td></td></tr><tr><td>genero_id</td><td>2</td><td></td></tr><tr><td>estado_civil_id</td><td>2</td><td></td></tr><tr><td>apellido_casadas</td><td>Cordón</td><td></td></tr><tr><td>telefono_personal</td><td>255</td><td></td></tr><tr><td>email</td><td>mar8a@gmail.com</td><td></td></tr><tr><td>seguro_social</td><td>323232</td><td></td></tr><tr><td>departamento_id</td><td>2</td><td></td></tr><tr><td>direccion</td><td>Barrio La Libertad</td><td></td></tr><tr><td>estado</td><td>Activo</td><td>Inactivo</td></tr></tbody></table>', 1, '2022-10-27 06:49:10', NULL),
(109, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_empleados35/edit-save/2', 'Actualizar información  en Dar de Baja', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>fecha_expiracion</td><td>2022-10-31</td><td></td></tr><tr><td>nit</td><td>12345678</td><td></td></tr><tr><td>estado_civil_id</td><td>2</td><td></td></tr><tr><td>apellido_casadas</td><td></td><td></td></tr><tr><td>telefono_casa</td><td>1233443</td><td></td></tr><tr><td>telefono_personal</td><td>1232</td><td></td></tr><tr><td>email</td><td>hannselcordon1@gmail.com</td><td></td></tr><tr><td>codigo_ocupacional</td><td>16001</td><td></td></tr><tr><td>profecion_id</td><td>3</td><td></td></tr><tr><td>colegiado_activo</td><td>122324</td><td></td></tr><tr><td>seguro_social</td><td>323232</td><td></td></tr><tr><td>departamento_id</td><td>2</td><td></td></tr><tr><td>direccion</td><td>Coban</td><td></td></tr><tr><td>estado</td><td>Activo</td><td>Inactivo</td></tr></tbody></table>', 1, '2022-10-27 06:54:56', NULL),
(110, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_empleados/edit-save/3', 'Actualizar información 30387632 en Empleados', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2022-10-27 07:01:56', NULL),
(111, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_empleados/edit-save/3', 'Actualizar información 30387632 en Empleados', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>primer_nombre</td><td>Eduardo</td><td>Sonia</td></tr><tr><td>segundo_nombre</td><td>Enrique</td><td>Maribel</td></tr><tr><td>primer_apellido</td><td>Garcia</td><td>Yat</td></tr><tr><td>segundo_apellido</td><td>Macz</td><td>Ochoa</td></tr></tbody></table>', 1, '2022-10-27 07:02:19', NULL),
(112, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-27 07:04:13', NULL),
(113, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-27 07:04:14', NULL),
(114, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-27 07:18:51', NULL),
(115, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-27 07:18:52', NULL),
(116, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-27 07:18:57', NULL),
(117, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-29 14:34:00', NULL),
(118, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-29 15:19:23', NULL),
(119, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-29 15:20:30', NULL),
(120, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-29 15:20:37', NULL),
(121, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-29 15:26:52', NULL),
(122, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-29 15:48:17', NULL),
(123, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-30 00:20:19', NULL),
(124, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-10-30 00:41:44', NULL),
(125, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', ' se desconectó', '', NULL, '2022-10-30 00:59:10', NULL),
(126, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-10-30 01:19:40', NULL),
(127, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-02 16:22:53', NULL),
(128, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-04 06:04:15', NULL),
(129, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-04 17:50:20', NULL),
(130, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-15 05:07:42', NULL),
(131, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/settings/add-save', 'Añadir nueva información image en Ajustes', '', 1, '2022-11-15 06:14:47', NULL),
(132, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/settings/add-save', 'Añadir nueva información login en Ajustes', '', 1, '2022-11-15 06:15:38', NULL),
(133, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/settings/edit-save/14', 'Actualizar información  en Ajustes', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>api_debug_mode</td><td></td></tr><tr><td>content</td><td>true</td><td></td></tr><tr><td>helper</td><td></td><td></td></tr></tbody></table>', 1, '2022-11-15 06:21:46', NULL),
(134, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-15 06:23:17', NULL),
(135, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-15 06:23:48', NULL),
(136, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-15 06:27:47', NULL),
(137, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-15 06:29:05', NULL),
(138, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-15 06:30:25', NULL),
(139, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-15 06:30:52', NULL),
(140, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-15 06:31:06', NULL),
(141, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-15 06:31:24', NULL),
(142, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-15 06:32:10', NULL),
(143, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-15 06:33:00', NULL),
(144, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-15 06:33:49', NULL),
(145, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-15 06:35:04', NULL),
(146, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-15 06:35:38', NULL),
(147, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-15 06:35:58', NULL),
(148, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/statistic_builder/add-save', 'Añadir nueva información Dashboard en Generador de Estadísticas', '', 1, '2022-11-15 06:37:47', NULL),
(149, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-15 14:18:34', NULL),
(150, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_contratos34/edit-save/2', 'Actualizar información  en Contrato', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>anio_antiguedad</td><td>233</td><td>1990</td></tr></tbody></table>', 1, '2022-11-15 14:34:21', NULL),
(151, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_contratos34/edit-save/1', 'Actualizar información  en Contrato', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>anio_antiguedad</td><td>222</td><td>1980</td></tr></tbody></table>', 1, '2022-11-15 14:34:34', NULL),
(152, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_empleados/delete-image', 'Eliminar la imagen de 1232 en Empleados', '', 1, '2022-11-15 16:04:20', NULL),
(153, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_empleados/edit-save/2', 'Actualizar información 34543456 en Empleados', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>segundo_nombre</td><td>Humberto</td><td>Crispín</td></tr><tr><td>primer_apellido</td><td>López</td><td>Aliñado</td></tr><tr><td>segundo_apellido</td><td>Valdez</td><td>Asturias</td></tr><tr><td>no_documento</td><td>2050968911602</td><td>2050928921601</td></tr><tr><td>telefono_casa</td><td>1233443</td><td></td></tr><tr><td>telefono_personal</td><td>1232</td><td>34543456</td></tr><tr><td>codigo_ocupacional</td><td>16001</td><td></td></tr><tr><td>colegiado_activo</td><td>122324</td><td></td></tr><tr><td>seguro_social</td><td>323232</td><td></td></tr></tbody></table>', 1, '2022-11-15 16:16:43', NULL),
(154, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_empleados/delete-image', 'Eliminar la imagen de 30387632 en Empleados', '', 1, '2022-11-15 16:19:16', NULL),
(155, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_empleados/edit-save/3', 'Actualizar información 30387632 en Empleados', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>seguro_social</td><td>323232</td><td></td></tr></tbody></table>', 1, '2022-11-15 16:19:32', NULL),
(156, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_empleados/edit-save/1', 'Actualizar información 30387632 en Empleados', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>primer_nombre</td><td>Hannsel</td><td>Hector</td></tr><tr><td>primer_apellido</td><td>Cordón</td><td>Molina</td></tr><tr><td>segundo_apellido</td><td>Ac</td><td>Mendez</td></tr><tr><td>fecha_nacimiento</td><td>1991-03-28</td><td>1989-11-22</td></tr><tr><td>apellido_casadas</td><td></td><td></td></tr></tbody></table>', 1, '2022-11-15 16:20:41', NULL),
(157, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_empleados/delete-image', 'Eliminar la imagen de 30387632 en Empleados', '', 1, '2022-11-15 16:20:49', NULL),
(158, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tb_empleados/edit-save/1', 'Actualizar información 30387632 en Empleados', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>fotografia</td><td></td><td>uploads/1/2022-11/m6.jpeg</td></tr><tr><td>apellido_casadas</td><td></td><td></td></tr></tbody></table>', 1, '2022-11-15 16:21:04', NULL);
INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `details`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(159, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://192.168.0.17/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.18', '', 1, '2022-11-16 01:07:39', NULL),
(160, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://192.168.0.17/admin/menu_management/edit-save/14', 'Actualizar información Empleados en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-users</td></tr><tr><td>parent_id</td><td>20</td><td></td></tr></tbody></table>', 1, '2022-11-16 01:08:05', NULL),
(161, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://192.168.0.17/admin/menu_management/edit-save/15', 'Actualizar información Datos Médicos en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-stethoscope</td></tr><tr><td>parent_id</td><td>20</td><td></td></tr><tr><td>sorting</td><td>2</td><td></td></tr></tbody></table>', 1, '2022-11-16 01:08:28', NULL),
(162, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://192.168.0.17/admin/menu_management/edit-save/16', 'Actualizar información Otros Seguros en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-university</td></tr><tr><td>parent_id</td><td>20</td><td></td></tr><tr><td>sorting</td><td>3</td><td></td></tr></tbody></table>', 1, '2022-11-16 01:09:14', NULL),
(163, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://192.168.0.17/admin/menu_management/edit-save/12', 'Actualizar información Profesiones en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-graduation-cap</td></tr><tr><td>parent_id</td><td>20</td><td></td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2022-11-16 01:09:31', NULL),
(164, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://192.168.0.17/admin/menu_management/edit-save/9', 'Actualizar información Documentos de Identificación en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-file-text</td></tr><tr><td>parent_id</td><td>11</td><td></td></tr></tbody></table>', 1, '2022-11-16 01:10:15', NULL),
(165, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://192.168.0.17/admin/menu_management/edit-save/10', 'Actualizar información Estado Civil en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-binoculars</td></tr><tr><td>parent_id</td><td>11</td><td></td></tr><tr><td>sorting</td><td>2</td><td></td></tr></tbody></table>', 1, '2022-11-16 01:10:51', NULL),
(166, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://192.168.0.17/admin/menu_management/edit-save/7', 'Actualizar información Genero en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-transgender</td></tr><tr><td>parent_id</td><td>11</td><td></td></tr><tr><td>sorting</td><td>3</td><td></td></tr></tbody></table>', 1, '2022-11-16 01:11:12', NULL),
(167, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://192.168.0.17/admin/menu_management/edit-save/8', 'Actualizar información Etnias en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-users</td></tr><tr><td>parent_id</td><td>11</td><td></td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2022-11-16 01:11:28', NULL),
(168, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://192.168.0.17/admin/menu_management/edit-save/14', 'Actualizar información Empleados en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>icon</td><td>fa fa-users</td><td>fa fa-user-plus</td></tr><tr><td>parent_id</td><td>20</td><td></td></tr></tbody></table>', 1, '2022-11-16 01:11:40', NULL),
(169, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://192.168.0.17/admin/menu_management/edit-save/19', 'Actualizar información Horas Laborables en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-clock-o</td></tr><tr><td>parent_id</td><td>11</td><td></td></tr><tr><td>sorting</td><td>5</td><td></td></tr></tbody></table>', 1, '2022-11-16 01:12:13', NULL),
(170, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://192.168.0.17/admin/menu_management/edit-save/18', 'Actualizar información Dias Laborables en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-barcode</td></tr><tr><td>parent_id</td><td>11</td><td></td></tr><tr><td>sorting</td><td>6</td><td></td></tr></tbody></table>', 1, '2022-11-16 01:13:10', NULL),
(171, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://192.168.0.17/admin/menu_management/edit-save/1', 'Actualizar información Pais en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-map-marker</td></tr><tr><td>parent_id</td><td>6</td><td></td></tr></tbody></table>', 1, '2022-11-16 01:13:24', NULL),
(172, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://192.168.0.17/admin/menu_management/edit-save/3', 'Actualizar información Departamentos en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-map-marker</td></tr><tr><td>parent_id</td><td>6</td><td></td></tr><tr><td>sorting</td><td>2</td><td></td></tr></tbody></table>', 1, '2022-11-16 01:13:37', NULL),
(173, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://192.168.0.17/admin/menu_management/edit-save/4', 'Actualizar información Municipios en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-map-marker</td></tr><tr><td>parent_id</td><td>6</td><td></td></tr><tr><td>sorting</td><td>3</td><td></td></tr></tbody></table>', 1, '2022-11-16 01:13:52', NULL),
(174, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://192.168.0.17/admin/menu_management/edit-save/5', 'Actualizar información Regiones en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-map-marker</td></tr><tr><td>parent_id</td><td>6</td><td></td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2022-11-16 01:14:06', NULL),
(175, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://192.168.0.17/admin/menu_management/edit-save/21', 'Actualizar información Tipos de Salario en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-navicon</td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2022-11-16 01:14:55', NULL),
(176, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://192.168.0.17/admin/menu_management/edit-save/24', 'Actualizar información Tipo de Contratos en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-legal</td></tr><tr><td>sorting</td><td>5</td><td></td></tr></tbody></table>', 1, '2022-11-16 01:15:24', NULL),
(177, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://192.168.0.17/admin/menu_management/edit-save/26', 'Actualizar información Contrato en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-file-text</td></tr><tr><td>sorting</td><td>6</td><td></td></tr></tbody></table>', 1, '2022-11-16 01:15:43', NULL),
(178, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://192.168.0.17/admin/menu_management/edit-save/27', 'Actualizar información Empleados de Bajas en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-user-times</td></tr><tr><td>sorting</td><td>7</td><td></td></tr></tbody></table>', 1, '2022-11-16 01:19:11', NULL),
(179, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://192.168.0.17/admin/menu_management/edit-save/28', 'Actualizar información Información Académica en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-graduation-cap</td></tr><tr><td>sorting</td><td>8</td><td></td></tr></tbody></table>', 1, '2022-11-16 01:19:29', NULL),
(180, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://192.168.0.17/admin/menu_management/edit-save/20', 'Actualizar información Empleados en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>icon</td><td>fa fa-users</td><td>fa fa-user-plus</td></tr></tbody></table>', 1, '2022-11-16 01:20:13', NULL),
(181, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://192.168.0.17/admin/menu_management/add-save', 'Añadir nueva información Dashboard en Gestión de Menús', '', 1, '2022-11-16 01:21:21', NULL),
(182, '192.168.0.17', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.17', '', 1, '2022-11-16 01:22:47', NULL),
(183, '192.168.0.17', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/statistic_builder/add-save', 'Añadir nueva información Cumpleañero del Mes en Generador de Estadísticas', '', 1, '2022-11-16 01:27:50', NULL),
(184, '192.168.0.17', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/menu_management/add-save', 'Añadir nueva información Cumpleañero del Mes en Gestión de Menús', '', 1, '2022-11-16 01:39:52', NULL),
(185, '192.168.0.17', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/menu_management/edit-save/30', 'Actualizar información Cumpleañero del Mes en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>parent_id</td><td>0</td><td></td></tr><tr><td>sorting</td><td></td><td></td></tr></tbody></table>', 1, '2022-11-16 01:40:46', NULL),
(186, '192.168.0.17', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/menu_management/edit-save/30', 'Actualizar información Cumpleañero del Mes en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>parent_id</td><td>0</td><td></td></tr></tbody></table>', 1, '2022-11-16 01:41:39', NULL),
(187, '192.168.0.17', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-16 01:43:29', NULL),
(188, '192.168.0.17', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.17', '', 1, '2022-11-16 01:43:32', NULL),
(189, '192.168.0.17', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/menu_management/edit-save/29', 'Actualizar información Dashboard2 en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Dashboard</td><td>Dashboard2</td></tr><tr><td>parent_id</td><td>20</td><td></td></tr></tbody></table>', 1, '2022-11-16 01:44:00', NULL),
(190, '192.168.0.17', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/menu_management/edit-save/29', 'Actualizar información Dashboard en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Dashboard2</td><td>Dashboard</td></tr><tr><td>parent_id</td><td>0</td><td></td></tr></tbody></table>', 1, '2022-11-16 01:44:55', NULL),
(191, '192.168.0.17', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/menu_management/edit-save/30', 'Actualizar información Cumpleañero del Mes en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>parent_id</td><td>0</td><td></td></tr><tr><td>sorting</td><td>2</td><td></td></tr></tbody></table>', 1, '2022-11-16 01:45:04', NULL),
(192, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.18', '', 1, '2022-11-16 01:55:18', NULL),
(193, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-16 01:55:25', NULL),
(194, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.18', '', 1, '2022-11-16 01:56:17', NULL),
(195, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-16 02:03:51', NULL),
(196, '192.168.0.17', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/users/edit-save/1', 'Actualizar información Hannsel Cordon en Gestión de usuarios', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>email_verified_at</td><td></td><td></td></tr><tr><td>password</td><td>$2y$10$YFjHmlJyMS8X9jP46ySzLuWZACa6YN2mNnWjAFfQ1CIiUS0uCsrP.</td><td>$2y$10$p.XwiQ3bUvqNJomwlJV.3eO/U0kh3/PbToPS4Grpoj43MIg47Rrdq</td></tr><tr><td>remember_token</td><td>0zzCpiYtYJSNH9I0ZLIc1mMnPspG0z6LqR55DX1STl88sD9NONtMTNSpcfZD</td><td></td></tr><tr><td>status</td><td>Active</td><td></td></tr></tbody></table>', 1, '2022-11-16 02:12:16', NULL),
(197, '192.168.0.17', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-16 02:12:22', NULL),
(198, '192.168.0.17', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.17', '', 1, '2022-11-16 02:12:25', NULL),
(199, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.18', '', 1, '2022-11-16 02:12:39', NULL),
(200, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/tb_identification_documents/add-save', 'Añadir nueva información  en Documentos de Identificación', '', 1, '2022-11-16 02:18:24', NULL),
(201, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/tb_etnias/add-save', 'Añadir nueva información  en Etnias', '', 1, '2022-11-16 02:21:00', NULL),
(202, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/tb_tipo_contratos32/add-save', 'Añadir nueva información  en Tipo de Contratos', '', 1, '2022-11-16 02:52:10', NULL),
(203, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/tb_empleados35/edit-save/3', 'Actualizar información  en Bajas', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>fecha_expiracion</td><td>2027-07-21</td><td></td></tr><tr><td>nit</td><td>12345678</td><td></td></tr><tr><td>genero_id</td><td>2</td><td></td></tr><tr><td>estado_civil_id</td><td>2</td><td></td></tr><tr><td>apellido_casadas</td><td>Cordón</td><td></td></tr><tr><td>telefono_casa</td><td></td><td></td></tr><tr><td>telefono_personal</td><td>30387632</td><td></td></tr><tr><td>email</td><td>mar8a@gmail.com</td><td></td></tr><tr><td>codigo_ocupacional</td><td></td><td></td></tr><tr><td>profecion_id</td><td>3</td><td></td></tr><tr><td>colegiado_activo</td><td></td><td></td></tr><tr><td>seguro_social</td><td>323232</td><td></td></tr><tr><td>departamento_id</td><td>2</td><td></td></tr><tr><td>direccion</td><td>Barrio La Libertad</td><td></td></tr><tr><td>estado</td><td>Inactivo</td><td>Activo</td></tr></tbody></table>', 1, '2022-11-16 03:03:44', NULL),
(204, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/users/add-save', 'Añadir nueva información RRHH en Gestión de usuarios', '', 1, '2022-11-16 03:13:35', NULL),
(205, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-16 03:14:34', NULL),
(206, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.18', '', 1, '2022-11-16 03:14:55', NULL),
(207, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-16 03:15:21', NULL),
(208, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/login', 'Ingreso de recursos@recursos.com desde la Dirección IP 192.168.0.18', '', 2, '2022-11-16 03:15:30', NULL),
(209, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/logout', 'recursos@recursos.com se desconectó', '', 2, '2022-11-16 03:15:54', NULL),
(210, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.18', '', 1, '2022-11-16 03:15:57', NULL),
(211, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/menu_management/edit-save/20', 'Actualizar información Empleados en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>2</td><td></td></tr></tbody></table>', 1, '2022-11-16 03:16:26', NULL),
(212, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/menu_management/edit-save/14', 'Actualizar información Empleados en Gestión de Menús', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>parent_id</td><td>20</td><td></td></tr></tbody></table>', 1, '2022-11-16 03:16:38', NULL),
(213, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-16 03:16:44', NULL),
(214, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/login', 'Ingreso de recursos@recursos.com desde la Dirección IP 192.168.0.18', '', 2, '2022-11-16 03:16:47', NULL),
(215, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/logout', 'recursos@recursos.com se desconectó', '', 2, '2022-11-16 03:17:00', NULL),
(216, '192.168.0.18', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.18', '', 1, '2022-11-16 03:17:05', NULL),
(217, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-16 23:04:25', NULL),
(218, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-16 23:09:03', NULL),
(219, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-16 23:37:36', NULL),
(220, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-16 23:37:43', NULL),
(221, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-16 23:41:20', NULL),
(222, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-16 23:51:44', NULL),
(223, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-16 23:58:58', NULL),
(224, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-17 00:20:50', NULL),
(225, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-17 00:38:44', NULL),
(226, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-17 00:38:50', NULL),
(227, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-17 00:54:12', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_menus`
--

CREATE TABLE `cms_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'url',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_dashboard` tinyint(1) NOT NULL DEFAULT '0',
  `id_cms_privileges` int(11) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_menus`
--

INSERT INTO `cms_menus` (`id`, `name`, `type`, `path`, `color`, `icon`, `parent_id`, `is_active`, `is_dashboard`, `id_cms_privileges`, `sorting`, `created_at`, `updated_at`) VALUES
(1, 'Pais', 'Route', 'AdminTbPais1ControllerGetIndex', 'normal', 'fa fa-map-marker', 6, 1, 0, 1, 1, '2022-10-25 22:33:09', '2022-11-16 01:13:24'),
(3, 'Departamentos', 'Route', 'AdminTbDepartamentos1ControllerGetIndex', 'normal', 'fa fa-map-marker', 6, 1, 0, 1, 2, '2022-10-26 00:27:20', '2022-11-16 01:13:37'),
(4, 'Municipios', 'Route', 'AdminTbMunicipios1ControllerGetIndex', 'normal', 'fa fa-map-marker', 6, 1, 0, 1, 3, '2022-10-26 00:38:21', '2022-11-16 01:13:52'),
(5, 'Regiones', 'Route', 'AdminTbRegiones1ControllerGetIndex', 'normal', 'fa fa-map-marker', 6, 1, 0, 1, 4, '2022-10-26 00:48:00', '2022-11-16 01:14:06'),
(6, 'División Política', 'URL', '#', 'normal', 'fa fa-map-marker', 0, 1, 0, 1, 4, '2022-10-26 00:55:20', NULL),
(7, 'Genero', 'Route', 'AdminTbGeneros1ControllerGetIndex', 'normal', 'fa fa-transgender', 11, 1, 0, 1, 3, '2022-10-26 01:03:24', '2022-11-16 01:11:12'),
(8, 'Etnias', 'Route', 'AdminTbEtnias1ControllerGetIndex', 'normal', 'fa fa-users', 11, 1, 0, 1, 4, '2022-10-26 01:05:08', '2022-11-16 01:11:28'),
(9, 'Documentos de Identificación', 'Route', 'AdminTbIdentificationDocuments1ControllerGetIndex', 'normal', 'fa fa-file-text', 11, 1, 0, 1, 1, '2022-10-26 01:07:37', '2022-11-16 01:10:15'),
(10, 'Estado Civil', 'Route', 'AdminTbMaritalStatus1ControllerGetIndex', 'normal', 'fa fa-binoculars', 11, 1, 0, 1, 2, '2022-10-26 01:12:25', '2022-11-16 01:10:51'),
(11, 'Ajustes de Información', 'Route', '#', 'normal', 'fa fa-info-circle', 0, 1, 0, 1, 3, '2022-10-26 01:16:46', NULL),
(12, 'Profesiones', 'Route', 'AdminTbProfeciones1ControllerGetIndex', 'normal', 'fa fa-graduation-cap', 11, 1, 0, 1, 5, '2022-10-27 00:11:01', '2022-11-16 01:09:31'),
(14, 'Empleados', 'Route', 'AdminTbEmpleados1ControllerGetIndex', 'normal', 'fa fa-user-plus', 20, 1, 0, 1, 1, '2022-10-27 00:27:03', '2022-11-16 03:16:38'),
(15, 'Datos Médicos', 'Route', 'AdminTbDatosMedicosControllerGetIndex', 'normal', 'fa fa-stethoscope', 20, 1, 0, 1, 3, '2022-10-27 00:43:09', '2022-11-16 01:08:28'),
(16, 'Otros Seguros', 'Route', 'AdminTbSegurosMedicos1ControllerGetIndex', 'normal', 'fa fa-university', 20, 1, 0, 1, 4, '2022-10-27 01:03:43', '2022-11-16 01:09:14'),
(18, 'Dias Laborables', 'Route', 'AdminTbDiasLaboradosControllerGetIndex', 'normal', 'fa fa-barcode', 11, 1, 0, 1, 7, '2022-10-27 00:39:41', '2022-11-16 01:13:10'),
(19, 'Horas Laborables', 'Route', 'AdminTbHrsLaboradasControllerGetIndex', 'normal', 'fa fa-clock-o', 11, 1, 0, 1, 6, '2022-10-27 00:42:42', '2022-11-16 01:12:13'),
(20, 'Empleados', 'URL', '#', 'normal', 'fa fa-user-plus', 0, 1, 0, 1, 2, '2022-10-27 00:47:14', '2022-11-16 03:16:26'),
(21, 'Tipos de Salario', 'Route', 'AdminTbTipoSalariosControllerGetIndex', 'normal', 'fa fa-navicon', 0, 1, 0, 1, 5, '2022-10-27 00:56:13', '2022-11-16 01:14:55'),
(24, 'Tipo de Contratos', 'Route', 'AdminTbTipoContratos32ControllerGetIndex', 'normal', 'fa fa-legal', 0, 1, 0, 1, 6, '2022-10-27 03:48:24', '2022-11-16 01:15:24'),
(26, 'Contrato', 'Route', 'AdminTbContratos34ControllerGetIndex', 'normal', 'fa fa-file-text', 0, 1, 0, 1, 7, '2022-10-27 04:40:31', '2022-11-16 01:15:43'),
(27, 'Empleados de Bajas', 'Route', 'AdminTbEmpleados35ControllerGetIndex', 'normal', 'fa fa-user-times', 0, 1, 0, 1, 8, '2022-10-27 06:26:57', '2022-11-16 01:19:11'),
(28, 'Información Académica', 'Route', 'AdminTbInformaciónAcademicas1ControllerGetIndex', 'normal', 'fa fa-graduation-cap', 20, 1, 0, 1, 2, '2022-11-04 17:51:09', '2022-11-16 01:19:29'),
(29, 'Dashboard', 'Statistic', 'statistic_builder/show/dashboard', 'normal', 'fa fa-dashboard', 0, 1, 1, 1, 1, '2022-11-16 01:21:21', '2022-11-16 01:44:55'),
(30, 'Cumpleañero del Mes', 'Statistic', 'statistic_builder/show/cumpleanero-del-mes', 'normal', 'fa fa-birthday-cake', 29, 1, 1, 1, 1, '2022-11-16 01:39:52', '2022-11-16 01:45:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_menus_privileges`
--

CREATE TABLE `cms_menus_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_menus` int(11) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_menus_privileges`
--

INSERT INTO `cms_menus_privileges` (`id`, `id_cms_menus`, `id_cms_privileges`) VALUES
(2, 2, 1),
(6, 6, 1),
(11, 11, 1),
(13, 13, 1),
(17, 17, 1),
(22, 22, 1),
(23, 23, 1),
(25, 25, 1),
(30, 15, 1),
(31, 16, 1),
(32, 12, 1),
(33, 9, 1),
(34, 10, 1),
(35, 7, 1),
(36, 8, 1),
(38, 19, 1),
(39, 18, 1),
(40, 1, 1),
(41, 3, 1),
(42, 4, 1),
(43, 5, 1),
(44, 21, 1),
(45, 24, 1),
(46, 26, 1),
(47, 27, 1),
(48, 28, 1),
(55, 29, 1),
(56, 30, 1),
(57, 20, 2),
(58, 20, 1),
(59, 14, 2),
(60, 14, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_moduls`
--

CREATE TABLE `cms_moduls` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_moduls`
--

INSERT INTO `cms_moduls` (`id`, `name`, `icon`, `path`, `table_name`, `controller`, `is_protected`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Notificaciones', 'fa fa-cog', 'notifications', 'cms_notifications', 'NotificationsController', 1, 1, '2022-10-25 22:18:42', NULL, NULL),
(2, 'Privilegios', 'fa fa-cog', 'privileges', 'cms_privileges', 'PrivilegesController', 1, 1, '2022-10-25 22:18:42', NULL, NULL),
(3, 'Privilegios & Roles', 'fa fa-cog', 'privileges_roles', 'cms_privileges_roles', 'PrivilegesRolesController', 1, 1, '2022-10-25 22:18:42', NULL, NULL),
(4, 'Gestión de usuarios', 'fa fa-users', 'users', 'cms_users', 'AdminCmsUsersController', 0, 1, '2022-10-25 22:18:42', NULL, NULL),
(5, 'Ajustes', 'fa fa-cog', 'settings', 'cms_settings', 'SettingsController', 1, 1, '2022-10-25 22:18:42', NULL, NULL),
(6, 'Generador de Módulos', 'fa fa-database', 'module_generator', 'cms_moduls', 'ModulsController', 1, 1, '2022-10-25 22:18:42', NULL, NULL),
(7, 'Gestión de Menús', 'fa fa-bars', 'menu_management', 'cms_menus', 'MenusController', 1, 1, '2022-10-25 22:18:42', NULL, NULL),
(8, 'Plantillas de Correo', 'fa fa-envelope-o', 'email_templates', 'cms_email_templates', 'EmailTemplatesController', 1, 1, '2022-10-25 22:18:42', NULL, NULL),
(9, 'Generador de Estadísticas', 'fa fa-dashboard', 'statistic_builder', 'cms_statistics', 'StatisticBuilderController', 1, 1, '2022-10-25 22:18:42', NULL, NULL),
(10, 'Generador de API', 'fa fa-cloud-download', 'api_generator', '', 'ApiCustomController', 1, 1, '2022-10-25 22:18:42', NULL, NULL),
(11, 'Log de Accesos (Usuarios)', 'fa fa-flag-o', 'logs', 'cms_logs', 'LogsController', 1, 1, '2022-10-25 22:18:42', NULL, NULL),
(12, 'Pais', 'fa fa-glass', 'tb_pais', 'tb_pais', 'AdminTbPais1Controller', 0, 0, '2022-10-25 22:33:09', NULL, NULL),
(13, 'Pais', 'fa fa-glass', 'tb_pais13', 'tb_pais', 'AdminTbPais13Controller', 0, 0, '2022-10-25 22:33:51', NULL, '2022-10-25 22:34:42'),
(14, 'Departamentos', 'fa fa-glass', 'tb_departamentos', 'tb_departamentos', 'AdminTbDepartamentos1Controller', 0, 0, '2022-10-26 00:27:20', NULL, NULL),
(15, 'Municipios', 'fa fa-glass', 'tb_municipios', 'tb_municipios', 'AdminTbMunicipios1Controller', 0, 0, '2022-10-26 00:38:21', NULL, NULL),
(16, 'Regiones', 'fa fa-glass', 'tb_regiones', 'tb_regiones', 'AdminTbRegiones1Controller', 0, 0, '2022-10-26 00:47:59', NULL, NULL),
(17, 'Genero', 'fa fa-glass', 'tb_generos', 'tb_generos', 'AdminTbGeneros1Controller', 0, 0, '2022-10-26 01:03:24', NULL, NULL),
(18, 'Etnias', 'fa fa-glass', 'tb_etnias', 'tb_etnias', 'AdminTbEtnias1Controller', 0, 0, '2022-10-26 01:05:08', NULL, NULL),
(19, 'Documentos de Identificación', 'fa fa-glass', 'tb_identification_documents', 'tb_identification_documents', 'AdminTbIdentificationDocuments1Controller', 0, 0, '2022-10-26 01:07:37', NULL, NULL),
(20, 'Estado Civil', 'fa fa-glass', 'tb_marital_status', 'tb_marital_status', 'AdminTbMaritalStatus1Controller', 0, 0, '2022-10-26 01:12:25', NULL, NULL),
(21, 'Profesiones', 'fa fa-glass', 'tb_profeciones', 'tb_profeciones', 'AdminTbProfeciones1Controller', 0, 0, '2022-10-27 00:11:01', NULL, NULL),
(22, 'Salarios', 'fa fa-glass', 'tb_salarios', 'tb_salarios', 'AdminTbSalarios1Controller', 0, 0, '2022-10-27 00:17:56', NULL, '2022-10-27 00:15:46'),
(23, 'Empleados', 'fa fa-glass', 'tb_empleados', 'tb_empleados', 'AdminTbEmpleados1Controller', 0, 0, '2022-10-27 00:27:03', NULL, NULL),
(24, 'Datos Médicos', 'fa fa-glass', 'tb_datos_medicos', 'tb_datos_medicos', 'AdminTbDatosMedicosController', 0, 0, '2022-10-27 00:43:09', NULL, NULL),
(25, 'Otros Seguros', 'fa fa-glass', 'tb_seguros_medicos', 'tb_seguros_medicos', 'AdminTbSegurosMedicos1Controller', 0, 0, '2022-10-27 01:03:42', NULL, NULL),
(26, 'Tipo de Contratos', 'fa fa-glass', 'tb_tipo_contratos', 'tb_tipo_contratos', 'AdminTbTipoContratos1Controller', 0, 0, '2022-10-27 01:17:04', NULL, '2022-10-27 00:15:40'),
(27, 'Dias Laborables', 'fa fa-glass', 'tb_dias_laborados', 'tb_dias_laborados', 'AdminTbDiasLaboradosController', 0, 0, '2022-10-27 00:39:41', NULL, NULL),
(28, 'Horas Laborables', 'fa fa-glass', 'tb_hrs_laboradas', 'tb_hrs_laboradas', 'AdminTbHrsLaboradasController', 0, 0, '2022-10-27 00:42:42', NULL, NULL),
(29, 'Tipos de Salario', 'fa fa-glass', 'tb_tipo_salarios', 'tb_tipo_salarios', 'AdminTbTipoSalariosController', 0, 0, '2022-10-27 00:56:13', NULL, NULL),
(30, 'Tipo de Contratos', 'fa fa-glass', 'tb_tipo_contratos30', 'tb_tipo_contratos', 'AdminTbTipoContratos30Controller', 0, 0, '2022-10-27 03:21:19', NULL, '2022-10-27 03:34:34'),
(31, 'Tipo de Contratos', 'fa fa-glass', 'tb_tipo_contratos31', 'tb_tipo_contratos', 'AdminTbTipoContratos31Controller', 0, 0, '2022-10-27 03:38:20', NULL, '2022-10-27 03:40:08'),
(32, 'Tipo de Contratos', 'fa fa-glass', 'tb_tipo_contratos32', 'tb_tipo_contratos', 'AdminTbTipoContratos32Controller', 0, 0, '2022-10-27 03:48:24', NULL, NULL),
(33, 'Contratos', 'fa fa-glass', 'tb_contratos', 'tb_contratos', 'AdminTbContratosController', 0, 0, '2022-10-27 04:05:33', NULL, '2022-10-27 04:39:27'),
(34, 'Contrato', 'fa fa-glass', 'tb_contratos34', 'tb_contratos', 'AdminTbContratos34Controller', 0, 0, '2022-10-27 04:40:31', NULL, NULL),
(35, 'Bajas', 'fa fa-glass', 'tb_empleados35', 'tb_empleados', 'AdminTbEmpleados35Controller', 0, 0, '2022-10-27 06:26:57', NULL, NULL),
(36, 'Información Académica', 'fa fa-glass', 'tb_información_academicas', 'tb_información_academicas', 'AdminTbInformaciónAcademicas1Controller', 0, 0, '2022-11-04 17:51:09', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_notifications`
--

CREATE TABLE `cms_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_users` int(11) DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_privileges`
--

CREATE TABLE `cms_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_superadmin` tinyint(1) DEFAULT NULL,
  `theme_color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_privileges`
--

INSERT INTO `cms_privileges` (`id`, `name`, `is_superadmin`, `theme_color`, `created_at`, `updated_at`) VALUES
(1, 'Super Administrator', 1, 'skin-blue-light', '2022-10-25 22:18:42', NULL),
(2, 'RRHH', 0, 'skin-green', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_privileges_roles`
--

CREATE TABLE `cms_privileges_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_visible` tinyint(1) DEFAULT NULL,
  `is_create` tinyint(1) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `is_edit` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `id_cms_moduls` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_privileges_roles`
--

INSERT INTO `cms_privileges_roles` (`id`, `is_visible`, `is_create`, `is_read`, `is_edit`, `is_delete`, `id_cms_privileges`, `id_cms_moduls`, `created_at`, `updated_at`) VALUES
(36, 1, 1, 1, 1, 1, 1, 35, NULL, NULL),
(37, 1, 1, 1, 1, 1, 1, 34, NULL, NULL),
(38, 1, 1, 1, 1, 1, 1, 24, NULL, NULL),
(39, 1, 1, 1, 1, 1, 1, 14, NULL, NULL),
(40, 1, 1, 1, 1, 1, 1, 27, NULL, NULL),
(41, 1, 1, 1, 1, 1, 1, 19, NULL, NULL),
(42, 1, 1, 1, 1, 1, 1, 23, NULL, NULL),
(43, 1, 1, 1, 1, 1, 1, 20, NULL, NULL),
(44, 1, 1, 1, 1, 1, 1, 18, NULL, NULL),
(45, 1, 1, 1, 1, 1, 1, 17, NULL, NULL),
(46, 1, 1, 1, 1, 1, 1, 4, NULL, NULL),
(47, 1, 1, 1, 1, 1, 1, 28, NULL, NULL),
(48, 1, 1, 1, 1, 1, 1, 15, NULL, NULL),
(49, 1, 1, 1, 1, 1, 1, 25, NULL, NULL),
(50, 1, 1, 1, 1, 1, 1, 12, NULL, NULL),
(51, 1, 1, 1, 1, 1, 1, 21, NULL, NULL),
(52, 1, 1, 1, 1, 1, 1, 16, NULL, NULL),
(53, 1, 1, 1, 1, 1, 1, 32, NULL, NULL),
(54, 1, 1, 1, 1, 1, 1, 29, NULL, NULL),
(55, 1, 1, 1, 1, 1, 1, 36, NULL, NULL),
(56, 1, 0, 0, 0, 0, 2, 23, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_settings`
--

CREATE TABLE `cms_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `content_input_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dataenum` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `helper` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group_setting` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_settings`
--

INSERT INTO `cms_settings` (`id`, `name`, `content`, `content_input_type`, `dataenum`, `helper`, `created_at`, `updated_at`, `group_setting`, `label`) VALUES
(1, 'login_background_color', '#5B747C', 'text', NULL, 'Input hexacode', '2022-10-25 22:18:42', NULL, 'Estilo de página de registro', 'Login Background Color'),
(2, 'login_font_color', '#5B747C', 'text', NULL, 'Input hexacode', '2022-10-25 22:18:42', NULL, 'Estilo de página de registro', 'Login Font Color'),
(3, 'login_background_image', 'uploads/2022-11/a9b4c9d6324cabf96c6712409ed9292c.jpg', 'upload_image', NULL, NULL, '2022-10-25 22:18:42', NULL, 'Estilo de página de registro', 'Login Background Image'),
(10, 'appname', 'FUNDEMI - TALITA KUMI', 'text', NULL, NULL, '2022-10-25 22:18:42', NULL, 'Ajustes de Aplicaciones', 'Application Name'),
(11, 'default_paper_size', 'Legal', 'text', NULL, 'Paper size, ex : A4, Legal, etc', '2022-10-25 22:18:42', NULL, 'Ajustes de Aplicaciones', 'Default Paper Print Size'),
(12, 'logo', 'uploads/2022-10/ed762f616ebf78635a2e65f96beb6eb5.png', 'upload_image', NULL, NULL, '2022-10-25 22:18:42', NULL, 'Ajustes de Aplicaciones', 'Logo'),
(13, 'favicon', 'uploads/2022-10/42b1a83f6ec2e646679a94ba9a56b0de.png', 'upload_image', NULL, NULL, '2022-10-25 22:18:42', NULL, 'Ajustes de Aplicaciones', 'Favicon'),
(14, 'api_debug_mode', 'true', 'select', 'true,false', NULL, '2022-10-25 22:18:42', '2022-11-15 06:21:46', 'Ajustes de Aplicaciones', 'API Debug Mode'),
(15, 'google_api_key', NULL, 'text', NULL, NULL, '2022-10-25 22:18:42', NULL, 'Ajustes de Aplicaciones', 'Google API Key'),
(16, 'google_fcm_key', NULL, 'text', NULL, NULL, '2022-10-25 22:18:42', NULL, 'Ajustes de Aplicaciones', 'Google FCM Key');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_statistics`
--

CREATE TABLE `cms_statistics` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_statistics`
--

INSERT INTO `cms_statistics` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Dashboard', 'dashboard', '2022-11-15 06:37:47', NULL),
(2, 'Cumpleañero del Mes', 'cumpleanero-del-mes', '2022-11-16 01:27:50', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_statistic_components`
--

CREATE TABLE `cms_statistic_components` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_statistics` int(11) DEFAULT NULL,
  `componentID` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `component_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_name` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `config` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_statistic_components`
--

INSERT INTO `cms_statistic_components` (`id`, `id_cms_statistics`, `componentID`, `component_name`, `area_name`, `sorting`, `name`, `config`, `created_at`, `updated_at`) VALUES
(1, 1, '93cb3e540140d3be6b2cd060fe3b5404', 'smallbox', 'area1', 0, NULL, '{\"name\":\"Usuarios\",\"icon\":\"bag-outline\",\"color\":\"bg-green\",\"link\":\"\\/admin\\/users\",\"sql\":\"SELECT COUNT(*) FROM `users`\"}', '2022-11-15 06:37:58', NULL),
(2, 1, '4f1065ab70327c3032e4ad286d471dce', 'smallbox', 'area2', 0, NULL, '{\"name\":\"Empleados\",\"icon\":\"bag-outline\",\"color\":\"bg-red\",\"link\":\"\\/admin\\/empleados\",\"sql\":\"SELECT COUNT(*) FROM `tb_empleados`\"}', '2022-11-15 06:38:01', NULL),
(3, 1, 'f4627768762f5dc9de0d91df203237b8', 'smallbox', 'area3', 0, NULL, '{\"name\":\"Contratos\",\"icon\":\"bag-outline\",\"color\":\"bg-aqua\",\"link\":\"\\/admin\\/contratos\",\"sql\":\"select count(id) from tb_contratos\\r\\n WHERE tb_contratos.status = \\\"Activo\\\"\"}', '2022-11-15 06:38:03', NULL),
(4, 1, '45c9932c4d14e767bda628122e417ec6', 'smallbox', 'area4', 0, NULL, '{\"name\":\"Tipos de Contratos\",\"icon\":\"ion-podium-outline\",\"color\":\"bg-yellow\",\"link\":\"#\",\"sql\":\"select count(id) from tb_tipo_contratos\"}', '2022-11-15 06:38:04', NULL),
(6, 1, 'a3e5d03be193eb77acd048eca5716791', 'table', 'area5', 0, NULL, '{\"name\":\"Empleados Mas Antiguos\",\"sql\":\"SELECT concat (tb_empleados.primer_nombre,\\\" \\\",tb_empleados.segundo_apellido,\\\" \\\",tb_empleados.primer_apellido,\\\" \\\",tb_empleados.segundo_apellido) As Empleados, (YEAR(NOW()) - tb_contratos.anio_antiguedad) as A\\u00f1os from tb_empleados, tb_contratos WHERE tb_contratos.anio_antiguedad < YEAR(Now()) and tb_empleados.id = tb_contratos.empleado_id ORDER by  anio_antiguedad ASC\"}', '2022-11-15 14:43:39', NULL),
(7, 1, 'bad07336bf1c92f183142e67094c29fb', 'chartarea', 'area5', 1, NULL, '{\"name\":\"Contratos por Empleados\",\"sql\":\"SELECT COUNT(tb_contratos.id) AS \'value\', tb_tipo_contratos.contrato AS \'label\'\\r\\nFROM tb_contratos,tb_empleados,tb_tipo_contratos\\r\\nWHERE\\r\\ntb_contratos.contrato_id = tb_tipo_contratos.id and tb_contratos.empleado_id = tb_empleados.id\\r\\nand tb_contratos.status = \\\"Activo\\\"\\r\\nGROUP BY\\r\\nlabel\",\"area_name\":\"label\",\"goals\":\"2\"}', '2022-11-15 14:50:49', NULL),
(8, 1, '16dd570d935c970b6361398b0df62f40', 'table', NULL, 0, 'Untitled', NULL, '2022-11-15 15:11:06', NULL),
(10, 2, '6bf17b06d31ce3fd798174bdbc6140c7', 'table', NULL, 0, 'Untitled', NULL, '2022-11-16 01:28:10', NULL),
(13, 2, 'a2477877a028a9c6b05e0f2011c2574d', 'table', 'area5', 0, NULL, '{\"name\":\"Personal que cumple a\\u00f1os en el mes.\",\"sql\":\"SELECT CONCAT(tb_empleados.primer_nombre,\\\" \\\",tb_empleados.segundo_nombre,\\\" \\\",tb_empleados.primer_apellido,\\\" \\\",tb_empleados.segundo_apellido) as \\\"Nombre Completo\\\", tb_empleados.fecha_nacimiento AS \\\"Fecha de Nacimiento\\\",YEAR(NOW()) - YEAR(tb_empleados.fecha_nacimiento) AS \\\"A\\u00f1os Cumplidos\\\" from tb_empleados \\r\\nWHERE MONTH(tb_empleados.fecha_nacimiento) = MONTH(NOW())\"}', '2022-11-16 01:28:27', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_users`
--

CREATE TABLE `cms_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `courses`
--

CREATE TABLE `courses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `length` int(11) NOT NULL DEFAULT '12',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `discounts`
--

CREATE TABLE `discounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double UNSIGNED NOT NULL DEFAULT '0',
  `type` enum('INSCRIPCION','MENSUALIDAD','DESCUENTO','OTRO') COLLATE utf8mb4_unicode_ci NOT NULL,
  `sign` enum('+','-','%','=') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups`
--

CREATE TABLE `groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `courses_id` bigint(20) NOT NULL,
  `campuses_id` bigint(20) NOT NULL,
  `total` int(10) UNSIGNED NOT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(30, '2021_05_30_164301_create_permission_tables', 2),
(31, '2022_03_12_153714_create_students_table', 2),
(32, '2022_03_12_154044_create_courses_table', 2),
(33, '2022_03_12_154209_create_group_table', 2),
(34, '2022_03_12_154608_create_registration_table', 2),
(35, '2022_03_12_155052_create_payment_table', 2),
(36, '2022_03_12_155131_create_campuses_table', 2),
(37, '2022_03_31_224009_create_discounts_table', 2),
(56, '2014_10_12_000000_create_users_table', 3),
(57, '2014_10_12_100000_create_password_resets_table', 3),
(58, '2016_08_07_145904_add_table_cms_apicustom', 3),
(59, '2016_08_07_150834_add_table_cms_dashboard', 3),
(60, '2016_08_07_151210_add_table_cms_logs', 3),
(61, '2016_08_07_151211_add_details_cms_logs', 3),
(62, '2016_08_07_152014_add_table_cms_privileges', 3),
(63, '2016_08_07_152214_add_table_cms_privileges_roles', 3),
(64, '2016_08_07_152320_add_table_cms_settings', 3),
(65, '2016_08_07_152421_add_table_cms_users', 3),
(66, '2016_08_07_154624_add_table_cms_menus_privileges', 3),
(67, '2016_08_07_154624_add_table_cms_moduls', 3),
(68, '2016_08_17_225409_add_status_cms_users', 3),
(69, '2016_08_20_125418_add_table_cms_notifications', 3),
(70, '2016_09_04_033706_add_table_cms_email_queues', 3),
(71, '2016_09_16_035347_add_group_setting', 3),
(72, '2016_09_16_045425_add_label_setting', 3),
(73, '2016_09_17_104728_create_nullable_cms_apicustom', 3),
(74, '2016_10_01_141740_add_method_type_apicustom', 3),
(75, '2016_10_01_141846_add_parameters_apicustom', 3),
(76, '2016_10_01_141934_add_responses_apicustom', 3),
(77, '2016_10_01_144826_add_table_apikey', 3),
(78, '2016_11_14_141657_create_cms_menus', 3),
(79, '2016_11_15_132350_create_cms_email_templates', 3),
(80, '2016_11_15_190410_create_cms_statistics', 3),
(81, '2016_11_17_102740_create_cms_statistic_components', 3),
(82, '2017_06_06_164501_add_deleted_at_cms_moduls', 3),
(83, '2019_08_19_000000_create_failed_jobs_table', 3),
(84, '2019_12_14_000001_create_personal_access_tokens_table', 3),
(85, '2022_10_21_013254_create_tb_identification_documents_table', 3),
(86, '2022_10_21_013356_create_tb_marital_status_table', 3),
(87, '2022_10_21_013521_create_tb_generos_table', 3),
(88, '2022_10_21_013531_create_tb_etnias_table', 3),
(89, '2022_10_21_045336_create_tb_pais_table', 3),
(90, '2022_10_21_045356_create_tb_departamentos_table', 3),
(91, '2022_10_21_045410_create_tb_municipios_table', 3),
(92, '2022_10_21_045445_create_tb_regiones_table', 3),
(93, '2022_10_21_045751_create_tb_profeciones_table', 3),
(94, '2022_10_21_045816_create_tb_empleados_table', 3),
(95, '2022_10_21_050127_create_tb_información_academicas_table', 3),
(96, '2022_10_21_050807_create_tb_datos_medicos_table', 3),
(98, '2022_10_21_051217_create_tb_seguros_medicos_table', 3),
(99, '2022_10_21_051425_create_tb_datos_emergencias_table', 3),
(100, '2022_10_21_051531_create_tb_ctabancarios_table', 3),
(103, '2022_10_21_050958_create_tb_salarios_table', 4),
(104, '2022_10_26_230547_create_tb_hrs_laboradas_table', 4),
(105, '2022_10_26_230629_create_tb_dias_laborados_table', 4),
(108, '2022_10_26_230752_create_tb_tipo_salarios_table', 4),
(111, '2022_10_26_230706_create_tb_tipo_contratos_table', 5),
(114, '2022_10_26_230721_create_tb_contratos_table', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('hannselcordon@gmail.com', '$2y$10$gSdxhuFIL3YG/8u3LfXhPe3a14U5nGhTsKE6xDy9XJaoHxriEsTjS', '2022-11-17 01:00:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `student_id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) UNSIGNED DEFAULT NULL,
  `number` int(10) UNSIGNED DEFAULT NULL,
  `type` enum('INSCRIPCION','MENSUALIDAD','EXAMEN EXTRAORDINARIO','BLS','PHTLS','IPR','OTROS') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'MENSUALIDAD',
  `discounts_id` int(11) NOT NULL DEFAULT '1',
  `amount` double NOT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `bank` text COLLATE utf8mb4_unicode_ci,
  `transaction` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_date` date DEFAULT NULL,
  `transaction_time` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registration`
--

CREATE TABLE `registration` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `student_id` bigint(20) UNSIGNED NOT NULL,
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `course_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `students`
--

CREATE TABLE `students` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `matricula` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombres` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ap_paterno` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ap_materno` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_nac` date NOT NULL,
  `lugar_nac` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `anos_cumplidos` int(11) NOT NULL,
  `direccion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ciudad` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `celular` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tel_casa` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nacionalidad` enum('MEXICANA','EXTRANGERA') COLLATE utf8mb4_unicode_ci NOT NULL,
  `nacionalidad_ext` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `peso` double(8,2) DEFAULT NULL,
  `altura` double(8,2) DEFAULT NULL,
  `talla_pantalon` int(11) DEFAULT NULL,
  `talla_playera` enum('CHICO','MEDIANO','GRANDE','EXTRA GRANDE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'MEDIANO',
  `secundaria` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secu_dir` text COLLATE utf8mb4_unicode_ci,
  `preparatoria` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prepa_dir` text COLLATE utf8mb4_unicode_ci,
  `universidad` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uni_dir` text COLLATE utf8mb4_unicode_ci,
  `licenciatura` text COLLATE utf8mb4_unicode_ci,
  `nombre_rep` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ident` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentesco` enum('PADRE','MADRE','ABUELO','ABUELA','SIN PARENTESCO','OTRO PARENTESCO') COLLATE utf8mb4_unicode_ci NOT NULL,
  `estudio_economico` enum('MUY BUENA','BUENA','REGULAR','MALA') COLLATE utf8mb4_unicode_ci NOT NULL,
  `vivienda` enum('PROPIA','RENTADA','PRESTADA') COLLATE utf8mb4_unicode_ci NOT NULL,
  `ocup_rep` enum('EMPLEADO PUBLICO','EMPLEADO PRIVADO','AUTONOMO','JUBILADO','NO TRABAJA') COLLATE utf8mb4_unicode_ci NOT NULL,
  `estudios_rep` enum('ANALFABETO','CURSOS EXTRAESCOLARES','PRIMARIA INCOMPLETA','PRIMARIA COMPLETA','SECUNDARIA INCOMPLETA','SECUNDARIA COMPLETA','PREPARATORIA INCOMPLETA','PREPARATORIA COMPLETA','UNIVERSIDAD INCOMPLETA','UNIVERSIDAD COMPLETA','POSGRADO INCOMPLETO','POSGRADO COMPLETO') COLLATE utf8mb4_unicode_ci NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_contratos`
--

CREATE TABLE `tb_contratos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `contrato_id` bigint(20) UNSIGNED NOT NULL,
  `empleado_id` bigint(20) UNSIGNED NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `anio_antiguedad` int(11) DEFAULT NULL,
  `salario_id` bigint(20) UNSIGNED NOT NULL,
  `forma_pago` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `observaciones` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `documentos` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tb_contratos`
--

INSERT INTO `tb_contratos` (`id`, `contrato_id`, `empleado_id`, `fecha_inicio`, `fecha_fin`, `anio_antiguedad`, `salario_id`, `forma_pago`, `observaciones`, `documentos`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2022-10-11', '2022-10-11', 1980, 1, 'Efectivo', 'd', NULL, 'Activo', '2022-10-27 04:52:00', '2022-11-15 14:34:34'),
(2, 1, 1, '2022-10-03', '2022-10-31', 1990, 2, 'Efectivo', '1', 'uploads/1/2022-10/modelo_de_contrato.doc', 'Activo', '2022-10-27 04:54:49', '2022-11-15 14:34:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_ctabancarios`
--

CREATE TABLE `tb_ctabancarios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `institucion_bancaria` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_cuenta` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_cuenta` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre_titular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `observaciones` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `empleado_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_datos_emergencias`
--

CREATE TABLE `tb_datos_emergencias` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre_completo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dirección` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parentezco` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `observaciones` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `empleado_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_datos_medicos`
--

CREATE TABLE `tb_datos_medicos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `grupo_sanguineo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enfermedades` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medicamentos` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `empleado_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tb_datos_medicos`
--

INSERT INTO `tb_datos_medicos` (`id`, `grupo_sanguineo`, `enfermedades`, `medicamentos`, `empleado_id`, `created_at`, `updated_at`) VALUES
(1, '0(+)', 'Hipertención', 'Aspirina', 1, '2022-10-27 00:59:30', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_departamentos`
--

CREATE TABLE `tb_departamentos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `codigo` int(11) NOT NULL,
  `departamento` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pais_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tb_departamentos`
--

INSERT INTO `tb_departamentos` (`id`, `codigo`, `departamento`, `pais_id`, `created_at`, `updated_at`) VALUES
(2, 1600, 'Alta Verapaz', 1, '2022-10-26 00:49:24', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_dias_laborados`
--

CREATE TABLE `tb_dias_laborados` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `dias_laborables` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `observaciones` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tb_dias_laborados`
--

INSERT INTO `tb_dias_laborados` (`id`, `dias_laborables`, `observaciones`, `created_at`, `updated_at`) VALUES
(1, 'Lunes - Viernes y Sabado Medio dia', 'Sabado se labora solamente medio', '2022-10-27 00:41:32', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_empleados`
--

CREATE TABLE `tb_empleados` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fotografia` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primer_nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `segundo_nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `primer_apellido` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `segundo_apellido` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `documento_id` bigint(20) UNSIGNED NOT NULL,
  `no_documento` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_expiracion` date NOT NULL,
  `nit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `genero_id` bigint(20) UNSIGNED NOT NULL,
  `etnia_id` bigint(20) UNSIGNED NOT NULL,
  `estado_civil_id` bigint(20) UNSIGNED NOT NULL,
  `apellido_casadas` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono_casa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono_personal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codigo_ocupacional` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profecion_id` bigint(20) UNSIGNED NOT NULL,
  `colegiado_activo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seguro_social` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pais_id` bigint(20) UNSIGNED NOT NULL,
  `region_id` bigint(20) UNSIGNED NOT NULL,
  `departamento_id` bigint(20) UNSIGNED NOT NULL,
  `municipio_id` bigint(20) UNSIGNED NOT NULL,
  `direccion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'Activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tb_empleados`
--

INSERT INTO `tb_empleados` (`id`, `fotografia`, `primer_nombre`, `segundo_nombre`, `primer_apellido`, `segundo_apellido`, `fecha_nacimiento`, `documento_id`, `no_documento`, `fecha_expiracion`, `nit`, `genero_id`, `etnia_id`, `estado_civil_id`, `apellido_casadas`, `telefono_casa`, `telefono_personal`, `email`, `codigo_ocupacional`, `profecion_id`, `colegiado_activo`, `seguro_social`, `pais_id`, `region_id`, `departamento_id`, `municipio_id`, `direccion`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'uploads/1/2022-11/m6.jpeg', 'Hector', 'Estuardo', 'Molina', 'Mendez', '1989-11-22', 1, '2050968911601', '2030-08-21', '6723018-0', 1, 1, 2, NULL, '30387632', '30387632', 'hannselcordon@gmail.com', '16001', 1, '122323', '323232', 1, 1, 2, 1, 'Coban Alta Verapaz', 'Activo', '2022-10-27 00:52:26', '2022-11-15 16:21:04'),
(2, 'uploads/1/2022-11/m1.jpeg', 'Carlos', 'Crispín', 'Aliñado', 'Asturias', '2000-01-01', 1, '2050928921601', '2022-10-31', '12345678', 1, 1, 2, NULL, '', '34543456', 'hannselcordon1@gmail.com', '16001', 3, '122324', '323232', 1, 1, 2, 1, 'Coban', 'Inactivo', '2022-10-27 06:00:06', '2022-11-15 16:16:43'),
(3, 'uploads/1/2022-11/f1.jpeg', 'Sonia', 'Maribel', 'Yat', 'Ochoa', '1986-10-26', 1, '2050968911602', '2027-07-21', '12345678', 2, 1, 2, 'Cordón', '', '30387632', 'mar8a@gmail.com', NULL, 3, NULL, '323232', 1, 1, 2, 1, 'Barrio La Libertad', 'Activo', '2022-10-27 06:24:26', '2022-11-16 03:03:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_etnias`
--

CREATE TABLE `tb_etnias` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `etnia` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tb_etnias`
--

INSERT INTO `tb_etnias` (`id`, `etnia`, `created_at`, `updated_at`) VALUES
(1, 'Maya', '2022-10-26 01:05:55', NULL),
(2, 'Ladino', '2022-11-16 02:21:00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_generos`
--

CREATE TABLE `tb_generos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `genero` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tb_generos`
--

INSERT INTO `tb_generos` (`id`, `genero`, `created_at`, `updated_at`) VALUES
(1, 'Masculino', '2022-10-26 01:04:31', NULL),
(2, 'Femenino', '2022-10-26 01:04:38', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_hrs_laboradas`
--

CREATE TABLE `tb_hrs_laboradas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hrs_laborables` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `observaciones` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tb_hrs_laboradas`
--

INSERT INTO `tb_hrs_laboradas` (`id`, `hrs_laborables`, `observaciones`, `created_at`, `updated_at`) VALUES
(1, '8 hrs. diarias', '8 hrs. diarias', '2022-10-27 00:45:20', NULL),
(2, '40 hrs. Semanales', '40 hrs. Semanales', '2022-10-27 00:45:32', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_identification_documents`
--

CREATE TABLE `tb_identification_documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `documento` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tb_identification_documents`
--

INSERT INTO `tb_identification_documents` (`id`, `documento`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 'DPI', 'Documento público, personal e intransferible, de carácter oficial. \r\nTodos los guatemaltecos y los extranjeros domiciliados, inscritos en el RENAP, tienen el derecho de solicitar y obtener el DPI. \r\nSerá el único documento para todos los actos civiles', '2022-10-26 01:11:45', NULL),
(2, 'Pasaporte', 'Descripcion', '2022-11-16 02:18:24', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_información_academicas`
--

CREATE TABLE `tb_información_academicas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nivel_academico` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grado` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `institucion_educativa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `anio` int(11) NOT NULL,
  `empleado_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_marital_status`
--

CREATE TABLE `tb_marital_status` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `estado_civil` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tb_marital_status`
--

INSERT INTO `tb_marital_status` (`id`, `estado_civil`, `created_at`, `updated_at`) VALUES
(1, 'Soltero (a)', '2022-10-26 01:14:00', NULL),
(2, 'Casado (a)', '2022-10-26 01:14:40', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_municipios`
--

CREATE TABLE `tb_municipios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `codigo` int(11) NOT NULL,
  `municipio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `departamento_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tb_municipios`
--

INSERT INTO `tb_municipios` (`id`, `codigo`, `municipio`, `departamento_id`, `created_at`, `updated_at`) VALUES
(1, 1601, 'Cobán', 2, '2022-10-26 00:50:06', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_pais`
--

CREATE TABLE `tb_pais` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `codigo` int(11) NOT NULL,
  `pais` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tb_pais`
--

INSERT INTO `tb_pais` (`id`, `codigo`, `pais`, `created_at`, `updated_at`) VALUES
(1, 502, 'Guatemala', '2022-10-25 22:34:54', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_profeciones`
--

CREATE TABLE `tb_profeciones` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `profecion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tb_profeciones`
--

INSERT INTO `tb_profeciones` (`id`, `profecion`, `created_at`, `updated_at`) VALUES
(1, 'Maestro de Educación Primaria', '2022-10-27 00:14:47', NULL),
(2, 'Doctor (a)', '2022-10-27 00:14:55', NULL),
(3, 'Licenciado (a) en Administración de Empresas', '2022-10-27 00:15:46', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_regiones`
--

CREATE TABLE `tb_regiones` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `region` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `municipio_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tb_regiones`
--

INSERT INTO `tb_regiones` (`id`, `region`, `municipio_id`, `created_at`, `updated_at`) VALUES
(1, 'Nor Oriente', 1, '2022-10-26 00:50:25', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_salarios`
--

CREATE TABLE `tb_salarios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipo_salario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `forma_pago` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `empleado_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_seguros_medicos`
--

CREATE TABLE `tb_seguros_medicos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `seguro` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `poliza` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `certificado` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vencimiento` date NOT NULL,
  `institucion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `observaciones` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `empleado_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tb_seguros_medicos`
--

INSERT INTO `tb_seguros_medicos` (`id`, `seguro`, `poliza`, `certificado`, `vencimiento`, `institucion`, `observaciones`, `empleado_id`, `created_at`, `updated_at`) VALUES
(1, 'Medico', '23R30A', '23323', '2022-10-29', 'SEGUROS S.A.', 'SEGUROS', 1, '2022-10-27 01:08:29', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_tipo_contratos`
--

CREATE TABLE `tb_tipo_contratos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `contrato` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_laboradas` bigint(20) UNSIGNED NOT NULL,
  `id_laborados` bigint(20) UNSIGNED NOT NULL,
  `justificacion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `formato` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `observacion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tb_tipo_contratos`
--

INSERT INTO `tb_tipo_contratos` (`id`, `contrato`, `id_laboradas`, `id_laborados`, `justificacion`, `formato`, `observacion`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Prestación de Servicios', 2, 1, 'Contrato elaborado para personal docente', 'uploads/1/2022-10/modelo_de_contrato.doc', 'Contrato elaborado para personal docente', 'Activo', '2022-10-27 03:56:45', '2022-10-27 05:28:04'),
(2, 'Tiempo Indefinido', 2, 1, 'Justificación', 'uploads/1/2022-11/modelo_de_contrato.doc', 'Observaciones', 'Activo', '2022-11-16 02:52:10', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_tipo_salarios`
--

CREATE TABLE `tb_tipo_salarios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `salario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `monto` double NOT NULL,
  `bonificacion_incentiva` double DEFAULT NULL,
  `fundamento_legal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `anio` int(11) NOT NULL,
  `observaciones` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tb_tipo_salarios`
--

INSERT INTO `tb_tipo_salarios` (`id`, `salario`, `monto`, `bonificacion_incentiva`, `fundamento_legal`, `estado`, `anio`, `observaciones`, `created_at`, `updated_at`) VALUES
(1, 'Actividades Agrícolas', 2954.35, 250, 'Decreto 278-2021', 'Activo', 2022, 'El Gobierno de la República de Guatemala ha emitido el Acuerdo Gubernativo  278-2021; en el cual se regula el salario que regirá durante el año 2022 para las actividades agrícolas.', '2022-10-27 01:01:07', '2022-10-27 05:44:18'),
(2, 'Actividades no agrícolas.', 3209.24, 250, 'Decreto 278-2021', 'Activo', 2022, 'El Gobierno de la República de Guatemala ha emitido el Acuerdo Gubernativo  278-2021; en el cual se regula el salario que regirá durante el año 2022 para las actividades no agrícolas.', '2022-10-27 01:15:37', '2022-10-27 05:43:48'),
(3, 'Actividades de exportadora y de maquila', 2954.35, 250, 'Decreto 278-2021', 'Activo', 2022, 'El Gobierno de la República de Guatemala ha emitido el Acuerdo Gubernativo  278-2021; en el cual se regula el salario que regirá durante el año 2022 para las Actividades de exportadora y de maquila.', '2022-10-27 02:42:23', '2022-10-27 06:13:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `photo`, `email`, `email_verified_at`, `password`, `id_cms_privileges`, `remember_token`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Hannsel Cordon', 'uploads/1/2022-10/talita.png', 'hannselcordon@gmail.com', NULL, '$2y$10$qB7BRdu95kebS355xg.GKeSd381Tv/.9l35viewOQC9eshcjYtZ22', 1, 'c4CJMRnjjFW5s4Ym8A5YCkPcPPI814T2ZbIvqfKP1cAZsZv0c9YM8whHneb5', 'Active', '2022-10-25 22:18:42', '2022-11-17 00:38:38'),
(2, 'RRHH', 'uploads/1/2022-11/descarga.png', 'recursos@recursos.com', NULL, '$2y$10$x.vHzY.CrV6cqI8SHyHkcuRL6nTh9YmLRAuDu7Sn9C7foxrb858Na', 2, NULL, NULL, '2022-11-16 03:13:35', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `campuses`
--
ALTER TABLE `campuses`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_apikey`
--
ALTER TABLE `cms_apikey`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_logs`
--
ALTER TABLE `cms_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_menus`
--
ALTER TABLE `cms_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_moduls`
--
ALTER TABLE `cms_moduls`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_notifications`
--
ALTER TABLE `cms_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_privileges`
--
ALTER TABLE `cms_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_settings`
--
ALTER TABLE `cms_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_statistics`
--
ALTER TABLE `cms_statistics`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_users`
--
ALTER TABLE `cms_users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indices de la tabla `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indices de la tabla `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indices de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indices de la tabla `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indices de la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tb_contratos`
--
ALTER TABLE `tb_contratos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_contratos_salario_id_foreign` (`salario_id`),
  ADD KEY `tb_contratos_contrato_id_foreign` (`contrato_id`),
  ADD KEY `tb_contratos_empleado_id_foreign` (`empleado_id`);

--
-- Indices de la tabla `tb_ctabancarios`
--
ALTER TABLE `tb_ctabancarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_ctabancarios_empleado_id_foreign` (`empleado_id`);

--
-- Indices de la tabla `tb_datos_emergencias`
--
ALTER TABLE `tb_datos_emergencias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_datos_emergencias_empleado_id_foreign` (`empleado_id`);

--
-- Indices de la tabla `tb_datos_medicos`
--
ALTER TABLE `tb_datos_medicos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_datos_medicos_empleado_id_foreign` (`empleado_id`);

--
-- Indices de la tabla `tb_departamentos`
--
ALTER TABLE `tb_departamentos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_departamentos_pais_id_foreign` (`pais_id`);

--
-- Indices de la tabla `tb_dias_laborados`
--
ALTER TABLE `tb_dias_laborados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tb_empleados`
--
ALTER TABLE `tb_empleados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_empleados_documento_id_foreign` (`documento_id`),
  ADD KEY `tb_empleados_genero_id_foreign` (`genero_id`),
  ADD KEY `tb_empleados_etnia_id_foreign` (`etnia_id`),
  ADD KEY `tb_empleados_estado_civil_id_foreign` (`estado_civil_id`),
  ADD KEY `tb_empleados_profecion_id_foreign` (`profecion_id`),
  ADD KEY `tb_empleados_pais_id_foreign` (`pais_id`),
  ADD KEY `tb_empleados_region_id_foreign` (`region_id`),
  ADD KEY `tb_empleados_departamento_id_foreign` (`departamento_id`),
  ADD KEY `tb_empleados_municipio_id_foreign` (`municipio_id`);

--
-- Indices de la tabla `tb_etnias`
--
ALTER TABLE `tb_etnias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tb_generos`
--
ALTER TABLE `tb_generos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tb_hrs_laboradas`
--
ALTER TABLE `tb_hrs_laboradas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tb_identification_documents`
--
ALTER TABLE `tb_identification_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tb_información_academicas`
--
ALTER TABLE `tb_información_academicas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_información_academicas_empleado_id_foreign` (`empleado_id`);

--
-- Indices de la tabla `tb_marital_status`
--
ALTER TABLE `tb_marital_status`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tb_municipios`
--
ALTER TABLE `tb_municipios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_municipios_departamento_id_foreign` (`departamento_id`);

--
-- Indices de la tabla `tb_pais`
--
ALTER TABLE `tb_pais`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tb_profeciones`
--
ALTER TABLE `tb_profeciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tb_regiones`
--
ALTER TABLE `tb_regiones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_regiones_municipio_id_foreign` (`municipio_id`);

--
-- Indices de la tabla `tb_salarios`
--
ALTER TABLE `tb_salarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_salarios_empleado_id_foreign` (`empleado_id`);

--
-- Indices de la tabla `tb_seguros_medicos`
--
ALTER TABLE `tb_seguros_medicos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_seguros_medicos_empleado_id_foreign` (`empleado_id`);

--
-- Indices de la tabla `tb_tipo_contratos`
--
ALTER TABLE `tb_tipo_contratos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_tipo_contratos_id_laboradas_foreign` (`id_laboradas`),
  ADD KEY `tb_tipo_contratos_id_laborados_foreign` (`id_laborados`);

--
-- Indices de la tabla `tb_tipo_salarios`
--
ALTER TABLE `tb_tipo_salarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `campuses`
--
ALTER TABLE `campuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cms_apikey`
--
ALTER TABLE `cms_apikey`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cms_logs`
--
ALTER TABLE `cms_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=228;

--
-- AUTO_INCREMENT de la tabla `cms_menus`
--
ALTER TABLE `cms_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT de la tabla `cms_moduls`
--
ALTER TABLE `cms_moduls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `cms_notifications`
--
ALTER TABLE `cms_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cms_privileges`
--
ALTER TABLE `cms_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT de la tabla `cms_settings`
--
ALTER TABLE `cms_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `cms_statistics`
--
ALTER TABLE `cms_statistics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `cms_users`
--
ALTER TABLE `cms_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `courses`
--
ALTER TABLE `courses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `groups`
--
ALTER TABLE `groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT de la tabla `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `registration`
--
ALTER TABLE `registration`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `students`
--
ALTER TABLE `students`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tb_contratos`
--
ALTER TABLE `tb_contratos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tb_ctabancarios`
--
ALTER TABLE `tb_ctabancarios`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tb_datos_emergencias`
--
ALTER TABLE `tb_datos_emergencias`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tb_datos_medicos`
--
ALTER TABLE `tb_datos_medicos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tb_departamentos`
--
ALTER TABLE `tb_departamentos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tb_dias_laborados`
--
ALTER TABLE `tb_dias_laborados`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tb_empleados`
--
ALTER TABLE `tb_empleados`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tb_etnias`
--
ALTER TABLE `tb_etnias`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tb_generos`
--
ALTER TABLE `tb_generos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tb_hrs_laboradas`
--
ALTER TABLE `tb_hrs_laboradas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tb_identification_documents`
--
ALTER TABLE `tb_identification_documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tb_información_academicas`
--
ALTER TABLE `tb_información_academicas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tb_marital_status`
--
ALTER TABLE `tb_marital_status`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tb_municipios`
--
ALTER TABLE `tb_municipios`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tb_pais`
--
ALTER TABLE `tb_pais`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tb_profeciones`
--
ALTER TABLE `tb_profeciones`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tb_regiones`
--
ALTER TABLE `tb_regiones`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tb_salarios`
--
ALTER TABLE `tb_salarios`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tb_seguros_medicos`
--
ALTER TABLE `tb_seguros_medicos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tb_tipo_contratos`
--
ALTER TABLE `tb_tipo_contratos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tb_tipo_salarios`
--
ALTER TABLE `tb_tipo_salarios`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tb_contratos`
--
ALTER TABLE `tb_contratos`
  ADD CONSTRAINT `tb_contratos_contrato_id_foreign` FOREIGN KEY (`contrato_id`) REFERENCES `tb_tipo_contratos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tb_contratos_empleado_id_foreign` FOREIGN KEY (`empleado_id`) REFERENCES `tb_empleados` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tb_contratos_salario_id_foreign` FOREIGN KEY (`salario_id`) REFERENCES `tb_tipo_salarios` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tb_ctabancarios`
--
ALTER TABLE `tb_ctabancarios`
  ADD CONSTRAINT `tb_ctabancarios_empleado_id_foreign` FOREIGN KEY (`empleado_id`) REFERENCES `tb_empleados` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tb_datos_emergencias`
--
ALTER TABLE `tb_datos_emergencias`
  ADD CONSTRAINT `tb_datos_emergencias_empleado_id_foreign` FOREIGN KEY (`empleado_id`) REFERENCES `tb_empleados` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tb_datos_medicos`
--
ALTER TABLE `tb_datos_medicos`
  ADD CONSTRAINT `tb_datos_medicos_empleado_id_foreign` FOREIGN KEY (`empleado_id`) REFERENCES `tb_empleados` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tb_departamentos`
--
ALTER TABLE `tb_departamentos`
  ADD CONSTRAINT `tb_departamentos_pais_id_foreign` FOREIGN KEY (`pais_id`) REFERENCES `tb_pais` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tb_empleados`
--
ALTER TABLE `tb_empleados`
  ADD CONSTRAINT `tb_empleados_departamento_id_foreign` FOREIGN KEY (`departamento_id`) REFERENCES `tb_departamentos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tb_empleados_documento_id_foreign` FOREIGN KEY (`documento_id`) REFERENCES `tb_identification_documents` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tb_empleados_estado_civil_id_foreign` FOREIGN KEY (`estado_civil_id`) REFERENCES `tb_marital_status` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tb_empleados_etnia_id_foreign` FOREIGN KEY (`etnia_id`) REFERENCES `tb_etnias` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tb_empleados_genero_id_foreign` FOREIGN KEY (`genero_id`) REFERENCES `tb_generos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tb_empleados_municipio_id_foreign` FOREIGN KEY (`municipio_id`) REFERENCES `tb_municipios` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tb_empleados_pais_id_foreign` FOREIGN KEY (`pais_id`) REFERENCES `tb_pais` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tb_empleados_profecion_id_foreign` FOREIGN KEY (`profecion_id`) REFERENCES `tb_profeciones` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tb_empleados_region_id_foreign` FOREIGN KEY (`region_id`) REFERENCES `tb_regiones` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tb_información_academicas`
--
ALTER TABLE `tb_información_academicas`
  ADD CONSTRAINT `tb_información_academicas_empleado_id_foreign` FOREIGN KEY (`empleado_id`) REFERENCES `tb_empleados` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tb_municipios`
--
ALTER TABLE `tb_municipios`
  ADD CONSTRAINT `tb_municipios_departamento_id_foreign` FOREIGN KEY (`departamento_id`) REFERENCES `tb_departamentos` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tb_regiones`
--
ALTER TABLE `tb_regiones`
  ADD CONSTRAINT `tb_regiones_municipio_id_foreign` FOREIGN KEY (`municipio_id`) REFERENCES `tb_municipios` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tb_salarios`
--
ALTER TABLE `tb_salarios`
  ADD CONSTRAINT `tb_salarios_empleado_id_foreign` FOREIGN KEY (`empleado_id`) REFERENCES `tb_empleados` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tb_seguros_medicos`
--
ALTER TABLE `tb_seguros_medicos`
  ADD CONSTRAINT `tb_seguros_medicos_empleado_id_foreign` FOREIGN KEY (`empleado_id`) REFERENCES `tb_empleados` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tb_tipo_contratos`
--
ALTER TABLE `tb_tipo_contratos`
  ADD CONSTRAINT `tb_tipo_contratos_id_laboradas_foreign` FOREIGN KEY (`id_laboradas`) REFERENCES `tb_hrs_laboradas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tb_tipo_contratos_id_laborados_foreign` FOREIGN KEY (`id_laborados`) REFERENCES `tb_dias_laborados` (`id`) ON DELETE CASCADE;
--
-- Base de datos: `fiberhome`
--
CREATE DATABASE IF NOT EXISTS `fiberhome` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci;
USE `fiberhome`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargos`
--

CREATE TABLE `cargos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cargos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cargos`
--

INSERT INTO `cargos` (`id`, `cargos`, `created_at`, `updated_at`) VALUES
(1, 'Supervisor', '2022-10-19 01:39:56', NULL),
(2, 'Gestor de Cobros', '2022-10-19 01:40:04', NULL),
(3, 'Vendedor Rutero', '2022-10-19 01:40:18', NULL),
(4, 'Secretaria', '2022-10-19 01:40:24', NULL),
(5, 'Gerente', '2022-10-19 01:40:29', NULL),
(6, 'Contador', '2022-10-19 01:40:40', NULL),
(7, 'Analista de Calidad', '2022-10-19 01:41:03', NULL),
(8, 'Desarrollador', '2022-10-19 01:41:16', NULL),
(9, 'Analista de IT', '2022-10-19 01:41:29', NULL),
(10, 'Técnico de Campo', '2022-11-02 19:54:00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cui` bigint(20) NOT NULL,
  `nombres` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` bigint(20) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `departamento_id` bigint(20) UNSIGNED NOT NULL,
  `municipio_id` bigint(20) UNSIGNED NOT NULL,
  `direccion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ubicacion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `empleado_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `cui`, `nombres`, `apellidos`, `telefono`, `email`, `departamento_id`, `municipio_id`, `direccion`, `ubicacion`, `empleado_id`, `created_at`, `updated_at`) VALUES
(1, 2050968911601, 'Hannsel Estuardo', 'Cordón Ac', 12345678, 'hannselcordon@gmail.com', 1, 1, 'Coban Alta Verapaz', '8a Avenida 5-87, Cobán, Guatemala', 2, '2022-10-19 12:43:33', NULL),
(2, 2050968911607, 'Carlos', 'Lopez', 12345678, 'carlos@gmail.com', 1, 16, 'Coban Alta Verapaz', 'RP85+9J7, Chisec, Guatemala', 2, '2022-11-03 22:26:11', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_apicustom`
--

CREATE TABLE `cms_apicustom` (
  `id` int(10) UNSIGNED NOT NULL,
  `permalink` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tabel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aksi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kolom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderby` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_query_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sql_where` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `method_type` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` longtext COLLATE utf8mb4_unicode_ci,
  `responses` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_apikey`
--

CREATE TABLE `cms_apikey` (
  `id` int(10) UNSIGNED NOT NULL,
  `screetkey` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hit` int(11) DEFAULT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_apikey`
--

INSERT INTO `cms_apikey` (`id`, `screetkey`, `hit`, `status`, `created_at`, `updated_at`) VALUES
(1, '3ed9a39896222f56234261e8200d9203', 0, 'active', '2022-10-19 06:02:34', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_dashboard`
--

CREATE TABLE `cms_dashboard` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_email_queues`
--

CREATE TABLE `cms_email_queues` (
  `id` int(10) UNSIGNED NOT NULL,
  `send_at` datetime DEFAULT NULL,
  `email_recipient` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_content` text COLLATE utf8mb4_unicode_ci,
  `email_attachments` text COLLATE utf8mb4_unicode_ci,
  `is_sent` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_email_templates`
--

CREATE TABLE `cms_email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_email_templates`
--

INSERT INTO `cms_email_templates` (`id`, `name`, `slug`, `subject`, `content`, `description`, `from_name`, `from_email`, `cc_email`, `created_at`, `updated_at`) VALUES
(1, 'Email Template Forgot Password Backend', 'forgot_password_backend', 'password', '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'fiberhomeumg@gmail.com', 'hannselcordon@gmail.com', '2022-10-19 00:36:28', '2022-10-19 06:25:25'),
(2, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2022-10-24 00:05:25', NULL),
(3, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2022-10-24 00:25:19', NULL),
(4, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2022-11-02 19:08:45', NULL),
(1, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2022-11-08 04:12:45', NULL),
(2, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2022-11-08 04:59:46', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_logs`
--

CREATE TABLE `cms_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `ipaddress` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `useragent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `id_cms_users` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_logs`
--

INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `details`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(1, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', ' logout', '', 2, '2022-10-19 00:37:24', NULL),
(2, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-19 00:37:31', NULL),
(3, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/users/add-save', 'Add New Data Hannsel at Users Management', '', 1, '2022-10-19 00:38:16', NULL),
(4, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/users/edit-save/1', 'Update data Super Admin at Users Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td></td><td>uploads/1/2022-10/147144.png</td></tr><tr><td>password</td><td>$2y$10$JqTRHCSwvojIIVSXtaEe8etGZM.CvaxyCz1E1VtrDd45.cxtp2FWq</td><td></td></tr><tr><td>status</td><td>Active</td><td></td></tr></tbody></table>', 1, '2022-10-19 00:39:49', NULL),
(5, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/4', 'Update data Estados at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-file</td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2022-10-19 00:53:51', NULL),
(6, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-10-19 00:58:04', NULL),
(7, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-10-19 00:58:15', NULL),
(8, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/estados/add-save', 'Add New Data  at Estados', '', 1, '2022-10-19 00:58:26', NULL),
(9, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/estados/add-save', 'Add New Data  at Estados', '', 1, '2022-10-19 00:58:38', NULL),
(10, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/estados/add-save', 'Add New Data  at Estados', '', 1, '2022-10-19 00:59:03', NULL),
(11, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tipoordenes/add-save', 'Add New Data  at Tipos de Ordenes de Servicio', '', 1, '2022-10-19 00:59:58', NULL),
(12, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tipoordenes/add-save', 'Add New Data  at Tipos de Ordenes de Servicio', '', 1, '2022-10-19 01:00:15', NULL),
(13, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tipoordenes/add-save', 'Add New Data  at Tipos de Ordenes de Servicio', '', 1, '2022-10-19 01:00:35', NULL),
(14, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tipopagos/add-save', 'Add New Data  at Tipo de Pagos', '', 1, '2022-10-19 01:00:55', NULL),
(15, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tipopagos/add-save', 'Add New Data  at Tipo de Pagos', '', 1, '2022-10-19 01:00:58', NULL),
(16, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-19 01:10:52', NULL),
(17, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-10-19 01:10:55', NULL),
(18, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 2, '2022-10-19 01:11:00', NULL),
(19, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-19 01:11:04', NULL),
(20, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/statistic_builder/add-save', 'Add New Data Prueba at Statistic Builder', '', 1, '2022-10-19 01:13:10', NULL),
(21, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/statistic_builder/edit-save/1', 'Update data Prueba at Statistic Builder', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>slug</td><td>prueba</td><td></td></tr></tbody></table>', 1, '2022-10-19 01:13:22', NULL),
(22, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/cargos/add-save', 'Add New Data  at Cargos', '', 1, '2022-10-19 01:39:56', NULL),
(23, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/cargos/add-save', 'Add New Data  at Cargos', '', 1, '2022-10-19 01:40:04', NULL),
(24, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/cargos/add-save', 'Add New Data  at Cargos', '', 1, '2022-10-19 01:40:18', NULL),
(25, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/cargos/add-save', 'Add New Data  at Cargos', '', 1, '2022-10-19 01:40:24', NULL),
(26, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/cargos/add-save', 'Add New Data  at Cargos', '', 1, '2022-10-19 01:40:29', NULL),
(27, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/cargos/add-save', 'Add New Data  at Cargos', '', 1, '2022-10-19 01:40:40', NULL),
(28, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/cargos/add-save', 'Add New Data  at Cargos', '', 1, '2022-10-19 01:41:03', NULL),
(29, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/cargos/add-save', 'Add New Data  at Cargos', '', 1, '2022-10-19 01:41:16', NULL),
(30, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/cargos/add-save', 'Add New Data  at Cargos', '', 1, '2022-10-19 01:41:29', NULL),
(31, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tipotrabajos/add-save', 'Add New Data  at Tipos de Orden de Trabajo', '', 1, '2022-10-19 02:17:55', NULL),
(32, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/empleados/add-save', 'Add New Data  at Empleados', '', 1, '2022-10-19 03:01:21', NULL),
(33, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/empleados/add-save', 'Add New Data  at Empleados', '', 1, '2022-10-19 03:02:31', NULL),
(34, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/jerarquias/add-save', 'Add New Data  at Jerarquia Administrativa', '', 1, '2022-10-19 03:37:50', NULL),
(35, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/jerarquias/edit-save/1', 'Update data  at Jerarquia Administrativa', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2022-10-19 04:01:57', NULL),
(36, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/clientes/add-save', 'Add New Data  at Clientes', '', 1, '2022-10-19 05:17:58', NULL),
(37, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/email_templates/edit-save/1', 'Update data Email Template Forgot Password Backend at Email Templates', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>subject</td><td></td><td>password</td></tr><tr><td>from_email</td><td>system@crudbooster.com</td><td>fiberhomeumg@gmail.com</td></tr><tr><td>cc_email</td><td></td><td>hannselcordon@gmail.com</td></tr></tbody></table>', 1, '2022-10-19 06:25:25', NULL),
(38, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data prueba at Menu Management', '', 1, '2022-10-19 07:18:58', NULL),
(39, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-19 07:19:48', NULL),
(40, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-10-19 07:19:53', NULL),
(41, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 2, '2022-10-19 07:19:59', NULL),
(42, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-10-19 07:20:17', NULL),
(43, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/statistic_builder/add-save', 'Add New Data prueba2 at Statistic Builder', '', 2, '2022-10-19 07:26:11', NULL),
(44, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data prueba2 at Menu Management', '', 2, '2022-10-19 07:29:26', NULL),
(45, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/13', 'Update data prueba2 at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td>normal</td><td>red</td></tr><tr><td>parent_id</td><td>0</td><td></td></tr><tr><td>sorting</td><td></td><td></td></tr></tbody></table>', 2, '2022-10-19 07:30:13', NULL),
(46, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/delete/12', 'Delete data prueba at Menu Management', '', 2, '2022-10-19 07:30:20', NULL),
(47, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/13', 'Update data prueba2 at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>path</td><td>statistic_builder/show/prueba2</td><td>statistic_builder/show/prueba</td></tr><tr><td>parent_id</td><td>0</td><td></td></tr><tr><td>sorting</td><td></td><td></td></tr></tbody></table>', 2, '2022-10-19 07:31:45', NULL),
(48, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/statistic_builder/delete/2', 'Delete data prueba2 at Statistic Builder', '', 2, '2022-10-19 07:36:22', NULL),
(49, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 2, '2022-10-19 08:16:31', NULL),
(50, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-10-19 08:17:45', NULL),
(51, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 2, '2022-10-19 08:19:26', NULL),
(52, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/module_generator/delete/22', 'Delete data Clientes at Module Generator', '', 2, '2022-10-19 10:01:14', NULL),
(53, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/estados/add-save', 'Add New Data  at Estados', '', 2, '2022-10-19 12:20:47', NULL),
(54, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/module_generator/delete/23', 'Delete data Clientes at Module Generator', '', 2, '2022-10-19 12:31:29', NULL),
(55, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/clientes/add-save', 'Add New Data  at Clientes', '', 2, '2022-10-19 12:43:33', NULL),
(56, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/productos/add-save', 'Add New Data  at Productos', '', 2, '2022-10-19 13:12:34', NULL),
(57, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/planes/add-save', 'Add New Data  at Planes y Servicios', '', 2, '2022-10-19 13:59:52', NULL),
(58, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/contratos/add-save', 'Add New Data  at Contratos de Servicios', '', 2, '2022-10-19 14:00:35', NULL),
(59, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-10-20 06:41:11', NULL),
(60, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/pagos/add-save', 'Add New Data  at Pagos', '', 2, '2022-10-20 08:17:02', NULL),
(61, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/settings/add-save', 'Add New Data image at Settings', '', 2, '2022-10-20 08:30:19', NULL),
(62, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 2, '2022-10-20 08:34:52', NULL),
(63, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-20 08:35:58', NULL),
(64, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-20 08:38:28', NULL),
(65, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-20 08:39:03', NULL),
(66, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/settings/add-save', 'Add New Data image at Settings', '', 1, '2022-10-20 08:40:00', NULL),
(67, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-20 08:40:18', NULL),
(68, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-20 08:40:30', NULL),
(69, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-20 08:56:13', NULL),
(70, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-20 09:22:34', NULL),
(71, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-20 09:23:05', NULL),
(72, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-20 09:25:14', NULL),
(73, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-20 09:30:24', NULL),
(74, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-20 09:30:47', NULL),
(75, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-20 09:32:51', NULL),
(76, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-20 09:35:25', NULL),
(77, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/contratos/delete/1', 'Delete data 1 at Contratos de Servicios', '', 1, '2022-10-20 09:53:51', NULL),
(78, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/module_generator/delete/26', 'Delete data Contratos de Servicios at Module Generator', '', 1, '2022-10-20 10:17:56', NULL),
(79, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/estados/add-save', 'Add New Data  at Estados', '', 1, '2022-10-20 11:13:28', NULL),
(80, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/estados/add-save', 'Add New Data  at Estados', '', 1, '2022-10-20 11:13:35', NULL),
(81, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/contratos29/add-save', 'Add New Data  at Contratos', '', 1, '2022-10-20 11:14:10', NULL),
(82, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/pagos/add-save', 'Add New Data  at Pagos', '', 1, '2022-10-20 11:15:31', NULL),
(83, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/module_generator/delete/21', 'Delete data Jerarquia Administrativa at Module Generator', '', 1, '2022-10-20 11:56:36', NULL),
(84, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-22 04:01:51', NULL),
(85, '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-22 04:02:53', NULL),
(86, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-22 04:05:35', NULL),
(87, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-22 04:05:42', NULL),
(88, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-10-22 04:07:38', NULL),
(89, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/users/add-save', 'Add New Data Secretaria at Users Management', '', 1, '2022-10-22 04:20:17', NULL),
(90, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-22 04:20:36', NULL),
(91, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'secretaria@gmail.com login with IP Address 127.0.0.1', '', 3, '2022-10-22 04:20:40', NULL),
(92, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'secretaria@gmail.com logout', '', 3, '2022-10-22 04:21:02', NULL),
(93, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'secretaria@gmail.com login with IP Address 127.0.0.1', '', 3, '2022-10-22 04:21:11', NULL),
(94, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'secretaria@gmail.com logout', '', 3, '2022-10-22 04:21:16', NULL),
(95, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-22 04:21:25', NULL),
(96, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data Empleados at Menu Management', '', 1, '2022-10-22 04:22:23', NULL),
(97, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data Gogogle at Menu Management', '', 1, '2022-10-22 04:23:08', NULL),
(98, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-22 04:23:22', NULL),
(99, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'secretaria@gmail.com login with IP Address 127.0.0.1', '', 3, '2022-10-22 04:23:27', NULL),
(100, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/clientes/add', 'Try add data at Clientes', '', 3, '2022-10-22 04:23:57', NULL),
(101, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'secretaria@gmail.com logout', '', 3, '2022-10-22 04:25:58', NULL),
(102, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-22 04:26:03', NULL),
(103, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-23 06:50:53', NULL),
(104, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-23 06:53:00', NULL),
(105, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'secretaria@gmail.com login with IP Address 127.0.0.1', '', 3, '2022-10-23 06:53:09', NULL),
(106, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'secretaria@gmail.com logout', '', 3, '2022-10-23 06:53:19', NULL),
(107, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-23 06:54:58', NULL),
(108, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-23 06:55:26', NULL),
(109, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-24 00:25:37', NULL),
(110, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-24 00:26:16', NULL),
(111, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-02 19:09:27', NULL),
(112, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:11:49', NULL),
(113, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:11:56', NULL),
(114, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:12:02', NULL),
(115, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:12:07', NULL),
(116, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:12:13', NULL),
(117, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:12:23', NULL),
(118, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:12:29', NULL),
(119, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:12:35', NULL),
(120, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:12:43', NULL),
(121, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:12:48', NULL),
(122, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:12:55', NULL),
(123, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:13:01', NULL),
(124, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:13:05', NULL),
(125, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:13:14', NULL),
(126, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:13:21', NULL),
(127, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:13:26', NULL),
(128, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:13:31', NULL),
(129, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:13:36', NULL),
(130, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:13:41', NULL),
(131, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:13:46', NULL),
(132, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:14:31', NULL),
(133, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:14:38', NULL),
(134, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:14:42', NULL),
(135, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:14:49', NULL),
(136, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:14:54', NULL),
(137, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:15:00', NULL),
(138, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:15:08', NULL),
(139, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:15:18', NULL),
(140, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:15:23', NULL),
(141, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:15:29', NULL),
(142, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:15:39', NULL),
(143, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:15:44', NULL),
(144, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:15:51', NULL),
(145, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:15:57', NULL),
(146, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:16:02', NULL),
(147, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:16:22', NULL),
(148, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/empleados/add-save', 'Add New Data  at Empleados', '', 1, '2022-11-02 19:40:54', NULL),
(149, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/cms_users/edit-save/1', 'Update data Super Admin at users', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td></td><td>uploads/1/2022-11/logo_search_grid_1x_1.png</td></tr><tr><td>password</td><td>$2y$10$2g5xVnibrMIUJNY5OWblbOnDes8OdSVwOMjfxStw5EQFDk2PbWf5G</td><td>$2y$10$tfhyD7L6fxmBRojApqjpxOR5uaeuGGSaWg71XFFNY2pBJBowWxd2y</td></tr></tbody></table>', 1, '2022-11-02 19:51:05', NULL),
(150, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/cargos/add-save', 'Add New Data  at Cargos', '', 1, '2022-11-02 19:54:00', NULL),
(151, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/empleados/edit-save/3', 'Update data  at Empleados', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>cargo_id</td><td>5</td><td>10</td></tr></tbody></table>', 1, '2022-11-02 19:54:24', NULL),
(152, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/module_generator/delete/29', 'Delete data Contratos at Module Generator', '', 1, '2022-11-02 20:13:21', NULL),
(153, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/contratos30/add-save', 'Add New Data  at Contratos', '', 1, '2022-11-02 20:36:29', NULL),
(154, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/contratos30/add-save', 'Add New Data  at Contratos', '', 1, '2022-11-02 20:48:09', NULL),
(155, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/empleados/edit-save/3', 'Update data  at Empleados', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>cargo_id</td><td>10</td><td>3</td></tr></tbody></table>', 1, '2022-11-02 20:48:57', NULL),
(156, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/contratos30/add-save', 'Add New Data  at Contratos', '', 1, '2022-11-02 20:49:24', NULL),
(157, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/empleados/edit-save/3', 'Update data  at Empleados', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>cargo_id</td><td>3</td><td>10</td></tr></tbody></table>', 1, '2022-11-02 23:48:00', NULL),
(158, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/ordenes/add-save', 'Add New Data  at Ordenes', '', 1, '2022-11-02 23:58:17', NULL),
(159, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/estados/add-save', 'Add New Data  at Estados', '', 1, '2022-11-03 00:26:50', NULL),
(160, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/estados/add-save', 'Add New Data  at Estados', '', 1, '2022-11-03 00:27:04', NULL),
(161, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data Division Politica at Menu Management', '', 1, '2022-11-03 01:00:46', NULL),
(162, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data Ordenes at Menu Management', '', 1, '2022-11-03 01:03:52', NULL),
(163, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data Empleados at Menu Management', '', 1, '2022-11-03 01:11:45', NULL),
(164, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/27', 'Update data Empleados at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td>normal</td><td>red</td></tr></tbody></table>', 1, '2022-11-03 01:12:43', NULL),
(165, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/27', 'Update data Empleados at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td>red</td><td>yellow</td></tr></tbody></table>', 1, '2022-11-03 01:12:53', NULL),
(166, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/18', 'Update data Pagos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-money</td></tr><tr><td>sorting</td><td>12</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:13:43', NULL),
(167, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/6', 'Update data Tipo de Pagos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-money</td><td>fa fa-credit-card</td></tr><tr><td>sorting</td><td>9</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:14:03', NULL),
(168, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/19', 'Update data users at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-user</td></tr><tr><td>sorting</td><td>13</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:14:33', NULL),
(169, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/4', 'Update data Estados at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>icon</td><td>fa fa-file</td><td>fa fa-files-o</td></tr><tr><td>sorting</td><td>8</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:15:28', NULL),
(170, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/4', 'Update data Estados at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td>normal</td><td>green</td></tr><tr><td>sorting</td><td>8</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:15:46', NULL),
(171, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/1', 'Update data Planes y Servicios at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>aqua</td></tr><tr><td>sorting</td><td>7</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:16:07', NULL),
(172, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/23', 'Update data Contratos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>muted</td></tr><tr><td>sorting</td><td>14</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:16:39', NULL),
(173, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/23', 'Update data Contratos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td>muted</td><td>green</td></tr><tr><td>sorting</td><td>14</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:17:02', NULL),
(174, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/24', 'Update data Ordenes at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-file-text</td></tr><tr><td>parent_id</td><td>26</td><td></td></tr><tr><td>sorting</td><td>3</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:21:51', NULL),
(175, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/6', 'Update data Tipo de Pagos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td>normal</td><td>green</td></tr><tr><td>sorting</td><td>9</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:22:38', NULL),
(176, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/15', 'Update data Clientes at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>yellow</td></tr><tr><td>sorting</td><td>10</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:22:56', NULL);
INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `details`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(177, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/16', 'Update data Productos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>aqua</td></tr><tr><td>sorting</td><td>11</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:23:14', NULL),
(178, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/18', 'Update data Pagos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td>normal</td><td>green</td></tr><tr><td>sorting</td><td>12</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:24:08', NULL),
(179, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/19', 'Update data users at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td>normal</td><td>yellow</td></tr><tr><td>sorting</td><td>13</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:24:26', NULL),
(180, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data Pagos at Menu Management', '', 1, '2022-11-03 01:25:03', NULL),
(181, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/15', 'Update data Clientes at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>10</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:30:11', NULL),
(182, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/23', 'Update data Contratos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>13</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:30:47', NULL),
(183, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/users/add-save', 'Add New Data Vendedor at Users Management', '', 1, '2022-11-03 01:31:41', NULL),
(184, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/users/delete-image', 'Delete the image of Vendedor at Users Management', '', 1, '2022-11-03 01:31:59', NULL),
(185, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/users/edit-save/2', 'Update data Vendedor at Users Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td></td><td>uploads/1/2022-11/logo_search_grid_1x_5.png</td></tr><tr><td>password</td><td>$2y$10$IlLmK77dpdXxaXQLOXrUsetvvZE4CIk9Rg1.H5H0KiYsMpR9F4pIK</td><td></td></tr><tr><td>status</td><td></td><td></td></tr></tbody></table>', 1, '2022-11-03 01:32:06', NULL),
(186, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 01:32:19', NULL),
(187, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'vendedor@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-11-03 01:32:24', NULL),
(188, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'vendedor@gmail.com logout', '', 2, '2022-11-03 01:32:47', NULL),
(189, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 01:32:59', NULL),
(190, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/statistic_builder/add-save', 'Add New Data Vendedor at Statistic Builder', '', 1, '2022-11-03 01:33:23', NULL),
(191, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data Vendedor at Menu Management', '', 1, '2022-11-03 01:37:16', NULL),
(192, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 01:37:27', NULL),
(193, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'vendedor@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-11-03 01:37:38', NULL),
(194, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'vendedor@gmail.com logout', '', 2, '2022-11-03 02:04:31', NULL),
(195, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 02:04:38', NULL),
(196, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 02:43:20', NULL),
(197, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'vendedor@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-11-03 02:43:35', NULL),
(198, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'vendedor@gmail.com logout', '', 2, '2022-11-03 02:45:43', NULL),
(199, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 02:45:49', NULL),
(200, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 02:54:03', NULL),
(201, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'vendedor@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-11-03 02:54:10', NULL),
(202, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'vendedor@gmail.com logout', '', 2, '2022-11-03 02:54:25', NULL),
(203, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 02:54:31', NULL),
(204, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 02:57:40', NULL),
(205, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'vendedor@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-11-03 02:57:45', NULL),
(206, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'vendedor@gmail.com logout', '', 2, '2022-11-03 02:58:00', NULL),
(207, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 02:58:07', NULL),
(208, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 03:01:56', NULL),
(209, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'vendedor@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-11-03 03:02:02', NULL),
(210, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', ' logout', '', NULL, '2022-11-03 15:18:58', NULL),
(211, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 15:19:08', NULL),
(212, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/ordenes32/edit-save/1', 'Update data  at Orden a Ejecutar', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>trabajo_id</td><td>1</td><td></td></tr><tr><td>servicio_id</td><td>1</td><td></td></tr><tr><td>tecnico_id</td><td>3</td><td></td></tr><tr><td>fecha_generacion</td><td>2022-11-02</td><td></td></tr><tr><td>supervisor_id</td><td>1</td><td></td></tr><tr><td>cliente_id</td><td>1</td><td></td></tr><tr><td>contrato_id</td><td>1</td><td></td></tr></tbody></table>', 1, '2022-11-03 16:13:14', NULL),
(213, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/ordenes32/edit-save/1', 'Update data  at Orden a Ejecutar', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>trabajo_id</td><td>1</td><td></td></tr><tr><td>servicio_id</td><td>1</td><td></td></tr><tr><td>tecnico_id</td><td>3</td><td></td></tr><tr><td>estado_id</td><td>2</td><td>7</td></tr><tr><td>fecha_generacion</td><td>2022-11-02</td><td></td></tr><tr><td>supervisor_id</td><td>1</td><td></td></tr><tr><td>cliente_id</td><td>1</td><td></td></tr><tr><td>contrato_id</td><td>1</td><td></td></tr></tbody></table>', 1, '2022-11-03 16:15:58', NULL),
(214, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/ordenes32/delete-image', 'Delete the image of 1 at Orden a Ejecutar', '', 1, '2022-11-03 16:16:57', NULL),
(215, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/ordenes32/edit-save/1', 'Update data  at Orden a Ejecutar', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>trabajo_id</td><td>1</td><td></td></tr><tr><td>servicio_id</td><td>1</td><td></td></tr><tr><td>tecnico_id</td><td>3</td><td></td></tr><tr><td>fecha_generacion</td><td>2022-11-02</td><td></td></tr><tr><td>supervisor_id</td><td>1</td><td></td></tr><tr><td>cliente_id</td><td>1</td><td></td></tr><tr><td>contrato_id</td><td>1</td><td></td></tr></tbody></table>', 1, '2022-11-03 16:17:04', NULL),
(216, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/ordenes32/edit-save/1', 'Update data  at Orden a Ejecutar', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>trabajo_id</td><td>1</td><td></td></tr><tr><td>servicio_id</td><td>1</td><td></td></tr><tr><td>tecnico_id</td><td>3</td><td></td></tr><tr><td>estado_id</td><td>7</td><td>8</td></tr><tr><td>fecha_generacion</td><td>2022-11-02</td><td></td></tr><tr><td>supervisor_id</td><td>1</td><td></td></tr><tr><td>evidencias</td><td></td><td>uploads/1/2022-11/tarea_09_2021.pdf</td></tr><tr><td>cliente_id</td><td>1</td><td></td></tr><tr><td>contrato_id</td><td>1</td><td></td></tr></tbody></table>', 1, '2022-11-03 16:17:29', NULL),
(217, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/ordenes/edit-save/1', 'Update data  at Ordenes de Trabajo', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2022-11-03 16:20:24', NULL),
(218, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/30', 'Update data Orden a Ejecutar at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-files-o</td></tr><tr><td>parent_id</td><td>26</td><td></td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2022-11-03 16:22:01', NULL),
(219, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/users/add-save', 'Add New Data Tecnico at Users Management', '', 1, '2022-11-03 16:28:29', NULL),
(220, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/30', 'Update data Orden a Ejecutar at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>parent_id</td><td>26</td><td></td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2022-11-03 16:29:06', NULL),
(221, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 16:29:10', NULL),
(222, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'tecnico@tecnico.com login with IP Address 127.0.0.1', '', 3, '2022-11-03 16:29:13', NULL),
(223, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'tecnico@tecnico.com logout', '', 3, '2022-11-03 16:29:34', NULL),
(224, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 16:29:41', NULL),
(225, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/30', 'Update data Orden a Ejecutar at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>parent_id</td><td>26</td><td></td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2022-11-03 16:30:14', NULL),
(226, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 16:31:04', NULL),
(227, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'tecnico@tecnico.com login with IP Address 127.0.0.1', '', 3, '2022-11-03 16:31:10', NULL),
(228, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'tecnico@tecnico.com logout', '', 3, '2022-11-03 16:31:24', NULL),
(229, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 16:31:30', NULL),
(230, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/26', 'Update data Ordenes at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2022-11-03 16:32:03', NULL),
(231, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 16:32:14', NULL),
(232, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'tecnico@tecnico.com login with IP Address 127.0.0.1', '', 3, '2022-11-03 16:32:20', NULL),
(233, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'tecnico@tecnico.com logout', '', 3, '2022-11-03 16:32:33', NULL),
(234, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 16:32:37', NULL),
(235, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/30', 'Update data Orden a Ejecutar at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>parent_id</td><td>26</td><td></td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2022-11-03 16:32:54', NULL),
(236, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 16:32:58', NULL),
(237, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'tecnico@tecnico.com login with IP Address 127.0.0.1', '', 3, '2022-11-03 16:33:02', NULL),
(238, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'tecnico@tecnico.com logout', '', 3, '2022-11-03 16:33:53', NULL),
(239, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'tecnico@tecnico.com login with IP Address 127.0.0.1', '', 3, '2022-11-03 16:33:55', NULL),
(240, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'tecnico@tecnico.com logout', '', 3, '2022-11-03 16:34:50', NULL),
(241, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'tecnico@tecnico.com login with IP Address 127.0.0.1', '', 3, '2022-11-03 16:34:54', NULL),
(242, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'tecnico@tecnico.com logout', '', 3, '2022-11-03 16:35:00', NULL),
(243, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 16:35:06', NULL),
(244, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/statistic_builder/add-save', 'Add New Data Tecnico at Statistic Builder', '', 1, '2022-11-03 16:58:22', NULL),
(245, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data Dashboard at Menu Management', '', 1, '2022-11-03 17:00:13', NULL),
(246, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 17:02:15', NULL),
(247, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'tecnico@tecnico.com login with IP Address 127.0.0.1', '', 3, '2022-11-03 17:02:21', NULL),
(248, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'tecnico@tecnico.com logout', '', 3, '2022-11-03 17:02:34', NULL),
(249, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 17:02:42', NULL),
(250, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/31', 'Update data Tecnico at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Dashboard</td><td>Tecnico</td></tr><tr><td>path</td><td>statistic_builder/show/vendedor</td><td>statistic_builder/show/tecnico</td></tr><tr><td>parent_id</td><td>0</td><td></td></tr><tr><td>sorting</td><td></td><td></td></tr></tbody></table>', 1, '2022-11-03 17:03:24', NULL),
(251, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 17:05:04', NULL),
(252, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'tecnico@tecnico.com login with IP Address 127.0.0.1', '', 3, '2022-11-03 17:05:10', NULL),
(253, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'tecnico@tecnico.com logout', '', 3, '2022-11-03 17:05:19', NULL),
(254, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 17:05:24', NULL),
(255, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/31', 'Update data Tecnico at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>parent_id</td><td>0</td><td></td></tr><tr><td>sorting</td><td></td><td></td></tr></tbody></table>', 1, '2022-11-03 17:05:42', NULL),
(256, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 17:05:47', NULL),
(257, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'tecnico@tecnico.com login with IP Address 127.0.0.1', '', 3, '2022-11-03 17:05:52', NULL),
(258, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'tecnico@tecnico.com logout', '', 3, '2022-11-03 17:06:40', NULL),
(259, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 17:06:47', NULL),
(260, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/ordenes/add-save', 'Add New Data  at Ordenes de Trabajo', '', 1, '2022-11-03 17:11:29', NULL),
(261, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/ordenes/add-save', 'Add New Data  at Ordenes de Trabajo', '', 1, '2022-11-03 17:20:53', NULL),
(262, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 17:34:34', NULL),
(263, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'tecnico@tecnico.com login with IP Address 127.0.0.1', '', 3, '2022-11-03 17:34:39', NULL),
(264, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/ordenes32/edit-save/3', 'Update data  at Orden a Ejecutar', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>estado_id</td><td>7</td><td>8</td></tr><tr><td>fecha_generacion</td><td>2022-11-03</td><td></td></tr><tr><td>comentario</td><td></td><td>Se realizo instalacion de servicio</td></tr><tr><td>evidencias</td><td></td><td>uploads/3/2022-11/tarea_09_2021.pdf</td></tr><tr><td>fecha_verificacion</td><td></td><td>2022-11-30</td></tr></tbody></table>', 3, '2022-11-03 17:35:50', NULL),
(265, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'tecnico@tecnico.com logout', '', 3, '2022-11-03 18:22:57', NULL),
(266, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 18:23:02', NULL),
(267, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 18:51:48', NULL),
(268, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'tecnico@tecnico.com login with IP Address 127.0.0.1', '', 3, '2022-11-03 18:52:03', NULL),
(269, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'tecnico@tecnico.com logout', '', 3, '2022-11-03 18:54:18', NULL),
(270, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 18:54:22', NULL),
(271, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/statistic_builder/edit-save/1', 'Update data Principal at Statistic Builder', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Prueba</td><td>Principal</td></tr><tr><td>slug</td><td>prueba</td><td></td></tr></tbody></table>', 1, '2022-11-03 18:55:01', NULL),
(272, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/contratos30/add-save', 'Add New Data  at Contratos', '', 1, '2022-11-03 20:39:14', NULL),
(273, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/26', 'Update data Ordenes at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2022-11-03 20:43:37', NULL),
(274, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/30', 'Update data Orden a Ejecutar at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>parent_id</td><td>26</td><td></td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2022-11-03 20:43:56', NULL),
(275, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/24', 'Update data Ordenes at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>parent_id</td><td>26</td><td></td></tr><tr><td>sorting</td><td>3</td><td></td></tr></tbody></table>', 1, '2022-11-03 20:44:07', NULL),
(276, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/23', 'Update data Contratos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>14</td><td></td></tr></tbody></table>', 1, '2022-11-03 20:44:29', NULL),
(277, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/users/add-save', 'Add New Data Supervisor at Users Management', '', 1, '2022-11-03 20:45:18', NULL),
(278, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 20:45:30', NULL),
(279, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'supervisor@supervisor.com login with IP Address 127.0.0.1', '', 4, '2022-11-03 20:45:33', NULL),
(280, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'supervisor@supervisor.com logout', '', 4, '2022-11-03 20:51:01', NULL),
(281, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 20:51:06', NULL),
(282, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/statistic_builder/add-save', 'Add New Data Supervisor at Statistic Builder', '', 1, '2022-11-03 20:54:35', NULL),
(283, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data Supervisor at Menu Management', '', 1, '2022-11-03 21:04:21', NULL),
(284, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 21:04:26', NULL),
(285, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'supervisor@supervisor.com login with IP Address 127.0.0.1', '', 4, '2022-11-03 21:04:31', NULL),
(286, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'supervisor@supervisor.com logout', '', 4, '2022-11-03 21:04:53', NULL),
(287, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 21:05:01', NULL),
(288, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/contratos30/add-save', 'Add New Data  at Contratos', '', 1, '2022-11-03 21:10:52', NULL),
(289, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/27', 'Update data Empleados at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>3</td><td></td></tr></tbody></table>', 1, '2022-11-03 21:20:07', NULL),
(290, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/7', 'Update data Cargos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>parent_id</td><td>27</td><td></td></tr><tr><td>sorting</td><td>2</td><td></td></tr></tbody></table>', 1, '2022-11-03 21:20:16', NULL),
(291, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/9', 'Update data Empleados at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>parent_id</td><td>27</td><td></td></tr></tbody></table>', 1, '2022-11-03 21:20:25', NULL),
(292, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/users/add-save', 'Add New Data Recursos Humanos at Users Management', '', 1, '2022-11-03 21:21:57', NULL),
(293, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 21:22:08', NULL),
(294, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'recursos@recursos.com login with IP Address 127.0.0.1', '', 5, '2022-11-03 21:22:10', NULL),
(295, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'recursos@recursos.com logout', '', 5, '2022-11-03 21:23:05', NULL),
(296, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 21:23:10', NULL),
(297, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/statistic_builder/add-save', 'Add New Data Recursos Humanos at Statistic Builder', '', 1, '2022-11-03 21:24:33', NULL),
(298, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data Recursos Humanos at Menu Management', '', 1, '2022-11-03 21:48:19', NULL),
(299, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 21:48:24', NULL),
(300, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'recursos@recursos.com login with IP Address 127.0.0.1', '', 5, '2022-11-03 21:48:28', NULL),
(301, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'recursos@recursos.com logout', '', 5, '2022-11-03 21:49:17', NULL),
(302, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 21:49:26', NULL),
(303, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/productos/add-save', 'Add New Data  at Productos', '', 1, '2022-11-03 21:52:34', NULL),
(304, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/empleados/add-save', 'Add New Data  at Empleados', '', 1, '2022-11-03 21:54:07', NULL),
(305, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data Cartera at Menu Management', '', 1, '2022-11-03 21:55:02', NULL),
(306, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 21:56:01', NULL),
(307, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'vendedor@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-11-03 21:56:04', NULL),
(308, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'vendedor@gmail.com logout', '', 2, '2022-11-03 21:56:11', NULL),
(309, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'vendedor@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-11-03 21:57:24', NULL),
(310, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'vendedor@gmail.com logout', '', 2, '2022-11-03 21:57:29', NULL),
(311, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 21:57:34', NULL),
(312, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/contratos33/add-save', 'Add New Data  at Servicios_Online', '', 1, '2022-11-03 22:05:26', NULL),
(313, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/1', 'Update data Planes y Servicios at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>13</td><td></td></tr></tbody></table>', 1, '2022-11-03 22:11:39', NULL),
(314, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/16', 'Update data Productos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>15</td><td></td></tr></tbody></table>', 1, '2022-11-03 22:12:34', NULL),
(315, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/18', 'Update data Pagos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>parent_id</td><td>28</td><td></td></tr><tr><td>sorting</td><td>2</td><td></td></tr></tbody></table>', 1, '2022-11-03 22:13:05', NULL),
(316, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/users/add-save', 'Add New Data Cliente Fiberhome at Users Management', '', 1, '2022-11-03 22:15:35', NULL),
(317, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 22:15:42', NULL),
(318, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'cliente@cliente.com login with IP Address 127.0.0.1', '', 6, '2022-11-03 22:15:44', NULL),
(319, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'cliente@cliente.com logout', '', 6, '2022-11-03 22:16:18', NULL),
(320, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 22:16:24', NULL),
(321, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/35', 'Update data Servicios_Online at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>sorting</td><td>18</td><td></td></tr></tbody></table>', 1, '2022-11-03 22:18:31', NULL),
(322, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 22:18:40', NULL),
(323, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'cliente@cliente.com login with IP Address 127.0.0.1', '', 6, '2022-11-03 22:18:48', NULL),
(324, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/contratos33', 'Try view the data :name at Servicios_Online', '', 6, '2022-11-03 22:18:52', NULL),
(325, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'cliente@cliente.com logout', '', 6, '2022-11-03 22:18:58', NULL),
(326, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 22:19:04', NULL),
(327, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 22:19:38', NULL),
(328, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'cliente@cliente.com login with IP Address 127.0.0.1', '', 6, '2022-11-03 22:19:44', NULL),
(329, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'cliente@cliente.com logout', '', 6, '2022-11-03 22:20:19', NULL),
(330, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 22:20:25', NULL),
(331, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/28', 'Update data Pagos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>6</td><td></td></tr></tbody></table>', 1, '2022-11-03 22:20:43', NULL),
(332, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 22:20:53', NULL),
(333, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'cliente@cliente.com login with IP Address 127.0.0.1', '', 6, '2022-11-03 22:20:57', NULL),
(334, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'cliente@cliente.com logout', '', 6, '2022-11-03 22:21:22', NULL),
(335, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 22:21:27', NULL),
(336, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/clientes/add-save', 'Add New Data  at Clientes', '', 1, '2022-11-03 22:26:11', NULL),
(337, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 22:27:19', NULL),
(338, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'cliente@cliente.com login with IP Address 127.0.0.1', '', 6, '2022-11-03 22:27:23', NULL),
(339, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'cliente@cliente.com logout', '', 6, '2022-11-03 22:27:33', NULL),
(340, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 22:27:38', NULL),
(341, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 22:28:27', NULL),
(1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-08 04:13:45', NULL),
(2, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-08 04:14:01', NULL),
(3, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-08 05:00:03', NULL);
INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `details`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(4, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-08 05:00:10', NULL),
(5, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-08 05:00:45', NULL),
(6, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-08 05:01:32', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_menus`
--

CREATE TABLE `cms_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'url',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_dashboard` tinyint(1) NOT NULL DEFAULT '0',
  `id_cms_privileges` int(11) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_menus`
--

INSERT INTO `cms_menus` (`id`, `name`, `type`, `path`, `color`, `icon`, `parent_id`, `is_active`, `is_dashboard`, `id_cms_privileges`, `sorting`, `created_at`, `updated_at`) VALUES
(1, 'Planes y Servicios', 'Route', 'AdminPlanesControllerGetIndex', 'aqua', 'fa fa-file-movie-o', 0, 1, 0, 1, 13, '2022-10-19 00:40:50', '2022-11-03 22:11:39'),
(2, 'Departamentos de Guatemala', 'Route', 'AdminDepartamentos1ControllerGetIndex', NULL, 'fa fa-map-marker', 25, 1, 0, 1, 1, '2022-10-19 00:41:54', NULL),
(3, 'Municipios de Guatemala', 'Route', 'AdminMunicipios1ControllerGetIndex', NULL, 'fa fa-map-marker', 25, 1, 0, 1, 2, '2022-10-19 00:42:50', NULL),
(4, 'Estados', 'Route', 'AdminEstados1ControllerGetIndex', 'green', 'fa fa-files-o', 0, 1, 0, 1, 14, '2022-10-19 00:49:51', '2022-11-03 01:15:46'),
(5, 'Tipos de Ordenes de Servicio', 'Route', 'AdminTipoordenesControllerGetIndex', NULL, 'fa fa-file', 26, 1, 0, 1, 1, '2022-10-19 00:55:17', NULL),
(6, 'Tipo de Pagos', 'Route', 'AdminTipopagos1ControllerGetIndex', 'green', 'fa fa-credit-card', 28, 1, 0, 1, 1, '2022-10-19 00:56:54', '2022-11-03 01:22:38'),
(7, 'Cargos', 'Route', 'AdminCargosControllerGetIndex', 'normal', 'fa fa-file-text', 27, 1, 0, 1, 2, '2022-10-19 01:38:42', '2022-11-03 21:20:16'),
(8, 'Tipos de Orden de Trabajo', 'Route', 'AdminTipotrabajosControllerGetIndex', NULL, 'fa fa-file', 26, 1, 0, 1, 2, '2022-10-19 02:11:51', NULL),
(9, 'Empleados', 'Route', 'AdminEmpleadosControllerGetIndex', 'normal', 'fa fa-user', 27, 1, 0, 1, 1, '2022-10-19 02:50:35', '2022-11-03 21:20:25'),
(13, 'prueba2', 'Statistic', 'statistic_builder/show/prueba', 'red', 'fa fa-search', 0, 1, 1, 1, 12, '2022-10-19 07:29:25', '2022-10-19 07:31:45'),
(15, 'Clientes', 'Route', 'AdminClientesControllerGetIndex', 'yellow', 'fa fa-user-plus', 34, 1, 0, 1, 1, '2022-10-19 12:32:58', '2022-11-03 01:30:11'),
(16, 'Productos', 'Route', 'AdminProductosControllerGetIndex', 'aqua', 'fa fa-product-hunt', 0, 1, 0, 1, 15, '2022-10-19 12:49:18', '2022-11-03 22:12:34'),
(18, 'Pagos', 'Route', 'AdminPagosControllerGetIndex', 'green', 'fa fa-money', 28, 1, 0, 1, 2, '2022-10-20 07:38:21', '2022-11-03 22:13:05'),
(19, 'users', 'Route', 'AdminCmsUsers1ControllerGetIndex', 'yellow', 'fa fa-user', 0, 1, 0, 1, 16, '2022-10-20 08:41:38', '2022-11-03 01:24:26'),
(21, 'Empleados', 'Module', 'empleados', 'normal', 'fa fa-users', 0, 1, 0, 1, 9, '2022-10-22 04:22:23', NULL),
(22, 'Gogogle', 'URL', 'https://www.google.com/', 'normal', 'fa fa-pagelines', 0, 1, 0, 1, 10, '2022-10-22 04:23:08', NULL),
(23, 'Contratos', 'Route', 'AdminContratos30ControllerGetIndex', 'green', 'fa fa-legal', 0, 1, 0, 1, 17, '2022-11-02 20:13:40', '2022-11-03 20:44:29'),
(24, 'Ordenes', 'Route', 'AdminOrdenesControllerGetIndex', 'normal', 'fa fa-file-text', 26, 1, 0, 1, 3, '2022-11-02 23:27:55', '2022-11-03 20:44:06'),
(25, 'Division Politica', 'URL', '#', 'aqua', 'fa fa-map-marker', 0, 1, 0, 1, 11, '2022-11-03 01:00:46', NULL),
(26, 'Ordenes', 'URL', '#', 'green', 'fa fa-file', 0, 1, 0, 1, 8, '2022-11-03 01:03:52', '2022-11-03 20:43:37'),
(27, 'Empleados', 'URL', '#', 'yellow', 'fa fa-users', 0, 1, 0, 1, 7, '2022-11-03 01:11:45', '2022-11-03 21:20:07'),
(28, 'Pagos', 'URL', '#', 'green', 'fa fa-money', 0, 1, 0, 1, 6, '2022-11-03 01:25:03', '2022-11-03 22:20:43'),
(29, 'Vendedor', 'Statistic', 'statistic_builder/show/vendedor', 'aqua', 'fa fa-dashboard', 0, 1, 1, 1, 5, '2022-11-03 01:37:16', NULL),
(30, 'Orden a Ejecutar', 'Route', 'AdminOrdenes32ControllerGetIndex', 'normal', 'fa fa-files-o', 26, 1, 0, 1, 4, '2022-11-03 15:22:32', '2022-11-03 20:43:56'),
(31, 'Tecnico', 'Statistic', 'statistic_builder/show/tecnico', 'green', 'fa fa-dashboard', 0, 1, 1, 1, 1, '2022-11-03 17:00:13', '2022-11-03 17:05:42'),
(32, 'Supervisor', 'Statistic', 'statistic_builder/show/supervisor', 'green', 'fa fa-dashboard', 0, 1, 1, 1, 2, '2022-11-03 21:04:21', NULL),
(33, 'Recursos Humanos', 'Statistic', 'statistic_builder/show/recursos-humanos', 'green', 'fa fa-dashboard', 0, 1, 1, 1, 3, '2022-11-03 21:48:19', NULL),
(34, 'Cartera', 'URL', '#', 'green', 'fa fa-users', 0, 1, 0, 1, 4, '2022-11-03 21:55:02', NULL),
(35, 'Servicios_Online', 'Route', 'AdminContratos33ControllerGetIndex', 'normal', 'fa fa-star', 0, 1, 0, 1, 18, '2022-11-03 21:58:51', '2022-11-03 22:18:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_menus_privileges`
--

CREATE TABLE `cms_menus_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_menus` int(11) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_menus_privileges`
--

INSERT INTO `cms_menus_privileges` (`id`, `id_cms_menus`, `id_cms_privileges`) VALUES
(2, 2, 1),
(3, 3, 1),
(6, 5, 1),
(9, 8, 1),
(11, 10, 1),
(12, 11, 1),
(13, 12, 1),
(16, 13, 1),
(17, 14, 1),
(20, 17, 1),
(23, 20, 1),
(24, 21, 2),
(25, 22, 2),
(28, 25, 1),
(37, 4, 1),
(42, 6, 1),
(46, 19, 1),
(48, 15, 1),
(49, 15, 3),
(52, 29, 3),
(64, 31, 4),
(65, 26, 1),
(66, 26, 5),
(67, 26, 4),
(68, 30, 1),
(69, 30, 5),
(70, 30, 4),
(71, 24, 1),
(72, 24, 5),
(73, 23, 1),
(74, 23, 5),
(75, 23, 3),
(76, 32, 5),
(77, 27, 6),
(78, 27, 1),
(79, 7, 6),
(80, 7, 1),
(81, 9, 6),
(82, 9, 1),
(83, 33, 6),
(84, 34, 1),
(85, 34, 3),
(87, 1, 7),
(88, 1, 1),
(89, 16, 7),
(90, 16, 1),
(91, 18, 7),
(92, 18, 1),
(93, 35, 7),
(94, 35, 1),
(95, 28, 7),
(96, 28, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_moduls`
--

CREATE TABLE `cms_moduls` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_moduls`
--

INSERT INTO `cms_moduls` (`id`, `name`, `icon`, `path`, `table_name`, `controller`, `is_protected`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Notifications', 'fa fa-cog', 'notifications', 'cms_notifications', 'NotificationsController', 1, 1, '2022-10-19 00:36:28', NULL, NULL),
(2, 'Privileges', 'fa fa-cog', 'privileges', 'cms_privileges', 'PrivilegesController', 1, 1, '2022-10-19 00:36:28', NULL, NULL),
(3, 'Privileges Roles', 'fa fa-cog', 'privileges_roles', 'cms_privileges_roles', 'PrivilegesRolesController', 1, 1, '2022-10-19 00:36:28', NULL, NULL),
(4, 'Users Management', 'fa fa-users', 'users', 'cms_users', 'AdminCmsUsersController', 0, 1, '2022-10-19 00:36:28', NULL, NULL),
(5, 'Settings', 'fa fa-cog', 'settings', 'cms_settings', 'SettingsController', 1, 1, '2022-10-19 00:36:28', NULL, NULL),
(6, 'Module Generator', 'fa fa-database', 'module_generator', 'cms_moduls', 'ModulsController', 1, 1, '2022-10-19 00:36:28', NULL, NULL),
(7, 'Menu Management', 'fa fa-bars', 'menu_management', 'cms_menus', 'MenusController', 1, 1, '2022-10-19 00:36:28', NULL, NULL),
(8, 'Email Templates', 'fa fa-envelope-o', 'email_templates', 'cms_email_templates', 'EmailTemplatesController', 1, 1, '2022-10-19 00:36:28', NULL, NULL),
(9, 'Statistic Builder', 'fa fa-dashboard', 'statistic_builder', 'cms_statistics', 'StatisticBuilderController', 1, 1, '2022-10-19 00:36:28', NULL, NULL),
(10, 'API Generator', 'fa fa-cloud-download', 'api_generator', '', 'ApiCustomController', 1, 1, '2022-10-19 00:36:28', NULL, NULL),
(11, 'Log User Access', 'fa fa-flag-o', 'logs', 'cms_logs', 'LogsController', 1, 1, '2022-10-19 00:36:28', NULL, NULL),
(12, 'Planes y Servicios', 'fa fa-mobile-phone', 'planes', 'planes', 'AdminPlanesController', 0, 0, '2022-10-19 00:40:49', NULL, NULL),
(13, 'Departamentos de Guatemala', 'fa fa-map-marker', 'departamentos', 'departamentos', 'AdminDepartamentos1Controller', 0, 0, '2022-10-19 00:41:54', NULL, NULL),
(14, 'Municipios de Guatemala', 'fa fa-map-marker', 'municipios', 'municipios', 'AdminMunicipios1Controller', 0, 0, '2022-10-19 00:42:50', NULL, NULL),
(15, 'Estados', 'fa fa-file', 'estados', 'estados', 'AdminEstados1Controller', 0, 0, '2022-10-19 00:49:51', NULL, NULL),
(16, 'Tipos de Ordenes de Servicio', 'fa fa-file', 'tipoordenes', 'tipoordenes', 'AdminTipoordenesController', 0, 0, '2022-10-19 00:55:17', NULL, NULL),
(17, 'Tipo de Pagos', 'fa fa-money', 'tipopagos', 'tipopagos', 'AdminTipopagos1Controller', 0, 0, '2022-10-19 00:56:54', NULL, NULL),
(18, 'Cargos', 'fa fa-file-text', 'cargos', 'cargos', 'AdminCargosController', 0, 0, '2022-10-19 01:38:42', NULL, NULL),
(19, 'Tipos de Orden de Trabajo', 'fa fa-file', 'tipotrabajos', 'tipotrabajos', 'AdminTipotrabajosController', 0, 0, '2022-10-19 02:11:51', NULL, NULL),
(20, 'Empleados', 'fa fa-user', 'empleados', 'empleados', 'AdminEmpleadosController', 0, 0, '2022-10-19 02:50:35', NULL, NULL),
(21, 'Jerarquia Administrativa', 'fa fa-empire', 'jerarquias', 'jerarquias', 'AdminJerarquiasController', 0, 0, '2022-10-19 03:18:02', NULL, '2022-10-20 11:56:36'),
(22, 'Clientes', 'fa fa-user-plus', 'clientes', 'clientes', 'AdminClientesController', 0, 0, '2022-10-19 04:46:05', NULL, '2022-10-19 10:01:15'),
(23, 'Clientes', 'fa fa-user-plus', 'clientes23', 'clientes', 'AdminClientes23Controller', 0, 0, '2022-10-19 10:01:38', NULL, '2022-10-19 12:31:29'),
(24, 'Clientes', 'fa fa-user-plus', 'clientes', 'clientes', 'AdminClientesController', 0, 0, '2022-10-19 12:32:58', NULL, NULL),
(25, 'Productos', 'fa fa-product-hunt', 'productos', 'productos', 'AdminProductosController', 0, 0, '2022-10-19 12:49:18', NULL, NULL),
(26, 'Contratos de Servicios', 'fa fa-cog', 'contratos', 'contratos', 'AdminContratosController', 0, 0, '2022-10-19 13:51:38', NULL, '2022-10-20 10:17:56'),
(27, 'Pagos', 'fa fa-glass', 'pagos', 'pagos', 'AdminPagosController', 0, 0, '2022-10-20 07:38:21', NULL, NULL),
(28, 'users', 'fa fa-glass', 'cms_users', 'cms_users', 'AdminCmsUsers1Controller', 0, 0, '2022-10-20 08:41:38', NULL, NULL),
(29, 'Contratos', 'fa fa-file', 'contratos29', 'contratos', 'AdminContratos29Controller', 0, 0, '2022-10-20 10:19:16', NULL, '2022-11-02 20:13:21'),
(30, 'Contratos', 'fa fa-legal', 'contratos30', 'contratos', 'AdminContratos30Controller', 0, 0, '2022-11-02 20:13:40', NULL, NULL),
(31, 'Ordenes de Trabajo', 'fa fa-file-word-o', 'ordenes', 'ordenes', 'AdminOrdenesController', 0, 0, '2022-11-02 23:27:55', NULL, NULL),
(32, 'Orden a Ejecutar', 'fa fa-glass', 'ordenes32', 'ordenes', 'AdminOrdenes32Controller', 0, 0, '2022-11-03 15:22:31', NULL, NULL),
(33, 'Servicios_Online', 'fa fa-star', 'contratos33', 'contratos', 'AdminContratos33Controller', 0, 0, '2022-11-03 21:58:51', NULL, NULL),
(1, 'Notifications', 'fa fa-cog', 'notifications', 'cms_notifications', 'NotificationsController', 1, 1, '2022-11-08 04:12:45', NULL, NULL),
(2, 'Privileges', 'fa fa-cog', 'privileges', 'cms_privileges', 'PrivilegesController', 1, 1, '2022-11-08 04:12:45', NULL, NULL),
(3, 'Privileges Roles', 'fa fa-cog', 'privileges_roles', 'cms_privileges_roles', 'PrivilegesRolesController', 1, 1, '2022-11-08 04:12:45', NULL, NULL),
(4, 'Users Management', 'fa fa-users', 'users', 'users', 'AdminCmsUsersController', 0, 1, '2022-11-08 04:12:45', NULL, NULL),
(5, 'Settings', 'fa fa-cog', 'settings', 'cms_settings', 'SettingsController', 1, 1, '2022-11-08 04:12:45', NULL, NULL),
(6, 'Module Generator', 'fa fa-database', 'module_generator', 'cms_moduls', 'ModulsController', 1, 1, '2022-11-08 04:12:45', NULL, NULL),
(7, 'Menu Management', 'fa fa-bars', 'menu_management', 'cms_menus', 'MenusController', 1, 1, '2022-11-08 04:12:45', NULL, NULL),
(8, 'Email Templates', 'fa fa-envelope-o', 'email_templates', 'cms_email_templates', 'EmailTemplatesController', 1, 1, '2022-11-08 04:12:45', NULL, NULL),
(9, 'Statistic Builder', 'fa fa-dashboard', 'statistic_builder', 'cms_statistics', 'StatisticBuilderController', 1, 1, '2022-11-08 04:12:45', NULL, NULL),
(10, 'API Generator', 'fa fa-cloud-download', 'api_generator', '', 'ApiCustomController', 1, 1, '2022-11-08 04:12:45', NULL, NULL),
(11, 'Log User Access', 'fa fa-flag-o', 'logs', 'cms_logs', 'LogsController', 1, 1, '2022-11-08 04:12:45', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_notifications`
--

CREATE TABLE `cms_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_users` int(11) DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_privileges`
--

CREATE TABLE `cms_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_superadmin` tinyint(1) DEFAULT NULL,
  `theme_color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_privileges`
--

INSERT INTO `cms_privileges` (`id`, `name`, `is_superadmin`, `theme_color`, `created_at`, `updated_at`) VALUES
(1, 'Super Administrator', 1, 'skin-green', '2022-10-19 00:36:28', NULL),
(2, 'Secretaria', 0, 'skin-purple', NULL, NULL),
(3, 'Vendedor', 0, 'skin-yellow', NULL, NULL),
(4, 'Tecnico', 0, 'skin-red', NULL, NULL),
(5, 'Supervisor', 0, 'skin-yellow', NULL, NULL),
(6, 'RH', 0, 'skin-blue', NULL, NULL),
(7, 'Clientes', 0, 'skin-green', NULL, NULL),
(1, 'Super Administrator', 1, 'skin-red', '2022-11-08 04:12:45', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_privileges_roles`
--

CREATE TABLE `cms_privileges_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_visible` tinyint(1) DEFAULT NULL,
  `is_create` tinyint(1) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `is_edit` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `id_cms_moduls` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_privileges_roles`
--

INSERT INTO `cms_privileges_roles` (`id`, `is_visible`, `is_create`, `is_read`, `is_edit`, `is_delete`, `id_cms_privileges`, `id_cms_moduls`, `created_at`, `updated_at`) VALUES
(52, 1, 1, 1, 0, 0, 2, 20, NULL, NULL),
(69, 1, 1, 1, 1, 1, 1, 18, NULL, NULL),
(70, 1, 1, 1, 1, 1, 1, 24, NULL, NULL),
(71, 1, 1, 1, 1, 1, 1, 30, NULL, NULL),
(72, 1, 1, 1, 1, 1, 1, 13, NULL, NULL),
(73, 1, 1, 1, 1, 1, 1, 20, NULL, NULL),
(74, 1, 1, 1, 1, 1, 1, 15, NULL, NULL),
(75, 1, 1, 1, 1, 1, 1, 14, NULL, NULL),
(76, 1, 1, 1, 1, 1, 1, 27, NULL, NULL),
(77, 1, 1, 1, 1, 1, 1, 12, NULL, NULL),
(78, 1, 1, 1, 1, 1, 1, 25, NULL, NULL),
(79, 1, 1, 1, 1, 1, 1, 17, NULL, NULL),
(80, 1, 1, 1, 1, 1, 1, 19, NULL, NULL),
(81, 1, 1, 1, 1, 1, 1, 16, NULL, NULL),
(82, 1, 1, 1, 1, 1, 1, 28, NULL, NULL),
(83, 1, 1, 1, 1, 1, 1, 4, NULL, NULL),
(84, 1, 1, 1, 1, 1, 1, 31, NULL, NULL),
(85, 1, 1, 1, 1, 1, 3, 24, NULL, NULL),
(86, 1, 1, 1, 1, 1, 3, 30, NULL, NULL),
(87, 1, 1, 1, 1, 1, 1, 32, NULL, NULL),
(88, 1, 0, 1, 1, 0, 4, 32, NULL, NULL),
(89, 1, 1, 1, 1, 1, 5, 24, NULL, NULL),
(90, 1, 1, 1, 1, 1, 5, 30, NULL, NULL),
(91, 1, 1, 1, 1, 1, 5, 32, NULL, NULL),
(92, 1, 1, 1, 1, 1, 5, 31, NULL, NULL),
(93, 1, 1, 1, 1, 1, 6, 18, NULL, NULL),
(94, 1, 1, 1, 1, 1, 6, 20, NULL, NULL),
(95, 1, 1, 1, 1, 1, 1, 33, NULL, NULL),
(103, 0, 1, 0, 0, 0, 7, 24, NULL, NULL),
(104, 1, 1, 1, 0, 0, 7, 27, NULL, NULL),
(105, 1, 0, 0, 0, 0, 7, 12, NULL, NULL),
(106, 1, 0, 0, 0, 0, 7, 25, NULL, NULL),
(107, 1, 1, 0, 0, 0, 7, 33, NULL, NULL),
(1, 1, 0, 0, 0, 0, 1, 1, '2022-11-08 04:12:45', NULL),
(2, 1, 1, 1, 1, 1, 1, 2, '2022-11-08 04:12:45', NULL),
(3, 0, 1, 1, 1, 1, 1, 3, '2022-11-08 04:12:45', NULL),
(4, 1, 1, 1, 1, 1, 1, 4, '2022-11-08 04:12:45', NULL),
(5, 1, 1, 1, 1, 1, 1, 5, '2022-11-08 04:12:45', NULL),
(6, 1, 1, 1, 1, 1, 1, 6, '2022-11-08 04:12:45', NULL),
(7, 1, 1, 1, 1, 1, 1, 7, '2022-11-08 04:12:45', NULL),
(8, 1, 1, 1, 1, 1, 1, 8, '2022-11-08 04:12:45', NULL),
(9, 1, 1, 1, 1, 1, 1, 9, '2022-11-08 04:12:45', NULL),
(10, 1, 1, 1, 1, 1, 1, 10, '2022-11-08 04:12:45', NULL),
(11, 1, 0, 1, 0, 1, 1, 11, '2022-11-08 04:12:45', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_settings`
--

CREATE TABLE `cms_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `content_input_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dataenum` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `helper` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group_setting` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_settings`
--

INSERT INTO `cms_settings` (`id`, `name`, `content`, `content_input_type`, `dataenum`, `helper`, `created_at`, `updated_at`, `group_setting`, `label`) VALUES
(1, 'login_background_color', '#138F8B', 'text', NULL, 'Input hexacode', '2022-10-19 00:36:28', NULL, 'Login Register Style', 'Login Background Color'),
(2, 'login_font_color', '#138F8B', 'text', NULL, 'Input hexacode', '2022-10-19 00:36:28', NULL, 'Login Register Style', 'Login Font Color'),
(3, 'login_background_image', 'uploads/2022-10/98f758a54bd0e8110b291084e9b4f8a7.webp', 'upload_image', NULL, NULL, '2022-10-19 00:36:28', NULL, 'Login Register Style', 'Login Background Image'),
(4, 'email_sender', 'soportefundemi@gmail.com', 'text', NULL, NULL, '2022-10-19 00:36:28', NULL, 'Email Setting', 'Email Sender'),
(5, 'smtp_driver', 'smtp', 'select', 'smtp,mail,sendmail', NULL, '2022-10-19 00:36:28', NULL, 'Email Setting', 'Mail Driver'),
(6, 'smtp_host', 'smtp.gmail.com', 'text', NULL, NULL, '2022-10-19 00:36:28', NULL, 'Email Setting', 'SMTP Host'),
(7, 'smtp_port', '587', 'text', NULL, 'default 25', '2022-10-19 00:36:28', NULL, 'Email Setting', 'SMTP Port'),
(8, 'smtp_username', 'soportefundemi@gmail.com', 'text', NULL, NULL, '2022-10-19 00:36:28', NULL, 'Email Setting', 'SMTP Username'),
(9, 'smtp_password', 'sivictjhjjbarwiz', 'text', NULL, NULL, '2022-10-19 00:36:28', NULL, 'Email Setting', 'SMTP Password'),
(10, 'appname', 'Fiberhome S.A.', 'text', NULL, NULL, '2022-10-19 00:36:28', NULL, 'Application Setting', 'Application Name'),
(11, 'default_paper_size', 'Legal', 'text', NULL, 'Paper size, ex : A4, Legal, etc', '2022-10-19 00:36:28', NULL, 'Application Setting', 'Default Paper Print Size'),
(12, 'logo', 'uploads/2022-10/b2c6112f364d71e26f1eb13e9b4a449c.png', 'upload_image', NULL, NULL, '2022-10-19 00:36:28', NULL, 'Application Setting', 'Logo'),
(13, 'favicon', 'uploads/2022-10/846a58030c82f01daeb7d542e14c5f5c.png', 'upload_image', NULL, NULL, '2022-10-19 00:36:28', NULL, 'Application Setting', 'Favicon'),
(14, 'api_debug_mode', 'true', 'select', 'true,false', NULL, '2022-10-19 00:36:28', NULL, 'Application Setting', 'API Debug Mode'),
(15, 'google_api_key', 'AIzaSyDL3jkSEmDNZa-45BOp9ThnmCtM3NhSXOo', 'text', NULL, NULL, '2022-10-19 00:36:28', NULL, 'Application Setting', 'Google API Key'),
(16, 'google_fcm_key', NULL, 'text', NULL, NULL, '2022-10-19 00:36:28', NULL, 'Application Setting', 'Google FCM Key'),
(17, 'image', 'uploads/2022-10/7cea44f9cc978831a984eb23252c39ef.png', 'upload_image', 'abc', NULL, '2022-10-20 08:30:19', NULL, 'General Setting', 'Image'),
(18, 'image', 'uploads/2022-10/7cea44f9cc978831a984eb23252c39ef.png', 'upload_image', 'abc', 'abc', '2022-10-20 08:40:00', NULL, 'Login Register Style', 'Image'),
(1, 'login_background_color', NULL, 'text', NULL, 'Input hexacode', '2022-11-08 04:12:45', NULL, 'Login Register Style', 'Login Background Color'),
(2, 'login_font_color', NULL, 'text', NULL, 'Input hexacode', '2022-11-08 04:12:45', NULL, 'Login Register Style', 'Login Font Color'),
(3, 'login_background_image', 'uploads/2022-11/62f2035afc7fd253fcb47916ab106885.webp', 'upload_image', NULL, NULL, '2022-11-08 04:12:45', NULL, 'Login Register Style', 'Login Background Image'),
(4, 'email_sender', 'support@crudbooster.com', 'text', NULL, NULL, '2022-11-08 04:12:45', NULL, 'Email Setting', 'Email Sender'),
(5, 'smtp_driver', 'mail', 'select', 'smtp,mail,sendmail', NULL, '2022-11-08 04:12:45', NULL, 'Email Setting', 'Mail Driver'),
(6, 'smtp_host', '', 'text', NULL, NULL, '2022-11-08 04:12:45', NULL, 'Email Setting', 'SMTP Host'),
(7, 'smtp_port', '25', 'text', NULL, 'default 25', '2022-11-08 04:12:45', NULL, 'Email Setting', 'SMTP Port'),
(8, 'smtp_username', '', 'text', NULL, NULL, '2022-11-08 04:12:45', NULL, 'Email Setting', 'SMTP Username'),
(9, 'smtp_password', '', 'text', NULL, NULL, '2022-11-08 04:12:45', NULL, 'Email Setting', 'SMTP Password'),
(10, 'appname', 'CRUDBooster', 'text', NULL, NULL, '2022-11-08 04:12:45', NULL, 'Application Setting', 'Application Name'),
(11, 'default_paper_size', 'Legal', 'text', NULL, 'Paper size, ex : A4, Legal, etc', '2022-11-08 04:12:45', NULL, 'Application Setting', 'Default Paper Print Size'),
(12, 'logo', '', 'upload_image', NULL, NULL, '2022-11-08 04:12:45', NULL, 'Application Setting', 'Logo'),
(13, 'favicon', '', 'upload_image', NULL, NULL, '2022-11-08 04:12:45', NULL, 'Application Setting', 'Favicon'),
(14, 'api_debug_mode', 'true', 'select', 'true,false', NULL, '2022-11-08 04:12:45', NULL, 'Application Setting', 'API Debug Mode'),
(15, 'google_api_key', '', 'text', NULL, NULL, '2022-11-08 04:12:45', NULL, 'Application Setting', 'Google API Key'),
(16, 'google_fcm_key', '', 'text', NULL, NULL, '2022-11-08 04:12:45', NULL, 'Application Setting', 'Google FCM Key');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_statistics`
--

CREATE TABLE `cms_statistics` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_statistics`
--

INSERT INTO `cms_statistics` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Principal', 'prueba', '2022-10-19 01:13:10', '2022-11-03 18:55:01'),
(2, 'Vendedor', 'vendedor', '2022-11-03 01:33:23', NULL),
(3, 'Tecnico', 'tecnico', '2022-11-03 16:58:22', NULL),
(4, 'Supervisor', 'supervisor', '2022-11-03 20:54:35', NULL),
(5, 'Recursos Humanos', 'recursos-humanos', '2022-11-03 21:24:33', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_statistic_components`
--

CREATE TABLE `cms_statistic_components` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_statistics` int(11) DEFAULT NULL,
  `componentID` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `component_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_name` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `config` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_statistic_components`
--

INSERT INTO `cms_statistic_components` (`id`, `id_cms_statistics`, `componentID`, `component_name`, `area_name`, `sorting`, `name`, `config`, `created_at`, `updated_at`) VALUES
(5, 1, 'e814afb3f79478c0cfb6da119aa45c15', 'smallbox', 'area1', 0, NULL, '{\"name\":\"Usuarios\",\"icon\":\"ion-bag\",\"color\":\"bg-green\",\"link\":\"http:\\/\\/127.0.0.1:8001\\/admin\\/cms_users\",\"sql\":\"select COUNT(*) from users\"}', '2022-10-19 07:36:34', NULL),
(6, 1, '301cceb8217bfe6317f43e0656ad272e', 'smallbox', 'area2', 0, NULL, '{\"name\":\"Clientes\",\"icon\":\"ion-bag\",\"color\":\"bg-red\",\"link\":\"http:\\/\\/127.0.0.1:8001\\/admin\\/clientes\",\"sql\":\"select COUNT(*)  from clientes\"}', '2022-10-19 07:37:45', NULL),
(7, 1, '36d07d9463740398e882cbb5708f29aa', 'smallbox', 'area3', 0, NULL, '{\"name\":\"Empleados\",\"icon\":\"ion-bag\",\"color\":\"bg-aqua\",\"link\":\"http:\\/\\/127.0.0.1:8001\\/admin\\/empleados\",\"sql\":\"select COUNT(*)  from empleados\"}', '2022-10-19 07:41:01', NULL),
(8, 1, '13c06ddf82b2bd57867d0f7e64031e9a', 'smallbox', 'area4', 0, NULL, '{\"name\":\"Tipos de Ordenes\",\"icon\":\"ion-bag\",\"color\":\"bg-yellow\",\"link\":\"http:\\/\\/127.0.0.1:8001\\/admin\\/tipoordenes\",\"sql\":\"select COUNT(*)  from tipoordenes\"}', '2022-10-19 07:44:05', NULL),
(9, 1, 'cc3d46fca2b39d2d50a53db856601d4f', 'chartarea', NULL, 0, 'Untitled', NULL, '2022-10-20 07:05:55', NULL),
(21, 1, 'ed6f1c23eb3d2c058c5bdad966c309b7', 'table', 'area5', 0, NULL, '{\"name\":\"Ventas\",\"sql\":\"Select CONCAT(empleados.nombres,\\\" \\\", empleados.apellidos) as Nombre,COUNT(contratos.id)as Cantidad_de_Servicios,sum(productos.costo)as Facturado,(sum(productos.costo) *0.30)as Comision ,MONTH(contratos.created_at) as Mes, YEAR(contratos.created_at) as a\\u00f1o FROM productos, contratos, empleados WHERE contratos.vendedor_id = empleados.id GROUP BY YEAR(contratos.created_at), MONTH(contratos.created_at), Nombre,productos.costo\"}', '2022-11-02 21:02:00', NULL),
(22, 2, '991f71452007719e96a0376f9697979f', 'table', 'area5', 0, NULL, '{\"name\":\"Ventas\",\"sql\":\"Select CONCAT(empleados.nombres,\\\" \\\", empleados.apellidos) as Nombre,COUNT(contratos.id)as Cantidad_de_Servicios,sum(productos.costo)as Facturado ,MONTH(contratos.created_at) as mes, YEAR(contratos.created_at) as a\\u00f1o FROM productos, contratos, empleados WHERE contratos.vendedor_id = empleados.id GROUP BY YEAR(contratos.created_at), MONTH(contratos.created_at), Nombre,productos.costo\"}', '2022-11-03 01:34:16', NULL),
(24, 2, '9b3a95dd9857a8813dfafd6e53be1211', 'smallbox', 'area1', 1, NULL, '{\"name\":\"Clientes\",\"icon\":\"ion-bag\",\"color\":\"bg-green\",\"link\":\"http:\\/\\/127.0.0.1:8001\\/admin\\/clientes\",\"sql\":\"select COUNT(*)  from clientes\"}', '2022-11-03 01:35:31', NULL),
(25, 2, 'a286d779056330ea64c440b5e6032374', 'chartbar', 'area5', 0, NULL, '{\"name\":\"PLanes mas contratados\",\"sql\":\"SELECT COUNT(contratos.id) AS \'value\', planes.planes_servicios AS \'label\'\\r\\nFROM contratos,planes\\r\\nWHERE\\r\\nplanes.id=contratos.plan_id AND MONTH(contratos.created_at)= MONTH(NOW())\\r\\nGROUP BY\\r\\nlabel\",\"area_name\":\"label\",\"goals\":null}', '2022-11-03 02:36:53', NULL),
(26, 2, '16d703187fd8a3c33d0a00bc8c521d37', 'chartarea', 'area3', 0, NULL, '{\"name\":\"Vendedores Rancking\",\"sql\":\"SELECT COUNT(contratos.id) AS \'value\', empleados.nombres AS \'label\'\\r\\nFROM contratos,planes,empleados\\r\\nWHERE\\r\\nempleados.id=contratos.vendedor_id AND MONTH(contratos.created_at)= MONTH(NOW())\\r\\nGROUP BY\\r\\nlabel\",\"area_name\":\"label\",\"goals\":\"2\"}', '2022-11-03 02:46:12', NULL),
(27, 2, 'a51627a3554ab8855fa55f42bd19f88f', 'chartarea', 'area4', 0, NULL, '{\"name\":\"Venta Anual\",\"sql\":\"SELECT COUNT(contratos.id) AS \'value\', empleados.nombres AS \'label\'\\r\\nFROM contratos,planes,empleados\\r\\nWHERE\\r\\nempleados.id=contratos.vendedor_id AND YEAR(contratos.created_at)= YEAR(NOW())\\r\\nGROUP BY\\r\\nlabel\",\"area_name\":\"label\",\"goals\":\"2\"}', '2022-11-03 02:49:55', NULL),
(29, 2, '1f61c3dceeeb8c191157f01cfa074655', 'smallbox', 'area2', 1, NULL, '{\"name\":\"Contratos\",\"icon\":\"ion-bag\",\"color\":\"bg-aqua\",\"link\":\"http:\\/\\/127.0.0.1:8001\\/admin\\/contratos30\",\"sql\":\"select COUNT(*)  FROM contratos\"}', '2022-11-03 02:54:48', NULL),
(32, 3, '7f58cbbfe33c93963bfce668c936521f', 'table', 'area5', 0, NULL, '{\"name\":\"Ordenes Ejecutadas\",\"sql\":\"SELECT  CONCAT(empleados.nombres,\\\" \\\", empleados.apellidos) as Tecnico,COUNT(ordenes.id) as Ordejes_Ejecutadas,SUM(tipotrabajos.costo)as Costo_Operativo, (Costo *0.20) as Comision, MONTH(ordenes.fecha_verificacion) Mes, YEAR(ordenes.fecha_verificacion) A\\u00f1o\\r\\nFROM ordenes, empleados, estados, tipotrabajos\\r\\nWHERE ordenes.estado_id = estados.id and ordenes.tecnico_id = empleados.id and ordenes.trabajo_id = tipotrabajos.id and ordenes.estado_id =\\\"8\\\" \\r\\nGROUP BY Tecnico, tipotrabajos.costo,Mes, A\\u00f1o\"}', '2022-11-03 16:58:30', NULL),
(34, 3, 'b9c070e8b02e06a9f63af22268db8fcd', 'smallbox', 'area1', 0, NULL, '{\"name\":\"Ordenes Pendientes\",\"icon\":\"ion-bag\",\"color\":\"bg-green\",\"link\":\"http:\\/\\/127.0.0.1:8001\\/admin\\/ordenes32\",\"sql\":\"SELECT COUNT(ordenes.id) FROM ordenes WHERE ordenes.estado_id =\\\"7\\\"\"}', '2022-11-03 18:48:24', NULL),
(39, 1, '44426516cee6eb892f7883745431f5e6', 'table', 'area5', 2, NULL, '{\"name\":\"Ordenes Realizadas\",\"sql\":\"SELECT  CONCAT(empleados.nombres,\\\" \\\", empleados.apellidos) as Tecnico,COUNT(ordenes.id) as Ordejes_Ejecutadas,SUM(tipotrabajos.costo)as Costo_Operativo, (Costo *0.20) as Comision, MONTH(ordenes.fecha_verificacion) Mes, YEAR(ordenes.fecha_verificacion) A\\u00f1o\\r\\nFROM ordenes, empleados, estados, tipotrabajos\\r\\nWHERE ordenes.estado_id = estados.id and ordenes.tecnico_id = empleados.id and ordenes.trabajo_id = tipotrabajos.id and ordenes.estado_id =\\\"8\\\" \\r\\nGROUP BY Tecnico, tipotrabajos.costo,Mes, A\\u00f1o\"}', '2022-11-03 18:57:17', NULL),
(40, 4, 'f406804c8ead7f98e80027cb2366298b', 'smallbox', 'area1', 0, NULL, '{\"name\":\"Servicios Pendientes de Instalaci\\u00f3n\",\"icon\":\"ion-bag\",\"color\":\"bg-green\",\"link\":\"http:\\/\\/127.0.0.1:8001\\/admin\\/contratos30\",\"sql\":\"SELECT COUNT(*) FROM contratos, estados \\r\\nWHERE contratos.estado_id = estados.id and estados.estado = \\\"Pendiente de Instalaci\\u00f3n\\\"\"}', '2022-11-03 20:54:43', NULL),
(41, 4, 'ff3b547425ab13a7c3382c9a7e99411b', 'smallbox', NULL, 0, 'Untitled', NULL, '2022-11-03 20:57:18', NULL),
(42, 4, '52413bf4d54d60e6ed29117238a81139', 'smallbox', 'area2', 0, NULL, '{\"name\":\"T\\u00e9cnicos de Campo\",\"icon\":\"ion-bag\",\"color\":\"bg-red\",\"link\":\"#\",\"sql\":\"SELECT COUNT(*) FROM empleados,cargos\\r\\nWHERE empleados.cargo_id = cargos.id AND cargos.cargos =\\\"T\\u00e9cnico de Campo\\\"\"}', '2022-11-03 20:57:20', NULL),
(43, 4, '7254b34a9ef3b1642ea71d27b3cd8858', 'smallbox', 'area3', 0, NULL, '{\"name\":\"Ordenes Ejecutadas\",\"icon\":\"ion-bag\",\"color\":\"bg-aqua\",\"link\":\"http:\\/\\/127.0.0.1:8001\\/admin\\/ordenes32\",\"sql\":\"SELECT COUNT(*) FROM ordenes, estados\\r\\nWHERE ordenes.estado_id = estados.id and estados.estado = \\\"Ejecutada\\\"\"}', '2022-11-03 21:00:12', NULL),
(44, 4, '91feb4649360c19efbed7b581709b93f', 'smallbox', 'area4', 0, NULL, '{\"name\":\"Ordenes Pendientes de Ejecutar\",\"icon\":\"ion-bag\",\"color\":\"bg-yellow\",\"link\":\"http:\\/\\/127.0.0.1:8001\\/admin\\/ordenes32\",\"sql\":\"SELECT COUNT(*) FROM ordenes, estados\\r\\nWHERE ordenes.estado_id = estados.id and estados.estado = \\\"Pendiente de Ejecutar\\\"\"}', '2022-11-03 21:01:52', NULL),
(45, 1, '111b111b9214f47feb7bfcc7b11d3773', 'chartarea', 'area5', 2, NULL, '{\"name\":\"Ventas del Mes\",\"sql\":\"SELECT COUNT(contratos.id) AS \'value\', empleados.nombres AS \'label\'\\r\\nFROM contratos,planes,empleados\\r\\nWHERE\\r\\nempleados.id=contratos.vendedor_id AND MONTH(contratos.created_at)= MONTH(NOW())\\r\\nGROUP BY\\r\\nlabel\",\"area_name\":\"label\",\"goals\":\"2\"}', '2022-11-03 21:06:14', NULL),
(46, 1, '9534d775e5b5a03ff1075df26397f085', 'chartline', 'area5', 3, NULL, '{\"name\":\"Ventas del A\\u00f1o\",\"sql\":\"SELECT COUNT(contratos.id) AS \'value\', empleados.nombres AS \'label\'\\r\\nFROM contratos,planes,empleados\\r\\nWHERE\\r\\nempleados.id=contratos.vendedor_id AND YEAR(contratos.created_at)= YEAR(NOW())\\r\\nGROUP BY\\r\\nlabel\",\"area_name\":\"label\",\"goals\":null}', '2022-11-03 21:08:47', NULL),
(47, 5, '7045aadd1d67d5424bd2864d4f874168', 'smallbox', 'area1', 0, NULL, '{\"name\":\"Total de Empleados\",\"icon\":\"ion-bag\",\"color\":\"bg-green\",\"link\":\"http:\\/\\/127.0.0.1:8001\\/admin\\/empleados\",\"sql\":\"select COUNT(*) FROM empleados\"}', '2022-11-03 21:24:40', NULL),
(48, 5, '8fcb80986fdf7a11b967ba40732754e4', 'chartbar', 'area5', 0, NULL, '{\"name\":\"Cargos\",\"sql\":\"SELECT COUNT(empleados.id) AS \'value\', cargos.cargos AS \'label\'\\r\\nFROM empleados,cargos\\r\\nWHERE\\r\\nempleados.cargo_id=cargos.id \\r\\nGROUP BY\\r\\nlabel\",\"area_name\":\"label\",\"goals\":null}', '2022-11-03 21:31:35', NULL),
(49, 5, '6d85999460c5c2e67aa674e349f01e0c', 'table', 'area5', 0, NULL, '{\"name\":\"Vendedor del Mes\",\"sql\":\"select CONCAT(empleados.nombres,\\\" \\\", empleados.apellidos) as Nombre, SUM(productos.costo) As Facturado\\r\\nfrom empleados, productos, contratos\\r\\nWHERE contratos.vendedor_id = empleados.id and MONTH(contratos.created_at)=MONTH(NOW()) AND YEAR(contratos.created_at) = YEAR(NOW())\\r\\nGROUP BY Nombre DESC LIMIT 1;\"}', '2022-11-03 21:32:24', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_users`
--

CREATE TABLE `cms_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contratos`
--

CREATE TABLE `contratos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente_id` bigint(20) UNSIGNED NOT NULL,
  `producto_id` bigint(20) UNSIGNED NOT NULL,
  `plan_id` bigint(20) UNSIGNED NOT NULL,
  `vendedor_id` bigint(20) UNSIGNED NOT NULL,
  `estado_id` bigint(20) UNSIGNED NOT NULL,
  `contrato_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `contratos`
--

INSERT INTO `contratos` (`id`, `cliente_id`, `producto_id`, `plan_id`, `vendedor_id`, `estado_id`, `contrato_file`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 2, 1, 'uploads/1/2022-11/modelo_de_contrato.doc', '2022-08-02 20:36:29', NULL),
(2, 1, 1, 1, 2, 1, 'uploads/1/2022-11/modelo_de_contrato.doc', '2022-08-02 20:48:09', NULL),
(3, 1, 1, 1, 3, 5, 'uploads/1/2022-11/modelo_de_contrato.doc', '2022-10-02 20:49:24', NULL),
(4, 1, 1, 1, 2, 1, 'uploads/1/2022-11/modelo_de_contrato.doc', '2022-10-03 20:39:14', NULL),
(5, 1, 1, 1, 2, 5, 'uploads/1/2022-11/modelo_de_contrato.doc', '2022-11-03 21:10:52', NULL),
(6, 1, 2, 1, 4, 1, 'uploads/1/2022-11/modelo_de_contrato.doc', '2022-11-03 22:05:26', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE `departamentos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `departamento` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `departamentos`
--

INSERT INTO `departamentos` (`id`, `departamento`, `created_at`, `updated_at`) VALUES
(1, 'Alta Verapaz', '2022-10-19 00:58:04', NULL),
(2, 'Baja Verapaz', '2022-10-22 04:07:38', NULL),
(3, 'Chimaltenango', '2022-11-02 19:11:49', NULL),
(4, 'Chiquimula', '2022-11-02 19:11:56', NULL),
(5, 'El Progreso', '2022-11-02 19:12:02', NULL),
(6, 'Escuintla', '2022-11-02 19:12:07', NULL),
(7, 'Departamento de Guatemala', '2022-11-02 19:12:13', NULL),
(8, 'Huehuetenango', '2022-11-02 19:12:23', NULL),
(9, 'Izabal', '2022-11-02 19:12:29', NULL),
(10, 'Jalapa', '2022-11-02 19:12:35', NULL),
(11, 'Jutiapa', '2022-11-02 19:12:43', NULL),
(12, 'Petén', '2022-11-02 19:12:48', NULL),
(13, 'Quetzaltenango', '2022-11-02 19:12:55', NULL),
(14, 'Quiché', '2022-11-02 19:13:01', NULL),
(15, 'Retalhuleu', '2022-11-02 19:13:05', NULL),
(16, 'Sacatepéquez', '2022-11-02 19:13:14', NULL),
(17, 'San Marcos', '2022-11-02 19:13:21', NULL),
(18, 'Santa Rosa', '2022-11-02 19:13:26', NULL),
(19, 'Sololá', '2022-11-02 19:13:31', NULL),
(20, 'Suchitepéquez', '2022-11-02 19:13:36', NULL),
(21, 'Totonicapán', '2022-11-02 19:13:41', NULL),
(22, 'Zacapa', '2022-11-02 19:13:46', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cui` bigint(20) NOT NULL,
  `nombres` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` bigint(20) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cargo_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`id`, `cui`, `nombres`, `apellidos`, `telefono`, `email`, `cargo_id`, `created_at`, `updated_at`) VALUES
(1, 2050968911601, 'Hannsel Estuardo', 'Cordón Ac', 12345678, 'hannselcordon@gmail.com', 1, '2022-10-19 03:01:21', NULL),
(2, 2050968911604, 'Rene Estuardo', 'Lopez Garcia', 87654321, 'estuardo@prueba.com', 3, '2022-10-19 03:02:31', NULL),
(3, 2050968911607, 'Julio Alberto', 'Morales Tut', 12345678, 'jmorales@gmal.com', 10, '2022-11-02 19:40:54', '2022-11-02 23:48:00'),
(4, 1010110101, 'System', 'Firberhome', 33343, 'system@sistema.com', 3, '2022-11-03 21:54:07', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `estado` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`id`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'Pendiente de Instalación', '2022-10-19 00:58:26', NULL),
(2, 'Suspendido por falta de pago', '2022-10-19 00:58:38', NULL),
(3, 'Dado de Baja', '2022-10-19 00:59:03', NULL),
(4, 'Sin Contrato', '2022-10-19 12:20:47', NULL),
(5, 'Activo', '2022-10-20 11:13:28', NULL),
(6, 'Inactivo', '2022-10-20 11:13:35', NULL),
(7, 'Pendiente de Ejecutar', '2022-11-03 00:26:50', NULL),
(8, 'Ejecutada', '2022-11-03 00:27:04', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_08_07_145904_add_table_cms_apicustom', 1),
(4, '2016_08_07_150834_add_table_cms_dashboard', 1),
(5, '2016_08_07_151210_add_table_cms_logs', 1),
(6, '2016_08_07_151211_add_details_cms_logs', 1),
(7, '2016_08_07_152014_add_table_cms_privileges', 1),
(8, '2016_08_07_152214_add_table_cms_privileges_roles', 1),
(9, '2016_08_07_152320_add_table_cms_settings', 1),
(11, '2016_08_07_154624_add_table_cms_menus_privileges', 1),
(12, '2016_08_07_154624_add_table_cms_moduls', 1),
(14, '2016_08_20_125418_add_table_cms_notifications', 1),
(15, '2016_09_04_033706_add_table_cms_email_queues', 1),
(16, '2016_09_16_035347_add_group_setting', 1),
(17, '2016_09_16_045425_add_label_setting', 1),
(18, '2016_09_17_104728_create_nullable_cms_apicustom', 1),
(19, '2016_10_01_141740_add_method_type_apicustom', 1),
(20, '2016_10_01_141846_add_parameters_apicustom', 1),
(21, '2016_10_01_141934_add_responses_apicustom', 1),
(22, '2016_10_01_144826_add_table_apikey', 1),
(23, '2016_11_14_141657_create_cms_menus', 1),
(24, '2016_11_15_132350_create_cms_email_templates', 1),
(25, '2016_11_15_190410_create_cms_statistics', 1),
(26, '2016_11_17_102740_create_cms_statistic_components', 1),
(27, '2017_06_06_164501_add_deleted_at_cms_moduls', 1),
(28, '2019_08_19_000000_create_failed_jobs_table', 1),
(29, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(30, '2022_10_18_160310_create_departamentos_table', 1),
(31, '2022_10_18_162124_create_municipios_table', 1),
(32, '2022_10_18_171919_create_tipopagos_table', 1),
(33, '2022_10_18_172114_create_estados_table', 1),
(34, '2022_10_18_174657_create_tipoordenes_table', 2),
(35, '2022_10_18_183041_create_planes_table', 2),
(36, '2022_10_18_193012_create_cargos_table', 3),
(37, '2022_10_18_200947_create_tipotrabajos_table', 4),
(38, '2022_10_18_202616_create_empleados_table', 5),
(39, '2022_10_18_211300_create_jerarquias_table', 6),
(40, '2022_10_18_220441_create_clientes_table', 7),
(41, '2022_10_19_062144_create_productos_table', 7),
(45, '2022_10_20_012912_create_pagos_table', 8),
(46, '2014_10_12_000000_create_users_table', 9),
(47, '2016_08_07_152421_add_table_cms_users', 9),
(48, '2016_08_17_225409_add_status_cms_users', 9),
(49, '2022_10_19_074301_create_contratos_table', 10),
(50, '2022_11_02_210530_create_ordenes_table', 11),
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_08_07_145904_add_table_cms_apicustom', 1),
(4, '2016_08_07_150834_add_table_cms_dashboard', 1),
(5, '2016_08_07_151210_add_table_cms_logs', 1),
(6, '2016_08_07_151211_add_details_cms_logs', 1),
(7, '2016_08_07_152014_add_table_cms_privileges', 1),
(8, '2016_08_07_152214_add_table_cms_privileges_roles', 1),
(9, '2016_08_07_152320_add_table_cms_settings', 1),
(10, '2016_08_07_152421_add_table_cms_users', 1),
(11, '2016_08_07_154624_add_table_cms_menus_privileges', 1),
(12, '2016_08_07_154624_add_table_cms_moduls', 1),
(13, '2016_08_17_225409_add_status_cms_users', 1),
(14, '2016_08_20_125418_add_table_cms_notifications', 1),
(15, '2016_09_04_033706_add_table_cms_email_queues', 1),
(16, '2016_09_16_035347_add_group_setting', 1),
(17, '2016_09_16_045425_add_label_setting', 1),
(18, '2016_09_17_104728_create_nullable_cms_apicustom', 1),
(19, '2016_10_01_141740_add_method_type_apicustom', 1),
(20, '2016_10_01_141846_add_parameters_apicustom', 1),
(21, '2016_10_01_141934_add_responses_apicustom', 1),
(22, '2016_10_01_144826_add_table_apikey', 1),
(23, '2016_11_14_141657_create_cms_menus', 1),
(24, '2016_11_15_132350_create_cms_email_templates', 1),
(25, '2016_11_15_190410_create_cms_statistics', 1),
(26, '2016_11_17_102740_create_cms_statistic_components', 1),
(27, '2017_06_06_164501_add_deleted_at_cms_moduls', 1),
(28, '2019_08_19_000000_create_failed_jobs_table', 1),
(29, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(30, '2022_10_18_160310_create_departamentos_table', 1),
(31, '2022_10_18_162124_create_municipios_table', 1),
(32, '2022_10_18_171919_create_tipopagos_table', 1),
(33, '2022_10_18_172114_create_estados_table', 1),
(34, '2022_10_18_174657_create_tipoordenes_table', 1),
(35, '2022_10_18_183041_create_planes_table', 1),
(36, '2022_10_18_193012_create_cargos_table', 1),
(37, '2022_10_18_200947_create_tipotrabajos_table', 1),
(38, '2022_10_18_202616_create_empleados_table', 1),
(39, '2022_10_18_211300_create_jerarquias_table', 1),
(40, '2022_10_18_220441_create_clientes_table', 1),
(41, '2022_10_19_062144_create_productos_table', 1),
(42, '2022_10_19_074301_create_contratos_table', 1),
(43, '2022_10_20_012912_create_pagos_table', 1),
(44, '2022_11_02_210530_create_ordenes_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipios`
--

CREATE TABLE `municipios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `municipio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `municipios`
--

INSERT INTO `municipios` (`id`, `municipio`, `created_at`, `updated_at`) VALUES
(1, 'Cobán', '2022-10-19 00:58:15', NULL),
(2, 'San Pedro Carcha', '2022-10-19 08:19:26', NULL),
(3, 'Santa Cruz Verapaz', '2022-11-02 19:14:31', NULL),
(4, 'San Cristobal Verapaz', '2022-11-02 19:14:38', NULL),
(5, 'Tactíc', '2022-11-02 19:14:42', NULL),
(6, 'Tamahú', '2022-11-02 19:14:49', NULL),
(7, 'San Miguel Tucurú', '2022-11-02 19:14:54', NULL),
(8, 'Panzos', '2022-11-02 19:15:00', NULL),
(9, 'Senahú', '2022-11-02 19:15:08', NULL),
(10, 'San Pedro Carchá', '2022-11-02 19:15:18', NULL),
(11, 'SanJuan Chamelco', '2022-11-02 19:15:23', NULL),
(12, 'Lanquín', '2022-11-02 19:15:29', NULL),
(13, 'Santa María Cahabón', '2022-11-02 19:15:39', NULL),
(14, 'Chisec', '2022-11-02 19:15:44', NULL),
(15, 'Chahal', '2022-11-02 19:15:51', NULL),
(16, 'Fray Bartolomé de las Casas', '2022-11-02 19:15:57', NULL),
(17, 'Santa Catarina La Tinta', '2022-11-02 19:16:02', NULL),
(18, 'Raxruha', '2022-11-02 19:16:22', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ordenes`
--

CREATE TABLE `ordenes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `trabajo_id` bigint(20) UNSIGNED DEFAULT NULL,
  `servicio_id` bigint(20) UNSIGNED DEFAULT NULL,
  `tecnico_id` bigint(20) UNSIGNED DEFAULT NULL,
  `estado_id` bigint(20) UNSIGNED DEFAULT NULL,
  `fecha_generacion` date DEFAULT NULL,
  `supervisor_id` bigint(20) UNSIGNED DEFAULT NULL,
  `comentario` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `evidencias` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_verificacion` date DEFAULT NULL,
  `cliente_id` bigint(20) UNSIGNED DEFAULT NULL,
  `contrato_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `ordenes`
--

INSERT INTO `ordenes` (`id`, `trabajo_id`, `servicio_id`, `tecnico_id`, `estado_id`, `fecha_generacion`, `supervisor_id`, `comentario`, `evidencias`, `fecha_verificacion`, `cliente_id`, `contrato_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 3, 8, '2022-11-02', 1, 'Comentario', 'uploads/1/2022-11/tarea_09_2021.pdf', '2022-11-15', 1, 1, '2022-11-02 23:58:17', '2022-11-03 16:20:24'),
(2, 1, 3, 3, 7, '2022-11-03', 1, NULL, NULL, NULL, 1, 3, '2022-11-03 17:11:29', NULL),
(3, 1, 1, 3, 8, '2022-11-03', 1, 'Se realizo instalacion de servicio', 'uploads/3/2022-11/tarea_09_2021.pdf', '2022-11-30', 1, 1, '2022-11-03 17:20:53', '2022-11-03 17:35:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente_id` bigint(20) UNSIGNED NOT NULL,
  `servicio_id` bigint(20) UNSIGNED NOT NULL,
  `tipopago_id` bigint(20) UNSIGNED NOT NULL,
  `fecha_pago` date NOT NULL,
  `monto` double NOT NULL,
  `saldo` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `pagos`
--

INSERT INTO `pagos` (`id`, `cliente_id`, `servicio_id`, `tipopago_id`, `fecha_pago`, `monto`, `saldo`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 2, '2022-10-19', 200, 200, '2022-10-20 11:15:31', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `planes`
--

CREATE TABLE `planes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `planes_servicios` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `planes`
--

INSERT INTO `planes` (`id`, `planes_servicios`, `created_at`, `updated_at`) VALUES
(1, 'Premium 12 Meses', '2022-10-19 13:59:52', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `producto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `costo` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `producto`, `descripcion`, `costo`, `created_at`, `updated_at`) VALUES
(1, 'Internet Corporativo Plus', 'Protección de datos Site to Site, 1 Dominio, 400 Usuarios internos y hasta 100 externos', 200, '2022-10-19 13:12:34', NULL),
(2, 'Correo Corporativo', 'Protección de Datos, Respaldo automatico, Encriptacion, etc.', 300, '2022-11-03 21:52:34', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoordenes`
--

CREATE TABLE `tipoordenes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipo_orden` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tipoordenes`
--

INSERT INTO `tipoordenes` (`id`, `tipo_orden`, `created_at`, `updated_at`) VALUES
(1, 'Instalación de Nuevo Servicio', '2022-10-19 00:59:58', NULL),
(2, 'Inspeccion de Servicio', '2022-10-19 01:00:15', NULL),
(3, 'Reclamo por falta de Servicio', '2022-10-19 01:00:35', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipopagos`
--

CREATE TABLE `tipopagos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tipopagos`
--

INSERT INTO `tipopagos` (`id`, `tipo`, `created_at`, `updated_at`) VALUES
(1, 'Efectivo', '2022-10-19 01:00:55', NULL),
(2, 'Tarjeta', '2022-10-19 01:00:58', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipotrabajos`
--

CREATE TABLE `tipotrabajos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipo_trabajo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `costo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tipotrabajos`
--

INSERT INTO `tipotrabajos` (`id`, `tipo_trabajo`, `descripcion`, `costo`, `created_at`, `updated_at`) VALUES
(1, 'Inspección de Servicio', 'Esta tarea es diseñada para el técnico de campo y tiene el proposito de realizar una inspeccion completa, detectar y solucionar el inconveniente.', '200', '2022-10-19 02:17:55', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `photo`, `email`, `password`, `id_cms_privileges`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'uploads/1/2022-11/logo_search_grid_1x_1.png', 'hannselcordon@gmail.com', '$2y$10$tfhyD7L6fxmBRojApqjpxOR5uaeuGGSaWg71XFFNY2pBJBowWxd2y', 1, 'Active', '2022-11-02 19:08:45', '2022-11-02 19:51:05'),
(2, 'Vendedor', 'uploads/1/2022-11/logo_search_grid_1x_5.png', 'vendedor@gmail.com', '$2y$10$IlLmK77dpdXxaXQLOXrUsetvvZE4CIk9Rg1.H5H0KiYsMpR9F4pIK', 3, NULL, '2022-11-03 01:31:41', '2022-11-03 01:32:06'),
(3, 'Tecnico', 'uploads/1/2022-11/logo_search_grid_1x_3.png', 'tecnico@tecnico.com', '$2y$10$fdCdbWtf3SQMLnOe0izl7eBTFbvV2aZ9ZZ5F2CvDxrLdCa46RPvAW', 4, NULL, '2022-11-03 16:28:29', NULL),
(4, 'Supervisor', 'uploads/1/2022-11/logo_search_grid_1x_2.png', 'supervisor@supervisor.com', '$2y$10$DSgqrRceKhmVFzUU.atx7Ojx/wUabZWevyA6Q73uKvPO8K0HjCUva', 5, NULL, '2022-11-03 20:45:18', NULL),
(5, 'Recursos Humanos', 'uploads/1/2022-11/logo_search_grid_1x_1.png', 'recursos@recursos.com', '$2y$10$1pb20W1OalNf4iJ67p.HvO06pB4/Yl9dgNotlqY7ejVqjzFe7SdsW', 6, NULL, '2022-11-03 21:21:57', NULL),
(6, 'Cliente Fiberhome', 'uploads/1/2022-11/logo_search_grid_1x_7.png', 'cliente@cliente.com', '$2y$10$NuihcPADz4FhlkpkzG3PNOU2u6GgP6NstMfSgbztEywbCVGww1Itm', 7, NULL, '2022-11-03 22:15:35', NULL),
(1, 'Super Admin', NULL, 'hannselcordon@gmail.com', '$2y$10$ahBEnaPfyj7rOIUQlogXPOsD0hwNbiREfloonoCULWWyjKzUPFO8K', 1, 'Active', '2022-11-08 04:12:45', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cargos`
--
ALTER TABLE `cargos`
  ADD PRIMARY KEY (`id`);
--
-- Base de datos: `fiberhome2`
--
CREATE DATABASE IF NOT EXISTS `fiberhome2` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci;
USE `fiberhome2`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargos`
--

CREATE TABLE `cargos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cargos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cui` bigint(20) NOT NULL,
  `nombres` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` bigint(20) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `departamento_id` bigint(20) UNSIGNED NOT NULL,
  `municipio_id` bigint(20) UNSIGNED NOT NULL,
  `direccion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ubicacion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `empleado_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_apicustom`
--

CREATE TABLE `cms_apicustom` (
  `id` int(10) UNSIGNED NOT NULL,
  `permalink` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tabel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aksi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kolom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderby` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_query_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sql_where` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `method_type` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` longtext COLLATE utf8mb4_unicode_ci,
  `responses` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_apikey`
--

CREATE TABLE `cms_apikey` (
  `id` int(10) UNSIGNED NOT NULL,
  `screetkey` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hit` int(11) DEFAULT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_dashboard`
--

CREATE TABLE `cms_dashboard` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_email_queues`
--

CREATE TABLE `cms_email_queues` (
  `id` int(10) UNSIGNED NOT NULL,
  `send_at` datetime DEFAULT NULL,
  `email_recipient` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_content` text COLLATE utf8mb4_unicode_ci,
  `email_attachments` text COLLATE utf8mb4_unicode_ci,
  `is_sent` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_email_templates`
--

CREATE TABLE `cms_email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_email_templates`
--

INSERT INTO `cms_email_templates` (`id`, `name`, `slug`, `subject`, `content`, `description`, `from_name`, `from_email`, `cc_email`, `created_at`, `updated_at`) VALUES
(1, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2022-11-07 22:39:41', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_logs`
--

CREATE TABLE `cms_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `ipaddress` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `useragent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `id_cms_users` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_logs`
--

INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `details`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(1, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/logout', ' logout', '', 6, '2022-11-07 22:43:45', NULL),
(2, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-07 22:44:19', NULL),
(3, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-07 22:44:33', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_menus`
--

CREATE TABLE `cms_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'url',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_dashboard` tinyint(1) NOT NULL DEFAULT '0',
  `id_cms_privileges` int(11) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_menus_privileges`
--

CREATE TABLE `cms_menus_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_menus` int(11) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_moduls`
--

CREATE TABLE `cms_moduls` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_moduls`
--

INSERT INTO `cms_moduls` (`id`, `name`, `icon`, `path`, `table_name`, `controller`, `is_protected`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Notifications', 'fa fa-cog', 'notifications', 'cms_notifications', 'NotificationsController', 1, 1, '2022-11-07 22:39:41', NULL, NULL),
(2, 'Privileges', 'fa fa-cog', 'privileges', 'cms_privileges', 'PrivilegesController', 1, 1, '2022-11-07 22:39:41', NULL, NULL),
(3, 'Privileges Roles', 'fa fa-cog', 'privileges_roles', 'cms_privileges_roles', 'PrivilegesRolesController', 1, 1, '2022-11-07 22:39:41', NULL, NULL),
(4, 'Users Management', 'fa fa-users', 'users', 'users', 'AdminCmsUsersController', 0, 1, '2022-11-07 22:39:41', NULL, NULL),
(5, 'Settings', 'fa fa-cog', 'settings', 'cms_settings', 'SettingsController', 1, 1, '2022-11-07 22:39:41', NULL, NULL),
(6, 'Module Generator', 'fa fa-database', 'module_generator', 'cms_moduls', 'ModulsController', 1, 1, '2022-11-07 22:39:41', NULL, NULL),
(7, 'Menu Management', 'fa fa-bars', 'menu_management', 'cms_menus', 'MenusController', 1, 1, '2022-11-07 22:39:41', NULL, NULL),
(8, 'Email Templates', 'fa fa-envelope-o', 'email_templates', 'cms_email_templates', 'EmailTemplatesController', 1, 1, '2022-11-07 22:39:41', NULL, NULL),
(9, 'Statistic Builder', 'fa fa-dashboard', 'statistic_builder', 'cms_statistics', 'StatisticBuilderController', 1, 1, '2022-11-07 22:39:41', NULL, NULL),
(10, 'API Generator', 'fa fa-cloud-download', 'api_generator', '', 'ApiCustomController', 1, 1, '2022-11-07 22:39:41', NULL, NULL),
(11, 'Log User Access', 'fa fa-flag-o', 'logs', 'cms_logs', 'LogsController', 1, 1, '2022-11-07 22:39:41', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_notifications`
--

CREATE TABLE `cms_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_users` int(11) DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_privileges`
--

CREATE TABLE `cms_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_superadmin` tinyint(1) DEFAULT NULL,
  `theme_color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_privileges`
--

INSERT INTO `cms_privileges` (`id`, `name`, `is_superadmin`, `theme_color`, `created_at`, `updated_at`) VALUES
(1, 'Super Administrator', 1, 'skin-red', '2022-11-07 22:39:41', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_privileges_roles`
--

CREATE TABLE `cms_privileges_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_visible` tinyint(1) DEFAULT NULL,
  `is_create` tinyint(1) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `is_edit` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `id_cms_moduls` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_privileges_roles`
--

INSERT INTO `cms_privileges_roles` (`id`, `is_visible`, `is_create`, `is_read`, `is_edit`, `is_delete`, `id_cms_privileges`, `id_cms_moduls`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 0, 0, 0, 1, 1, '2022-11-07 22:39:41', NULL),
(2, 1, 1, 1, 1, 1, 1, 2, '2022-11-07 22:39:41', NULL),
(3, 0, 1, 1, 1, 1, 1, 3, '2022-11-07 22:39:41', NULL),
(4, 1, 1, 1, 1, 1, 1, 4, '2022-11-07 22:39:41', NULL),
(5, 1, 1, 1, 1, 1, 1, 5, '2022-11-07 22:39:41', NULL),
(6, 1, 1, 1, 1, 1, 1, 6, '2022-11-07 22:39:41', NULL),
(7, 1, 1, 1, 1, 1, 1, 7, '2022-11-07 22:39:41', NULL),
(8, 1, 1, 1, 1, 1, 1, 8, '2022-11-07 22:39:41', NULL),
(9, 1, 1, 1, 1, 1, 1, 9, '2022-11-07 22:39:41', NULL),
(10, 1, 1, 1, 1, 1, 1, 10, '2022-11-07 22:39:41', NULL),
(11, 1, 0, 1, 0, 1, 1, 11, '2022-11-07 22:39:41', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_settings`
--

CREATE TABLE `cms_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `content_input_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dataenum` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `helper` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group_setting` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_settings`
--

INSERT INTO `cms_settings` (`id`, `name`, `content`, `content_input_type`, `dataenum`, `helper`, `created_at`, `updated_at`, `group_setting`, `label`) VALUES
(1, 'login_background_color', NULL, 'text', NULL, 'Input hexacode', '2022-11-07 22:39:41', NULL, 'Login Register Style', 'Login Background Color'),
(2, 'login_font_color', NULL, 'text', NULL, 'Input hexacode', '2022-11-07 22:39:41', NULL, 'Login Register Style', 'Login Font Color'),
(3, 'login_background_image', NULL, 'upload_image', NULL, NULL, '2022-11-07 22:39:41', NULL, 'Login Register Style', 'Login Background Image'),
(4, 'email_sender', 'support@crudbooster.com', 'text', NULL, NULL, '2022-11-07 22:39:41', NULL, 'Email Setting', 'Email Sender'),
(5, 'smtp_driver', 'mail', 'select', 'smtp,mail,sendmail', NULL, '2022-11-07 22:39:41', NULL, 'Email Setting', 'Mail Driver'),
(6, 'smtp_host', '', 'text', NULL, NULL, '2022-11-07 22:39:41', NULL, 'Email Setting', 'SMTP Host'),
(7, 'smtp_port', '25', 'text', NULL, 'default 25', '2022-11-07 22:39:41', NULL, 'Email Setting', 'SMTP Port'),
(8, 'smtp_username', '', 'text', NULL, NULL, '2022-11-07 22:39:41', NULL, 'Email Setting', 'SMTP Username'),
(9, 'smtp_password', '', 'text', NULL, NULL, '2022-11-07 22:39:41', NULL, 'Email Setting', 'SMTP Password'),
(10, 'appname', 'CRUDBooster', 'text', NULL, NULL, '2022-11-07 22:39:41', NULL, 'Application Setting', 'Application Name'),
(11, 'default_paper_size', 'Legal', 'text', NULL, 'Paper size, ex : A4, Legal, etc', '2022-11-07 22:39:41', NULL, 'Application Setting', 'Default Paper Print Size'),
(12, 'logo', '', 'upload_image', NULL, NULL, '2022-11-07 22:39:41', NULL, 'Application Setting', 'Logo'),
(13, 'favicon', '', 'upload_image', NULL, NULL, '2022-11-07 22:39:41', NULL, 'Application Setting', 'Favicon'),
(14, 'api_debug_mode', 'true', 'select', 'true,false', NULL, '2022-11-07 22:39:41', NULL, 'Application Setting', 'API Debug Mode'),
(15, 'google_api_key', '', 'text', NULL, NULL, '2022-11-07 22:39:41', NULL, 'Application Setting', 'Google API Key'),
(16, 'google_fcm_key', '', 'text', NULL, NULL, '2022-11-07 22:39:41', NULL, 'Application Setting', 'Google FCM Key');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_statistics`
--

CREATE TABLE `cms_statistics` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_statistic_components`
--

CREATE TABLE `cms_statistic_components` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_statistics` int(11) DEFAULT NULL,
  `componentID` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `component_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_name` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `config` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_users`
--

CREATE TABLE `cms_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contratos`
--

CREATE TABLE `contratos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente_id` bigint(20) UNSIGNED NOT NULL,
  `producto_id` bigint(20) UNSIGNED NOT NULL,
  `plan_id` bigint(20) UNSIGNED NOT NULL,
  `vendedor_id` bigint(20) UNSIGNED NOT NULL,
  `estado_id` bigint(20) UNSIGNED NOT NULL,
  `contrato_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE `departamentos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `departamento` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cui` bigint(20) NOT NULL,
  `nombres` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` bigint(20) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cargo_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `estado` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jerarquias`
--

CREATE TABLE `jerarquias` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `empleado_id` bigint(20) UNSIGNED NOT NULL,
  `jefe_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_08_07_145904_add_table_cms_apicustom', 1),
(4, '2016_08_07_150834_add_table_cms_dashboard', 1),
(5, '2016_08_07_151210_add_table_cms_logs', 1),
(6, '2016_08_07_151211_add_details_cms_logs', 1),
(7, '2016_08_07_152014_add_table_cms_privileges', 1),
(8, '2016_08_07_152214_add_table_cms_privileges_roles', 1),
(9, '2016_08_07_152320_add_table_cms_settings', 1),
(10, '2016_08_07_152421_add_table_cms_users', 1),
(11, '2016_08_07_154624_add_table_cms_menus_privileges', 1),
(12, '2016_08_07_154624_add_table_cms_moduls', 1),
(13, '2016_08_17_225409_add_status_cms_users', 1),
(14, '2016_08_20_125418_add_table_cms_notifications', 1),
(15, '2016_09_04_033706_add_table_cms_email_queues', 1),
(16, '2016_09_16_035347_add_group_setting', 1),
(17, '2016_09_16_045425_add_label_setting', 1),
(18, '2016_09_17_104728_create_nullable_cms_apicustom', 1),
(19, '2016_10_01_141740_add_method_type_apicustom', 1),
(20, '2016_10_01_141846_add_parameters_apicustom', 1),
(21, '2016_10_01_141934_add_responses_apicustom', 1),
(22, '2016_10_01_144826_add_table_apikey', 1),
(23, '2016_11_14_141657_create_cms_menus', 1),
(24, '2016_11_15_132350_create_cms_email_templates', 1),
(25, '2016_11_15_190410_create_cms_statistics', 1),
(26, '2016_11_17_102740_create_cms_statistic_components', 1),
(27, '2017_06_06_164501_add_deleted_at_cms_moduls', 1),
(28, '2019_08_19_000000_create_failed_jobs_table', 1),
(29, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(30, '2022_10_18_160310_create_departamentos_table', 1),
(31, '2022_10_18_162124_create_municipios_table', 1),
(32, '2022_10_18_171919_create_tipopagos_table', 1),
(33, '2022_10_18_172114_create_estados_table', 1),
(34, '2022_10_18_174657_create_tipoordenes_table', 1),
(35, '2022_10_18_183041_create_planes_table', 1),
(36, '2022_10_18_193012_create_cargos_table', 1),
(37, '2022_10_18_200947_create_tipotrabajos_table', 1),
(38, '2022_10_18_202616_create_empleados_table', 1),
(39, '2022_10_18_211300_create_jerarquias_table', 1),
(40, '2022_10_18_220441_create_clientes_table', 1),
(41, '2022_10_19_062144_create_productos_table', 1),
(42, '2022_10_19_074301_create_contratos_table', 1),
(43, '2022_10_20_012912_create_pagos_table', 1),
(44, '2022_11_02_210530_create_ordenes_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipios`
--

CREATE TABLE `municipios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `municipio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ordenes`
--

CREATE TABLE `ordenes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `trabajo_id` bigint(20) UNSIGNED NOT NULL,
  `servicio_id` bigint(20) UNSIGNED NOT NULL,
  `tecnico_id` bigint(20) UNSIGNED NOT NULL,
  `estado_id` bigint(20) UNSIGNED NOT NULL,
  `fecha_generacion` date NOT NULL,
  `supervisor_id` bigint(20) UNSIGNED DEFAULT NULL,
  `comentario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `evidencias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_verificacion` date NOT NULL,
  `cliente_id` bigint(20) UNSIGNED NOT NULL,
  `contrato_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente_id` bigint(20) UNSIGNED NOT NULL,
  `servicio_id` bigint(20) UNSIGNED NOT NULL,
  `tipopago_id` bigint(20) UNSIGNED NOT NULL,
  `fecha_pago` date NOT NULL,
  `monto` double NOT NULL,
  `saldo` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `planes`
--

CREATE TABLE `planes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `planes_servicios` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `producto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `costo` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoordenes`
--

CREATE TABLE `tipoordenes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipo_orden` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipopagos`
--

CREATE TABLE `tipopagos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipotrabajos`
--

CREATE TABLE `tipotrabajos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipo_trabajo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `costo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `photo`, `email`, `password`, `id_cms_privileges`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', NULL, 'hannselcordon@gmail.com', '$2y$10$UISwMNYMA4YbOCC6owU/DeGsd/BNg5MepHyZ.K1btuOQAewvxWsYK', 1, 'Active', '2022-11-07 22:39:41', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cargos`
--
ALTER TABLE `cargos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clientes_departamento_id_foreign` (`departamento_id`),
  ADD KEY `clientes_municipio_id_foreign` (`municipio_id`),
  ADD KEY `clientes_empleado_id_foreign` (`empleado_id`);

--
-- Indices de la tabla `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_apikey`
--
ALTER TABLE `cms_apikey`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_logs`
--
ALTER TABLE `cms_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_menus`
--
ALTER TABLE `cms_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_moduls`
--
ALTER TABLE `cms_moduls`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_notifications`
--
ALTER TABLE `cms_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_privileges`
--
ALTER TABLE `cms_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_settings`
--
ALTER TABLE `cms_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_statistics`
--
ALTER TABLE `cms_statistics`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_users`
--
ALTER TABLE `cms_users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contratos`
--
ALTER TABLE `contratos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contratos_producto_id_foreign` (`producto_id`),
  ADD KEY `contratos_plan_id_foreign` (`plan_id`),
  ADD KEY `contratos_vendedor_id_foreign` (`vendedor_id`),
  ADD KEY `contratos_estado_id_foreign` (`estado_id`),
  ADD KEY `contratos_cliente_id_foreign` (`cliente_id`);

--
-- Indices de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `empleados_cargo_id_foreign` (`cargo_id`);

--
-- Indices de la tabla `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indices de la tabla `jerarquias`
--
ALTER TABLE `jerarquias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jerarquias_empleado_id_foreign` (`empleado_id`),
  ADD KEY `jerarquias_jefe_id_foreign` (`jefe_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `municipios`
--
ALTER TABLE `municipios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ordenes`
--
ALTER TABLE `ordenes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ordenes_trabajo_id_foreign` (`trabajo_id`),
  ADD KEY `ordenes_servicio_id_foreign` (`servicio_id`),
  ADD KEY `ordenes_tecnico_id_foreign` (`tecnico_id`),
  ADD KEY `ordenes_estado_id_foreign` (`estado_id`),
  ADD KEY `ordenes_supervisor_id_foreign` (`supervisor_id`),
  ADD KEY `ordenes_cliente_id_foreign` (`cliente_id`),
  ADD KEY `ordenes_contrato_id_foreign` (`contrato_id`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pagos_cliente_id_foreign` (`cliente_id`),
  ADD KEY `pagos_servicio_id_foreign` (`servicio_id`),
  ADD KEY `pagos_tipopago_id_foreign` (`tipopago_id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indices de la tabla `planes`
--
ALTER TABLE `planes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipoordenes`
--
ALTER TABLE `tipoordenes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipopagos`
--
ALTER TABLE `tipopagos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipotrabajos`
--
ALTER TABLE `tipotrabajos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cargos`
--
ALTER TABLE `cargos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cms_apikey`
--
ALTER TABLE `cms_apikey`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cms_logs`
--
ALTER TABLE `cms_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `cms_menus`
--
ALTER TABLE `cms_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cms_moduls`
--
ALTER TABLE `cms_moduls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `cms_notifications`
--
ALTER TABLE `cms_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cms_privileges`
--
ALTER TABLE `cms_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `cms_settings`
--
ALTER TABLE `cms_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `cms_statistics`
--
ALTER TABLE `cms_statistics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cms_users`
--
ALTER TABLE `cms_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `contratos`
--
ALTER TABLE `contratos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `empleados`
--
ALTER TABLE `empleados`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `estados`
--
ALTER TABLE `estados`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `jerarquias`
--
ALTER TABLE `jerarquias`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT de la tabla `municipios`
--
ALTER TABLE `municipios`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ordenes`
--
ALTER TABLE `ordenes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `planes`
--
ALTER TABLE `planes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipoordenes`
--
ALTER TABLE `tipoordenes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipopagos`
--
ALTER TABLE `tipopagos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipotrabajos`
--
ALTER TABLE `tipotrabajos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD CONSTRAINT `clientes_departamento_id_foreign` FOREIGN KEY (`departamento_id`) REFERENCES `departamentos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `clientes_empleado_id_foreign` FOREIGN KEY (`empleado_id`) REFERENCES `empleados` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `clientes_municipio_id_foreign` FOREIGN KEY (`municipio_id`) REFERENCES `municipios` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `contratos`
--
ALTER TABLE `contratos`
  ADD CONSTRAINT `contratos_cliente_id_foreign` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `contratos_estado_id_foreign` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `contratos_plan_id_foreign` FOREIGN KEY (`plan_id`) REFERENCES `planes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `contratos_producto_id_foreign` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `contratos_vendedor_id_foreign` FOREIGN KEY (`vendedor_id`) REFERENCES `empleados` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD CONSTRAINT `empleados_cargo_id_foreign` FOREIGN KEY (`cargo_id`) REFERENCES `cargos` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `jerarquias`
--
ALTER TABLE `jerarquias`
  ADD CONSTRAINT `jerarquias_empleado_id_foreign` FOREIGN KEY (`empleado_id`) REFERENCES `empleados` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jerarquias_jefe_id_foreign` FOREIGN KEY (`jefe_id`) REFERENCES `empleados` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `ordenes`
--
ALTER TABLE `ordenes`
  ADD CONSTRAINT `ordenes_cliente_id_foreign` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ordenes_contrato_id_foreign` FOREIGN KEY (`contrato_id`) REFERENCES `contratos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ordenes_estado_id_foreign` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ordenes_servicio_id_foreign` FOREIGN KEY (`servicio_id`) REFERENCES `tipoordenes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ordenes_supervisor_id_foreign` FOREIGN KEY (`supervisor_id`) REFERENCES `empleados` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ordenes_tecnico_id_foreign` FOREIGN KEY (`tecnico_id`) REFERENCES `empleados` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ordenes_trabajo_id_foreign` FOREIGN KEY (`trabajo_id`) REFERENCES `tipotrabajos` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD CONSTRAINT `pagos_cliente_id_foreign` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pagos_servicio_id_foreign` FOREIGN KEY (`servicio_id`) REFERENCES `contratos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pagos_tipopago_id_foreign` FOREIGN KEY (`tipopago_id`) REFERENCES `tipopagos` (`id`) ON DELETE CASCADE;
--
-- Base de datos: `laravelfirehome`
--
CREATE DATABASE IF NOT EXISTS `laravelfirehome` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci;
USE `laravelfirehome`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargos`
--

CREATE TABLE `cargos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cargos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cargos`
--

INSERT INTO `cargos` (`id`, `cargos`, `created_at`, `updated_at`) VALUES
(1, 'Supervisor', '2022-10-19 01:39:56', NULL),
(2, 'Gestor de Cobros', '2022-10-19 01:40:04', NULL),
(3, 'Vendedor Rutero', '2022-10-19 01:40:18', NULL),
(4, 'Secretaria', '2022-10-19 01:40:24', NULL),
(5, 'Gerente', '2022-10-19 01:40:29', NULL),
(6, 'Contador', '2022-10-19 01:40:40', NULL),
(7, 'Analista de Calidad', '2022-10-19 01:41:03', NULL),
(8, 'Desarrollador', '2022-10-19 01:41:16', NULL),
(9, 'Analista de IT', '2022-10-19 01:41:29', NULL),
(10, 'Técnico de Campo', '2022-11-02 19:54:00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cui` bigint(20) NOT NULL,
  `nombres` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` bigint(20) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `departamento_id` bigint(20) UNSIGNED NOT NULL,
  `municipio_id` bigint(20) UNSIGNED NOT NULL,
  `direccion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ubicacion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `empleado_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `cui`, `nombres`, `apellidos`, `telefono`, `email`, `departamento_id`, `municipio_id`, `direccion`, `ubicacion`, `empleado_id`, `created_at`, `updated_at`) VALUES
(1, 2050968911601, 'Hannsel Estuardo', 'Cordón Ac', 12345678, 'hannselcordon@gmail.com', 1, 1, 'Coban Alta Verapaz', '8a Avenida 5-87, Cobán, Guatemala', 2, '2022-10-19 12:43:33', NULL),
(2, 2050968911607, 'Carlos', 'Lopez', 12345678, 'carlos@gmail.com', 1, 16, 'Coban Alta Verapaz', 'RP85+9J7, Chisec, Guatemala', 2, '2022-11-03 22:26:11', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_apicustom`
--

CREATE TABLE `cms_apicustom` (
  `id` int(10) UNSIGNED NOT NULL,
  `permalink` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tabel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aksi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kolom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderby` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_query_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sql_where` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `method_type` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` longtext COLLATE utf8mb4_unicode_ci,
  `responses` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_apicustom`
--

INSERT INTO `cms_apicustom` (`id`, `permalink`, `tabel`, `aksi`, `kolom`, `orderby`, `sub_query_1`, `sql_where`, `nama`, `keterangan`, `parameter`, `created_at`, `updated_at`, `method_type`, `parameters`, `responses`) VALUES
(1, 'clientes', 'clientes', 'list', NULL, NULL, NULL, NULL, 'clientes', NULL, NULL, NULL, NULL, 'get', 'a:10:{i:0;a:5:{s:4:\"name\";s:2:\"id\";s:4:\"type\";s:6:\"string\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:1;a:5:{s:4:\"name\";s:3:\"cui\";s:4:\"type\";s:6:\"string\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:2;a:5:{s:4:\"name\";s:7:\"nombres\";s:4:\"type\";s:6:\"string\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:3;a:5:{s:4:\"name\";s:9:\"apellidos\";s:4:\"type\";s:6:\"string\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:4;a:5:{s:4:\"name\";s:8:\"telefono\";s:4:\"type\";s:6:\"string\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:5;a:5:{s:4:\"name\";s:5:\"email\";s:4:\"type\";s:5:\"email\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:6;a:5:{s:4:\"name\";s:15:\"departamento_id\";s:4:\"type\";s:7:\"integer\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:7;a:5:{s:4:\"name\";s:12:\"municipio_id\";s:4:\"type\";s:7:\"integer\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:8;a:5:{s:4:\"name\";s:9:\"direccion\";s:4:\"type\";s:6:\"string\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:9;a:5:{s:4:\"name\";s:9:\"ubicacion\";s:4:\"type\";s:6:\"string\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}}', 'a:11:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:4:\"type\";s:6:\"bigint\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:1;a:4:{s:4:\"name\";s:3:\"cui\";s:4:\"type\";s:6:\"bigint\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:2;a:4:{s:4:\"name\";s:7:\"nombres\";s:4:\"type\";s:6:\"string\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:3;a:4:{s:4:\"name\";s:9:\"apellidos\";s:4:\"type\";s:6:\"string\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:4;a:4:{s:4:\"name\";s:8:\"telefono\";s:4:\"type\";s:6:\"bigint\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:5;a:4:{s:4:\"name\";s:5:\"email\";s:4:\"type\";s:5:\"email\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:6;a:4:{s:4:\"name\";s:15:\"departamento_id\";s:4:\"type\";s:7:\"integer\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:7;a:4:{s:4:\"name\";s:12:\"municipio_id\";s:4:\"type\";s:7:\"integer\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:8;a:4:{s:4:\"name\";s:9:\"direccion\";s:4:\"type\";s:6:\"string\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:9;a:4:{s:4:\"name\";s:9:\"ubicacion\";s:4:\"type\";s:6:\"string\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:10;a:4:{s:4:\"name\";s:11:\"empleado_id\";s:4:\"type\";s:7:\"integer\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}}'),
(2, 'usuarios_create', 'users', 'save_add', NULL, NULL, NULL, NULL, 'usuarios_create', NULL, NULL, NULL, NULL, 'post', 'a:7:{i:0;a:5:{s:4:\"name\";s:4:\"name\";s:4:\"type\";s:6:\"string\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"1\";}i:1;a:5:{s:4:\"name\";s:5:\"photo\";s:4:\"type\";s:5:\"image\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"1\";}i:2;a:5:{s:4:\"name\";s:5:\"email\";s:4:\"type\";s:5:\"email\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"1\";}i:3;a:5:{s:4:\"name\";s:8:\"password\";s:4:\"type\";s:8:\"password\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"1\";}i:4;a:5:{s:4:\"name\";s:17:\"id_cms_privileges\";s:4:\"type\";s:7:\"integer\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"1\";}i:5;a:5:{s:4:\"name\";s:14:\"remember_token\";s:4:\"type\";s:6:\"string\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"1\";}i:6;a:5:{s:4:\"name\";s:6:\"status\";s:4:\"type\";s:6:\"string\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"1\";}}', 'a:11:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:4:\"type\";s:6:\"bigint\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:1;a:4:{s:4:\"name\";s:4:\"name\";s:4:\"type\";s:6:\"string\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:2;a:4:{s:4:\"name\";s:5:\"photo\";s:4:\"type\";s:5:\"image\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:3;a:4:{s:4:\"name\";s:5:\"email\";s:4:\"type\";s:5:\"email\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:4;a:4:{s:4:\"name\";s:8:\"password\";s:4:\"type\";s:8:\"password\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:5;a:4:{s:4:\"name\";s:17:\"id_cms_privileges\";s:4:\"type\";s:7:\"integer\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:6;a:4:{s:4:\"name\";s:19:\"cms_privileges_name\";s:4:\"type\";s:6:\"string\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:7;a:4:{s:4:\"name\";s:28:\"cms_privileges_is_superadmin\";s:4:\"type\";s:7:\"tinyint\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:8;a:4:{s:4:\"name\";s:26:\"cms_privileges_theme_color\";s:4:\"type\";s:6:\"string\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:9;a:4:{s:4:\"name\";s:14:\"remember_token\";s:4:\"type\";s:6:\"string\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:10;a:4:{s:4:\"name\";s:6:\"status\";s:4:\"type\";s:6:\"string\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}}'),
(3, 'users_list', 'users', 'list', NULL, NULL, NULL, NULL, 'users_list', NULL, NULL, NULL, NULL, 'get', 'a:8:{i:0;a:5:{s:4:\"name\";s:2:\"id\";s:4:\"type\";s:6:\"string\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:1;a:5:{s:4:\"name\";s:4:\"name\";s:4:\"type\";s:6:\"string\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:2;a:5:{s:4:\"name\";s:5:\"photo\";s:4:\"type\";s:5:\"image\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:3;a:5:{s:4:\"name\";s:5:\"email\";s:4:\"type\";s:5:\"email\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:4;a:5:{s:4:\"name\";s:8:\"password\";s:4:\"type\";s:8:\"password\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:5;a:5:{s:4:\"name\";s:17:\"id_cms_privileges\";s:4:\"type\";s:7:\"integer\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:6;a:5:{s:4:\"name\";s:14:\"remember_token\";s:4:\"type\";s:6:\"string\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}i:7;a:5:{s:4:\"name\";s:6:\"status\";s:4:\"type\";s:6:\"string\";s:6:\"config\";N;s:8:\"required\";s:1:\"0\";s:4:\"used\";s:1:\"0\";}}', 'a:11:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:4:\"type\";s:6:\"bigint\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:1;a:4:{s:4:\"name\";s:4:\"name\";s:4:\"type\";s:6:\"string\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:2;a:4:{s:4:\"name\";s:5:\"photo\";s:4:\"type\";s:5:\"image\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:3;a:4:{s:4:\"name\";s:5:\"email\";s:4:\"type\";s:5:\"email\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:4;a:4:{s:4:\"name\";s:8:\"password\";s:4:\"type\";s:8:\"password\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:5;a:4:{s:4:\"name\";s:17:\"id_cms_privileges\";s:4:\"type\";s:7:\"integer\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:6;a:4:{s:4:\"name\";s:19:\"cms_privileges_name\";s:4:\"type\";s:6:\"string\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:7;a:4:{s:4:\"name\";s:28:\"cms_privileges_is_superadmin\";s:4:\"type\";s:7:\"tinyint\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:8;a:4:{s:4:\"name\";s:26:\"cms_privileges_theme_color\";s:4:\"type\";s:6:\"string\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:9;a:4:{s:4:\"name\";s:14:\"remember_token\";s:4:\"type\";s:6:\"string\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:10;a:4:{s:4:\"name\";s:6:\"status\";s:4:\"type\";s:6:\"string\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_apikey`
--

CREATE TABLE `cms_apikey` (
  `id` int(10) UNSIGNED NOT NULL,
  `screetkey` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hit` int(11) DEFAULT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_apikey`
--

INSERT INTO `cms_apikey` (`id`, `screetkey`, `hit`, `status`, `created_at`, `updated_at`) VALUES
(1, '3ed9a39896222f56234261e8200d9203', 0, 'active', '2022-10-19 06:02:34', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_dashboard`
--

CREATE TABLE `cms_dashboard` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_email_queues`
--

CREATE TABLE `cms_email_queues` (
  `id` int(10) UNSIGNED NOT NULL,
  `send_at` datetime DEFAULT NULL,
  `email_recipient` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_content` text COLLATE utf8mb4_unicode_ci,
  `email_attachments` text COLLATE utf8mb4_unicode_ci,
  `is_sent` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_email_templates`
--

CREATE TABLE `cms_email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_email_templates`
--

INSERT INTO `cms_email_templates` (`id`, `name`, `slug`, `subject`, `content`, `description`, `from_name`, `from_email`, `cc_email`, `created_at`, `updated_at`) VALUES
(1, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2022-11-08 04:12:45', NULL),
(2, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2022-11-08 04:59:46', NULL),
(3, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2022-10-24 00:25:19', NULL),
(4, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2022-11-02 19:08:45', NULL),
(5, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2022-11-08 08:02:16', NULL),
(6, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2022-11-08 08:03:30', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_logs`
--

CREATE TABLE `cms_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `ipaddress` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `useragent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `id_cms_users` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_logs`
--

INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `details`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-08 04:13:45', NULL),
(2, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-08 04:14:01', NULL),
(3, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-08 05:00:03', NULL),
(4, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-08 05:00:10', NULL),
(5, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-08 05:00:45', NULL),
(6, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-08 05:01:32', NULL),
(7, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-10-19 00:58:15', NULL),
(8, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/estados/add-save', 'Add New Data  at Estados', '', 1, '2022-10-19 00:58:26', NULL),
(9, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/estados/add-save', 'Add New Data  at Estados', '', 1, '2022-10-19 00:58:38', NULL),
(10, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/estados/add-save', 'Add New Data  at Estados', '', 1, '2022-10-19 00:59:03', NULL),
(11, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tipoordenes/add-save', 'Add New Data  at Tipos de Ordenes de Servicio', '', 1, '2022-10-19 00:59:58', NULL),
(12, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tipoordenes/add-save', 'Add New Data  at Tipos de Ordenes de Servicio', '', 1, '2022-10-19 01:00:15', NULL),
(13, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tipoordenes/add-save', 'Add New Data  at Tipos de Ordenes de Servicio', '', 1, '2022-10-19 01:00:35', NULL),
(14, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tipopagos/add-save', 'Add New Data  at Tipo de Pagos', '', 1, '2022-10-19 01:00:55', NULL),
(15, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tipopagos/add-save', 'Add New Data  at Tipo de Pagos', '', 1, '2022-10-19 01:00:58', NULL),
(16, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-19 01:10:52', NULL),
(17, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-10-19 01:10:55', NULL),
(18, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 2, '2022-10-19 01:11:00', NULL),
(19, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-19 01:11:04', NULL),
(20, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/statistic_builder/add-save', 'Add New Data Prueba at Statistic Builder', '', 1, '2022-10-19 01:13:10', NULL),
(21, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/statistic_builder/edit-save/1', 'Update data Prueba at Statistic Builder', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>slug</td><td>prueba</td><td></td></tr></tbody></table>', 1, '2022-10-19 01:13:22', NULL),
(22, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/cargos/add-save', 'Add New Data  at Cargos', '', 1, '2022-10-19 01:39:56', NULL),
(23, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/cargos/add-save', 'Add New Data  at Cargos', '', 1, '2022-10-19 01:40:04', NULL),
(24, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/cargos/add-save', 'Add New Data  at Cargos', '', 1, '2022-10-19 01:40:18', NULL),
(25, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/cargos/add-save', 'Add New Data  at Cargos', '', 1, '2022-10-19 01:40:24', NULL),
(26, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/cargos/add-save', 'Add New Data  at Cargos', '', 1, '2022-10-19 01:40:29', NULL),
(27, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/cargos/add-save', 'Add New Data  at Cargos', '', 1, '2022-10-19 01:40:40', NULL),
(28, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/cargos/add-save', 'Add New Data  at Cargos', '', 1, '2022-10-19 01:41:03', NULL),
(29, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/cargos/add-save', 'Add New Data  at Cargos', '', 1, '2022-10-19 01:41:16', NULL),
(30, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/cargos/add-save', 'Add New Data  at Cargos', '', 1, '2022-10-19 01:41:29', NULL),
(31, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/tipotrabajos/add-save', 'Add New Data  at Tipos de Orden de Trabajo', '', 1, '2022-10-19 02:17:55', NULL),
(32, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/empleados/add-save', 'Add New Data  at Empleados', '', 1, '2022-10-19 03:01:21', NULL),
(33, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/empleados/add-save', 'Add New Data  at Empleados', '', 1, '2022-10-19 03:02:31', NULL),
(34, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/jerarquias/add-save', 'Add New Data  at Jerarquia Administrativa', '', 1, '2022-10-19 03:37:50', NULL),
(35, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/jerarquias/edit-save/1', 'Update data  at Jerarquia Administrativa', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2022-10-19 04:01:57', NULL),
(36, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/clientes/add-save', 'Add New Data  at Clientes', '', 1, '2022-10-19 05:17:58', NULL),
(37, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/email_templates/edit-save/1', 'Update data Email Template Forgot Password Backend at Email Templates', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>subject</td><td></td><td>password</td></tr><tr><td>from_email</td><td>system@crudbooster.com</td><td>fiberhomeumg@gmail.com</td></tr><tr><td>cc_email</td><td></td><td>hannselcordon@gmail.com</td></tr></tbody></table>', 1, '2022-10-19 06:25:25', NULL),
(38, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data prueba at Menu Management', '', 1, '2022-10-19 07:18:58', NULL),
(39, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-19 07:19:48', NULL),
(40, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-10-19 07:19:53', NULL),
(41, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 2, '2022-10-19 07:19:59', NULL),
(42, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-10-19 07:20:17', NULL),
(43, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/statistic_builder/add-save', 'Add New Data prueba2 at Statistic Builder', '', 2, '2022-10-19 07:26:11', NULL),
(44, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data prueba2 at Menu Management', '', 2, '2022-10-19 07:29:26', NULL),
(45, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/13', 'Update data prueba2 at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td>normal</td><td>red</td></tr><tr><td>parent_id</td><td>0</td><td></td></tr><tr><td>sorting</td><td></td><td></td></tr></tbody></table>', 2, '2022-10-19 07:30:13', NULL),
(46, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/delete/12', 'Delete data prueba at Menu Management', '', 2, '2022-10-19 07:30:20', NULL),
(47, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/13', 'Update data prueba2 at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>path</td><td>statistic_builder/show/prueba2</td><td>statistic_builder/show/prueba</td></tr><tr><td>parent_id</td><td>0</td><td></td></tr><tr><td>sorting</td><td></td><td></td></tr></tbody></table>', 2, '2022-10-19 07:31:45', NULL),
(48, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/statistic_builder/delete/2', 'Delete data prueba2 at Statistic Builder', '', 2, '2022-10-19 07:36:22', NULL),
(49, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 2, '2022-10-19 08:16:31', NULL),
(50, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-10-19 08:17:45', NULL),
(51, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 2, '2022-10-19 08:19:26', NULL),
(52, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/module_generator/delete/22', 'Delete data Clientes at Module Generator', '', 2, '2022-10-19 10:01:14', NULL),
(53, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/estados/add-save', 'Add New Data  at Estados', '', 2, '2022-10-19 12:20:47', NULL),
(54, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/module_generator/delete/23', 'Delete data Clientes at Module Generator', '', 2, '2022-10-19 12:31:29', NULL),
(55, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/clientes/add-save', 'Add New Data  at Clientes', '', 2, '2022-10-19 12:43:33', NULL),
(56, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/productos/add-save', 'Add New Data  at Productos', '', 2, '2022-10-19 13:12:34', NULL),
(57, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/planes/add-save', 'Add New Data  at Planes y Servicios', '', 2, '2022-10-19 13:59:52', NULL),
(58, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/contratos/add-save', 'Add New Data  at Contratos de Servicios', '', 2, '2022-10-19 14:00:35', NULL),
(59, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-10-20 06:41:11', NULL),
(60, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/pagos/add-save', 'Add New Data  at Pagos', '', 2, '2022-10-20 08:17:02', NULL),
(61, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/settings/add-save', 'Add New Data image at Settings', '', 2, '2022-10-20 08:30:19', NULL),
(62, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 2, '2022-10-20 08:34:52', NULL),
(63, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-20 08:35:58', NULL),
(64, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-20 08:38:28', NULL),
(65, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-20 08:39:03', NULL),
(66, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/settings/add-save', 'Add New Data image at Settings', '', 1, '2022-10-20 08:40:00', NULL),
(67, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-20 08:40:18', NULL),
(68, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-20 08:40:30', NULL),
(69, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-20 08:56:13', NULL),
(70, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-20 09:22:34', NULL),
(71, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-20 09:23:05', NULL),
(72, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-20 09:25:14', NULL),
(73, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-20 09:30:24', NULL),
(74, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-20 09:30:47', NULL),
(75, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-20 09:32:51', NULL),
(76, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-20 09:35:25', NULL),
(77, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/contratos/delete/1', 'Delete data 1 at Contratos de Servicios', '', 1, '2022-10-20 09:53:51', NULL),
(78, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/module_generator/delete/26', 'Delete data Contratos de Servicios at Module Generator', '', 1, '2022-10-20 10:17:56', NULL),
(79, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/estados/add-save', 'Add New Data  at Estados', '', 1, '2022-10-20 11:13:28', NULL),
(80, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/estados/add-save', 'Add New Data  at Estados', '', 1, '2022-10-20 11:13:35', NULL),
(81, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/contratos29/add-save', 'Add New Data  at Contratos', '', 1, '2022-10-20 11:14:10', NULL),
(82, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/pagos/add-save', 'Add New Data  at Pagos', '', 1, '2022-10-20 11:15:31', NULL),
(83, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/module_generator/delete/21', 'Delete data Jerarquia Administrativa at Module Generator', '', 1, '2022-10-20 11:56:36', NULL),
(84, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-22 04:01:51', NULL),
(85, '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-22 04:02:53', NULL),
(86, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-22 04:05:35', NULL),
(87, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-22 04:05:42', NULL),
(88, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-10-22 04:07:38', NULL),
(89, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/users/add-save', 'Add New Data Secretaria at Users Management', '', 1, '2022-10-22 04:20:17', NULL),
(90, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-22 04:20:36', NULL),
(91, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'secretaria@gmail.com login with IP Address 127.0.0.1', '', 3, '2022-10-22 04:20:40', NULL),
(92, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'secretaria@gmail.com logout', '', 3, '2022-10-22 04:21:02', NULL),
(93, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'secretaria@gmail.com login with IP Address 127.0.0.1', '', 3, '2022-10-22 04:21:11', NULL),
(94, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'secretaria@gmail.com logout', '', 3, '2022-10-22 04:21:16', NULL),
(95, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-22 04:21:25', NULL),
(96, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data Empleados at Menu Management', '', 1, '2022-10-22 04:22:23', NULL),
(97, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data Gogogle at Menu Management', '', 1, '2022-10-22 04:23:08', NULL),
(98, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-22 04:23:22', NULL),
(99, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'secretaria@gmail.com login with IP Address 127.0.0.1', '', 3, '2022-10-22 04:23:27', NULL),
(100, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/clientes/add', 'Try add data at Clientes', '', 3, '2022-10-22 04:23:57', NULL),
(101, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'secretaria@gmail.com logout', '', 3, '2022-10-22 04:25:58', NULL),
(102, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-22 04:26:03', NULL),
(103, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-23 06:50:53', NULL),
(104, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-23 06:53:00', NULL),
(105, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'secretaria@gmail.com login with IP Address 127.0.0.1', '', 3, '2022-10-23 06:53:09', NULL),
(106, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'secretaria@gmail.com logout', '', 3, '2022-10-23 06:53:19', NULL),
(107, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-23 06:54:58', NULL),
(108, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-23 06:55:26', NULL),
(109, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2022-10-24 00:25:37', NULL),
(110, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'admin@crudbooster.com logout', '', 1, '2022-10-24 00:26:16', NULL),
(111, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-02 19:09:27', NULL),
(112, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:11:49', NULL),
(113, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:11:56', NULL),
(114, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:12:02', NULL),
(115, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:12:07', NULL),
(116, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:12:13', NULL),
(117, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:12:23', NULL),
(118, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:12:29', NULL),
(119, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:12:35', NULL),
(120, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:12:43', NULL),
(121, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:12:48', NULL),
(122, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:12:55', NULL),
(123, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:13:01', NULL),
(124, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:13:05', NULL),
(125, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:13:14', NULL),
(126, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:13:21', NULL),
(127, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:13:26', NULL),
(128, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:13:31', NULL),
(129, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:13:36', NULL),
(130, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:13:41', NULL),
(131, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/departamentos/add-save', 'Add New Data  at Departamentos de Guatemala', '', 1, '2022-11-02 19:13:46', NULL),
(132, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:14:31', NULL),
(133, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:14:38', NULL),
(134, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:14:42', NULL),
(135, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:14:49', NULL),
(136, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:14:54', NULL),
(137, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:15:00', NULL),
(138, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:15:08', NULL),
(139, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:15:18', NULL),
(140, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:15:23', NULL),
(141, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:15:29', NULL),
(142, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:15:39', NULL),
(143, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:15:44', NULL),
(144, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:15:51', NULL),
(145, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:15:57', NULL),
(146, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:16:02', NULL),
(147, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/municipios/add-save', 'Add New Data  at Municipios de Guatemala', '', 1, '2022-11-02 19:16:22', NULL),
(148, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/empleados/add-save', 'Add New Data  at Empleados', '', 1, '2022-11-02 19:40:54', NULL),
(149, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/cms_users/edit-save/1', 'Update data Super Admin at users', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td></td><td>uploads/1/2022-11/logo_search_grid_1x_1.png</td></tr><tr><td>password</td><td>$2y$10$2g5xVnibrMIUJNY5OWblbOnDes8OdSVwOMjfxStw5EQFDk2PbWf5G</td><td>$2y$10$tfhyD7L6fxmBRojApqjpxOR5uaeuGGSaWg71XFFNY2pBJBowWxd2y</td></tr></tbody></table>', 1, '2022-11-02 19:51:05', NULL),
(150, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/cargos/add-save', 'Add New Data  at Cargos', '', 1, '2022-11-02 19:54:00', NULL),
(151, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/empleados/edit-save/3', 'Update data  at Empleados', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>cargo_id</td><td>5</td><td>10</td></tr></tbody></table>', 1, '2022-11-02 19:54:24', NULL),
(152, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/module_generator/delete/29', 'Delete data Contratos at Module Generator', '', 1, '2022-11-02 20:13:21', NULL),
(153, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/contratos30/add-save', 'Add New Data  at Contratos', '', 1, '2022-11-02 20:36:29', NULL),
(154, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/contratos30/add-save', 'Add New Data  at Contratos', '', 1, '2022-11-02 20:48:09', NULL),
(155, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/empleados/edit-save/3', 'Update data  at Empleados', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>cargo_id</td><td>10</td><td>3</td></tr></tbody></table>', 1, '2022-11-02 20:48:57', NULL),
(156, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/contratos30/add-save', 'Add New Data  at Contratos', '', 1, '2022-11-02 20:49:24', NULL),
(157, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/empleados/edit-save/3', 'Update data  at Empleados', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>cargo_id</td><td>3</td><td>10</td></tr></tbody></table>', 1, '2022-11-02 23:48:00', NULL),
(158, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/ordenes/add-save', 'Add New Data  at Ordenes', '', 1, '2022-11-02 23:58:17', NULL),
(159, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/estados/add-save', 'Add New Data  at Estados', '', 1, '2022-11-03 00:26:50', NULL),
(160, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/estados/add-save', 'Add New Data  at Estados', '', 1, '2022-11-03 00:27:04', NULL),
(161, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data Division Politica at Menu Management', '', 1, '2022-11-03 01:00:46', NULL),
(162, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data Ordenes at Menu Management', '', 1, '2022-11-03 01:03:52', NULL),
(163, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data Empleados at Menu Management', '', 1, '2022-11-03 01:11:45', NULL),
(164, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/27', 'Update data Empleados at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td>normal</td><td>red</td></tr></tbody></table>', 1, '2022-11-03 01:12:43', NULL),
(165, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/27', 'Update data Empleados at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td>red</td><td>yellow</td></tr></tbody></table>', 1, '2022-11-03 01:12:53', NULL),
(166, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/18', 'Update data Pagos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-money</td></tr><tr><td>sorting</td><td>12</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:13:43', NULL),
(167, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/6', 'Update data Tipo de Pagos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-money</td><td>fa fa-credit-card</td></tr><tr><td>sorting</td><td>9</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:14:03', NULL),
(168, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/19', 'Update data users at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-user</td></tr><tr><td>sorting</td><td>13</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:14:33', NULL),
(169, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/4', 'Update data Estados at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>icon</td><td>fa fa-file</td><td>fa fa-files-o</td></tr><tr><td>sorting</td><td>8</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:15:28', NULL),
(170, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/4', 'Update data Estados at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td>normal</td><td>green</td></tr><tr><td>sorting</td><td>8</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:15:46', NULL),
(171, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/1', 'Update data Planes y Servicios at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>aqua</td></tr><tr><td>sorting</td><td>7</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:16:07', NULL),
(172, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/23', 'Update data Contratos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>muted</td></tr><tr><td>sorting</td><td>14</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:16:39', NULL),
(173, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/23', 'Update data Contratos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td>muted</td><td>green</td></tr><tr><td>sorting</td><td>14</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:17:02', NULL),
(174, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/24', 'Update data Ordenes at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-file-text</td></tr><tr><td>parent_id</td><td>26</td><td></td></tr><tr><td>sorting</td><td>3</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:21:51', NULL),
(175, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/6', 'Update data Tipo de Pagos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td>normal</td><td>green</td></tr><tr><td>sorting</td><td>9</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:22:38', NULL),
(176, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/15', 'Update data Clientes at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>yellow</td></tr><tr><td>sorting</td><td>10</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:22:56', NULL),
(177, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/16', 'Update data Productos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>aqua</td></tr><tr><td>sorting</td><td>11</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:23:14', NULL);
INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `details`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(178, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/18', 'Update data Pagos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td>normal</td><td>green</td></tr><tr><td>sorting</td><td>12</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:24:08', NULL),
(179, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/19', 'Update data users at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td>normal</td><td>yellow</td></tr><tr><td>sorting</td><td>13</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:24:26', NULL),
(180, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data Pagos at Menu Management', '', 1, '2022-11-03 01:25:03', NULL),
(181, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/15', 'Update data Clientes at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>10</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:30:11', NULL),
(182, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/23', 'Update data Contratos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>13</td><td></td></tr></tbody></table>', 1, '2022-11-03 01:30:47', NULL),
(183, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/users/add-save', 'Add New Data Vendedor at Users Management', '', 1, '2022-11-03 01:31:41', NULL),
(184, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/users/delete-image', 'Delete the image of Vendedor at Users Management', '', 1, '2022-11-03 01:31:59', NULL),
(185, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/users/edit-save/2', 'Update data Vendedor at Users Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td></td><td>uploads/1/2022-11/logo_search_grid_1x_5.png</td></tr><tr><td>password</td><td>$2y$10$IlLmK77dpdXxaXQLOXrUsetvvZE4CIk9Rg1.H5H0KiYsMpR9F4pIK</td><td></td></tr><tr><td>status</td><td></td><td></td></tr></tbody></table>', 1, '2022-11-03 01:32:06', NULL),
(186, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 01:32:19', NULL),
(187, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'vendedor@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-11-03 01:32:24', NULL),
(188, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'vendedor@gmail.com logout', '', 2, '2022-11-03 01:32:47', NULL),
(189, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 01:32:59', NULL),
(190, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/statistic_builder/add-save', 'Add New Data Vendedor at Statistic Builder', '', 1, '2022-11-03 01:33:23', NULL),
(191, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data Vendedor at Menu Management', '', 1, '2022-11-03 01:37:16', NULL),
(192, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 01:37:27', NULL),
(193, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'vendedor@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-11-03 01:37:38', NULL),
(194, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'vendedor@gmail.com logout', '', 2, '2022-11-03 02:04:31', NULL),
(195, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 02:04:38', NULL),
(196, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 02:43:20', NULL),
(197, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'vendedor@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-11-03 02:43:35', NULL),
(198, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'vendedor@gmail.com logout', '', 2, '2022-11-03 02:45:43', NULL),
(199, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 02:45:49', NULL),
(200, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 02:54:03', NULL),
(201, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'vendedor@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-11-03 02:54:10', NULL),
(202, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'vendedor@gmail.com logout', '', 2, '2022-11-03 02:54:25', NULL),
(203, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 02:54:31', NULL),
(204, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 02:57:40', NULL),
(205, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'vendedor@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-11-03 02:57:45', NULL),
(206, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'vendedor@gmail.com logout', '', 2, '2022-11-03 02:58:00', NULL),
(207, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 02:58:07', NULL),
(208, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 03:01:56', NULL),
(209, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'vendedor@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-11-03 03:02:02', NULL),
(210, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', ' logout', '', NULL, '2022-11-03 15:18:58', NULL),
(211, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 15:19:08', NULL),
(212, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/ordenes32/edit-save/1', 'Update data  at Orden a Ejecutar', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>trabajo_id</td><td>1</td><td></td></tr><tr><td>servicio_id</td><td>1</td><td></td></tr><tr><td>tecnico_id</td><td>3</td><td></td></tr><tr><td>fecha_generacion</td><td>2022-11-02</td><td></td></tr><tr><td>supervisor_id</td><td>1</td><td></td></tr><tr><td>cliente_id</td><td>1</td><td></td></tr><tr><td>contrato_id</td><td>1</td><td></td></tr></tbody></table>', 1, '2022-11-03 16:13:14', NULL),
(213, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/ordenes32/edit-save/1', 'Update data  at Orden a Ejecutar', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>trabajo_id</td><td>1</td><td></td></tr><tr><td>servicio_id</td><td>1</td><td></td></tr><tr><td>tecnico_id</td><td>3</td><td></td></tr><tr><td>estado_id</td><td>2</td><td>7</td></tr><tr><td>fecha_generacion</td><td>2022-11-02</td><td></td></tr><tr><td>supervisor_id</td><td>1</td><td></td></tr><tr><td>cliente_id</td><td>1</td><td></td></tr><tr><td>contrato_id</td><td>1</td><td></td></tr></tbody></table>', 1, '2022-11-03 16:15:58', NULL),
(214, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/ordenes32/delete-image', 'Delete the image of 1 at Orden a Ejecutar', '', 1, '2022-11-03 16:16:57', NULL),
(215, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/ordenes32/edit-save/1', 'Update data  at Orden a Ejecutar', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>trabajo_id</td><td>1</td><td></td></tr><tr><td>servicio_id</td><td>1</td><td></td></tr><tr><td>tecnico_id</td><td>3</td><td></td></tr><tr><td>fecha_generacion</td><td>2022-11-02</td><td></td></tr><tr><td>supervisor_id</td><td>1</td><td></td></tr><tr><td>cliente_id</td><td>1</td><td></td></tr><tr><td>contrato_id</td><td>1</td><td></td></tr></tbody></table>', 1, '2022-11-03 16:17:04', NULL),
(216, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/ordenes32/edit-save/1', 'Update data  at Orden a Ejecutar', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>trabajo_id</td><td>1</td><td></td></tr><tr><td>servicio_id</td><td>1</td><td></td></tr><tr><td>tecnico_id</td><td>3</td><td></td></tr><tr><td>estado_id</td><td>7</td><td>8</td></tr><tr><td>fecha_generacion</td><td>2022-11-02</td><td></td></tr><tr><td>supervisor_id</td><td>1</td><td></td></tr><tr><td>evidencias</td><td></td><td>uploads/1/2022-11/tarea_09_2021.pdf</td></tr><tr><td>cliente_id</td><td>1</td><td></td></tr><tr><td>contrato_id</td><td>1</td><td></td></tr></tbody></table>', 1, '2022-11-03 16:17:29', NULL),
(217, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/ordenes/edit-save/1', 'Update data  at Ordenes de Trabajo', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2022-11-03 16:20:24', NULL),
(218, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/30', 'Update data Orden a Ejecutar at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-files-o</td></tr><tr><td>parent_id</td><td>26</td><td></td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2022-11-03 16:22:01', NULL),
(219, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/users/add-save', 'Add New Data Tecnico at Users Management', '', 1, '2022-11-03 16:28:29', NULL),
(220, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/30', 'Update data Orden a Ejecutar at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>parent_id</td><td>26</td><td></td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2022-11-03 16:29:06', NULL),
(221, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 16:29:10', NULL),
(222, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'tecnico@tecnico.com login with IP Address 127.0.0.1', '', 3, '2022-11-03 16:29:13', NULL),
(223, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'tecnico@tecnico.com logout', '', 3, '2022-11-03 16:29:34', NULL),
(224, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 16:29:41', NULL),
(225, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/30', 'Update data Orden a Ejecutar at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>parent_id</td><td>26</td><td></td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2022-11-03 16:30:14', NULL),
(226, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 16:31:04', NULL),
(227, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'tecnico@tecnico.com login with IP Address 127.0.0.1', '', 3, '2022-11-03 16:31:10', NULL),
(228, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'tecnico@tecnico.com logout', '', 3, '2022-11-03 16:31:24', NULL),
(229, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 16:31:30', NULL),
(230, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/26', 'Update data Ordenes at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2022-11-03 16:32:03', NULL),
(231, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 16:32:14', NULL),
(232, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'tecnico@tecnico.com login with IP Address 127.0.0.1', '', 3, '2022-11-03 16:32:20', NULL),
(233, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'tecnico@tecnico.com logout', '', 3, '2022-11-03 16:32:33', NULL),
(234, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 16:32:37', NULL),
(235, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/30', 'Update data Orden a Ejecutar at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>parent_id</td><td>26</td><td></td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2022-11-03 16:32:54', NULL),
(236, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 16:32:58', NULL),
(237, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'tecnico@tecnico.com login with IP Address 127.0.0.1', '', 3, '2022-11-03 16:33:02', NULL),
(238, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'tecnico@tecnico.com logout', '', 3, '2022-11-03 16:33:53', NULL),
(239, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'tecnico@tecnico.com login with IP Address 127.0.0.1', '', 3, '2022-11-03 16:33:55', NULL),
(240, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'tecnico@tecnico.com logout', '', 3, '2022-11-03 16:34:50', NULL),
(241, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'tecnico@tecnico.com login with IP Address 127.0.0.1', '', 3, '2022-11-03 16:34:54', NULL),
(242, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'tecnico@tecnico.com logout', '', 3, '2022-11-03 16:35:00', NULL),
(243, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 16:35:06', NULL),
(244, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/statistic_builder/add-save', 'Add New Data Tecnico at Statistic Builder', '', 1, '2022-11-03 16:58:22', NULL),
(245, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data Dashboard at Menu Management', '', 1, '2022-11-03 17:00:13', NULL),
(246, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 17:02:15', NULL),
(247, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'tecnico@tecnico.com login with IP Address 127.0.0.1', '', 3, '2022-11-03 17:02:21', NULL),
(248, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'tecnico@tecnico.com logout', '', 3, '2022-11-03 17:02:34', NULL),
(249, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 17:02:42', NULL),
(250, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/31', 'Update data Tecnico at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Dashboard</td><td>Tecnico</td></tr><tr><td>path</td><td>statistic_builder/show/vendedor</td><td>statistic_builder/show/tecnico</td></tr><tr><td>parent_id</td><td>0</td><td></td></tr><tr><td>sorting</td><td></td><td></td></tr></tbody></table>', 1, '2022-11-03 17:03:24', NULL),
(251, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 17:05:04', NULL),
(252, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'tecnico@tecnico.com login with IP Address 127.0.0.1', '', 3, '2022-11-03 17:05:10', NULL),
(253, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'tecnico@tecnico.com logout', '', 3, '2022-11-03 17:05:19', NULL),
(254, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 17:05:24', NULL),
(255, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/31', 'Update data Tecnico at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>parent_id</td><td>0</td><td></td></tr><tr><td>sorting</td><td></td><td></td></tr></tbody></table>', 1, '2022-11-03 17:05:42', NULL),
(256, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 17:05:47', NULL),
(257, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'tecnico@tecnico.com login with IP Address 127.0.0.1', '', 3, '2022-11-03 17:05:52', NULL),
(258, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'tecnico@tecnico.com logout', '', 3, '2022-11-03 17:06:40', NULL),
(259, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 17:06:47', NULL),
(260, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/ordenes/add-save', 'Add New Data  at Ordenes de Trabajo', '', 1, '2022-11-03 17:11:29', NULL),
(261, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/ordenes/add-save', 'Add New Data  at Ordenes de Trabajo', '', 1, '2022-11-03 17:20:53', NULL),
(262, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 17:34:34', NULL),
(263, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'tecnico@tecnico.com login with IP Address 127.0.0.1', '', 3, '2022-11-03 17:34:39', NULL),
(264, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/ordenes32/edit-save/3', 'Update data  at Orden a Ejecutar', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>estado_id</td><td>7</td><td>8</td></tr><tr><td>fecha_generacion</td><td>2022-11-03</td><td></td></tr><tr><td>comentario</td><td></td><td>Se realizo instalacion de servicio</td></tr><tr><td>evidencias</td><td></td><td>uploads/3/2022-11/tarea_09_2021.pdf</td></tr><tr><td>fecha_verificacion</td><td></td><td>2022-11-30</td></tr></tbody></table>', 3, '2022-11-03 17:35:50', NULL),
(265, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'tecnico@tecnico.com logout', '', 3, '2022-11-03 18:22:57', NULL),
(266, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 18:23:02', NULL),
(267, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 18:51:48', NULL),
(268, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'tecnico@tecnico.com login with IP Address 127.0.0.1', '', 3, '2022-11-03 18:52:03', NULL),
(269, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'tecnico@tecnico.com logout', '', 3, '2022-11-03 18:54:18', NULL),
(270, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 18:54:22', NULL),
(271, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/statistic_builder/edit-save/1', 'Update data Principal at Statistic Builder', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Prueba</td><td>Principal</td></tr><tr><td>slug</td><td>prueba</td><td></td></tr></tbody></table>', 1, '2022-11-03 18:55:01', NULL),
(272, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/contratos30/add-save', 'Add New Data  at Contratos', '', 1, '2022-11-03 20:39:14', NULL),
(273, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/26', 'Update data Ordenes at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2022-11-03 20:43:37', NULL),
(274, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/30', 'Update data Orden a Ejecutar at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>parent_id</td><td>26</td><td></td></tr><tr><td>sorting</td><td>4</td><td></td></tr></tbody></table>', 1, '2022-11-03 20:43:56', NULL),
(275, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/24', 'Update data Ordenes at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>parent_id</td><td>26</td><td></td></tr><tr><td>sorting</td><td>3</td><td></td></tr></tbody></table>', 1, '2022-11-03 20:44:07', NULL),
(276, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/23', 'Update data Contratos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>14</td><td></td></tr></tbody></table>', 1, '2022-11-03 20:44:29', NULL),
(277, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/users/add-save', 'Add New Data Supervisor at Users Management', '', 1, '2022-11-03 20:45:18', NULL),
(278, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 20:45:30', NULL),
(279, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'supervisor@supervisor.com login with IP Address 127.0.0.1', '', 4, '2022-11-03 20:45:33', NULL),
(280, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'supervisor@supervisor.com logout', '', 4, '2022-11-03 20:51:01', NULL),
(281, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 20:51:06', NULL),
(282, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/statistic_builder/add-save', 'Add New Data Supervisor at Statistic Builder', '', 1, '2022-11-03 20:54:35', NULL),
(283, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data Supervisor at Menu Management', '', 1, '2022-11-03 21:04:21', NULL),
(284, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 21:04:26', NULL),
(285, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'supervisor@supervisor.com login with IP Address 127.0.0.1', '', 4, '2022-11-03 21:04:31', NULL),
(286, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'supervisor@supervisor.com logout', '', 4, '2022-11-03 21:04:53', NULL),
(287, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 21:05:01', NULL),
(288, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/contratos30/add-save', 'Add New Data  at Contratos', '', 1, '2022-11-03 21:10:52', NULL),
(289, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/27', 'Update data Empleados at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>3</td><td></td></tr></tbody></table>', 1, '2022-11-03 21:20:07', NULL),
(290, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/7', 'Update data Cargos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>parent_id</td><td>27</td><td></td></tr><tr><td>sorting</td><td>2</td><td></td></tr></tbody></table>', 1, '2022-11-03 21:20:16', NULL),
(291, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/9', 'Update data Empleados at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>parent_id</td><td>27</td><td></td></tr></tbody></table>', 1, '2022-11-03 21:20:25', NULL),
(292, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/users/add-save', 'Add New Data Recursos Humanos at Users Management', '', 1, '2022-11-03 21:21:57', NULL),
(293, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 21:22:08', NULL),
(294, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'recursos@recursos.com login with IP Address 127.0.0.1', '', 5, '2022-11-03 21:22:10', NULL),
(295, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'recursos@recursos.com logout', '', 5, '2022-11-03 21:23:05', NULL),
(296, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 21:23:10', NULL),
(297, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/statistic_builder/add-save', 'Add New Data Recursos Humanos at Statistic Builder', '', 1, '2022-11-03 21:24:33', NULL),
(298, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data Recursos Humanos at Menu Management', '', 1, '2022-11-03 21:48:19', NULL),
(299, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 21:48:24', NULL),
(300, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'recursos@recursos.com login with IP Address 127.0.0.1', '', 5, '2022-11-03 21:48:28', NULL),
(301, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'recursos@recursos.com logout', '', 5, '2022-11-03 21:49:17', NULL),
(302, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 21:49:26', NULL),
(303, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/productos/add-save', 'Add New Data  at Productos', '', 1, '2022-11-03 21:52:34', NULL),
(304, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/empleados/add-save', 'Add New Data  at Empleados', '', 1, '2022-11-03 21:54:07', NULL),
(305, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/add-save', 'Add New Data Cartera at Menu Management', '', 1, '2022-11-03 21:55:02', NULL),
(306, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 21:56:01', NULL),
(307, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'vendedor@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-11-03 21:56:04', NULL),
(308, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'vendedor@gmail.com logout', '', 2, '2022-11-03 21:56:11', NULL),
(309, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'vendedor@gmail.com login with IP Address 127.0.0.1', '', 2, '2022-11-03 21:57:24', NULL),
(310, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'vendedor@gmail.com logout', '', 2, '2022-11-03 21:57:29', NULL),
(311, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 21:57:34', NULL),
(312, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/contratos33/add-save', 'Add New Data  at Servicios_Online', '', 1, '2022-11-03 22:05:26', NULL),
(313, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/1', 'Update data Planes y Servicios at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>13</td><td></td></tr></tbody></table>', 1, '2022-11-03 22:11:39', NULL),
(314, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/16', 'Update data Productos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>15</td><td></td></tr></tbody></table>', 1, '2022-11-03 22:12:34', NULL),
(315, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/18', 'Update data Pagos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>parent_id</td><td>28</td><td></td></tr><tr><td>sorting</td><td>2</td><td></td></tr></tbody></table>', 1, '2022-11-03 22:13:05', NULL),
(316, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/users/add-save', 'Add New Data Cliente Fiberhome at Users Management', '', 1, '2022-11-03 22:15:35', NULL),
(317, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 22:15:42', NULL),
(318, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'cliente@cliente.com login with IP Address 127.0.0.1', '', 6, '2022-11-03 22:15:44', NULL),
(319, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'cliente@cliente.com logout', '', 6, '2022-11-03 22:16:18', NULL),
(320, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 22:16:24', NULL),
(321, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/35', 'Update data Servicios_Online at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>sorting</td><td>18</td><td></td></tr></tbody></table>', 1, '2022-11-03 22:18:31', NULL),
(322, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 22:18:40', NULL),
(323, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'cliente@cliente.com login with IP Address 127.0.0.1', '', 6, '2022-11-03 22:18:48', NULL),
(324, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/contratos33', 'Try view the data :name at Servicios_Online', '', 6, '2022-11-03 22:18:52', NULL),
(325, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'cliente@cliente.com logout', '', 6, '2022-11-03 22:18:58', NULL),
(326, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 22:19:04', NULL),
(327, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 22:19:38', NULL),
(328, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'cliente@cliente.com login with IP Address 127.0.0.1', '', 6, '2022-11-03 22:19:44', NULL),
(329, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'cliente@cliente.com logout', '', 6, '2022-11-03 22:20:19', NULL),
(330, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 22:20:25', NULL),
(331, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/menu_management/edit-save/28', 'Update data Pagos at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>6</td><td></td></tr></tbody></table>', 1, '2022-11-03 22:20:43', NULL),
(332, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 22:20:53', NULL),
(333, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'cliente@cliente.com login with IP Address 127.0.0.1', '', 6, '2022-11-03 22:20:57', NULL),
(334, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'cliente@cliente.com logout', '', 6, '2022-11-03 22:21:22', NULL),
(335, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 22:21:27', NULL),
(336, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/clientes/add-save', 'Add New Data  at Clientes', '', 1, '2022-11-03 22:26:11', NULL),
(337, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 22:27:19', NULL),
(338, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'cliente@cliente.com login with IP Address 127.0.0.1', '', 6, '2022-11-03 22:27:23', NULL),
(339, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'cliente@cliente.com logout', '', 6, '2022-11-03 22:27:33', NULL),
(340, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-03 22:27:38', NULL),
(341, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-03 22:28:27', NULL),
(342, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-08 05:09:13', NULL),
(343, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-08 05:09:47', NULL),
(344, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-08 05:10:08', NULL);
INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `details`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(345, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/cms_users/edit-save/6', 'Update data Cliente Fiberhome at users', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td>uploads/1/2022-11/logo_search_grid_1x_7.png</td><td></td></tr><tr><td>password</td><td>$2y$10$NuihcPADz4FhlkpkzG3PNOU2u6GgP6NstMfSgbztEywbCVGww1Itm</td><td>$2y$10$e7uAWK5FDZ1iexaEcnuYKuV7Qejpq8aLmUfc5/dm4BFu9obKfJ4gC</td></tr></tbody></table>', 1, '2022-11-08 05:10:43', NULL),
(346, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/cms_users/edit-save/5', 'Update data Recursos Humanos at users', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td>uploads/1/2022-11/logo_search_grid_1x_1.png</td><td></td></tr><tr><td>password</td><td>$2y$10$1pb20W1OalNf4iJ67p.HvO06pB4/Yl9dgNotlqY7ejVqjzFe7SdsW</td><td>$2y$10$ydDmIWrK.u2KMYS9Nbnxv./kt7PRjAWE8JY2t24mIRQa9kioSvagi</td></tr></tbody></table>', 1, '2022-11-08 05:11:03', NULL),
(347, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/cms_users/edit-save/5', 'Update data Recursos Humanos at users', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>password</td><td>$2y$10$ydDmIWrK.u2KMYS9Nbnxv./kt7PRjAWE8JY2t24mIRQa9kioSvagi</td><td>$2y$10$nU7oIsEQlvnE3A1NUUoyLOh3nN27.Cd3h7LzNzDJKvD5jDBytV/R.</td></tr></tbody></table>', 1, '2022-11-08 05:11:03', NULL),
(348, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/cms_users/edit-save/4', 'Update data Supervisor at users', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td>uploads/1/2022-11/logo_search_grid_1x_2.png</td><td></td></tr><tr><td>password</td><td>$2y$10$DSgqrRceKhmVFzUU.atx7Ojx/wUabZWevyA6Q73uKvPO8K0HjCUva</td><td>$2y$10$f4mBo2gDjkHAsb/hkxpiUOx2mzIujzd2WyKh.pOI1TsQ2CXeJ.ufq</td></tr></tbody></table>', 1, '2022-11-08 05:11:21', NULL),
(349, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/cms_users/edit-save/3', 'Update data Tecnico at users', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td>uploads/1/2022-11/logo_search_grid_1x_3.png</td><td></td></tr><tr><td>password</td><td>$2y$10$fdCdbWtf3SQMLnOe0izl7eBTFbvV2aZ9ZZ5F2CvDxrLdCa46RPvAW</td><td>$2y$10$nrx0n8l3.Ymwswht5OWZLevLnEy70bgM07Kst4FVp1rVxrtjA6y7K</td></tr></tbody></table>', 1, '2022-11-08 05:11:37', NULL),
(350, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/cms_users/edit-save/2', 'Update data Vendedor at users', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td>uploads/1/2022-11/logo_search_grid_1x_5.png</td><td></td></tr><tr><td>password</td><td>$2y$10$IlLmK77dpdXxaXQLOXrUsetvvZE4CIk9Rg1.H5H0KiYsMpR9F4pIK</td><td>$2y$10$7mJKG1YG7XeE4zIOvyg9T.6znBxUQpNYK4JTlIzbOUGa02B5oR8ha</td></tr></tbody></table>', 1, '2022-11-08 05:11:53', NULL),
(351, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/cms_users/edit-save/1', 'Update data Super Admin at users', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td></td><td>uploads/1/2022-11/fondo_abstracto_blanco_23_2148833155.webp</td></tr><tr><td>password</td><td>$2y$10$ahBEnaPfyj7rOIUQlogXPOsD0hwNbiREfloonoCULWWyjKzUPFO8K</td><td>$2y$10$/qo6ip5zS3Dv6xgpEli0yeoiXlyUOsDJy/TOP8AEueTXvPzyZS9/u</td></tr></tbody></table>', 1, '2022-11-08 05:12:25', NULL),
(352, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-08 05:12:33', NULL),
(353, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'cliente@cliente.com login with IP Address 127.0.0.1', '', 6, '2022-11-08 05:12:40', NULL),
(354, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/logout', 'cliente@cliente.com logout', '', 6, '2022-11-08 05:13:57', NULL),
(355, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-08 05:14:06', NULL),
(356, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-08 05:15:31', NULL),
(357, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://localhost/proyectoass2/public/admin/login', 'hannselcordon@gmail.com login with IP Address ::1', '', 1, '2022-11-08 05:17:00', NULL),
(358, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://localhost/proyectoass2/public/admin/users/delete-image', 'Delete the image of Super Admin at Users Management', '', 1, '2022-11-08 05:26:42', NULL),
(359, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://localhost/proyectoass2/public/admin/users/edit-save/1', 'Update data Super Admin at Users Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td></td><td>uploads/1/2022-11/superadmin.png</td></tr><tr><td>password</td><td>$2y$10$/qo6ip5zS3Dv6xgpEli0yeoiXlyUOsDJy/TOP8AEueTXvPzyZS9/u</td><td>$2y$10$ZMe8kpj17hNldyuxkK6hWe5adfW7jJvS1uAqsdD6BiIA0YpWOWhl2</td></tr><tr><td>id_cms_privileges</td><td>1</td><td></td></tr><tr><td>status</td><td>Active</td><td></td></tr></tbody></table>', 1, '2022-11-08 05:26:56', NULL),
(360, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://localhost/proyectoass2/public/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-08 05:27:02', NULL),
(361, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://localhost/proyectoass2/public/admin/login', 'hannselcordon@gmail.com login with IP Address ::1', '', 1, '2022-11-08 05:27:04', NULL),
(362, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://localhost/proyectoass2/public/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-08 05:34:13', NULL),
(363, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://localhost/proyectoass2/public/admin/login', 'hannselcordon@gmail.com login with IP Address ::1', '', 1, '2022-11-08 05:34:19', NULL),
(364, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://localhost/proyectoass2/public/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-08 05:52:54', NULL),
(365, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://localhost/proyectoass2/public/admin/login', 'hannselcordon@gmail.com login with IP Address ::1', '', 1, '2022-11-08 05:52:56', NULL),
(366, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://localhost/proyectoass2/public/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-08 06:00:31', NULL),
(367, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://localhost/proyectoass2/public/admin/login', 'hannselcordon@gmail.com login with IP Address ::1', '', 1, '2022-11-08 06:00:32', NULL),
(368, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://localhost/proyectoass2/public/admin/login', 'hannselcordon@gmail.com login with IP Address ::1', '', 1, '2022-11-08 06:00:33', NULL),
(369, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://localhost/proyectoass2/public/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-08 06:02:12', NULL),
(370, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-08 06:02:24', NULL),
(371, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://localhost/proyectoass2/public/admin/login', 'hannselcordon@gmail.com login with IP Address ::1', '', 1, '2022-11-08 06:05:51', NULL),
(372, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-08 07:58:42', NULL),
(373, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-08 08:00:38', NULL),
(374, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-08 08:02:28', NULL),
(375, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-08 08:03:17', NULL),
(376, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-08 08:07:04', NULL),
(377, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-08 08:07:52', NULL),
(378, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-08 08:11:37', NULL),
(379, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/logout', 'hannselcordon@gmail.com logout', '', 1, '2022-11-08 08:12:03', NULL),
(380, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'hannselcordon@gmail.com login with IP Address 127.0.0.1', '', 1, '2022-11-08 08:59:33', NULL),
(381, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-08 03:06:51', NULL),
(382, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-08 03:07:02', NULL),
(383, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-08 03:08:01', NULL),
(384, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-08 03:08:29', NULL),
(385, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-08 03:08:32', NULL),
(386, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/users/delete-image', 'Eliminar la imagen de Super Admin en Users Management', '', 1, '2022-11-08 03:14:51', NULL),
(387, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/users/edit-save/1', 'Actualizar información Super Admin en Users Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td></td><td>uploads/1/2022-11/icons8_configuracion_del_administrador_48.png</td></tr><tr><td>password</td><td>$2y$10$ld4VsNYBbT1SWi1WutI2x.QVkPYn1XTgvvaPHCqkKvts3/E0TaumC</td><td>$2y$10$JlmQQG4uRxsYNZhd0mMUXeW.QCjF2gTReryzojBpRAyoqo8mNBN.C</td></tr><tr><td>id_cms_privileges</td><td>1</td><td></td></tr><tr><td>remember_token</td><td>o2ERSPhVJ6rGg38Q0X6CASS04QkS15Gc6RNde1EDi602oVCRqoJJEqMXnges</td><td></td></tr><tr><td>status</td><td>Active</td><td></td></tr></tbody></table>', 1, '2022-11-08 03:15:10', NULL),
(388, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/users/edit-save/1', 'Actualizar información Super Admin en Users Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>password</td><td>$2y$10$JlmQQG4uRxsYNZhd0mMUXeW.QCjF2gTReryzojBpRAyoqo8mNBN.C</td><td>$2y$10$dLmnR0wwSNaT/Dg9sR4rIuDcb8GULubE33T/iEW2HDYQfNpKZTDTO</td></tr><tr><td>id_cms_privileges</td><td>1</td><td></td></tr><tr><td>remember_token</td><td>o2ERSPhVJ6rGg38Q0X6CASS04QkS15Gc6RNde1EDi602oVCRqoJJEqMXnges</td><td></td></tr><tr><td>status</td><td>Active</td><td></td></tr></tbody></table>', 1, '2022-11-08 03:15:14', NULL),
(389, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-08 03:15:19', NULL),
(390, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-08 03:15:21', NULL),
(391, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/cms_users/edit-save/6', 'Actualizar información Cliente Fiberhome en users', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td></td><td>uploads/1/2022-11/icons8_gestion_de_clientes_16.png</td></tr><tr><td>password</td><td>$2y$10$e7uAWK5FDZ1iexaEcnuYKuV7Qejpq8aLmUfc5/dm4BFu9obKfJ4gC</td><td>$2y$10$EgSwnN42dLSd/2YzORlJEunIXeuDRBFGINbZoRqnLWey40cedfer2</td></tr><tr><td>remember_token</td><td></td><td></td></tr></tbody></table>', 1, '2022-11-08 03:16:15', NULL),
(392, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/cms_users/delete-image', 'Eliminar la imagen de Super Admin en users', '', 1, '2022-11-08 03:17:18', NULL),
(393, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/cms_users/edit-save/1', 'Actualizar información Super Admin en users', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td></td><td>uploads/1/2022-11/icons8_tipo_de_mascara_de_grupo_de_administradores_de_base_de_datos_sql_7_48.png</td></tr><tr><td>password</td><td>$2y$10$dLmnR0wwSNaT/Dg9sR4rIuDcb8GULubE33T/iEW2HDYQfNpKZTDTO</td><td>$2y$10$R/YjfbYNYNiVdSDZupKyeuA8/Di4YUYiw.wvqGEZpyZ9NrKbK7OJm</td></tr><tr><td>remember_token</td><td>o2ERSPhVJ6rGg38Q0X6CASS04QkS15Gc6RNde1EDi602oVCRqoJJEqMXnges</td><td></td></tr></tbody></table>', 1, '2022-11-08 03:17:29', NULL),
(394, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/cms_users/edit-save/2', 'Actualizar información Vendedor en users', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td></td><td>uploads/1/2022-11/icons8_salesman_skin_type_5_48.png</td></tr><tr><td>password</td><td>$2y$10$7mJKG1YG7XeE4zIOvyg9T.6znBxUQpNYK4JTlIzbOUGa02B5oR8ha</td><td>$2y$10$XVfkKcJxpqQ96II540OCDuPjum7mpxrcTpW8KI5qlJxujeUlUU3sK</td></tr><tr><td>remember_token</td><td></td><td></td></tr></tbody></table>', 1, '2022-11-08 03:19:00', NULL),
(395, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/cms_users/edit-save/5', 'Actualizar información Recursos Humanos en users', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td></td><td>uploads/1/2022-11/icons8_configuracion_del_administrador_48.png</td></tr><tr><td>password</td><td>$2y$10$nU7oIsEQlvnE3A1NUUoyLOh3nN27.Cd3h7LzNzDJKvD5jDBytV/R.</td><td>$2y$10$bJSqwRscPHz12bkmNP0kJesmJHukMwZz0FsQCghJeY1L7npocPfP.</td></tr><tr><td>remember_token</td><td></td><td></td></tr></tbody></table>', 1, '2022-11-08 03:19:38', NULL),
(396, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/cms_users/edit-save/4', 'Actualizar información Supervisor en users', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td></td><td>uploads/1/2022-11/icons8_supervisor_64.png</td></tr><tr><td>password</td><td>$2y$10$f4mBo2gDjkHAsb/hkxpiUOx2mzIujzd2WyKh.pOI1TsQ2CXeJ.ufq</td><td>$2y$10$/IgLpjXhuxmX2FQJWcyjkO4vaK1Kf0cNR3oS0PRQiDNLhq2JotaRS</td></tr><tr><td>remember_token</td><td></td><td></td></tr></tbody></table>', 1, '2022-11-08 03:20:07', NULL),
(397, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/cms_users/edit-save/3', 'Actualizar información Tecnico en users', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td></td><td>uploads/1/2022-11/icons8_trabajadores_masculinos_48.png</td></tr><tr><td>password</td><td>$2y$10$nrx0n8l3.Ymwswht5OWZLevLnEy70bgM07Kst4FVp1rVxrtjA6y7K</td><td>$2y$10$YhBT27tiEbtoSdR/FMuCXOcwWtAjflNpDqr9OJryC5U/0J171vRE.</td></tr><tr><td>remember_token</td><td></td><td></td></tr></tbody></table>', 1, '2022-11-08 03:20:30', NULL),
(398, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/cms_users/delete-image', 'Eliminar la imagen de Cliente Fiberhome en users', '', 1, '2022-11-08 03:22:10', NULL),
(399, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/cms_users/edit-save/6', 'Actualizar información Cliente Fiberhome en users', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td></td><td>uploads/1/2022-11/icons8_man_with_beard_medium_skin_tone_48.png</td></tr><tr><td>password</td><td>$2y$10$EgSwnN42dLSd/2YzORlJEunIXeuDRBFGINbZoRqnLWey40cedfer2</td><td>$2y$10$dfJoo9ZBCZVO1yxYGRfP1.2apv1zraVoWbnbC2125QGr9iRKOZ3pm</td></tr><tr><td>remember_token</td><td></td><td></td></tr></tbody></table>', 1, '2022-11-08 03:22:20', NULL),
(400, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-08 03:31:07', NULL),
(401, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/login', 'Ingreso de cliente@cliente.com desde la Dirección IP 127.0.0.1', '', 6, '2022-11-08 03:31:24', NULL),
(402, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/logout', 'cliente@cliente.com se desconectó', '', 6, '2022-11-08 03:31:36', NULL),
(403, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/login', 'Ingreso de recursos@recursos.com desde la Dirección IP 127.0.0.1', '', 5, '2022-11-08 03:31:43', NULL),
(404, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/logout', 'recursos@recursos.com se desconectó', '', 5, '2022-11-08 03:31:52', NULL),
(405, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/login', 'Ingreso de supervisor@supervisor.com desde la Dirección IP 127.0.0.1', '', 4, '2022-11-08 03:32:00', NULL),
(406, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/logout', 'supervisor@supervisor.com se desconectó', '', 4, '2022-11-08 03:32:09', NULL),
(407, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/login', 'Ingreso de tecnico@tecnico.com desde la Dirección IP 127.0.0.1', '', 3, '2022-11-08 03:32:17', NULL),
(408, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/logout', ' se desconectó', '', NULL, '2022-11-08 14:51:42', NULL),
(409, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-08 14:51:54', NULL),
(410, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-08 14:52:29', NULL),
(411, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-08 14:53:40', NULL),
(412, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-08 16:56:10', NULL),
(413, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/login', 'Ingreso de cliente@cliente.com desde la Dirección IP 127.0.0.1', '', 6, '2022-11-08 16:56:16', NULL),
(414, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/logout', 'cliente@cliente.com se desconectó', '', 6, '2022-11-08 16:57:21', NULL),
(415, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/login', 'Ingreso de recursos@recursos.com desde la Dirección IP 127.0.0.1', '', 5, '2022-11-08 16:57:25', NULL),
(416, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/logout', 'recursos@recursos.com se desconectó', '', 5, '2022-11-08 16:58:01', NULL),
(417, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/login', 'Ingreso de supervisor@supervisor.com desde la Dirección IP 127.0.0.1', '', 4, '2022-11-08 16:58:06', NULL),
(418, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/logout', 'supervisor@supervisor.com se desconectó', '', 4, '2022-11-08 16:58:17', NULL),
(419, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/login', 'Ingreso de tecnico@tecnico.com desde la Dirección IP 127.0.0.1', '', 3, '2022-11-08 16:58:22', NULL),
(420, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/logout', 'tecnico@tecnico.com se desconectó', '', 3, '2022-11-08 16:59:59', NULL),
(421, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-08 17:11:54', NULL),
(422, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-08 17:34:09', NULL),
(423, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-08 17:37:22', NULL),
(424, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/users/add-save', 'Añadir nueva información Secretaria en Users Management', '', 1, '2022-11-08 18:01:25', NULL),
(425, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-08 18:01:43', NULL),
(426, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/login', 'Ingreso de secretaria@secretaria.com desde la Dirección IP 127.0.0.1', '', 7, '2022-11-08 18:01:57', NULL),
(427, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/logout', 'secretaria@secretaria.com se desconectó', '', 7, '2022-11-08 18:02:28', NULL),
(428, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-08 18:03:04', NULL),
(429, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-08 18:03:11', NULL),
(430, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/login', 'Ingreso de cliente@cliente.com desde la Dirección IP 127.0.0.1', '', 6, '2022-11-08 18:03:17', NULL),
(431, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/logout', 'cliente@cliente.com se desconectó', '', 6, '2022-11-08 18:05:18', NULL),
(432, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-08 18:05:22', NULL),
(433, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-08 18:07:35', NULL),
(434, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-08 18:07:48', NULL),
(435, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/cms_users/delete-image', 'Eliminar la imagen de Secretaria en users', '', 1, '2022-11-08 18:10:35', NULL),
(436, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/cms_users/edit-save/7', 'Actualizar información Secretaria en users', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td></td><td>uploads/1/2022-11/secretaria.png</td></tr><tr><td>password</td><td>$2y$10$ErFIQCTUicsbmE1PTSurTuq3kh/TE0XxQEx0agC/ad/GjoApWuNCi</td><td>$2y$10$NWUz6xLJcPiD8uzFMwNZBexACc2aMBPOsV1uAIMUMce0eYui8Z4Fe</td></tr><tr><td>remember_token</td><td></td><td></td></tr><tr><td>status</td><td></td><td>Activo</td></tr></tbody></table>', 1, '2022-11-08 18:10:52', NULL),
(437, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8002/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-08 18:38:54', NULL),
(438, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-08 20:22:22', NULL),
(439, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-08 20:31:44', NULL),
(440, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 127.0.0.1', '', 1, '2022-11-08 20:31:52', NULL),
(441, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-08 20:32:10', NULL),
(442, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de cliente@cliente.com desde la Dirección IP 127.0.0.1', '', 6, '2022-11-08 20:32:21', NULL),
(443, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'cliente@cliente.com se desconectó', '', 6, '2022-11-08 20:32:46', NULL),
(444, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de supervisor@supervisor.com desde la Dirección IP 127.0.0.1', '', 4, '2022-11-08 20:32:57', NULL),
(445, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'supervisor@supervisor.com se desconectó', '', 4, '2022-11-08 20:33:21', NULL),
(446, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de recursos@recursos.com desde la Dirección IP 127.0.0.1', '', 5, '2022-11-08 20:33:33', NULL),
(447, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'recursos@recursos.com se desconectó', '', 5, '2022-11-08 20:33:56', NULL),
(448, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de tecnico@tecnico.com desde la Dirección IP 127.0.0.1', '', 3, '2022-11-08 20:34:12', NULL),
(449, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/logout', 'tecnico@tecnico.com se desconectó', '', 3, '2022-11-08 20:34:36', NULL),
(450, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://127.0.0.1:8001/admin/login', 'Ingreso de vendedor@gmail.com desde la Dirección IP 127.0.0.1', '', 2, '2022-11-08 20:34:52', NULL),
(451, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://192.168.0.17:8001/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.22', '', 1, '2022-11-08 21:02:35', NULL),
(452, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://firehome.local/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.22', '', 1, '2022-11-08 21:12:00', NULL),
(453, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://firehome.local/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-08 21:14:07', NULL),
(454, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://firehome.local/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.22', '', 1, '2022-11-08 21:15:52', NULL),
(455, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://firehome.local/admin/cms_users/add-save', 'Añadir nueva información Prueba en users', '', 1, '2022-11-08 21:17:57', NULL),
(456, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://firehome.local/admin/users/edit-save/8', 'Actualizar información Prueba en Users Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>password</td><td>$2y$10$IhXtVoaVgHT0MajKmFxtquwVfwdEySomCMd6TvaayQznqBc6fl1oK</td><td></td></tr><tr><td>id_cms_privileges</td><td>2</td><td>8</td></tr><tr><td>remember_token</td><td></td><td></td></tr><tr><td>status</td><td>Activo</td><td></td></tr></tbody></table>', 1, '2022-11-08 21:18:50', NULL),
(457, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://firehome.local/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-08 21:18:58', NULL),
(458, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://firehome.local/admin/login', 'Ingreso de prueba@prueba.com desde la Dirección IP 192.168.0.22', '', 8, '2022-11-08 21:19:10', NULL),
(459, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/login', 'Ingreso de prueba@prueba.com desde la Dirección IP 192.168.0.22', '', 8, '2022-11-08 21:21:06', NULL),
(460, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/logout', 'prueba@prueba.com se desconectó', '', 8, '2022-11-08 21:21:25', NULL),
(461, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.22', '', 1, '2022-11-08 21:22:52', NULL),
(462, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/users/edit-save/5', 'Actualizar información Recursos Humanos en Users Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>password</td><td>$2y$10$bJSqwRscPHz12bkmNP0kJesmJHukMwZz0FsQCghJeY1L7npocPfP.</td><td>$2y$10$IH9ykgnYl6Glmxs7b9R8Hem1FeuFXWX/k3DmNJKXsVDLCwqHcEbd2</td></tr><tr><td>remember_token</td><td></td><td></td></tr><tr><td>status</td><td>Activo</td><td></td></tr></tbody></table>', 1, '2022-11-08 21:23:31', NULL),
(463, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-08 21:23:37', NULL),
(464, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/logout', ' se desconectó', '', NULL, '2022-11-08 21:23:37', NULL),
(465, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/login', 'Ingreso de recursos@recursos.com desde la Dirección IP 192.168.0.22', '', 5, '2022-11-08 21:23:41', NULL),
(466, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/logout', 'recursos@recursos.com se desconectó', '', 5, '2022-11-08 21:24:12', NULL),
(467, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.22', '', 1, '2022-11-08 21:24:16', NULL),
(468, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:106.0) Gecko/20100101 Firefox/106.0', 'http://firehome.local/admin/logout', 'prueba@prueba.com se desconectó', '', 8, '2022-11-09 01:04:57', NULL),
(469, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-09 02:13:53', NULL),
(470, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.22', '', 1, '2022-11-09 02:17:45', NULL),
(471, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-09 02:23:27', NULL),
(472, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.22', '', 1, '2022-11-09 02:23:40', NULL),
(473, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/users/edit-save/9', 'Actualizar información Invitado en Users Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>photo</td><td></td><td>uploads/1/2022-11/descarga.jpg</td></tr><tr><td>password</td><td>$2y$10$ob2.YnDqhozRkyoiY6sUJOaS5yFNwxP9xV3uxDJyPBVwY2Pxia2Ue</td><td>$2y$10$AHWVPi7HZgUYOBDBPcyeJuvlvKlARNFNQAN5G/hCWIKEN8aAGjF5m</td></tr><tr><td>id_cms_privileges</td><td>5</td><td>8</td></tr><tr><td>remember_token</td><td></td><td></td></tr><tr><td>status</td><td></td><td></td></tr></tbody></table>', 1, '2022-11-09 02:28:36', NULL),
(474, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-09 02:29:36', NULL),
(475, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/login', 'Ingreso de invitado@invitado.com desde la Dirección IP 192.168.0.22', '', 9, '2022-11-09 02:29:45', NULL),
(476, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/logout', 'invitado@invitado.com se desconectó', '', 9, '2022-11-09 02:29:54', NULL),
(477, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.22', '', 1, '2022-11-09 02:29:59', NULL),
(478, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-09 02:30:33', NULL),
(479, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/login', 'Ingreso de invitado@invitado.com desde la Dirección IP 192.168.0.22', '', 9, '2022-11-09 02:30:38', NULL),
(480, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/logout', 'invitado@invitado.com se desconectó', '', 9, '2022-11-09 02:30:47', NULL),
(481, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.22', '', 1, '2022-11-09 02:30:52', NULL),
(482, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/menu_management/edit-save/27', 'Actualizar información Empleados en Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>sorting</td><td>7</td><td></td></tr></tbody></table>', 1, '2022-11-09 02:31:36', NULL),
(483, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/menu_management/edit-save/7', 'Actualizar información Cargos en Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>parent_id</td><td>27</td><td></td></tr><tr><td>sorting</td><td>2</td><td></td></tr></tbody></table>', 1, '2022-11-09 02:31:59', NULL),
(484, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-09 02:32:04', NULL),
(485, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/login', 'Ingreso de invitado@invitado.com desde la Dirección IP 192.168.0.22', '', 9, '2022-11-09 02:32:09', NULL),
(486, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/logout', 'invitado@invitado.com se desconectó', '', 9, '2022-11-09 02:32:31', NULL),
(487, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.22', '', 1, '2022-11-09 02:32:45', NULL),
(488, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-09 02:41:19', NULL),
(489, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.22', '', 1, '2022-11-09 02:41:43', NULL),
(490, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/users/edit-save/2', 'Actualizar información Vendedor en Users Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>password</td><td>$2y$10$XVfkKcJxpqQ96II540OCDuPjum7mpxrcTpW8KI5qlJxujeUlUU3sK</td><td>$2y$10$.6yMBgVwetZsC.kJlkOGXOLSQrTIvLyrJ8SBm3VxiHXscJhFYK3Di</td></tr><tr><td>remember_token</td><td></td><td></td></tr><tr><td>status</td><td>Activo</td><td></td></tr></tbody></table>', 1, '2022-11-09 02:42:24', NULL),
(491, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-09 02:42:30', NULL),
(492, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/login', 'Ingreso de vendedor@gmail.com desde la Dirección IP 192.168.0.22', '', 2, '2022-11-09 02:42:35', NULL),
(493, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/logout', 'vendedor@gmail.com se desconectó', '', 2, '2022-11-09 02:47:32', NULL),
(494, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.22', '', 1, '2022-11-09 02:47:39', NULL),
(495, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/users/edit-save/4', 'Actualizar información Supervisor en Users Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>password</td><td>$2y$10$/IgLpjXhuxmX2FQJWcyjkO4vaK1Kf0cNR3oS0PRQiDNLhq2JotaRS</td><td>$2y$10$N2U3bJGzwTXXgO.m1w7eZusZkkodR6O4WZc2hRCXBIs5CBT5WrSS2</td></tr><tr><td>remember_token</td><td></td><td></td></tr><tr><td>status</td><td>Activo</td><td></td></tr></tbody></table>', 1, '2022-11-09 02:48:28', NULL),
(496, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-09 02:48:38', NULL),
(497, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/login', 'Ingreso de supervisor@supervisor.com desde la Dirección IP 192.168.0.22', '', 4, '2022-11-09 02:48:44', NULL),
(498, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/ordenes/add-save', 'Añadir nueva información  en Ordenes de Trabajo', '', 4, '2022-11-09 02:54:35', NULL);
INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `details`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(499, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/logout', 'supervisor@supervisor.com se desconectó', '', 4, '2022-11-09 02:56:41', NULL),
(500, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.22', '', 1, '2022-11-09 02:56:45', NULL),
(501, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/users/edit-save/3', 'Actualizar información Tecnico en Users Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>password</td><td>$2y$10$YhBT27tiEbtoSdR/FMuCXOcwWtAjflNpDqr9OJryC5U/0J171vRE.</td><td>$2y$10$zLUvRsRd1Jop1MIc3ek9Qe6E2bJ/EGAtrtuLo2UPmE.ly1QpMn8.e</td></tr><tr><td>remember_token</td><td></td><td></td></tr><tr><td>status</td><td>Activo</td><td></td></tr></tbody></table>', 1, '2022-11-09 02:57:27', NULL),
(502, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-09 02:57:33', NULL),
(503, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/login', 'Ingreso de tecnico@tecnico.com desde la Dirección IP 192.168.0.22', '', 3, '2022-11-09 02:57:38', NULL),
(504, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/ordenes32/edit-save/4', 'Actualizar información  en Orden a Ejecutar', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>estado_id</td><td>7</td><td>8</td></tr><tr><td>comentario</td><td></td><td>Servicio instalado</td></tr><tr><td>evidencias</td><td></td><td>uploads/3/2022-11/evidencia.pdf</td></tr><tr><td>fecha_verificacion</td><td></td><td>2022-11-08</td></tr><tr><td>contrato_id</td><td>4</td><td></td></tr></tbody></table>', 3, '2022-11-09 02:59:00', NULL),
(505, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/logout', 'tecnico@tecnico.com se desconectó', '', 3, '2022-11-09 02:59:23', NULL),
(506, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.22', '', 1, '2022-11-09 02:59:28', NULL),
(507, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/users/edit-save/5', 'Actualizar información Recursos Humanos en Users Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>password</td><td>$2y$10$IH9ykgnYl6Glmxs7b9R8Hem1FeuFXWX/k3DmNJKXsVDLCwqHcEbd2</td><td>$2y$10$H5G91NuwbRe0f4R3j8vt4e/tXszYHWehvenfKuzBTWPZ33T3s2HNO</td></tr><tr><td>remember_token</td><td></td><td></td></tr><tr><td>status</td><td>Activo</td><td></td></tr></tbody></table>', 1, '2022-11-09 03:00:19', NULL),
(508, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/logout', 'hannselcordon@gmail.com se desconectó', '', 1, '2022-11-09 03:00:29', NULL),
(509, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/login', 'Ingreso de recursos@recursos.com desde la Dirección IP 192.168.0.22', '', 5, '2022-11-09 03:00:35', NULL),
(510, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/logout', 'recursos@recursos.com se desconectó', '', 5, '2022-11-09 03:02:35', NULL),
(511, '192.168.0.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36', 'http://firehome.local/admin/login', 'Ingreso de hannselcordon@gmail.com desde la Dirección IP 192.168.0.22', '', 1, '2022-11-09 03:02:40', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_menus`
--

CREATE TABLE `cms_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'url',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_dashboard` tinyint(1) NOT NULL DEFAULT '0',
  `id_cms_privileges` int(11) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_menus`
--

INSERT INTO `cms_menus` (`id`, `name`, `type`, `path`, `color`, `icon`, `parent_id`, `is_active`, `is_dashboard`, `id_cms_privileges`, `sorting`, `created_at`, `updated_at`) VALUES
(1, 'Planes y Servicios', 'Route', 'AdminPlanesControllerGetIndex', 'aqua', 'fa fa-file-movie-o', 0, 1, 0, 1, 13, '2022-10-19 00:40:50', '2022-11-03 22:11:39'),
(2, 'Departamentos de Guatemala', 'Route', 'AdminDepartamentos1ControllerGetIndex', NULL, 'fa fa-map-marker', 25, 1, 0, 1, 1, '2022-10-19 00:41:54', NULL),
(3, 'Municipios de Guatemala', 'Route', 'AdminMunicipios1ControllerGetIndex', NULL, 'fa fa-map-marker', 25, 1, 0, 1, 2, '2022-10-19 00:42:50', NULL),
(4, 'Estados', 'Route', 'AdminEstados1ControllerGetIndex', 'green', 'fa fa-files-o', 0, 1, 0, 1, 14, '2022-10-19 00:49:51', '2022-11-03 01:15:46'),
(5, 'Tipos de Ordenes de Servicio', 'Route', 'AdminTipoordenesControllerGetIndex', NULL, 'fa fa-file', 26, 1, 0, 1, 1, '2022-10-19 00:55:17', NULL),
(6, 'Tipo de Pagos', 'Route', 'AdminTipopagos1ControllerGetIndex', 'green', 'fa fa-credit-card', 28, 1, 0, 1, 1, '2022-10-19 00:56:54', '2022-11-03 01:22:38'),
(7, 'Cargos', 'Route', 'AdminCargosControllerGetIndex', 'normal', 'fa fa-file-text', 27, 1, 0, 1, 2, '2022-10-19 01:38:42', '2022-11-09 02:31:59'),
(8, 'Tipos de Orden de Trabajo', 'Route', 'AdminTipotrabajosControllerGetIndex', NULL, 'fa fa-file', 26, 1, 0, 1, 2, '2022-10-19 02:11:51', NULL),
(9, 'Empleados', 'Route', 'AdminEmpleadosControllerGetIndex', 'normal', 'fa fa-user', 27, 1, 0, 1, 1, '2022-10-19 02:50:35', '2022-11-03 21:20:25'),
(13, 'prueba2', 'Statistic', 'statistic_builder/show/prueba', 'red', 'fa fa-search', 0, 1, 1, 1, 12, '2022-10-19 07:29:25', '2022-10-19 07:31:45'),
(15, 'Clientes', 'Route', 'AdminClientesControllerGetIndex', 'yellow', 'fa fa-user-plus', 34, 1, 0, 1, 1, '2022-10-19 12:32:58', '2022-11-03 01:30:11'),
(16, 'Productos', 'Route', 'AdminProductosControllerGetIndex', 'aqua', 'fa fa-product-hunt', 0, 1, 0, 1, 15, '2022-10-19 12:49:18', '2022-11-03 22:12:34'),
(18, 'Pagos', 'Route', 'AdminPagosControllerGetIndex', 'green', 'fa fa-money', 28, 1, 0, 1, 2, '2022-10-20 07:38:21', '2022-11-03 22:13:05'),
(19, 'users', 'Route', 'AdminCmsUsers1ControllerGetIndex', 'yellow', 'fa fa-user', 0, 1, 0, 1, 16, '2022-10-20 08:41:38', '2022-11-03 01:24:26'),
(21, 'Empleados', 'Module', 'empleados', 'normal', 'fa fa-users', 0, 1, 0, 1, 9, '2022-10-22 04:22:23', NULL),
(22, 'Gogogle', 'URL', 'https://www.google.com/', 'normal', 'fa fa-pagelines', 0, 1, 0, 1, 10, '2022-10-22 04:23:08', NULL),
(23, 'Contratos', 'Route', 'AdminContratos30ControllerGetIndex', 'green', 'fa fa-legal', 0, 1, 0, 1, 17, '2022-11-02 20:13:40', '2022-11-03 20:44:29'),
(24, 'Ordenes', 'Route', 'AdminOrdenesControllerGetIndex', 'normal', 'fa fa-file-text', 26, 1, 0, 1, 3, '2022-11-02 23:27:55', '2022-11-03 20:44:06'),
(25, 'Division Politica', 'URL', '#', 'aqua', 'fa fa-map-marker', 0, 1, 0, 1, 11, '2022-11-03 01:00:46', NULL),
(26, 'Ordenes', 'URL', '#', 'green', 'fa fa-file', 0, 1, 0, 1, 8, '2022-11-03 01:03:52', '2022-11-03 20:43:37'),
(27, 'Empleados', 'URL', '#', 'yellow', 'fa fa-users', 0, 1, 0, 1, 7, '2022-11-03 01:11:45', '2022-11-09 02:31:36'),
(28, 'Pagos', 'URL', '#', 'green', 'fa fa-money', 0, 1, 0, 1, 6, '2022-11-03 01:25:03', '2022-11-03 22:20:43'),
(29, 'Vendedor', 'Statistic', 'statistic_builder/show/vendedor', 'aqua', 'fa fa-dashboard', 0, 1, 1, 1, 5, '2022-11-03 01:37:16', NULL),
(30, 'Orden a Ejecutar', 'Route', 'AdminOrdenes32ControllerGetIndex', 'normal', 'fa fa-files-o', 26, 1, 0, 1, 4, '2022-11-03 15:22:32', '2022-11-03 20:43:56'),
(31, 'Tecnico', 'Statistic', 'statistic_builder/show/tecnico', 'green', 'fa fa-dashboard', 0, 1, 1, 1, 1, '2022-11-03 17:00:13', '2022-11-03 17:05:42'),
(32, 'Supervisor', 'Statistic', 'statistic_builder/show/supervisor', 'green', 'fa fa-dashboard', 0, 1, 1, 1, 2, '2022-11-03 21:04:21', NULL),
(33, 'Recursos Humanos', 'Statistic', 'statistic_builder/show/recursos-humanos', 'green', 'fa fa-dashboard', 0, 1, 1, 1, 3, '2022-11-03 21:48:19', NULL),
(34, 'Cartera', 'URL', '#', 'green', 'fa fa-users', 0, 1, 0, 1, 4, '2022-11-03 21:55:02', NULL),
(35, 'Servicios_Online', 'Route', 'AdminContratos33ControllerGetIndex', 'normal', 'fa fa-star', 0, 1, 0, 1, 18, '2022-11-03 21:58:51', '2022-11-03 22:18:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_menus_privileges`
--

CREATE TABLE `cms_menus_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_menus` int(11) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_menus_privileges`
--

INSERT INTO `cms_menus_privileges` (`id`, `id_cms_menus`, `id_cms_privileges`) VALUES
(2, 2, 1),
(3, 3, 1),
(6, 5, 1),
(9, 8, 1),
(11, 10, 1),
(12, 11, 1),
(13, 12, 1),
(16, 13, 1),
(17, 14, 1),
(20, 17, 1),
(23, 20, 1),
(24, 21, 2),
(25, 22, 2),
(28, 25, 1),
(37, 4, 1),
(42, 6, 1),
(46, 19, 1),
(48, 15, 1),
(49, 15, 3),
(52, 29, 3),
(64, 31, 4),
(65, 26, 1),
(66, 26, 5),
(67, 26, 4),
(68, 30, 1),
(69, 30, 5),
(70, 30, 4),
(71, 24, 1),
(72, 24, 5),
(73, 23, 1),
(74, 23, 5),
(75, 23, 3),
(76, 32, 5),
(81, 9, 6),
(82, 9, 1),
(83, 33, 6),
(84, 34, 1),
(85, 34, 3),
(87, 1, 7),
(88, 1, 1),
(89, 16, 7),
(90, 16, 1),
(91, 18, 7),
(92, 18, 1),
(93, 35, 7),
(94, 35, 1),
(95, 28, 7),
(96, 28, 1),
(97, 27, 8),
(98, 27, 6),
(99, 27, 1),
(100, 7, 8),
(101, 7, 6),
(102, 7, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_moduls`
--

CREATE TABLE `cms_moduls` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_moduls`
--

INSERT INTO `cms_moduls` (`id`, `name`, `icon`, `path`, `table_name`, `controller`, `is_protected`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Notifications', 'fa fa-cog', 'notifications', 'cms_notifications', 'NotificationsController', 1, 1, '2022-11-08 04:12:45', NULL, NULL),
(2, 'Privileges', 'fa fa-cog', 'privileges', 'cms_privileges', 'PrivilegesController', 1, 1, '2022-11-08 04:12:45', NULL, NULL),
(3, 'Privileges Roles', 'fa fa-cog', 'privileges_roles', 'cms_privileges_roles', 'PrivilegesRolesController', 1, 1, '2022-11-08 04:12:45', NULL, NULL),
(4, 'Users Management', 'fa fa-users', 'users', 'users', 'AdminCmsUsersController', 0, 1, '2022-11-08 04:12:45', NULL, NULL),
(5, 'Settings', 'fa fa-cog', 'settings', 'cms_settings', 'SettingsController', 1, 1, '2022-11-08 04:12:45', NULL, NULL),
(6, 'Module Generator', 'fa fa-database', 'module_generator', 'cms_moduls', 'ModulsController', 1, 1, '2022-11-08 04:12:45', NULL, NULL),
(7, 'Menu Management', 'fa fa-bars', 'menu_management', 'cms_menus', 'MenusController', 1, 1, '2022-11-08 04:12:45', NULL, NULL),
(8, 'Email Templates', 'fa fa-envelope-o', 'email_templates', 'cms_email_templates', 'EmailTemplatesController', 1, 1, '2022-11-08 04:12:45', NULL, NULL),
(9, 'Statistic Builder', 'fa fa-dashboard', 'statistic_builder', 'cms_statistics', 'StatisticBuilderController', 1, 1, '2022-11-08 04:12:45', NULL, NULL),
(10, 'API Generator', 'fa fa-cloud-download', 'api_generator', '', 'ApiCustomController', 1, 1, '2022-11-08 04:12:45', NULL, NULL),
(11, 'Log User Access', 'fa fa-flag-o', 'logs', 'cms_logs', 'LogsController', 1, 1, '2022-11-08 04:12:45', NULL, NULL),
(12, 'Planes y Servicios', 'fa fa-mobile-phone', 'planes', 'planes', 'AdminPlanesController', 0, 0, '2022-10-19 00:40:49', NULL, NULL),
(13, 'Departamentos de Guatemala', 'fa fa-map-marker', 'departamentos', 'departamentos', 'AdminDepartamentos1Controller', 0, 0, '2022-10-19 00:41:54', NULL, NULL),
(14, 'Municipios de Guatemala', 'fa fa-map-marker', 'municipios', 'municipios', 'AdminMunicipios1Controller', 0, 0, '2022-10-19 00:42:50', NULL, NULL),
(15, 'Estados', 'fa fa-file', 'estados', 'estados', 'AdminEstados1Controller', 0, 0, '2022-10-19 00:49:51', NULL, NULL),
(16, 'Tipos de Ordenes de Servicio', 'fa fa-file', 'tipoordenes', 'tipoordenes', 'AdminTipoordenesController', 0, 0, '2022-10-19 00:55:17', NULL, NULL),
(17, 'Tipo de Pagos', 'fa fa-money', 'tipopagos', 'tipopagos', 'AdminTipopagos1Controller', 0, 0, '2022-10-19 00:56:54', NULL, NULL),
(18, 'Cargos', 'fa fa-file-text', 'cargos', 'cargos', 'AdminCargosController', 0, 0, '2022-10-19 01:38:42', NULL, NULL),
(19, 'Tipos de Orden de Trabajo', 'fa fa-file', 'tipotrabajos', 'tipotrabajos', 'AdminTipotrabajosController', 0, 0, '2022-10-19 02:11:51', NULL, NULL),
(20, 'Empleados', 'fa fa-user', 'empleados', 'empleados', 'AdminEmpleadosController', 0, 0, '2022-10-19 02:50:35', NULL, NULL),
(21, 'Jerarquia Administrativa', 'fa fa-empire', 'jerarquias', 'jerarquias', 'AdminJerarquiasController', 0, 0, '2022-10-19 03:18:02', NULL, '2022-10-20 11:56:36'),
(22, 'Clientes', 'fa fa-user-plus', 'clientes', 'clientes', 'AdminClientesController', 0, 0, '2022-10-19 04:46:05', NULL, '2022-10-19 10:01:15'),
(23, 'Clientes', 'fa fa-user-plus', 'clientes23', 'clientes', 'AdminClientes23Controller', 0, 0, '2022-10-19 10:01:38', NULL, '2022-10-19 12:31:29'),
(24, 'Clientes', 'fa fa-user-plus', 'clientes', 'clientes', 'AdminClientesController', 0, 0, '2022-10-19 12:32:58', NULL, NULL),
(25, 'Productos', 'fa fa-product-hunt', 'productos', 'productos', 'AdminProductosController', 0, 0, '2022-10-19 12:49:18', NULL, NULL),
(26, 'Contratos de Servicios', 'fa fa-cog', 'contratos', 'contratos', 'AdminContratosController', 0, 0, '2022-10-19 13:51:38', NULL, '2022-10-20 10:17:56'),
(27, 'Pagos', 'fa fa-glass', 'pagos', 'pagos', 'AdminPagosController', 0, 0, '2022-10-20 07:38:21', NULL, NULL),
(28, 'users', 'fa fa-glass', 'cms_users', 'cms_users', 'AdminCmsUsers1Controller', 0, 0, '2022-10-20 08:41:38', NULL, NULL),
(29, 'Contratos', 'fa fa-file', 'contratos29', 'contratos', 'AdminContratos29Controller', 0, 0, '2022-10-20 10:19:16', NULL, '2022-11-02 20:13:21'),
(30, 'Contratos', 'fa fa-legal', 'contratos30', 'contratos', 'AdminContratos30Controller', 0, 0, '2022-11-02 20:13:40', NULL, NULL),
(31, 'Ordenes de Trabajo', 'fa fa-file-word-o', 'ordenes', 'ordenes', 'AdminOrdenesController', 0, 0, '2022-11-02 23:27:55', NULL, NULL),
(32, 'Orden a Ejecutar', 'fa fa-glass', 'ordenes32', 'ordenes', 'AdminOrdenes32Controller', 0, 0, '2022-11-03 15:22:31', NULL, NULL),
(33, 'Servicios_Online', 'fa fa-star', 'contratos33', 'contratos', 'AdminContratos33Controller', 0, 0, '2022-11-03 21:58:51', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_notifications`
--

CREATE TABLE `cms_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_users` int(11) DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_privileges`
--

CREATE TABLE `cms_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_superadmin` tinyint(1) DEFAULT NULL,
  `theme_color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_privileges`
--

INSERT INTO `cms_privileges` (`id`, `name`, `is_superadmin`, `theme_color`, `created_at`, `updated_at`) VALUES
(1, 'Super Administrator', 1, 'skin-red', '2022-11-08 04:12:45', NULL),
(2, 'Secretaria', 0, 'skin-purple', NULL, NULL),
(3, 'Vendedor', 0, 'skin-yellow', NULL, NULL),
(4, 'Tecnico', 0, 'skin-red', NULL, NULL),
(5, 'Supervisor', 0, 'skin-yellow', NULL, NULL),
(6, 'RH', 0, 'skin-blue', NULL, NULL),
(7, 'Clientes', 0, 'skin-green', NULL, NULL),
(8, 'Prueba', 0, 'skin-blue', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_privileges_roles`
--

CREATE TABLE `cms_privileges_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_visible` tinyint(1) DEFAULT NULL,
  `is_create` tinyint(1) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `is_edit` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `id_cms_moduls` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_privileges_roles`
--

INSERT INTO `cms_privileges_roles` (`id`, `is_visible`, `is_create`, `is_read`, `is_edit`, `is_delete`, `id_cms_privileges`, `id_cms_moduls`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 0, 0, 0, 1, 1, '2022-11-08 04:12:45', NULL),
(2, 1, 1, 1, 1, 1, 1, 2, '2022-11-08 04:12:45', NULL),
(3, 0, 1, 1, 1, 1, 1, 3, '2022-11-08 04:12:45', NULL),
(4, 1, 1, 1, 1, 1, 1, 4, '2022-11-08 04:12:45', NULL),
(5, 1, 1, 1, 1, 1, 1, 5, '2022-11-08 04:12:45', NULL),
(6, 1, 1, 1, 1, 1, 1, 6, '2022-11-08 04:12:45', NULL),
(7, 1, 1, 1, 1, 1, 1, 7, '2022-11-08 04:12:45', NULL),
(8, 1, 1, 1, 1, 1, 1, 8, '2022-11-08 04:12:45', NULL),
(9, 1, 1, 1, 1, 1, 1, 9, '2022-11-08 04:12:45', NULL),
(10, 1, 1, 1, 1, 1, 1, 10, '2022-11-08 04:12:45', NULL),
(11, 1, 0, 1, 0, 1, 1, 11, '2022-11-08 04:12:45', NULL),
(52, 1, 1, 1, 0, 0, 2, 20, NULL, NULL),
(69, 1, 1, 1, 1, 1, 1, 18, NULL, NULL),
(70, 1, 1, 1, 1, 1, 1, 24, NULL, NULL),
(71, 1, 1, 1, 1, 1, 1, 30, NULL, NULL),
(72, 1, 1, 1, 1, 1, 1, 13, NULL, NULL),
(73, 1, 1, 1, 1, 1, 1, 20, NULL, NULL),
(74, 1, 1, 1, 1, 1, 1, 15, NULL, NULL),
(75, 1, 1, 1, 1, 1, 1, 14, NULL, NULL),
(76, 1, 1, 1, 1, 1, 1, 27, NULL, NULL),
(77, 1, 1, 1, 1, 1, 1, 12, NULL, NULL),
(78, 1, 1, 1, 1, 1, 1, 25, NULL, NULL),
(79, 1, 1, 1, 1, 1, 1, 17, NULL, NULL),
(80, 1, 1, 1, 1, 1, 1, 19, NULL, NULL),
(81, 1, 1, 1, 1, 1, 1, 16, NULL, NULL),
(82, 1, 1, 1, 1, 1, 1, 28, NULL, NULL),
(83, 1, 1, 1, 1, 1, 1, 4, NULL, NULL),
(84, 1, 1, 1, 1, 1, 1, 31, NULL, NULL),
(85, 1, 1, 1, 1, 1, 3, 24, NULL, NULL),
(86, 1, 1, 1, 1, 1, 3, 30, NULL, NULL),
(87, 1, 1, 1, 1, 1, 1, 32, NULL, NULL),
(88, 1, 0, 1, 1, 0, 4, 32, NULL, NULL),
(89, 1, 1, 1, 1, 1, 5, 24, NULL, NULL),
(90, 1, 1, 1, 1, 1, 5, 30, NULL, NULL),
(91, 1, 1, 1, 1, 1, 5, 32, NULL, NULL),
(92, 1, 1, 1, 1, 1, 5, 31, NULL, NULL),
(93, 1, 1, 1, 1, 1, 6, 18, NULL, NULL),
(94, 1, 1, 1, 1, 1, 6, 20, NULL, NULL),
(95, 1, 1, 1, 1, 1, 1, 33, NULL, NULL),
(103, 0, 1, 0, 0, 0, 7, 24, NULL, NULL),
(104, 1, 1, 1, 0, 0, 7, 27, NULL, NULL),
(105, 1, 0, 0, 0, 0, 7, 12, NULL, NULL),
(106, 1, 0, 0, 0, 0, 7, 25, NULL, NULL),
(107, 1, 1, 0, 0, 0, 7, 33, NULL, NULL),
(108, 1, 1, 1, 1, 1, 8, 18, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_settings`
--

CREATE TABLE `cms_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `content_input_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dataenum` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `helper` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group_setting` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_settings`
--

INSERT INTO `cms_settings` (`id`, `name`, `content`, `content_input_type`, `dataenum`, `helper`, `created_at`, `updated_at`, `group_setting`, `label`) VALUES
(1, 'login_background_color', NULL, 'text', NULL, 'Input hexacode', '2022-11-08 04:12:45', NULL, 'Login Register Style', 'Login Background Color'),
(2, 'login_font_color', NULL, 'text', NULL, 'Input hexacode', '2022-11-08 04:12:45', NULL, 'Login Register Style', 'Login Font Color'),
(3, 'login_background_image', 'uploads/2022-11/72bc9db62e3b453f2ebb2b3beb9ee2bc.webp', 'upload_image', NULL, NULL, '2022-11-08 04:12:45', NULL, 'Login Register Style', 'Login Background Image'),
(4, 'email_sender', 'support@crudbooster.com', 'text', NULL, NULL, '2022-11-08 04:12:45', NULL, 'Email Setting', 'Email Sender'),
(5, 'smtp_driver', 'mail', 'select', 'smtp,mail,sendmail', NULL, '2022-11-08 04:12:45', NULL, 'Email Setting', 'Mail Driver'),
(6, 'smtp_host', '', 'text', NULL, NULL, '2022-11-08 04:12:45', NULL, 'Email Setting', 'SMTP Host'),
(7, 'smtp_port', '25', 'text', NULL, 'default 25', '2022-11-08 04:12:45', NULL, 'Email Setting', 'SMTP Port'),
(8, 'smtp_username', '', 'text', NULL, NULL, '2022-11-08 04:12:45', NULL, 'Email Setting', 'SMTP Username'),
(9, 'smtp_password', '', 'text', NULL, NULL, '2022-11-08 04:12:45', NULL, 'Email Setting', 'SMTP Password'),
(10, 'appname', 'Fiberhome S.A.', 'text', NULL, NULL, '2022-11-08 04:12:45', NULL, 'Application Setting', 'Application Name'),
(11, 'default_paper_size', 'Legal', 'text', NULL, 'Paper size, ex : A4, Legal, etc', '2022-11-08 04:12:45', NULL, 'Application Setting', 'Default Paper Print Size'),
(12, 'logo', 'uploads/2022-11/5263eb2d03cd65d5fb687326bbd1de77.png', 'upload_image', NULL, NULL, '2022-11-08 04:12:45', NULL, 'Application Setting', 'Logo'),
(13, 'favicon', 'uploads/2022-11/c1c9fc9ffa96b7246f3b1fbb962e9488.png', 'upload_image', NULL, NULL, '2022-11-08 04:12:45', NULL, 'Application Setting', 'Favicon'),
(14, 'api_debug_mode', 'true', 'select', 'true,false', NULL, '2022-11-08 04:12:45', NULL, 'Application Setting', 'API Debug Mode'),
(15, 'google_api_key', NULL, 'text', NULL, NULL, '2022-11-08 04:12:45', NULL, 'Application Setting', 'Google API Key'),
(16, 'google_fcm_key', NULL, 'text', NULL, NULL, '2022-11-08 04:12:45', NULL, 'Application Setting', 'Google FCM Key'),
(17, 'image', 'uploads/2022-11/7f4536f1033c5136ad9b92205a205253.jpg', 'upload_image', 'abc', NULL, '2022-10-20 08:30:19', NULL, 'General Setting', 'Image'),
(18, 'image', 'uploads/2022-11/7f4536f1033c5136ad9b92205a205253.jpg', 'upload_image', 'abc', 'abc', '2022-10-20 08:40:00', NULL, 'Login Register Style', 'Image');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_statistics`
--

CREATE TABLE `cms_statistics` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_statistics`
--

INSERT INTO `cms_statistics` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Principal', 'prueba', '2022-10-19 01:13:10', '2022-11-03 18:55:01'),
(2, 'Vendedor', 'vendedor', '2022-11-03 01:33:23', NULL),
(3, 'Tecnico', 'tecnico', '2022-11-03 16:58:22', NULL),
(4, 'Supervisor', 'supervisor', '2022-11-03 20:54:35', NULL),
(5, 'Recursos Humanos', 'recursos-humanos', '2022-11-03 21:24:33', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_statistic_components`
--

CREATE TABLE `cms_statistic_components` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_statistics` int(11) DEFAULT NULL,
  `componentID` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `component_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_name` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `config` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cms_statistic_components`
--

INSERT INTO `cms_statistic_components` (`id`, `id_cms_statistics`, `componentID`, `component_name`, `area_name`, `sorting`, `name`, `config`, `created_at`, `updated_at`) VALUES
(5, 1, 'e814afb3f79478c0cfb6da119aa45c15', 'smallbox', 'area1', 0, NULL, '{\"name\":\"Usuarios\",\"icon\":\"ion-bag\",\"color\":\"bg-green\",\"link\":\"http:\\/\\/127.0.0.1:8001\\/admin\\/cms_users\",\"sql\":\"select COUNT(*) from users\"}', '2022-10-19 07:36:34', NULL),
(6, 1, '301cceb8217bfe6317f43e0656ad272e', 'smallbox', 'area2', 0, NULL, '{\"name\":\"Clientes\",\"icon\":\"ion-bag\",\"color\":\"bg-red\",\"link\":\"http:\\/\\/127.0.0.1:8001\\/admin\\/clientes\",\"sql\":\"select COUNT(*)  from clientes\"}', '2022-10-19 07:37:45', NULL),
(7, 1, '36d07d9463740398e882cbb5708f29aa', 'smallbox', 'area3', 0, NULL, '{\"name\":\"Empleados\",\"icon\":\"ion-bag\",\"color\":\"bg-aqua\",\"link\":\"http:\\/\\/127.0.0.1:8001\\/admin\\/empleados\",\"sql\":\"select COUNT(*)  from empleados\"}', '2022-10-19 07:41:01', NULL),
(8, 1, '13c06ddf82b2bd57867d0f7e64031e9a', 'smallbox', 'area4', 0, NULL, '{\"name\":\"Tipos de Ordenes\",\"icon\":\"ion-bag\",\"color\":\"bg-yellow\",\"link\":\"http:\\/\\/127.0.0.1:8001\\/admin\\/tipoordenes\",\"sql\":\"select COUNT(*)  from tipoordenes\"}', '2022-10-19 07:44:05', NULL),
(9, 1, 'cc3d46fca2b39d2d50a53db856601d4f', 'chartarea', NULL, 0, 'Untitled', NULL, '2022-10-20 07:05:55', NULL),
(21, 1, 'ed6f1c23eb3d2c058c5bdad966c309b7', 'table', 'area5', 0, NULL, '{\"name\":\"Ventas\",\"sql\":\"Select CONCAT(empleados.nombres,\\\" \\\", empleados.apellidos) as Nombre,COUNT(contratos.id)as Cantidad_de_Servicios,sum(productos.costo)as Facturado,(sum(productos.costo) *0.30)as Comision ,MONTH(contratos.created_at) as Mes, YEAR(contratos.created_at) as a\\u00f1o FROM productos, contratos, empleados WHERE contratos.vendedor_id = empleados.id GROUP BY YEAR(contratos.created_at), MONTH(contratos.created_at), Nombre,productos.costo\"}', '2022-11-02 21:02:00', NULL),
(22, 2, '991f71452007719e96a0376f9697979f', 'table', 'area5', 0, NULL, '{\"name\":\"Ventas\",\"sql\":\"Select CONCAT(empleados.nombres,\\\" \\\", empleados.apellidos) as Nombre,COUNT(contratos.id)as Cantidad_de_Servicios,sum(productos.costo)as Facturado ,MONTH(contratos.created_at) as mes, YEAR(contratos.created_at) as a\\u00f1o FROM productos, contratos, empleados WHERE contratos.vendedor_id = empleados.id GROUP BY YEAR(contratos.created_at), MONTH(contratos.created_at), Nombre,productos.costo\"}', '2022-11-03 01:34:16', NULL),
(24, 2, '9b3a95dd9857a8813dfafd6e53be1211', 'smallbox', 'area1', 1, NULL, '{\"name\":\"Clientes\",\"icon\":\"ion-bag\",\"color\":\"bg-green\",\"link\":\"http:\\/\\/127.0.0.1:8001\\/admin\\/clientes\",\"sql\":\"select COUNT(*)  from clientes\"}', '2022-11-03 01:35:31', NULL),
(25, 2, 'a286d779056330ea64c440b5e6032374', 'chartbar', 'area5', 0, NULL, '{\"name\":\"PLanes mas contratados\",\"sql\":\"SELECT COUNT(contratos.id) AS \'value\', planes.planes_servicios AS \'label\'\\r\\nFROM contratos,planes\\r\\nWHERE\\r\\nplanes.id=contratos.plan_id AND MONTH(contratos.created_at)= MONTH(NOW())\\r\\nGROUP BY\\r\\nlabel\",\"area_name\":\"label\",\"goals\":null}', '2022-11-03 02:36:53', NULL),
(26, 2, '16d703187fd8a3c33d0a00bc8c521d37', 'chartarea', 'area3', 0, NULL, '{\"name\":\"Vendedores Rancking\",\"sql\":\"SELECT COUNT(contratos.id) AS \'value\', empleados.nombres AS \'label\'\\r\\nFROM contratos,planes,empleados\\r\\nWHERE\\r\\nempleados.id=contratos.vendedor_id AND MONTH(contratos.created_at)= MONTH(NOW())\\r\\nGROUP BY\\r\\nlabel\",\"area_name\":\"label\",\"goals\":\"2\"}', '2022-11-03 02:46:12', NULL),
(27, 2, 'a51627a3554ab8855fa55f42bd19f88f', 'chartarea', 'area4', 0, NULL, '{\"name\":\"Venta Anual\",\"sql\":\"SELECT COUNT(contratos.id) AS \'value\', empleados.nombres AS \'label\'\\r\\nFROM contratos,planes,empleados\\r\\nWHERE\\r\\nempleados.id=contratos.vendedor_id AND YEAR(contratos.created_at)= YEAR(NOW())\\r\\nGROUP BY\\r\\nlabel\",\"area_name\":\"label\",\"goals\":\"2\"}', '2022-11-03 02:49:55', NULL),
(29, 2, '1f61c3dceeeb8c191157f01cfa074655', 'smallbox', 'area2', 1, NULL, '{\"name\":\"Contratos\",\"icon\":\"ion-bag\",\"color\":\"bg-aqua\",\"link\":\"http:\\/\\/127.0.0.1:8001\\/admin\\/contratos30\",\"sql\":\"select COUNT(*)  FROM contratos\"}', '2022-11-03 02:54:48', NULL),
(32, 3, '7f58cbbfe33c93963bfce668c936521f', 'table', 'area5', 0, NULL, '{\"name\":\"Ordenes Ejecutadas\",\"sql\":\"SELECT  CONCAT(empleados.nombres,\\\" \\\", empleados.apellidos) as Tecnico,COUNT(ordenes.id) as Ordejes_Ejecutadas,SUM(tipotrabajos.costo)as Costo_Operativo, (Costo *0.20) as Comision, MONTH(ordenes.fecha_verificacion) Mes, YEAR(ordenes.fecha_verificacion) A\\u00f1o\\r\\nFROM ordenes, empleados, estados, tipotrabajos\\r\\nWHERE ordenes.estado_id = estados.id and ordenes.tecnico_id = empleados.id and ordenes.trabajo_id = tipotrabajos.id and ordenes.estado_id =\\\"8\\\" \\r\\nGROUP BY Tecnico, tipotrabajos.costo,Mes, A\\u00f1o\"}', '2022-11-03 16:58:30', NULL),
(34, 3, 'b9c070e8b02e06a9f63af22268db8fcd', 'smallbox', 'area1', 0, NULL, '{\"name\":\"Ordenes Pendientes\",\"icon\":\"ion-bag\",\"color\":\"bg-green\",\"link\":\"http:\\/\\/127.0.0.1:8001\\/admin\\/ordenes32\",\"sql\":\"SELECT COUNT(ordenes.id) FROM ordenes WHERE ordenes.estado_id =\\\"7\\\"\"}', '2022-11-03 18:48:24', NULL),
(39, 1, '44426516cee6eb892f7883745431f5e6', 'table', 'area5', 2, NULL, '{\"name\":\"Ordenes Realizadas\",\"sql\":\"SELECT  CONCAT(empleados.nombres,\\\" \\\", empleados.apellidos) as Tecnico,COUNT(ordenes.id) as Ordejes_Ejecutadas,SUM(tipotrabajos.costo)as Costo_Operativo, (Costo *0.20) as Comision, MONTH(ordenes.fecha_verificacion) Mes, YEAR(ordenes.fecha_verificacion) A\\u00f1o\\r\\nFROM ordenes, empleados, estados, tipotrabajos\\r\\nWHERE ordenes.estado_id = estados.id and ordenes.tecnico_id = empleados.id and ordenes.trabajo_id = tipotrabajos.id and ordenes.estado_id =\\\"8\\\" \\r\\nGROUP BY Tecnico, tipotrabajos.costo,Mes, A\\u00f1o\"}', '2022-11-03 18:57:17', NULL),
(40, 4, 'f406804c8ead7f98e80027cb2366298b', 'smallbox', 'area1', 0, NULL, '{\"name\":\"Servicios Pendientes de Instalaci\\u00f3n\",\"icon\":\"ion-bag\",\"color\":\"bg-green\",\"link\":\"http:\\/\\/127.0.0.1:8001\\/admin\\/contratos30\",\"sql\":\"SELECT COUNT(*) FROM contratos, estados \\r\\nWHERE contratos.estado_id = estados.id and estados.estado = \\\"Pendiente de Instalaci\\u00f3n\\\"\"}', '2022-11-03 20:54:43', NULL),
(41, 4, 'ff3b547425ab13a7c3382c9a7e99411b', 'smallbox', NULL, 0, 'Untitled', NULL, '2022-11-03 20:57:18', NULL),
(42, 4, '52413bf4d54d60e6ed29117238a81139', 'smallbox', 'area2', 0, NULL, '{\"name\":\"T\\u00e9cnicos de Campo\",\"icon\":\"ion-bag\",\"color\":\"bg-red\",\"link\":\"#\",\"sql\":\"SELECT COUNT(*) FROM empleados,cargos\\r\\nWHERE empleados.cargo_id = cargos.id AND cargos.cargos =\\\"T\\u00e9cnico de Campo\\\"\"}', '2022-11-03 20:57:20', NULL),
(43, 4, '7254b34a9ef3b1642ea71d27b3cd8858', 'smallbox', 'area3', 0, NULL, '{\"name\":\"Ordenes Ejecutadas\",\"icon\":\"ion-bag\",\"color\":\"bg-aqua\",\"link\":\"http:\\/\\/127.0.0.1:8001\\/admin\\/ordenes32\",\"sql\":\"SELECT COUNT(*) FROM ordenes, estados\\r\\nWHERE ordenes.estado_id = estados.id and estados.estado = \\\"Ejecutada\\\"\"}', '2022-11-03 21:00:12', NULL),
(44, 4, '91feb4649360c19efbed7b581709b93f', 'smallbox', 'area4', 0, NULL, '{\"name\":\"Ordenes Pendientes de Ejecutar\",\"icon\":\"ion-bag\",\"color\":\"bg-yellow\",\"link\":\"http:\\/\\/127.0.0.1:8001\\/admin\\/ordenes32\",\"sql\":\"SELECT COUNT(*) FROM ordenes, estados\\r\\nWHERE ordenes.estado_id = estados.id and estados.estado = \\\"Pendiente de Ejecutar\\\"\"}', '2022-11-03 21:01:52', NULL),
(45, 1, '111b111b9214f47feb7bfcc7b11d3773', 'chartarea', 'area5', 2, NULL, '{\"name\":\"Ventas del Mes\",\"sql\":\"SELECT COUNT(contratos.id) AS \'value\', empleados.nombres AS \'label\'\\r\\nFROM contratos,planes,empleados\\r\\nWHERE\\r\\nempleados.id=contratos.vendedor_id AND MONTH(contratos.created_at)= MONTH(NOW())\\r\\nGROUP BY\\r\\nlabel\",\"area_name\":\"label\",\"goals\":\"2\"}', '2022-11-03 21:06:14', NULL),
(46, 1, '9534d775e5b5a03ff1075df26397f085', 'chartline', 'area5', 3, NULL, '{\"name\":\"Ventas del A\\u00f1o\",\"sql\":\"SELECT COUNT(contratos.id) AS \'value\', empleados.nombres AS \'label\'\\r\\nFROM contratos,planes,empleados\\r\\nWHERE\\r\\nempleados.id=contratos.vendedor_id AND YEAR(contratos.created_at)= YEAR(NOW())\\r\\nGROUP BY\\r\\nlabel\",\"area_name\":\"label\",\"goals\":null}', '2022-11-03 21:08:47', NULL),
(47, 5, '7045aadd1d67d5424bd2864d4f874168', 'smallbox', 'area1', 0, NULL, '{\"name\":\"Total de Empleados\",\"icon\":\"ion-bag\",\"color\":\"bg-green\",\"link\":\"http:\\/\\/127.0.0.1:8001\\/admin\\/empleados\",\"sql\":\"select COUNT(*) FROM empleados\"}', '2022-11-03 21:24:40', NULL),
(48, 5, '8fcb80986fdf7a11b967ba40732754e4', 'chartbar', 'area5', 0, NULL, '{\"name\":\"Cargos\",\"sql\":\"SELECT COUNT(empleados.id) AS \'value\', cargos.cargos AS \'label\'\\r\\nFROM empleados,cargos\\r\\nWHERE\\r\\nempleados.cargo_id=cargos.id \\r\\nGROUP BY\\r\\nlabel\",\"area_name\":\"label\",\"goals\":null}', '2022-11-03 21:31:35', NULL),
(49, 5, '6d85999460c5c2e67aa674e349f01e0c', 'table', 'area5', 0, NULL, '{\"name\":\"Vendedor del Mes\",\"sql\":\"select CONCAT(empleados.nombres,\\\" \\\", empleados.apellidos) as Nombre, SUM(productos.costo) As Facturado\\r\\nfrom empleados, productos, contratos\\r\\nWHERE contratos.vendedor_id = empleados.id and MONTH(contratos.created_at)=MONTH(NOW()) AND YEAR(contratos.created_at) = YEAR(NOW())\\r\\nGROUP BY Nombre DESC LIMIT 1;\"}', '2022-11-03 21:32:24', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_users`
--

CREATE TABLE `cms_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contratos`
--

CREATE TABLE `contratos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente_id` bigint(20) UNSIGNED NOT NULL,
  `producto_id` bigint(20) UNSIGNED NOT NULL,
  `plan_id` bigint(20) UNSIGNED NOT NULL,
  `vendedor_id` bigint(20) UNSIGNED NOT NULL,
  `estado_id` bigint(20) UNSIGNED NOT NULL,
  `contrato_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `contratos`
--

INSERT INTO `contratos` (`id`, `cliente_id`, `producto_id`, `plan_id`, `vendedor_id`, `estado_id`, `contrato_file`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 2, 1, 'uploads/1/2022-11/modelo_de_contrato.doc', '2022-08-02 20:36:29', NULL),
(2, 1, 1, 1, 2, 1, 'uploads/1/2022-11/modelo_de_contrato.doc', '2022-08-02 20:48:09', NULL),
(3, 1, 1, 1, 3, 5, 'uploads/1/2022-11/modelo_de_contrato.doc', '2022-10-02 20:49:24', NULL),
(4, 1, 1, 1, 2, 1, 'uploads/1/2022-11/modelo_de_contrato.doc', '2022-10-03 20:39:14', NULL),
(5, 1, 1, 1, 2, 5, 'uploads/1/2022-11/modelo_de_contrato.doc', '2022-11-03 21:10:52', NULL),
(6, 1, 2, 1, 4, 1, 'uploads/1/2022-11/modelo_de_contrato.doc', '2022-11-03 22:05:26', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE `departamentos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `departamento` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `departamentos`
--

INSERT INTO `departamentos` (`id`, `departamento`, `created_at`, `updated_at`) VALUES
(1, 'Alta Verapaz', '2022-10-19 00:58:04', NULL),
(2, 'Baja Verapaz', '2022-10-22 04:07:38', NULL),
(3, 'Chimaltenango', '2022-11-02 19:11:49', NULL),
(4, 'Chiquimula', '2022-11-02 19:11:56', NULL),
(5, 'El Progreso', '2022-11-02 19:12:02', NULL),
(6, 'Escuintla', '2022-11-02 19:12:07', NULL),
(7, 'Departamento de Guatemala', '2022-11-02 19:12:13', NULL),
(8, 'Huehuetenango', '2022-11-02 19:12:23', NULL),
(9, 'Izabal', '2022-11-02 19:12:29', NULL),
(10, 'Jalapa', '2022-11-02 19:12:35', NULL),
(11, 'Jutiapa', '2022-11-02 19:12:43', NULL),
(12, 'Petén', '2022-11-02 19:12:48', NULL),
(13, 'Quetzaltenango', '2022-11-02 19:12:55', NULL),
(14, 'Quiché', '2022-11-02 19:13:01', NULL),
(15, 'Retalhuleu', '2022-11-02 19:13:05', NULL),
(16, 'Sacatepéquez', '2022-11-02 19:13:14', NULL),
(17, 'San Marcos', '2022-11-02 19:13:21', NULL),
(18, 'Santa Rosa', '2022-11-02 19:13:26', NULL),
(19, 'Sololá', '2022-11-02 19:13:31', NULL),
(20, 'Suchitepéquez', '2022-11-02 19:13:36', NULL),
(21, 'Totonicapán', '2022-11-02 19:13:41', NULL),
(22, 'Zacapa', '2022-11-02 19:13:46', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cui` bigint(20) NOT NULL,
  `nombres` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` bigint(20) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cargo_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`id`, `cui`, `nombres`, `apellidos`, `telefono`, `email`, `cargo_id`, `created_at`, `updated_at`) VALUES
(1, 2050968911601, 'Hannsel Estuardo', 'Cordón Ac', 12345678, 'hannselcordon@gmail.com', 1, '2022-10-19 03:01:21', NULL),
(2, 2050968911604, 'Rene Estuardo', 'Lopez Garcia', 87654321, 'estuardo@prueba.com', 3, '2022-10-19 03:02:31', NULL),
(3, 2050968911607, 'Julio Alberto', 'Morales Tut', 12345678, 'jmorales@gmal.com', 10, '2022-11-02 19:40:54', '2022-11-02 23:48:00'),
(4, 1010110101, 'System', 'Firberhome', 33343, 'system@sistema.com', 3, '2022-11-03 21:54:07', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `estado` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`id`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'Pendiente de Instalación', '2022-10-19 00:58:26', NULL),
(2, 'Suspendido por falta de pago', '2022-10-19 00:58:38', NULL),
(3, 'Dado de Baja', '2022-10-19 00:59:03', NULL),
(4, 'Sin Contrato', '2022-10-19 12:20:47', NULL),
(5, 'Activo', '2022-10-20 11:13:28', NULL),
(6, 'Inactivo', '2022-10-20 11:13:35', NULL),
(7, 'Pendiente de Ejecutar', '2022-11-03 00:26:50', NULL),
(8, 'Ejecutada', '2022-11-03 00:27:04', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jerarquias`
--

CREATE TABLE `jerarquias` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `empleado_id` bigint(20) UNSIGNED NOT NULL,
  `jefe_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_08_07_145904_add_table_cms_apicustom', 1),
(4, '2016_08_07_150834_add_table_cms_dashboard', 1),
(5, '2016_08_07_151210_add_table_cms_logs', 1),
(6, '2016_08_07_151211_add_details_cms_logs', 1),
(7, '2016_08_07_152014_add_table_cms_privileges', 1),
(8, '2016_08_07_152214_add_table_cms_privileges_roles', 1),
(9, '2016_08_07_152320_add_table_cms_settings', 1),
(10, '2016_08_07_152421_add_table_cms_users', 1),
(11, '2016_08_07_154624_add_table_cms_menus_privileges', 1),
(12, '2016_08_07_154624_add_table_cms_moduls', 1),
(13, '2016_08_17_225409_add_status_cms_users', 1),
(14, '2016_08_20_125418_add_table_cms_notifications', 1),
(15, '2016_09_04_033706_add_table_cms_email_queues', 1),
(16, '2016_09_16_035347_add_group_setting', 1),
(17, '2016_09_16_045425_add_label_setting', 1),
(18, '2016_09_17_104728_create_nullable_cms_apicustom', 1),
(19, '2016_10_01_141740_add_method_type_apicustom', 1),
(20, '2016_10_01_141846_add_parameters_apicustom', 1),
(21, '2016_10_01_141934_add_responses_apicustom', 1),
(22, '2016_10_01_144826_add_table_apikey', 1),
(23, '2016_11_14_141657_create_cms_menus', 1),
(24, '2016_11_15_132350_create_cms_email_templates', 1),
(25, '2016_11_15_190410_create_cms_statistics', 1),
(26, '2016_11_17_102740_create_cms_statistic_components', 1),
(27, '2017_06_06_164501_add_deleted_at_cms_moduls', 1),
(28, '2019_08_19_000000_create_failed_jobs_table', 1),
(29, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(30, '2022_10_18_160310_create_departamentos_table', 1),
(31, '2022_10_18_162124_create_municipios_table', 1),
(32, '2022_10_18_171919_create_tipopagos_table', 1),
(33, '2022_10_18_172114_create_estados_table', 1),
(34, '2022_10_18_174657_create_tipoordenes_table', 1),
(35, '2022_10_18_183041_create_planes_table', 1),
(36, '2022_10_18_193012_create_cargos_table', 1),
(37, '2022_10_18_200947_create_tipotrabajos_table', 1),
(38, '2022_10_18_202616_create_empleados_table', 1),
(39, '2022_10_18_211300_create_jerarquias_table', 1),
(40, '2022_10_18_220441_create_clientes_table', 1),
(41, '2022_10_19_062144_create_productos_table', 1),
(42, '2022_10_19_074301_create_contratos_table', 1),
(43, '2022_10_20_012912_create_pagos_table', 1),
(44, '2022_11_02_210530_create_ordenes_table', 1),
(45, '2022_10_20_012912_create_pagos_table', 8),
(46, '2014_10_12_000000_create_users_table', 9),
(47, '2016_08_07_152421_add_table_cms_users', 9),
(48, '2016_08_17_225409_add_status_cms_users', 9),
(49, '2022_10_19_074301_create_contratos_table', 10),
(50, '2022_11_02_210530_create_ordenes_table', 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipios`
--

CREATE TABLE `municipios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `municipio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `municipios`
--

INSERT INTO `municipios` (`id`, `municipio`, `created_at`, `updated_at`) VALUES
(1, 'Cobán', '2022-10-19 00:58:15', NULL),
(2, 'San Pedro Carcha', '2022-10-19 08:19:26', NULL),
(3, 'Santa Cruz Verapaz', '2022-11-02 19:14:31', NULL),
(4, 'San Cristobal Verapaz', '2022-11-02 19:14:38', NULL),
(5, 'Tactíc', '2022-11-02 19:14:42', NULL),
(6, 'Tamahú', '2022-11-02 19:14:49', NULL),
(7, 'San Miguel Tucurú', '2022-11-02 19:14:54', NULL),
(8, 'Panzos', '2022-11-02 19:15:00', NULL),
(9, 'Senahú', '2022-11-02 19:15:08', NULL),
(10, 'San Pedro Carchá', '2022-11-02 19:15:18', NULL),
(11, 'SanJuan Chamelco', '2022-11-02 19:15:23', NULL),
(12, 'Lanquín', '2022-11-02 19:15:29', NULL),
(13, 'Santa María Cahabón', '2022-11-02 19:15:39', NULL),
(14, 'Chisec', '2022-11-02 19:15:44', NULL),
(15, 'Chahal', '2022-11-02 19:15:51', NULL),
(16, 'Fray Bartolomé de las Casas', '2022-11-02 19:15:57', NULL),
(17, 'Santa Catarina La Tinta', '2022-11-02 19:16:02', NULL),
(18, 'Raxruha', '2022-11-02 19:16:22', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ordenes`
--

CREATE TABLE `ordenes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `trabajo_id` bigint(20) UNSIGNED NOT NULL,
  `servicio_id` bigint(20) UNSIGNED NOT NULL,
  `tecnico_id` bigint(20) UNSIGNED NOT NULL,
  `estado_id` bigint(20) UNSIGNED DEFAULT NULL,
  `fecha_generacion` date DEFAULT NULL,
  `supervisor_id` bigint(20) UNSIGNED DEFAULT NULL,
  `comentario` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `evidencias` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_verificacion` date DEFAULT NULL,
  `cliente_id` bigint(20) UNSIGNED DEFAULT NULL,
  `contrato_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `ordenes`
--

INSERT INTO `ordenes` (`id`, `trabajo_id`, `servicio_id`, `tecnico_id`, `estado_id`, `fecha_generacion`, `supervisor_id`, `comentario`, `evidencias`, `fecha_verificacion`, `cliente_id`, `contrato_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 3, 8, '2022-11-02', 1, 'Comentario', 'uploads/1/2022-11/tarea_09_2021.pdf', '2022-11-15', 1, 1, '2022-11-02 23:58:17', '2022-11-03 16:20:24'),
(2, 1, 3, 3, 7, '2022-11-03', 1, '', '', '0000-00-00', 1, 3, '2022-11-03 17:11:29', NULL),
(3, 1, 1, 3, 8, '2022-11-03', 1, 'Se realizo instalacion de servicio', 'uploads/3/2022-11/tarea_09_2021.pdf', '2022-11-30', 1, 1, '2022-11-03 17:20:53', '2022-11-03 17:35:50'),
(4, 1, 1, 3, 8, '2022-11-08', 1, 'Servicio instalado', 'uploads/3/2022-11/evidencia.pdf', '2022-11-08', 1, 4, '2022-11-09 02:54:35', '2022-11-09 02:59:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente_id` bigint(20) UNSIGNED NOT NULL,
  `servicio_id` bigint(20) UNSIGNED NOT NULL,
  `tipopago_id` bigint(20) UNSIGNED NOT NULL,
  `fecha_pago` date NOT NULL,
  `monto` double NOT NULL,
  `saldo` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `pagos`
--

INSERT INTO `pagos` (`id`, `cliente_id`, `servicio_id`, `tipopago_id`, `fecha_pago`, `monto`, `saldo`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 2, '2022-10-19', 200, 200, '2022-10-20 11:15:31', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `planes`
--

CREATE TABLE `planes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `planes_servicios` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `planes`
--

INSERT INTO `planes` (`id`, `planes_servicios`, `created_at`, `updated_at`) VALUES
(1, 'Premium 12 Meses', '2022-10-19 13:59:52', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `producto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `costo` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `producto`, `descripcion`, `costo`, `created_at`, `updated_at`) VALUES
(1, 'Internet Corporativo Plus', 'Protección de datos Site to Site, 1 Dominio, 400 Usuarios internos y hasta 100 externos', 200, '2022-10-19 13:12:34', NULL),
(2, 'Correo Corporativo', 'Protección de Datos, Respaldo automatico, Encriptacion, etc.', 300, '2022-11-03 21:52:34', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoordenes`
--

CREATE TABLE `tipoordenes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipo_orden` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tipoordenes`
--

INSERT INTO `tipoordenes` (`id`, `tipo_orden`, `created_at`, `updated_at`) VALUES
(1, 'Instalación de Nuevo Servicio', '2022-10-19 00:59:58', NULL),
(2, 'Inspeccion de Servicio', '2022-10-19 01:00:15', NULL),
(3, 'Reclamo por falta de Servicio', '2022-10-19 01:00:35', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipopagos`
--

CREATE TABLE `tipopagos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tipopagos`
--

INSERT INTO `tipopagos` (`id`, `tipo`, `created_at`, `updated_at`) VALUES
(1, 'Efectivo', '2022-10-19 01:00:55', NULL),
(2, 'Tarjeta', '2022-10-19 01:00:58', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipotrabajos`
--

CREATE TABLE `tipotrabajos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipo_trabajo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `costo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tipotrabajos`
--

INSERT INTO `tipotrabajos` (`id`, `tipo_trabajo`, `descripcion`, `costo`, `created_at`, `updated_at`) VALUES
(1, 'Inspección de Servicio', 'Esta tarea es diseñada para el técnico de campo y tiene el proposito de realizar una inspeccion completa, detectar y solucionar el inconveniente.', '200', '2022-10-19 02:17:55', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `photo`, `email`, `password`, `id_cms_privileges`, `remember_token`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'uploads/1/2022-11/icons8_tipo_de_mascara_de_grupo_de_administradores_de_base_de_datos_sql_7_48.png', 'hannselcordon@gmail.com', '$2y$10$.DaIOAFwxg.Jz03fK1GAq.lmJRv27UaTwvcXQyUatfL0XirWEX1nK', 1, 'A9M0hH5TUtCLMMlRss6xVhWAvtudx1zpbyYhI9aU21esCP0MXoUFSRmvuk6a', 'Active', '2022-11-08 04:12:45', '2022-11-09 02:17:35'),
(2, 'Vendedor', 'uploads/1/2022-11/icons8_salesman_skin_type_5_48.png', 'vendedor@gmail.com', '$2y$10$.6yMBgVwetZsC.kJlkOGXOLSQrTIvLyrJ8SBm3VxiHXscJhFYK3Di', 3, '', 'Activo', '2022-11-03 01:31:41', '2022-11-09 02:42:24'),
(3, 'Tecnico', 'uploads/1/2022-11/icons8_trabajadores_masculinos_48.png', 'tecnico@tecnico.com', '$2y$10$zLUvRsRd1Jop1MIc3ek9Qe6E2bJ/EGAtrtuLo2UPmE.ly1QpMn8.e', 4, '', 'Activo', '2022-11-03 16:28:29', '2022-11-09 02:57:27'),
(4, 'Supervisor', 'uploads/1/2022-11/icons8_supervisor_64.png', 'supervisor@supervisor.com', '$2y$10$N2U3bJGzwTXXgO.m1w7eZusZkkodR6O4WZc2hRCXBIs5CBT5WrSS2', 5, '', 'Activo', '2022-11-03 20:45:18', '2022-11-09 02:48:28'),
(5, 'Recursos Humanos', 'uploads/1/2022-11/icons8_configuracion_del_administrador_48.png', 'recursos@recursos.com', '$2y$10$H5G91NuwbRe0f4R3j8vt4e/tXszYHWehvenfKuzBTWPZ33T3s2HNO', 6, '', 'Activo', '2022-11-03 21:21:57', '2022-11-09 03:00:19'),
(6, 'Cliente Fiberhome', 'uploads/1/2022-11/icons8_man_with_beard_medium_skin_tone_48.png', 'cliente@cliente.com', '$2y$10$dfJoo9ZBCZVO1yxYGRfP1.2apv1zraVoWbnbC2125QGr9iRKOZ3pm', 7, '', 'Activo', '2022-11-03 22:15:35', '2022-11-08 03:22:20'),
(7, 'Secretaria', 'uploads/1/2022-11/secretaria.png', 'secretaria@secretaria.com', '$2y$10$NWUz6xLJcPiD8uzFMwNZBexACc2aMBPOsV1uAIMUMce0eYui8Z4Fe', 2, NULL, 'Activo', '2022-11-08 18:01:25', '2022-11-08 18:10:52'),
(8, 'Prueba', 'uploads/1/2022-11/772041.png', 'prueba@prueba.com', '$2y$10$IhXtVoaVgHT0MajKmFxtquwVfwdEySomCMd6TvaayQznqBc6fl1oK', 8, NULL, 'Activo', '2022-11-08 21:17:57', '2022-11-08 21:18:50'),
(9, 'Invitado', 'uploads/1/2022-11/descarga.jpg', 'invitado@invitado.com', '$2y$10$AHWVPi7HZgUYOBDBPcyeJuvlvKlARNFNQAN5G/hCWIKEN8aAGjF5m', 8, NULL, NULL, '2022-11-08 21:58:30', '2022-11-09 02:28:36'),
(10, 'Api', NULL, 'api@api.com', '$2y$10$V59t9cXO2t/NuDbRWd6NruJ/94MkCiIPKCIUboSn06pBWumGzbB0S', 5, NULL, NULL, '2022-11-09 02:39:29', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cargos`
--
ALTER TABLE `cargos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clientes_departamento_id_foreign` (`departamento_id`),
  ADD KEY `clientes_municipio_id_foreign` (`municipio_id`),
  ADD KEY `clientes_empleado_id_foreign` (`empleado_id`);

--
-- Indices de la tabla `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_apikey`
--
ALTER TABLE `cms_apikey`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_logs`
--
ALTER TABLE `cms_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_menus`
--
ALTER TABLE `cms_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_moduls`
--
ALTER TABLE `cms_moduls`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_notifications`
--
ALTER TABLE `cms_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_privileges`
--
ALTER TABLE `cms_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_settings`
--
ALTER TABLE `cms_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_statistics`
--
ALTER TABLE `cms_statistics`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_users`
--
ALTER TABLE `cms_users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contratos`
--
ALTER TABLE `contratos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contratos_producto_id_foreign` (`producto_id`),
  ADD KEY `contratos_plan_id_foreign` (`plan_id`),
  ADD KEY `contratos_vendedor_id_foreign` (`vendedor_id`),
  ADD KEY `contratos_estado_id_foreign` (`estado_id`),
  ADD KEY `contratos_cliente_id_foreign` (`cliente_id`);

--
-- Indices de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `empleados_cargo_id_foreign` (`cargo_id`);

--
-- Indices de la tabla `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indices de la tabla `jerarquias`
--
ALTER TABLE `jerarquias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jerarquias_empleado_id_foreign` (`empleado_id`),
  ADD KEY `jerarquias_jefe_id_foreign` (`jefe_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `municipios`
--
ALTER TABLE `municipios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ordenes`
--
ALTER TABLE `ordenes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ordenes_trabajo_id_foreign` (`trabajo_id`),
  ADD KEY `ordenes_servicio_id_foreign` (`servicio_id`),
  ADD KEY `ordenes_tecnico_id_foreign` (`tecnico_id`),
  ADD KEY `ordenes_estado_id_foreign` (`estado_id`),
  ADD KEY `ordenes_supervisor_id_foreign` (`supervisor_id`),
  ADD KEY `ordenes_cliente_id_foreign` (`cliente_id`),
  ADD KEY `ordenes_contrato_id_foreign` (`contrato_id`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pagos_cliente_id_foreign` (`cliente_id`),
  ADD KEY `pagos_servicio_id_foreign` (`servicio_id`),
  ADD KEY `pagos_tipopago_id_foreign` (`tipopago_id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indices de la tabla `planes`
--
ALTER TABLE `planes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipoordenes`
--
ALTER TABLE `tipoordenes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipopagos`
--
ALTER TABLE `tipopagos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipotrabajos`
--
ALTER TABLE `tipotrabajos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cargos`
--
ALTER TABLE `cargos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `cms_apikey`
--
ALTER TABLE `cms_apikey`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `cms_logs`
--
ALTER TABLE `cms_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=512;

--
-- AUTO_INCREMENT de la tabla `cms_menus`
--
ALTER TABLE `cms_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT de la tabla `cms_moduls`
--
ALTER TABLE `cms_moduls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `cms_notifications`
--
ALTER TABLE `cms_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cms_privileges`
--
ALTER TABLE `cms_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT de la tabla `cms_settings`
--
ALTER TABLE `cms_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `cms_statistics`
--
ALTER TABLE `cms_statistics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT de la tabla `cms_users`
--
ALTER TABLE `cms_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `contratos`
--
ALTER TABLE `contratos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `empleados`
--
ALTER TABLE `empleados`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `estados`
--
ALTER TABLE `estados`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `jerarquias`
--
ALTER TABLE `jerarquias`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT de la tabla `municipios`
--
ALTER TABLE `municipios`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `ordenes`
--
ALTER TABLE `ordenes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `planes`
--
ALTER TABLE `planes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipoordenes`
--
ALTER TABLE `tipoordenes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipopagos`
--
ALTER TABLE `tipopagos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipotrabajos`
--
ALTER TABLE `tipotrabajos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD CONSTRAINT `clientes_departamento_id_foreign` FOREIGN KEY (`departamento_id`) REFERENCES `departamentos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `clientes_empleado_id_foreign` FOREIGN KEY (`empleado_id`) REFERENCES `empleados` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `clientes_municipio_id_foreign` FOREIGN KEY (`municipio_id`) REFERENCES `municipios` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `contratos`
--
ALTER TABLE `contratos`
  ADD CONSTRAINT `contratos_cliente_id_foreign` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `contratos_estado_id_foreign` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `contratos_plan_id_foreign` FOREIGN KEY (`plan_id`) REFERENCES `planes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `contratos_producto_id_foreign` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `contratos_vendedor_id_foreign` FOREIGN KEY (`vendedor_id`) REFERENCES `empleados` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD CONSTRAINT `empleados_cargo_id_foreign` FOREIGN KEY (`cargo_id`) REFERENCES `cargos` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `jerarquias`
--
ALTER TABLE `jerarquias`
  ADD CONSTRAINT `jerarquias_empleado_id_foreign` FOREIGN KEY (`empleado_id`) REFERENCES `empleados` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jerarquias_jefe_id_foreign` FOREIGN KEY (`jefe_id`) REFERENCES `empleados` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `ordenes`
--
ALTER TABLE `ordenes`
  ADD CONSTRAINT `ordenes_cliente_id_foreign` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ordenes_contrato_id_foreign` FOREIGN KEY (`contrato_id`) REFERENCES `contratos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ordenes_estado_id_foreign` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ordenes_servicio_id_foreign` FOREIGN KEY (`servicio_id`) REFERENCES `tipoordenes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ordenes_supervisor_id_foreign` FOREIGN KEY (`supervisor_id`) REFERENCES `empleados` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ordenes_tecnico_id_foreign` FOREIGN KEY (`tecnico_id`) REFERENCES `empleados` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ordenes_trabajo_id_foreign` FOREIGN KEY (`trabajo_id`) REFERENCES `tipotrabajos` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD CONSTRAINT `pagos_cliente_id_foreign` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pagos_servicio_id_foreign` FOREIGN KEY (`servicio_id`) REFERENCES `contratos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pagos_tipopago_id_foreign` FOREIGN KEY (`tipopago_id`) REFERENCES `tipopagos` (`id`) ON DELETE CASCADE;
--
-- Base de datos: `phpmyadmin`
--
CREATE DATABASE IF NOT EXISTS `phpmyadmin` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `phpmyadmin`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__bookmark`
--

CREATE TABLE `pma__bookmark` (
  `id` int(10) UNSIGNED NOT NULL,
  `dbase` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `query` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Bookmarks';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__central_columns`
--

CREATE TABLE `pma__central_columns` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_length` text COLLATE utf8_bin,
  `col_collation` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_isNull` tinyint(1) NOT NULL,
  `col_extra` varchar(255) COLLATE utf8_bin DEFAULT '',
  `col_default` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Central list of columns';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__column_info`
--

CREATE TABLE `pma__column_info` (
  `id` int(5) UNSIGNED NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `column_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mimetype` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__designer_settings`
--

CREATE TABLE `pma__designer_settings` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `settings_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Settings related to Designer';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__export_templates`
--

CREATE TABLE `pma__export_templates` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `export_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `template_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `template_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved export templates';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__favorite`
--

CREATE TABLE `pma__favorite` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Favorite tables';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__history`
--

CREATE TABLE `pma__history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sqlquery` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='SQL history for phpMyAdmin';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__navigationhiding`
--

CREATE TABLE `pma__navigationhiding` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Hidden items of navigation tree';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__pdf_pages`
--

CREATE TABLE `pma__pdf_pages` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `page_nr` int(10) UNSIGNED NOT NULL,
  `page_descr` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='PDF relation pages for phpMyAdmin';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__recent`
--

CREATE TABLE `pma__recent` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Recently accessed tables';

--
-- Volcado de datos para la tabla `pma__recent`
--

INSERT INTO `pma__recent` (`username`, `tables`) VALUES
('root', '[{\"db\":\"db_rrhh\",\"table\":\"tb_empleados\"},{\"db\":\"db_rrhh\",\"table\":\"tb_tipo_contratos\"},{\"db\":\"db_rrhh\",\"table\":\"cms_statistic_components\"},{\"db\":\"laravelfirehome\",\"table\":\"cms_statistic_components\"},{\"db\":\"laravelfirehome\",\"table\":\"cms_statistics\"},{\"db\":\"db_rrhh\",\"table\":\"tb_contratos\"},{\"db\":\"db_rrhh\",\"table\":\"cms_moduls\"},{\"db\":\"db_rrhh\",\"table\":\"users\"},{\"db\":\"db_rrhh\",\"table\":\"cms_settings\"},{\"db\":\"db_rrhh\",\"table\":\"cms_notifications\"}]');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__relation`
--

CREATE TABLE `pma__relation` (
  `master_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Relation table';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__savedsearches`
--

CREATE TABLE `pma__savedsearches` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved searches';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__table_coords`
--

CREATE TABLE `pma__table_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pdf_page_number` int(11) NOT NULL DEFAULT '0',
  `x` float UNSIGNED NOT NULL DEFAULT '0',
  `y` float UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for phpMyAdmin PDF output';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__table_info`
--

CREATE TABLE `pma__table_info` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__table_uiprefs`
--

CREATE TABLE `pma__table_uiprefs` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `prefs` text COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tables'' UI preferences';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__tracking`
--

CREATE TABLE `pma__tracking` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `version` int(10) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `schema_snapshot` text COLLATE utf8_bin NOT NULL,
  `schema_sql` text COLLATE utf8_bin,
  `data_sql` longtext COLLATE utf8_bin,
  `tracking` set('UPDATE','REPLACE','INSERT','DELETE','TRUNCATE','CREATE DATABASE','ALTER DATABASE','DROP DATABASE','CREATE TABLE','ALTER TABLE','RENAME TABLE','DROP TABLE','CREATE INDEX','DROP INDEX','CREATE VIEW','ALTER VIEW','DROP VIEW') COLLATE utf8_bin DEFAULT NULL,
  `tracking_active` int(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Database changes tracking for phpMyAdmin';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__userconfig`
--

CREATE TABLE `pma__userconfig` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `config_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User preferences storage for phpMyAdmin';

--
-- Volcado de datos para la tabla `pma__userconfig`
--

INSERT INTO `pma__userconfig` (`username`, `timevalue`, `config_data`) VALUES
('root', '2022-11-17 01:34:27', '{\"Console\\/Mode\":\"collapse\",\"lang\":\"es\",\"NavigationWidth\":242}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__usergroups`
--

CREATE TABLE `pma__usergroups` (
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL,
  `tab` varchar(64) COLLATE utf8_bin NOT NULL,
  `allowed` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User groups with configured menu items';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pma__users`
--

CREATE TABLE `pma__users` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users and their assignments to user groups';

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pma__central_columns`
--
ALTER TABLE `pma__central_columns`
  ADD PRIMARY KEY (`db_name`,`col_name`);

--
-- Indices de la tabla `pma__column_info`
--
ALTER TABLE `pma__column_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `db_name` (`db_name`,`table_name`,`column_name`);

--
-- Indices de la tabla `pma__designer_settings`
--
ALTER TABLE `pma__designer_settings`
  ADD PRIMARY KEY (`username`);

--
-- Indices de la tabla `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_user_type_template` (`username`,`export_type`,`template_name`);

--
-- Indices de la tabla `pma__favorite`
--
ALTER TABLE `pma__favorite`
  ADD PRIMARY KEY (`username`);

--
-- Indices de la tabla `pma__history`
--
ALTER TABLE `pma__history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`,`db`,`table`,`timevalue`);

--
-- Indices de la tabla `pma__navigationhiding`
--
ALTER TABLE `pma__navigationhiding`
  ADD PRIMARY KEY (`username`,`item_name`,`item_type`,`db_name`,`table_name`);

--
-- Indices de la tabla `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  ADD PRIMARY KEY (`page_nr`),
  ADD KEY `db_name` (`db_name`);

--
-- Indices de la tabla `pma__recent`
--
ALTER TABLE `pma__recent`
  ADD PRIMARY KEY (`username`);

--
-- Indices de la tabla `pma__relation`
--
ALTER TABLE `pma__relation`
  ADD PRIMARY KEY (`master_db`,`master_table`,`master_field`),
  ADD KEY `foreign_field` (`foreign_db`,`foreign_table`);

--
-- Indices de la tabla `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_savedsearches_username_dbname` (`username`,`db_name`,`search_name`);

--
-- Indices de la tabla `pma__table_coords`
--
ALTER TABLE `pma__table_coords`
  ADD PRIMARY KEY (`db_name`,`table_name`,`pdf_page_number`);

--
-- Indices de la tabla `pma__table_info`
--
ALTER TABLE `pma__table_info`
  ADD PRIMARY KEY (`db_name`,`table_name`);

--
-- Indices de la tabla `pma__table_uiprefs`
--
ALTER TABLE `pma__table_uiprefs`
  ADD PRIMARY KEY (`username`,`db_name`,`table_name`);

--
-- Indices de la tabla `pma__tracking`
--
ALTER TABLE `pma__tracking`
  ADD PRIMARY KEY (`db_name`,`table_name`,`version`);

--
-- Indices de la tabla `pma__userconfig`
--
ALTER TABLE `pma__userconfig`
  ADD PRIMARY KEY (`username`);

--
-- Indices de la tabla `pma__usergroups`
--
ALTER TABLE `pma__usergroups`
  ADD PRIMARY KEY (`usergroup`,`tab`,`allowed`);

--
-- Indices de la tabla `pma__users`
--
ALTER TABLE `pma__users`
  ADD PRIMARY KEY (`username`,`usergroup`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pma__column_info`
--
ALTER TABLE `pma__column_info`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pma__history`
--
ALTER TABLE `pma__history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  MODIFY `page_nr` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Base de datos: `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `test`;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
