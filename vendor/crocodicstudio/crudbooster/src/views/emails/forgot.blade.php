@include("crudbooster::emails.header")

<p>Hola,</p>
<p>Alguien con la Dirección IP {{$_SERVER['REMOTE_ADDR']}} at {{date('Y-m-d H:i:s')}} a solicitado una nueva contraseña : </p>
<p>Contraseña : {{$password}}</p>

@include("crudbooster::emails.footer")